<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Home\HomeController@index')->name('welcome');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', 'User\UserController@viewDashboard')->name('home');
Route::get('refresh-dashboard', 'User\UserController@changeDash')->name('refresh.dashboard');
Route::post('check-email', 'Auth\RegisterController@chkEmailExist')->name('user.check.email');
Route::post('user-change-mobile', 'Auth\RegisterController@changeMobile')->name('user.change.mobile');
//phone verify guest
Route::get('guest-verify-phone-{userId}','Auth\RegisterController@guestEmailPhoneVerifyPage')->name('guest.phone.verify');
Route::post('guest-verify-phone-{userId}','Auth\RegisterController@guestEmailPhoneVerifyPagePost')->name('guest.phone.verify.post');

Route::post('/user/verify/phone','Auth\RegisterController@verifyPhone')->name('user.check.phone');
Route::post('/user/resend/phone/otp','Auth\RegisterController@verifyPhoneResendOtp')->name('user.resend.otp');
Route::get('/user/verify/{vcode}/{id}', 'Auth\RegisterController@verifyEmail')->name('verify');


//forget password
Route::get('/forget-password','Auth\ForgotPasswordController@showFormResetPassword')->name('user.forget.password.set');
Route::post('/reset-link','Auth\ForgotPasswordController@sendResetPasswordLink')->name('user.link.forget.password.set');
Route::get('/reset-link','Auth\ForgotPasswordController@sendResetPasswordLink')->name('user.link.forget.password.set');
Route::get('/reset-user-password/{id}','Auth\ForgotPasswordController@updateResetPassword')->name('user.update.forget.password.set');
Route::post('/reset-user-password/{id}','Auth\ForgotPasswordController@updateResetPassword')->name('user.update.forget.password.set');

// for social login
Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('social/login/page/{id}', 'Auth\LoginController@facebookEmailLoginPage')->name('facebook.email.login.page');
Route::post('social/login/{id}', 'Auth\LoginController@facebookEmailLogin')->name('facebook.email.login');

Route::get('dashboard', 'User\UserController@viewDashboard')->name('dashboard');
Route::get('edit-profile', 'User\UserController@editProfile')->name('user.edit.profile');
Route::post('store-user-details', 'User\UserController@storeUserDetails')->name('store.user.details');
Route::post('user-certificates-post','User\UserController@addUserCertificate')->name('store.user.certificates');
Route::get('user-certificates-get','User\UserController@getUserCertificate')->name('get.user.certificates');
Route::get('user-certificates-delete-{ids?}','User\UserController@deleteUserCertificate')->name('delete.user.certificates');

// Conversation Part
Route::get('create-conversation','Conversation\ConversationController@createConversationPage')->name('create.conversation');
Route::get('get-sub-category/{catIds?}','Conversation\ConversationController@getSubCategory')->name('get.sub.category');
Route::get('get-sub-category/topic/{subcatIds?}','Conversation\ConversationController@getSubCategoryTopic')->name('get.sub.category.topic');
Route::post('create-conversation-post','Conversation\ConversationController@createConversationPost')->name('create.conversation.store');
Route::get('my-rooms','Conversation\ConversationController@upcomingConversationPage')->name('upcoming.conversation');
Route::get('conversation-details-{conversationIds?}','Conversation\ConversationController@viewConversationDetails')->name('view.conversation.details');
Route::get('conversation-user-status-change-{conversationUsersIds?}/{conversationIds?}/{type?}','Conversation\ConversationController@approveRejectConversationUserRequest')->name('conversation.user.approved.reject');
Route::get('past-rooms','Conversation\ConversationController@pastConversationPage')->name('past.conversation');

Route::get('/past-rooms-details-{converIds?}','Conversation\ConversationController@viewFeedbackPage')->name('past.conversation.details');

Route::get('referral-and-earn','User\UserController@refferandearnpage')->name('referral.earn');
Route::get('refered-users-lists','User\UserController@refferUsersListspage')->name('referral.user.list');
Route::get('/ref/{ref_id?}','Home\HomeController@index')->name('referral.code.link');

// review part
Route::post('/conversation-review','Conversation\ConversationController@reviewConversationPost')->name('post.conversation.review');
Route::get('/conversation-rating-review-{conversIds?}-{ratetype?}','Conversation\ConversationController@reviewPostConversations')->name('post.review.conversation');

//search part
Route::get('/search/{subCat?}','Search\SearchController@searchPage')->name('search.page');
Route::get('search-results','Search\SearchController@searchResult')->name('search.results');
Route::get('search-single-topic/{topicids?}/{contype?}','Search\SearchController@searchSingleTopic')->name('search.single.topic');
//public profile
Route::get('public-profile/{hostId?}','Search\SearchController@publicProfile')->name('public.profile');

//conversation landing page
Route::get('/conversation-landing-page','Search\SearchController@landingPage')->name('landing.con.page');
Route::get('/conversation-{conIds?}','Search\SearchController@conversationDetailsPage')->name('conversation.details.page');

Route::post('joining-conversation','Search\SearchController@joiningConversation')->name('joining.conversation');

Route::get('conversation-cancel/{converIds?}','Conversation\ConversationController@cancelConversationByUser')->name('cancel.conversation.by.user');

//auto-complete
Route::post('autocomplete/keyword', 'Search\SearchController@autoComplete')->name('autocomplete.keyword');

//my connect page
Route::get('my-connect','User\UserController@myConnectList')->name('my.connect');
Route::get('my-connect-{cIds?}-{sta?}','User\UserController@myConnectStatusChange')->name('my.connect.status.change');
Route::get('my-block-list','User\UserController@myBlockList')->name('my.block.list');
Route::post('create-group','User\UserController@createGroup')->name('create.group');


// Video Call Route
Route::get('video-call/{conversationId?}','VideoCall\VideoCallController@index')->name('join.video.call');
Route::get('video-call/get-twilio-token/room', 'VideoCall\VideoCallController@getToken')->name('get.twilio.token');
Route::post('remove-user', 'VideoCall\VideoCallController@removeUser')->name('remove.user');
Route::post('remove-eviction-request', 'VideoCall\VideoCallController@removeEvictionRequest')->name('remove.eviction.request');
Route::post('leave-eviction', 'VideoCall\VideoCallController@evictionUserLeave')->name('leave.eviction');
Route::post('parament-leave', 'VideoCall\VideoCallController@paramentLeave')->name('parament.leave');
Route::post('extend-call', 'VideoCall\VideoCallController@extendCall')->name('extend.call');
Route::post('call-end-refund', 'VideoCall\VideoCallController@callEndRefund')->name('call.end.refund');
Route::post('warning-user', 'VideoCall\VideoCallController@sendWarning')->name('warning.user');
Route::post('report-user', 'VideoCall\VideoCallController@AddReport')->name('report.user');
Route::post('connect-user-public-profile', 'VideoCall\VideoCallController@connectUserPublicProfile')->name('connect.user.public.profile');
Route::any('create-request', 'VideoCall\VideoCallController@createRequest')->name('create.request');

Route::any('request-status', 'VideoCall\VideoCallController@requestCallStatus')->name('request.call.status');
Route::post('update-call-time', 'VideoCall\VideoCallController@updateCallTime')->name('update.call.time');

Route::any('send-message', 'Message\MessageController@sendMessage')->name('send.message');
Route::any('fetch-message', 'Message\MessageController@fetchMessage')->name('fetch.message');



//setings
Route::get('settings','User\UserController@settingsPage')->name('user.settings');
Route::post('check-email-update-time-post/{userIds?}', 'User\UserController@updatechkEmailExistPost')->name('user.update.check.email.post');
Route::post('check-email-update-time', 'User\UserController@updatechkEmailExist')->name('user.update.check.email');
Route::get('user/update/email/{emailvcode}/{userid}', 'User\UserController@updateTempEmail');

Route::post('/user/update/phone/verify/{userId}','User\UserController@verifyUserUpdatePhone');
Route::post('/user/update/phone/otp/verify','User\UserController@verifyUserUpdatePhoneByOtp')->name('user.varify.otp.update.mobile');


//purchase package part packagePurchasePage
Route::get('/purchase-package','Package\PackageController@packagePurchasePage')->name('purchase.package');
// Route::get('/user-purchase-package/{packageIds?}','Package\PackageController@packagePurchaseByUser')->name('purchase.package.user');
Route::get('/my-package','Package\PackageController@packagePurchaseListUser')->name('user.package');

Route::get('/purchase-confirmation/{packageIds}','Package\PackageController@purchasePackageConfirmation')->name('purchase.package.confirmation');
Route::get('/user-purchase-package/{packageIds?}','Package\PackageController@savePayment')->name('purchase.package.user');
Route::get('/pay-now/{orderID}','Package\PackageController@paymentProcess')->name('pay.now');
Route::post('/save-payment-confirmation/{id}','Package\PackageController@paymentConfirmation')->name('save.payment.confirmation');

// user to connect
Route::post('user-request-send','Search\SearchController@requestSendUsers')->name('request.send.users');

Route::get('before-url-set-session-{convIds?}','Search\SearchController@setSessions')->name('session.set.before.login');

// Ycoin history
Route::get('ycoins-usage','Package\PackageController@YcoinHistoryList')->name('ycoin.usage');

Route::get('/see-all-topic/{convType?}','Search\SearchController@seeAllTopicPage')->name('see.all.topic');

// static-pages
Route::get('contact-us','StaticPages\StaticPagesController@contactUs')->name('user.contact.us');
Route::post('contact-us/mail','StaticPages\StaticPagesController@sendmail')->name('user.contact.us.mail');
Route::get('faqs','StaticPages\StaticPagesController@FAQ')->name('user.faq');
Route::get('about-us','StaticPages\StaticPagesController@aboutUs')->name('user.about.us');
Route::get('how-it-works','StaticPages\StaticPagesController@howItWorks')->name('user.how.it.works');
Route::get('how-it-works','StaticPages\StaticPagesController@howItWorks')->name('user.how.it.works');
Route::get('privacy-policy','StaticPages\StaticPagesController@privacyPolicy')->name('user.privacy.policy');
Route::get('terms-of-service','StaticPages\StaticPagesController@termsOfService')->name('user.privacy.terms');
