<?php

use Illuminate\Support\Facades\Route;

// Dashboard
Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('home');

// Login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Register
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Reset Password
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{email}/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Confirm Password
Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');

// Verify Email
// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
// Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
// Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
Route::get('/my-profile-details', 'MyProfile\MyProfileController@index')->name('view.profile');
Route::post('/my-profile-details', 'MyProfile\MyProfileController@index')->name('view.profile');
Route::get('/chk-duplicate-email-update', 'Modules\MyProfile\MyProfileController@checkAdminDuplicateEmail')->name('check.duplicate.email.update');
Route::get('/chk-duplicate-phone-update', 'Modules\MyProfile\MyProfileController@checkAdminDuplicatePhone')->name('check.duplicate.phone.update');

//category
Route::get('manage-category','Category\CategoryController@manageCategory')->name('manage.category');
Route::get('add-category','Category\CategoryController@AddCategory')->name('add.category');
Route::get('add-bulk-category','Category\CategoryController@addBulkCategorypage')->name('add.bulk.category');
Route::post('add-category','Category\CategoryController@AddCategory')->name('add.category.post');
Route::get('edit-category-{id}','Category\CategoryController@editCategory')->name('edit.category');
Route::post('edit-category-{id}','Category\CategoryController@editCategory')->name('edit.category.post');
Route::get('status-change-category/{slug?}','Category\CategoryController@updateStatus')->name('change.status.category');
Route::get('delete-category/{id?}','Category\CategoryController@categoryDelete')->name('delete.category');
Route::get('category-header-status-{courseId}','Category\CategoryController@updateHeaderStatus')->name('category.header.status.change');
//Topic
Route::get('/manage-topic','Topic\TopicController@manageTopic')->name('manage.topic');
Route::get('/add-topic','Topic\TopicController@addTopic')->name('add.topic');
Route::post('/add-topic','Topic\TopicController@addTopicPost')->name('add.topic.post');
Route::any('/status-change-topic/{id?}','Topic\TopicController@updateTopicStatus')->name('status.topic.change');
Route::get('/edit-topic/{id?}','Topic\TopicController@editTopic')->name('topic.edit');
Route::post('/update-topic/{id?}','Topic\TopicController@editTopicPost')->name('edit.topic.post');
Route::get('/delete-topic/{id?}','Topic\TopicController@topicDeletePost')->name('delete.topic');

// Conversation Part
Route::get('manage-conversation','Conversation\ConversationController@manageConversation')->name('manage.conversation');
Route::get('add-conversation','Conversation\ConversationController@createConversationPage')->name('add.conversation');
Route::get('get-sub-category/{catIds?}/{sub_category?}','Conversation\ConversationController@getSubCategory')->name('get.sub.category');
Route::get('get-sub-category/topic/{subcatIds?}','Conversation\ConversationController@getSubCategoryTopic')->name('get.sub.category.topic');
Route::post('create-conversation-post','Conversation\ConversationController@createConversationPost')->name('create.conversation.store');
Route::get('conversation-status-change-{ids?}','Conversation\ConversationController@ConversationStatusChange')->name('conversation.status.change');
Route::get('conversation-details-{ids?}','Conversation\ConversationController@viewConversationPage')->name('conversation.view');
Route::get('edit-conversation-{conId?}','Conversation\ConversationController@editConversationPage')->name('edit.conversation');
Route::post('update-conversation-{conId?}','Conversation\ConversationController@updateConversationPost')->name('update.conversation');

//manage user
Route::get('manage-users','User\UserController@manageUser')->name('manage.users');
Route::get('status-change-user-{id}','User\UserController@UserStatus')->name('user.status');
Route::get('delete-user-{id}','User\UserController@UserDelete')->name('user.delete');
Route::get('view-user-{id}','User\UserController@viewUser')->name('user.view');
Route::post('send-ycoin-by-admin','User\UserController@sendYcoins')->name('send.ycoin');
Route::get('view-conversation-user-{id}','User\UserController@viewUserConversation')->name('user.view.conversation');
// manage schedule
Route::get('manage-schedule','Schedule\ScheduleController@manageSchedule')->name('manage.schedule');
Route::get('add-schedule','Schedule\ScheduleController@addSchedulePage')->name('add.schedule');
Route::post('add-schedule','Schedule\ScheduleController@addSchedulePost')->name('add.schedule.post');
Route::get('edit-schedule/{masterIds?}','Schedule\ScheduleController@editSchedulePage')->name('edit.schedule');
Route::post('edit-schedule/{masterIds?}','Schedule\ScheduleController@editSchedulePost')->name('edit.schedule.post');
Route::get('cron-schedule','Schedule\ScheduleController@cronJobsScheduleUpdate');
Route::get('delete-schedule/{id?}','Schedule\ScheduleController@deleteSchedulePost')->name('delete.schedule');

// manage package
Route::get('manage-package','Package\PackageController@managePackage')->name('manage.package');
Route::get('add-package','Package\PackageController@addPackagePage')->name('add.package');
Route::post('add-package','Package\PackageController@addPackagePost')->name('add.package.post');
Route::get('edit-package-{packageids?}','Package\PackageController@editPackagePage')->name('edit.package');
Route::post('edit-package-{packageids?}','Package\PackageController@updatePackage')->name('update.package');
Route::any('delete-package-{packageids?}','Package\PackageController@deletePackage')->name('delete.package');

// manage Package Purchase
Route::any('manage-package-purchase','PackagePurchase\PackagePurchaseController@index')->name('manage.package.purchase');

