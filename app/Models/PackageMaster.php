<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageMaster extends Model
{
    protected $table = 'package_master';
    protected $guarded = [];  
}
