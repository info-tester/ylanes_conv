<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRef extends Model
{
    protected $table = 'user_ref';
    protected $guarded = [];  
    public function user(){
        return $this->hasOne('App\Models\User', 'id','user_id')->whereNotIn('status',['D']);
    }
}
