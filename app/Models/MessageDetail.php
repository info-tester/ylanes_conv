<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageDetail extends Model
{
    protected $table = 'chat_details';
    protected $guarded = [];

    public function getSender() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

}
