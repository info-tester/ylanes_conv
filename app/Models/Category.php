<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];  
    public function immediateParent(){
        return $this->hasOne('App\Models\Category', 'id', 'parent_id')->whereIn('status',['I','A']);
    }
    public function subCategories() {
    	return $this->hasMany('App\Models\Category', 'parent_id', 'id')->whereIn('status',['A'])->latest()->take(4);
    }
    public function allsubCategories() {
    	return $this->hasMany('App\Models\Category', 'parent_id', 'id')->whereIn('status',['A']);
    }
    public function allsubCategoriesTopics() {
    	return $this->hasMany('App\Models\Topic', 'sub_category_id', 'id')->whereIn('status',['A'])->groupBy('sub_category_id');
    }
}
