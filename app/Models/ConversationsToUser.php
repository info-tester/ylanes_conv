<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationsToUser extends Model
{
    protected $table = 'conversations_to_users';
    protected $guarded = [];

    public function users(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function getConversation(){
        return $this->hasOne('App\Models\Conversation', 'id', 'conversation_id')->where('status','!=','D');
    }
}
