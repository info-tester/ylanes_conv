<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YcoinHistory extends Model
{
    protected $table = 'ycoin_history';
    protected $guarded = [];  
}
