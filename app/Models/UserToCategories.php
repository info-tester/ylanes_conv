<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToCategories extends Model
{
    protected $table = 'user_to_categories';
    protected $guarded = [];
    public function category(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }
}
