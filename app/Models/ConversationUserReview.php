<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationUserReview extends Model
{
    protected $table = 'conversation_user_reviews';
    protected $guarded = [];
}
