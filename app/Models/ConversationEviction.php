<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationEviction extends Model
{
    protected $table = 'conversation_eviction';
    protected $guarded = [];

    public function users(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
