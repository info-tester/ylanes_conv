<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationScheduleDetail extends Model
{
    protected $table = 'conv_schedule_details';
    protected $guarded = [];
    public function topics(){
        return $this->hasOne('App\Models\Topic', 'id','topic_id')->whereIn('status',['I','A']);
    }
}
