<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationScheduleMaster extends Model
{
    protected $table = 'conv_schedule_master';
    protected $guarded = [];  

    public function ConversationScheduleDetails(){
        return $this->hasMany('App\Models\ConversationScheduleDetail', 'conv_schedule_master_id', 'id')->groupBy('topic_id');
    }
}
