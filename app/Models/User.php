<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Auth;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_to_category(){
        return $this->hasMany('App\Models\UserToCategories', 'user_id','id')->where('type','R');
    }
    public function user_to_category_moderator(){
        return $this->hasMany('App\Models\UserToCategories', 'user_id','id')->where('type','M');
    }
    public function userCountryDetails(){
        return $this->hasOne('App\Models\Country','id','country');
    }
    public function user_to_certificates(){
        return $this->hasMany('App\Models\UserToCertificate', 'user_id','id')->orderBy('year','asc');
    }
    public function userRef(){
        return $this->hasMany('App\Models\UserRef', 'ref_user_id','id');
    }
}
