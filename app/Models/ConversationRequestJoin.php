<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationRequestJoin extends Model
{
    protected $table = 'conversation_request_join';
    protected $guarded = [];
}
