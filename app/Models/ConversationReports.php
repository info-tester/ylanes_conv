<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationReports extends Model
{
    protected $table = 'conversation_reports';
    protected $guarded = [];

    public function users(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
