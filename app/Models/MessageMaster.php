<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageMaster extends Model
{
    protected $table = 'chat_master';
    protected $guarded = [];

}
