<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConnectsGroup extends Model
{
    protected $table = 'connects_groups';
    protected $guarded = [];  
}
