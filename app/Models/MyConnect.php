<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MyConnect extends Model
{
    protected $table = 'my_connect';
    protected $guarded = [];  
    public function touser(){
        return $this->hasOne('App\Models\User', 'id','to_user_id')->whereIn('status',['I','A','U']);
    }
    public function fromuser(){
        return $this->hasOne('App\Models\User', 'id','from_user_id')->whereIn('status',['I','A','U']);
    }
    public function topic(){
        return $this->hasOne('App\Models\Topic', 'id','topic_id')->whereIn('status',['I','A']);
    }
    public function fromGroup(){
        return $this->hasOne('App\Models\ConnectsGroup', 'id','from_user_group_id');
    }
    public function toGroup(){
        return $this->hasOne('App\Models\ConnectsGroup', 'id','to_user_group_id');
    }
}
