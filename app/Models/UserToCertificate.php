<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToCertificate extends Model
{
    protected $table = 'user_to_certificates';
    protected $guarded = [];
}
