<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationReviews extends Model
{
    protected $table = 'conversation_reviews';
    protected $guarded = [];
}
