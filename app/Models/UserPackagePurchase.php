<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPackagePurchase extends Model
{
    protected $table = 'user_package_purchase';
    protected $guarded = [];

    public function getPayment(){
        return $this->hasOne('App\Models\Payment', 'id', 'payment_master_id');
    }
    public function getUser(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
