<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topics';
    protected $guarded = [];  
    public function category(){
        return $this->hasOne('App\Models\Category', 'id','category_id')->whereIn('status',['I','A']);
    }
    public function subcategory(){
        return $this->hasOne('App\Models\Category', 'id','sub_category_id')->whereIn('status',['I','A']);
    }
    public function conversation(){
        return $this->hasMany('App\Models\Conversation', 'topic_id','id')->whereIn('status',['I','A']);
    }
}
