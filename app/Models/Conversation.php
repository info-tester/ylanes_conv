<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class Conversation extends Model
{
    protected $table = 'conversations';
    protected $guarded = [];
    public function topic(){
        return $this->hasOne('App\Models\Topic', 'id', 'topic_id')->whereIn('status',['I','A']);
    }
    public function users(){
        return $this->hasOne('App\Models\User', 'id', 'creator_user_id');
    }
    public function category(){
        return $this->hasOne('App\Models\Category', 'id', 'category_id')->whereIn('status',['I','A']);
    }
    public function sub_category(){
        return $this->hasOne('App\Models\Category', 'id', 'sub_category_id')->whereIn('status',['I','A']);
    }
    public function conversationstouserDef(){
        return $this->hasOne('App\Models\ConversationsToUser', 'conversation_id', 'id')->where('is_creator','Y')->where('status','!=','D');
    }
    public function conversationstousers(){
        return $this->hasMany('App\Models\ConversationsToUser', 'conversation_id', 'id')->where('status','C');
    }
    public function conversationstouserDef2(){
        return $this->hasOne('App\Models\ConversationsToUser', 'conversation_id', 'id')->where('status','!=','D');
    }
    public function conversation(){
        return $this->hasMany('App\Models\Conversation', 'topic_id', 'topic_id')->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i'))->where('status','!=','C');
    }
    public function conversationsconditionalCountUser(){
        return $this->hasMany('App\Models\ConversationsToUser', 'conversation_id', 'id')->where('is_creator','!=','Y')->where('status','!=','D');
    }
    public function conversationsReviews(){
        return $this->hasMany('App\Models\ConversationUserReview', 'conversation_id', 'id')->where('from_user_id',@auth::user()->id);
    }
    public function conversationReviews(){
        return $this->hasOne('App\Models\ConversationReviews', 'conversation_id', 'id')->where('user_id',@auth::user()->id);
    }
}
