<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use File;
use Auth;
use Mail;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Mail\VerificationMail;
use Session;
use Str;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Laravel\Socialite\Facades\Socialite;
use App\Models\ConversationsToUser;
use App\Models\Conversation;
use App\Models\YcoinHistory;
use Twilio\Rest\Client;
use App\Models\Country;
use Exception;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/dashboard';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $this->sessionId = session()->getId();
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        # check user's cart is empty or not.
        // $this->getCart(session()->getId());

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }
    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if(@$this->guard()->user()->is_phone_verified == 'N'){
            $userCreate['phone_vcode'] = mt_rand(100000, 999999);
            @$this->guard()->user()->update(@$userCreate);
            Session::put('user_ids',@$this->guard()->user()->id);
            @auth::logout();
            session()->flash("success",__('success.-4159'));
            return redirect()->route('guest.phone.verify',Session::get('user_ids'));
        }
        // return $this->authenticated($request, $this->guard()->user())
        // ?: redirect()->intended($this->redirectPath());
        if(empty(@$this->guard()->user()->country) || empty(@$this->guard()->user()->profile_name) || empty(@$this->guard()->user()->year_of_birth) || empty(@$this->guard()->user()->gender)){
            return redirect()->route('user.edit.profile');
        }
        if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
            $this->joiningConversationAfterLoginRegister(@$this->guard()->user()->id);
            return redirect()->route('upcoming.conversation');
        }
        if(!empty(Session::get('url_redirect_after_purchase_balance')))
        {
            if(empty(@$this->guard()->user()->conversation_balance_total) && @$this->guard()->user()->conversation_balance_total == 0){
                return redirect()->route('purchase.package');
            }
            else{
                return redirect(Session::get('url_redirect_after_purchase_balance'));
            }
        }
        return redirect()->route('landing.con.page');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        $user = User::where($this->username(), $request->{$this->username()})->first();
        if(!@$user){
            $user = User::where('phone', $request->{$this->username()})->first();
        }
        // dd($user);

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->status == 'I') {
            $errors = [$this->username() => 'Your account is not active.'];
        }
        // if ($user && \Hash::check($request->password, $user->password) && $user->is_phone_verified == 'N') {
        //     $errors = [$this->username() => 'Your account is not active by Admin.'];
        // }
        if ($user && \Hash::check($request->password, $user->password) && $user->is_email_verified == 'N') {
            try{
                $userCreate['email_vcode'] = mt_rand(100000, 999999);
                $user->update($userCreate);
                $user->refresh();
                $this->sendVerificationMail($user);
            }catch(\Exception $e){

            }
            $errors = ['not verified' => 'Your email is not verified please verify your email.'];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        // dd($errors);
        return redirect()->route('login')
        ->withInput($request->only($this->username(), 'remember'))
        ->withErrors($errors);
    }
     /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
     protected function credentials(Request $request)
     {
        // $credentials =  $request->only($this->username(), 'password');
        $username = $request->email;
        $user = User::where(['email'=>$request->email,'status'=>'A','is_email_verified'=>'Y'])->first();
        $credentials['email'] = $request->email;

        $credentials['password'] = $request->password;
        $credentials['status'] = 'A';
        $credentials['is_email_verified'] = 'Y';
        if(!empty($user)){
            $this->refCodeUpdate(@$user->id);
        }
        return $credentials;
    }
    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }
    public function handleProviderCallback(Request $request,$social)
    {
        $this->sessionId = session()->getId();
        if (!$request->has('code') || $request->has('denied')) {
            return redirect()->route('login');
        }
        $userSocial = Socialite::driver($social)->stateless()->user();
        if($social=='facebook') {
            $signup_frm   = 'F';
        }
        else {
            $signup_frm   = 'G';
        }
        if($social=='facebook') {
            $user = User::where(['provider_id'   => $userSocial->getId()])->where('status', '!=', 'D')->first();
        }
        else{
            // $user = User::where(['email' => $userSocial->getEmail(),'provider_id'   => $userSocial->getId()])->where('status', '!=', 'D')->first();
            $user = User::where(['provider_id'   => $userSocial->getId()])->where('status', '!=', 'D')->first();
        }
        if($user) {
            $this->refCodeUpdate(@$user->id);
            if(!empty($user->email)){
                if($user->signup_from == 'S')
                {
                    session()->flash('error',__('errors.-33000'));
                    return redirect()->route('login');
                }
                $user->update(['provider_id' => $userSocial->getId(),'signup_from' => @$signup_frm]);
                if(@$user->is_email_verified == 'Y' && @$user->is_phone_verified == 'Y'){
                    if(@$user->status == 'A')
                    {
                        Auth::login($user);
                        if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
                            $this->joiningConversationAfterLoginRegister(@$user->id);
                            return redirect()->route('upcoming.conversation');
                        }
                        if(!empty(Session::get('url_redirect_after_purchase_balance')))
                        {
                            if(empty(@$user->conversation_balance_total) && @$user->conversation_balance_total == 0){
                                return redirect()->route('purchase.package');
                            }
                            else{
                                return redirect(Session::get('url_redirect_after_purchase_balance'));
                            }
                        }
                        return redirect()->route('landing.con.page');
                    }
                    else{
                        session()->flash('error',__('errors.-5068'));
                        return redirect()->route('login');
                    }
                }
                else{
                    return redirect()->route('facebook.email.login.page',@$user->provider_id);
                }
            }
            else{
                return redirect()->route('facebook.email.login.page',@$user->provider_id);
            }
        } else {
            if(!empty($userSocial->getEmail())){
                $cheExistUser = User::where(['email' => $userSocial->getEmail()])->where('status', '!=', 'D')->first();
                if(!empty($cheExistUser)){
                    if($cheExistUser->signup_from == 'S')
                    {
                        session()->flash('error',__('errors.-33000'));
                        return redirect()->route('login');
                    }
                    $cheExistUser->update(['provider_id' => $userSocial->getId(),'signup_from' => @$signup_frm]);
                    if(@$cheExistUser->is_email_verified == 'Y' && @$cheExistUser->is_phone_verified == 'Y'){
                        if($cheExistUser->status == 'A'){
                            Auth::login($cheExistUser);
                            $this->refCodeUpdate(@$cheExistUser->id);
                            if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
                                $this->joiningConversationAfterLoginRegister(@$cheExistUser->id);
                                return redirect()->route('upcoming.conversation');
                            }
                            if(!empty(Session::get('url_redirect_after_purchase_balance')))
                            {
                                if(empty(@$cheExistUser->conversation_balance_total) && @$cheExistUser->conversation_balance_total == 0){
                                    return redirect()->route('purchase.package');
                                }
                                else{
                                    return redirect(Session::get('url_redirect_after_purchase_balance'));
                                }
                            }
                            return redirect()->route('landing.con.page');
                        }
                        else{
                            session()->flash('error',__('errors.-5068'));
                            return redirect()->route('login');
                        }
                    }
                    else{
                        return redirect()->route('facebook.email.login.page',@$cheExistUser->provider_id);
                    }
                }
            }
            //split full name
            $name = $userSocial->getName();
            $splitName = explode(' ', $name, 2);
            $first_name = $splitName[0];
            $last_name = !empty($splitName[1]) ? $splitName[1] : '';
            if($userSocial->getEmail()) {
                //save profile image
                $fileContents = file_get_contents($userSocial->getAvatar());
                $file_name = $userSocial->getId() . ".jpg";
                File::put('storage/app/public/customer/profile_pics/' . $file_name, $fileContents);
                $image_resize = Image::make('storage/app/public/customer/profile_pics/' . $file_name);
                $image_resize->resize(133, 133);
                $image_resize->save('storage/app/public/customer/profile_pics/' .$file_name);

                if($social=='facebook') {
                    $user = User::create([
                        // 'first_name'    => @$first_name,
                        // 'last_name'     => @$last_name,
                        'image'         => @$file_name,
                        'email'         => @$userSocial->getEmail(),
                        'provider_id'   => $userSocial->getId(),
                        'signup_from' => 'F',
                        'status'      => 'A',
                        'is_email_verified' => 'Y',
                    ]);
                }
                else {
                    $user = User::create([
                        // 'first_name'  => @$first_name,
                        // 'last_name'   => @$last_name,
                        'image'       => @$file_name,
                        'email'       => @$userSocial->getEmail(),
                        'provider_id'   => @$userSocial->getId(),
                        'signup_from' => 'G',
                        'status'      => 'A',
                        'is_email_verified' => 'Y',
                    ]);
                }
                $this->refCodeUpdate(@$user->id);
                return redirect()->route('facebook.email.login.page',@$user->provider_id);
            } else {
                if($social=='facebook') {
                    $signup_frm   = 'F';
                }
                else {
                    $signup_frm   = 'G';
                }
                $fileContents = file_get_contents($userSocial->getAvatar());
                $file_name = $userSocial->getId() . ".jpg";
                File::put('storage/app/public/customer/profile_pics/' . $file_name, $fileContents);
                $image_resize = Image::make('storage/app/public/customer/profile_pics/' . $file_name);
                $image_resize->resize(133, 133);
                $image_resize->save('storage/app/public/customer/profile_pics/' .$file_name);

                $user = User::create([
                    // 'first_name'    => @$first_name,
                    // 'last_name'     => @$last_name,
                    'image'         => @$file_name,
                    'provider_id'   => @$userSocial->getId(),
                    'email'         => @$userSocial->getEmail(),
                    'signup_from'   => $signup_frm,
                    'status'        => 'A',
                    // 'is_email_verified' => 'Y'
                ]);
                $this->refCodeUpdate(@$user->id);
                return redirect()->route('facebook.email.login.page',$userSocial->getId());
            }
        }
    }
    public function facebookEmailLoginPage($id)
    {
        $data['users'] =  User::where('provider_id', $id)->where('status','!=','D')->first();
        return view('auth.facebook_email_login',$data)->with('id', $id);
    }
    public function facebookEmailLogin(Request $request, $id)
    {
        $user = User::where('provider_id', $id)->where('status','!=','D')->first();
        if(!empty($user))
        {
            $request->validate([
                'profile_name' => [
                    'required',
                    'string',
                ],
                'country_code' => [
                    'required',
                    'numeric',
                ],
                'email' => [
                    'required',
                    'email',
                    'max:255',
                    Rule::unique('users')->where(function($query) use($request,$user) {
                        $query->where('status','!=','D')
                        ->where('id','!=',@$user->id);
                    })
                ],
                'phone' => [
                    'required',
                    'numeric',
                    'digits_between:10,10',
                    Rule::unique('users')->where(function($query) use($request,$user) {
                        $query->where('status','!=','D')
                        ->where('id','!=',@$user->id);
                    })
                ]
            ],[
                'phone.digits_between'=>'Please enter a valid phone no.'
            ]);
            $update['email'] = $request->email;
            $update['phone'] = $request->phone;
            $update['country'] = $request->country_code;
            $update['profile_name'] = $request->profile_name;
            $six_digit_phone_vcode = mt_rand(100000, 999999);
            $six_digit_email_vcode = mt_rand(100000, 999999);
            $update['phone_vcode'] = $six_digit_phone_vcode;
            $update['email_vcode'] = $six_digit_email_vcode;
            if(@$user->is_email_verified == 'N' && !empty(@$request->email)){
                @$user['email'] = @$request->email;
                @$user['email_vcode'] = @$six_digit_email_vcode;
                session()->flash("success",__('success.-4093'));
                $this->sendVerificationMail($user);
            }
            $country=Country::where('id',$request->country_code)->first();
            $message= 'Welcome to ylanes ! '.'Your Verification OTP :' . $six_digit_phone_vcode;
            // $receiverPhoneNumber = '+919804559517';
            $receiverPhoneNumber = '+'.@$country->phonecode.$request->phone;
            // dd($receiverPhoneNumber);
            $this->sendMessage($message, $receiverPhoneNumber);
            @$user->update($update);
            return redirect()->route('guest.phone.verify',@$user->id);
        }
        else{
            session()->flash('error',__('errors.-33108'));
            return redirect()->route('login');
        }
    }
    public function sendVerificationMail($reqData)  {

        Mail::send(new VerificationMail($reqData));
    }
    private function refCodeUpdate($userId){
        $userId = sprintf('%05d', $userId);
        $refNumber = "YL".$userId;
        $cUser = User::where('id',@$userId)->first();
        if(empty(@$cUser->ref_code)){
            if(!empty($cUser)){
                $cUser->update(['ref_code' => @$refNumber]);
            }
        }
        $u_userId = sprintf('%03d', @$userId);
        $u_refNumber = "Y".@$u_userId;
        if(empty(@$cUser->unique_id)){
            if(!empty($cUser)){
                $cUser->update(['unique_id' => @$u_refNumber]);
            }
        }
        if(!empty($cUser)){
            $checkYHis = YcoinHistory::where('user_id',@$cUser->id)->where('category','S')->first();
            if(empty($checkYHis)){
                YcoinHistory::create([
                    'user_id' => @$cUser->id,
                    'coin_free' => '1000',
                    'coin_paid' => '0',
                    'balance_coin_free' => '1000',
                    'balance_coin_paid' => @$cUser->conversation_balance_paid,
                    'type' => 'IN',
                    'category' => 'S'
                ]);
                $cUser->update(['conversation_balance_free' => '1000','conversation_balance_total'=>'1000']);
            }
        }
    }
    public function joiningConversationAfterLoginRegister($userId){
        if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
            @$userData = User::whereId(@$userId)->first();
            if(empty(@$userData->conversation_balance_total) && @$userData->conversation_balance_total == 0){
                Session::forget('joining_conversation_id');
                Session::forget('joining_conversation_text');
                Session::forget('joining_display_name');
                Session::forget('is_anonymously');
                return false;
            }
            $jConIds = Session::get('joining_conversation_id');
            $jConText = Session::get('joining_conversation_text');
            $is_anonymously = Session::get('is_anonymously');
            $joining_display_name = Session::get('joining_display_name');
            $checkConver = Conversation::where('status','A')->where('date','>=',date('Y-m-d'))->where('id',@$jConIds)->first();
            if(!empty($checkConver)){
                if (@$checkConver->is_full == 'N') {
                    if (@$checkConver->type == 'ON') {
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@$userId)->first();
                        if(empty(@$chechConverStionUsers)){
                            @$userData = User::whereId(@$userId)->first();
                            if(@$userData->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                User::whereId(@$userData->id)->decrement('conversation_balance_paid');
                            }
                            else{
                                $is_paid = 'F';
                                User::whereId(@$userData->id)->decrement('conversation_balance_free');
                            }
                            ConversationsToUser::create([
                                'user_id' => @$userId,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$is_anonymously,
                                'display_name' => @$joining_display_name,
                                'conversation_text' => @$jConText,
                                'status' => 'C',
                                'conv_balance_type' => $is_paid,
                            ]);
                            @$checkConver->increment('no_of_participants');
                            @$checkConver->update(['is_full' => 'Y']);
                            $totalBalance = User::whereId(@$userData->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@$userData->id)->update(['conversation_balance_total' => @$totalBalance]);
                        }
                    }
                    elseif(@$checkConver->type == 'OP'){
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@$userId)->first();
                        if(empty(@$chechConverStionUsers)){
                            @$userData = User::whereId(@$userId)->first();
                            if(@$userData->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                User::whereId(@$userData->id)->decrement('conversation_balance_paid');
                            }
                            else{
                                $is_paid = 'F';
                                User::whereId(@$userData->id)->decrement('conversation_balance_free');
                            }
                            ConversationsToUser::create([
                                'user_id' => @$userId,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$is_anonymously,
                                'display_name' => @$joining_display_name,
                                'conversation_text' => @$jConText,
                                'status' => 'C',
                                'conv_balance_type' => $is_paid,
                            ]);
                            @$checkConver->increment('no_of_participants');
                            if(@$checkConver->conversationstousers->count() > 4){
                                @$checkConver->update(['is_full' => 'Y']);
                            }
                            $totalBalance = User::whereId(@$userData->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@$userData->id)->update(['conversation_balance_total' => @$totalBalance]);
                        }
                    }
                    elseif(@$checkConver->type == 'CO'){
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@$userId)->first();
                        if(empty(@$chechConverStionUsers)){
                            ConversationsToUser::create([
                                'user_id' => @$userId,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$is_anonymously,
                                'display_name' => @$joining_display_name,
                                'conversation_text' => @$jConText,
                                'status' => 'U',
                                'conv_balance_type' => null,
                            ]);
                        }
                    }
                }
                Session::forget('joining_conversation_id');
                Session::forget('joining_conversation_text');
                Session::forget('joining_display_name');
                Session::forget('is_anonymously');
            }
        }
    }

    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param  $recipients string or array of phone number of recepient
     */
    private function sendMessage($message, $recipients)
    {
        try{

            $account_sid = env("TWILIO_ACCOUNT_SID");
            $auth_token = env("TWILIO_ACCOUNT_TOKEN");
            $twilio_number = env("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create( $recipients, ['from' => $twilio_number, 'body' => $message]
            );
        }catch(Exception $e){
            return $e;
        }
    }
}
