<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use DB;
use App\Models\UserRef;
use Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Mail\VerificationMail;
use Auth;
use Mail;
use Illuminate\Validation\Rule;
use App\Models\ConversationsToUser;
use App\Models\Conversation;
use App\Models\YcoinHistory;
use Twilio\Rest\Client;
use App\Models\Country;
use Exception;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest',['except' => ['verifyEmail']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'profile_name' => ['required', 'string', 'max:15'],
            // 'first_name' => ['required', 'string', 'max:255'],
            // 'last_name' => ['required', 'string',  'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->where(function($query)  {
                    $query->where('status','!=','D');
                })
            ],
            'phone' => [
                'required',
                'numeric',
                Rule::unique('users')->where(function($query) {
                    $query->where('status','!=','D');
                })
            ],
            // 'username' => [
            //     'required',
            //     'string',
            //     'max:255',
            //     Rule::unique('users')->where(function($query)  {
            //         $query->where('status','!=','D');
            //     })
            // ],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $userCreate['email_vcode'] = mt_rand(100000, 999999);
        $userCreate['phone_vcode'] = mt_rand(100000, 999999);
        if(!empty($data['user_type'])){
            $user_type = 'H';
        }
        else{
            $user_type = 'G';
        }
        $user = User::create([
            // 'first_name' => $data['profile_name'],
            // 'last_name' => $data['profile_name'],
            'profile_name' => $data['profile_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'country' => $data['country_code'],
            'email_vcode' => @$userCreate['email_vcode'],
            'phone_vcode' => @$userCreate['phone_vcode'],
            'password' => Hash::make($data['password']),
            // 'user_type' => @$user_type,
        ]);
        $this->refCodeUpdate(@$user->id);
        $country=Country::where('id',$data['country_code'])->first();
        $message= 'Welcome to ylanes ! '.'Your Verification OTP :' . $userCreate['phone_vcode'];
        // $receiverPhoneNumber = '+919804559517';
        $receiverPhoneNumber = '+'.@$country->phonecode.$data['phone'];
        $this->sendMessage($message, $receiverPhoneNumber);
        $upd=[];
        $code='US';
        $sum=str_pad(@$user->id, 7, '0', STR_PAD_LEFT);
        $upd['user_id']=$code.$sum;
        User::where('id',$user->id)->update($upd);
        if(@$user['email']){
            try{
                $this->sendVerificationMail($user);
            }catch(\Exception $e){

            }
        }
        return $user;
    }
    public function sendVerificationMail($reqData)  {

        Mail::send(new VerificationMail($reqData));
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $existUser = User::whereIn('status',['I','A','U']);
        $existUser = $existUser->where(function($q1) use($request) {
            $q1 = $q1->where('email', $request->email)
            ->orWhere("phone", $request->phone);
        });
        $existUser = $existUser->first();
        if(@$existUser){
            session()->flash("error",__('errors.-5080'));
            return redirect()->route('login');
        }
        event(new Registered($user = $this->create($request->all())));

        $this->registered($request, $user)
        ?: redirect($this->redirectPath());
        session()->flash("success",__('success.-4093'));
        return redirect()->route('guest.phone.verify',@$user->id);
    }

    public function chkEmailExist(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];

        $isPhone = is_numeric(trim($request->params['email']));
        if($isPhone)
        {
            $merchant = User::where(['phone' => trim($request->params['email'])]);
        }
        else{
            $merchant = User::where(['email' => trim($request->params['email'])]);
        }
        $merchant = $merchant->where(function($query) use($request){
            $query->Where('status','!=','D');
        });
        if(!empty(@$request->params['user_id']))
        {
            $merchant = $merchant->where('id','!=',@$request->params['user_id']);
        }
        if(@$request->params['user_id']){
            $merchant = $merchant->where('id','!=',$request->params['user_id']);
        }
        $merchant = $merchant->first();
        // dd($merchant);
        // if(empty($merchant)){
        //     $merchant = User::where(['tmp_email' => trim($request->params['email'])])->Where('status','!=','D')->first();
        // }
        if(@$merchant) {
            if($isPhone)
            {
                $response['error']['merchant'] = 'This mobile no already exists.';
            }
            else{
                $response['error']['merchant'] = 'This email id already exists.';
            }

        } else {
            $response['result']['status']  = false;
            if($isPhone)
            {
                $response['result']['message'] = 'This mobile no is available.';
            }
            else{
                $response['result']['message'] = 'This email is available.';
            }
        }
        return response()->json($response);
    }
    public function verifyPhone(Request $request){
        if(!empty(@$request->phone)){
            $userPhone = User::where('phone',$request->phone)->where('status','!=','D')->first();
            if(!empty(@$userPhone))
            {
                if(@$userPhone->status == 'I'){
                    return 2;
                }
                else{
                    $user = User::where('phone',$request->phone)->where('status','!=','D')->first();
                    $six_digit_phone_vcode = mt_rand(100000, 999999);
                    $update['phone_vcode'] = $six_digit_phone_vcode;
                    $country=Country::where('id',$user->country)->first();
                    $message= 'Welcome to ylanes ! '.'Your Verification OTP :' . $six_digit_phone_vcode;
                    // $receiverPhoneNumber = '+919804559517';
                    $receiverPhoneNumber = '+'.@$country->phonecode.$user->phone;
                    // dd($receiverPhoneNumber);
                    $this->sendMessage($message, $receiverPhoneNumber);
                    $user->update($update);
                    return $user;
                }
            }
            else{
                $userPhones = User::where('phone',$request->phone)->where('is_phone_verified','N')->where('status','!=','D')->first();
                if(!empty(@$userPhones)){
                    return 1;
                }
                else{
                    return 0;
                }
            }
        }
        if(!empty(@$request->phone_otp)){
            $user = User::where('id',$request->user_id)->where('status','!=','D')->first();
            if(@$user->phone_vcode == $request->phone_otp){
                $update['phone_vcode'] = null;
                if($user->status=='U'){
                    $update['status'] = 'A';
                    $update['is_phone_verified'] = 'Y';
                }
                $user->update($update);
                Auth::login($user);
                // if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
                //     $this->joiningConversationAfterLoginRegister(@$user->id);
                //     return 'c_page';
                // }
                if(empty(@auth::user()->country) || empty(@auth::user()->profile_name) || empty(@auth::user()->year_of_birth) || empty(@auth::user()->gender)){
                    return route('user.edit.profile');
                }
                if(!empty(Session::get('url_redirect_after_purchase_balance')))
                {
                    if(empty(@$user->conversation_balance_total) && @$user->conversation_balance_total == 0){
                        return 'p_page';
                    }
                    else{
                        return Session::get('url_redirect_after_purchase_balance');
                    }
                }
                else{
                    return 'e_page';
                }
                return 'e_page';
            }
            else{
                return 0;
            }
        }
    }
    public function verifyEmail($vcode, $id)
    {
        $merchant = User::where('email_vcode', $vcode)->where('status','!=','D')->first();
        if(!empty($merchant)){
            if(@$merchant->email_vcode != null) {
                $update['email_vcode'] = NULL;
                $update['status'] = 'A';
                $update['is_email_verified'] = 'Y';
                User::where('id', $merchant->id)->update($update);
                $this->UserRefCreate($merchant->id);
                session()->flash('success',__('success.-4094'));
                if(!empty(Auth::user())){
                    return redirect()->route('user.edit.profile');
                }
                else{
                    return redirect()->route('login');
                }
            } else {
                session()->flash('success',__('success.-4095'));
                return redirect()->route('login');
            }
        }
        else{
            session()->flash('success',__('success.-4095'));
            return redirect()->route('login');
        }
    }
    public function guestEmailPhoneVerifyPage($userid){
        $data['user'] = User::where('id',@$userid)->where('is_phone_verified','N')->where('status','!=','D')->first();
        if(!empty($data['user'])){
            return view('auth.guest_phone_verify',$data);
        }
        else{
            $datas = User::where('id',@$userid)->where('is_phone_verified','Y')->where('status','!=','D')->first();
            if(!empty($datas))
            {
                session()->flash('success',__('success.-4112'));
                return redirect()->route('login');
            }
            else{
                session()->flash('error',__('errors.-33108'));
                return redirect()->route('login');
            }
        }
    }
    public function guestEmailPhoneVerifyPagePost(Request $request,$userids){
        $user = User::where('id',@$userids)->where('is_phone_verified','N')->where('status','!=','D')->first();
        if (!empty(@$user)) {
            if(@$request->phone_otp == @$user->phone_vcode)
            {
                $user->update([
                    'is_phone_verified' => 'Y',
                    'phone_vcode' => null
                ]);
                if($user->status=='U'){
                    $user->update([
                        'status' => 'A'
                    ]);
                }
                // if($user->user_type == 'G'){
                    $this->UserRefCreate($user->id);
                    Auth::login($user);
                    if(empty(@auth::user()->country) || empty(@auth::user()->profile_name) || empty(@auth::user()->year_of_birth) || empty(@auth::user()->gender)){
                        return redirect()->route('user.edit.profile');
                    }
                    if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
                        $this->joiningConversationAfterLoginRegister(@$user->id);
                        return redirect()->route('upcoming.conversation');
                    }
                    if(!empty(Session::get('url_redirect_after_purchase_balance')))
                    {
                        if(empty(@$user->conversation_balance_total) && @$user->conversation_balance_total == 0){
                            return redirect()->route('purchase.package');
                        }
                        else{
                            return redirect(Session::get('url_redirect_after_purchase_balance'));
                        }
                    }
                    return redirect()->route('user.edit.profile');
                // }
                // else{
                //     Auth::login($user);
                //     return redirect()->route('host.edit.profile');
                // }
                // session()->flash('success',__('success.-4113'));
                // return redirect()->route('login');
            }
            else{
                session()->flash('error',__('errors.-33039'));
                return redirect()->back();
            }
        }
        else{
            session()->flash('error',__('errors.-33108'));
            return redirect()->route('login');
        }
    }
    public function verifyPhoneResendOtp(Request $request){
        $user = User::where('phone',$request->phone)->where('status','!=','D')->first();
        $six_digit_phone_vcode = mt_rand(100000, 999999);
        $update['phone_vcode'] = $six_digit_phone_vcode;
        $user->update($update);

        $country=Country::where('id',$user->country)->first();
        $message= 'Welcome to ylanes ! '.'Your Verification OTP :' . $six_digit_phone_vcode;
        // $receiverPhoneNumber = '+919804559517';
        $receiverPhoneNumber = '+'.@$country->phonecode.$user->phone;
        // dd($receiverPhoneNumber);
        $this->sendMessage($message, $receiverPhoneNumber);
        return $user;
    }
    private function refCodeUpdate($userId){
        $userId = sprintf('%05d', $userId);
        $refNumber = "YL".$userId;
        $cUser = User::where('id',@$userId)->first();
        if(empty(@$cUser->ref_code)){
            if(!empty($cUser)){
                $cUser->update(['ref_code' => @$refNumber]);
            }
        }
        $u_userId = sprintf('%03d', @$userId);
        $u_refNumber = "Y".@$u_userId;
        if(empty(@$cUser->unique_id)){
            if(!empty($cUser)){
                $cUser->update(['unique_id' => @$u_refNumber]);
            }
        }
        if(!empty($cUser)){
            YcoinHistory::create([
                'user_id' => @$cUser->id,
                'coin_free' => '1000',
                'coin_paid' => '0',
                'balance_coin_free' => '1000',
                'balance_coin_paid' => @$cUser->conversation_balance_paid,
                'type' => 'IN',
                'category' => 'S'
            ]);
            $cUser->update(['conversation_balance_free' => '1000','conversation_balance_total'=>'1000']);
        }
    }
    private function UserRefCreate($userIds){
        if(!empty(Session::get('ref_user_id'))){
            $user = User::where('id',@Session::get('ref_user_id'))->where('status','!=','D')->first();
            // dd($user);
            if(!empty($user)){
                $checkRefer = UserRef::where('ref_user_id',@$user->id)->where('user_id',@$userIds)->first();
                if (empty(@$checkRefer)) {
                    UserRef::create([
                        'ref_user_id' => @$user->id,
                        'user_id' => @$userIds,
                    ]);
                    Session::forget('ref_user_id');
                }
            }
        }
    }
    public function joiningConversationAfterLoginRegister($userId){
        if(!empty(Session::get('joining_conversation_id')) && !empty(Session::get('joining_conversation_text'))){
            @$userData = User::whereId(@$userId)->first();
            if(empty(@$userData->conversation_balance_total) && @$userData->conversation_balance_total == 0){
                Session::forget('joining_conversation_id');
                Session::forget('joining_conversation_text');
                Session::forget('joining_display_name');
                Session::forget('is_anonymously');
                return false;
            }
            $jConIds = Session::get('joining_conversation_id');
            $jConText = Session::get('joining_conversation_text');
            $is_anonymously = Session::get('is_anonymously');
            $joining_display_name = Session::get('joining_display_name');
            $checkConver = Conversation::where('status','A')->where('date','>=',date('Y-m-d'))->where('id',@$jConIds)->first();
            if(!empty($checkConver)){
                if (@$checkConver->is_full == 'N') {
                    if (@$checkConver->type == 'ON') {
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@$userId)->first();
                        if(empty(@$chechConverStionUsers)){
                            @$userData = User::whereId(@$userId)->first();
                            if(@$userData->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                User::whereId(@$userData->id)->decrement('conversation_balance_paid');
                            }
                            else{
                                $is_paid = 'F';
                                User::whereId(@$userData->id)->decrement('conversation_balance_free');
                            }
                            ConversationsToUser::create([
                                'user_id' => @$userId,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$is_anonymously,
                                'display_name' => @$joining_display_name,
                                'conversation_text' => @$jConText,
                                'status' => 'C',
                                'conv_balance_type' => $is_paid,
                            ]);
                            @$checkConver->increment('no_of_participants');
                            @$checkConver->update(['is_full' => 'Y']);
                            $totalBalance = User::whereId(@$userData->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@$userData->id)->update(['conversation_balance_total' => @$totalBalance]);
                        }
                    }
                    elseif(@$checkConver->type == 'OP'){
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@$userId)->first();
                        if(empty(@$chechConverStionUsers)){
                            @$userData = User::whereId(@$userId)->first();
                            if(@$userData->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                User::whereId(@$userData->id)->decrement('conversation_balance_paid');
                            }
                            else{
                                $is_paid = 'F';
                                User::whereId(@$userData->id)->decrement('conversation_balance_free');
                            }
                            ConversationsToUser::create([
                                'user_id' => @$userId,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$is_anonymously,
                                'display_name' => @$joining_display_name,
                                'conversation_text' => @$jConText,
                                'status' => 'C',
                                'conv_balance_type' => $is_paid,
                            ]);
                            @$checkConver->increment('no_of_participants');
                            if(@$checkConver->conversationstousers->count() > 4){
                                @$checkConver->update(['is_full' => 'Y']);
                            }
                            $totalBalance = User::whereId(@$userData->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@$userData->id)->update(['conversation_balance_total' => @$totalBalance]);
                        }
                    }
                    elseif(@$checkConver->type == 'CO'){
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@$userId)->first();
                        if(empty(@$chechConverStionUsers)){
                            ConversationsToUser::create([
                                'user_id' => @$userId,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$is_anonymously,
                                'display_name' => @$joining_display_name,
                                'conversation_text' => @$jConText,
                                'status' => 'U',
                                'conv_balance_type' => null,
                            ]);
                        }
                    }
                }
                Session::forget('joining_conversation_id');
                Session::forget('joining_conversation_text');
                Session::forget('joining_display_name');
                Session::forget('is_anonymously');
            }
        }
    }


    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param  $recipients string or array of phone number of recepient
     */
    private function sendMessage($message, $recipients)
    {
        try{

            $account_sid = env("TWILIO_ACCOUNT_SID");
            $auth_token = env("TWILIO_ACCOUNT_TOKEN");
            $twilio_number = env("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create( $recipients, ['from' => $twilio_number, 'body' => $message]
            );
        }catch(Exception $e){
            // return $e;
        }
    }

    public function changeMobile(Request $request){
        $userData = User::where('id',$request->user_id)->where('is_phone_verified','N')->where('status','!=','D')->first();
        if($userData){
            $userCreate['phone_vcode'] = mt_rand(100000, 999999);
            $userCreate['phone'] = $request->phone;
            $userCreate['country'] = $request->country_code;
            $userData->update(@$userCreate);
            $country=Country::where('id',$request->country_code)->first();
            $message= 'Welcome to ylanes ! '.'Your Verification OTP :' . $userCreate['phone_vcode'];
            $receiverPhoneNumber = '+'.@$country->phonecode.$request->phone;
            $this->sendMessage($message, $receiverPhoneNumber);
            Session::put('user_ids',$userData->id);
            session()->flash("success",__('success.-4159'));
            return redirect()->route('guest.phone.verify',Session::get('user_ids'));
        }
        return redirect()->back();
    }
}
