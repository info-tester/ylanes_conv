<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Mail;
use App\Mail\ResetPasswordCustomerMail;
use Illuminate\Support\Facades\Hash;
use Twilio\Rest\Client;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showFormResetPassword(){
        // $otp = 02355;
        // $message= 'Welcome to MyLO!'.'Your Verification Code :' . $otp;
        // $receiverPhoneNumber = '+919804559517';

        // $this->sendMessage($message, $receiverPhoneNumber);
        return view('auth.reset_password');
    }
    /*
    * Method        : sendResetPasswordLink
    * Description   : This method is used to send reset password link
    * Author        : Jayatri
    */
    public function sendResetPasswordLink(Request $request){

        if(@$request->all()){
            // dd($request->all());
            $customer = User::where('email',$request->email)->where('status', 'A')->first();
            if(@$customer){
                $otp = mt_rand(100000,999999);
                $update['email_vcode'] = $otp;
                $update['phone_vcode'] = $otp;
                User::where('email',$request->email)->where('status', 'A')->update($update);
            }
            if(@$customer){
                if(@$customer->email){
                    $customer = User::where('email',@$customer->email)->where('status', 'A')->first();
                    $user               =   new \stdClass();
                    $user->fname        =   @$customer->first_name;
                    $user->lname        =   @$customer->last_name;
                    $user->email        =   @$customer->email;
                    $user->id           =   encrypt(@$customer->id);
                    $user->email_vcode  =   @$customer->email_vcode;
                    Mail::send(new ResetPasswordCustomerMail($user));
                }

                //success
                session()->flash("success",__('success.-4073'));
                // return redirect()->route('verify.otp',$customer);
                // return redirect()->route('register.success',['id' => encrypt($customer->id)]);
                return redirect()->route('user.update.forget.password.set',encrypt($customer->id));
            }else{
                //error
                session()->flash("error",__('errors.-5083'));
                return redirect()->back();
            }
        }
    }


    public function updateResetPassword($id,Request $request){
        $userId = decrypt($id);
        $user = User::where('id',$userId)->where('status', 'A')->first();
        if(@$request->all()){

            if(@$user){
                if(@$request->all()){
                    if($request->new_password == $request->password_confirmation && @$user->email_vcode == @$request->otp){
                        $update['password'] = Hash::make($request->new_password);
                        $update['email_vcode'] = NULL;
                        $update['is_email_verified'] = 'Y';
                        // dd($update);
                        User::where(['id' => $userId])->update($update);
                        session()->flash("success",__('success.-4074'));
                        return redirect()->route('login');
                    } else {
                        //error message
                        session()->flash("error",__('errors.-5072'));
                        return redirect()->back();
                    }
                } else {
                    $data['otp'] = @$user->email_vcode;
                    return view('auth.set_new_password',$data);
                }
            } else {
                //error messsage
                // session()->flash("error","User not found! Password not changed! Please try again!");
                session()->flash("error",__('errors.-5072'));
                return redirect()->back();
            }
        }else{
            $data['id'] = $id;
            $data['otp'] = @$user->email_vcode;
            return view('auth.set_new_password',$data);
        }
    }

    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param  $recipients string or array of phone number of recepient
     */
    private function sendMessage($message, $recipients)
    {
        try{

            $account_sid = env("TWILIO_ACCOUNT_SID");
            $auth_token = env("TWILIO_ACCOUNT_TOKEN");
            $twilio_number = env("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create( $recipients, ['from' => $twilio_number, 'body' => $message]
            );
        }catch(Exception $e){
            // return $e;
        }
    }
}
