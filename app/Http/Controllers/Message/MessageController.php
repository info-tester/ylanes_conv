<?php

namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MessageDetail;
use App\Models\MessageMaster;
use App\Models\Conversation;
use App\Models\ConversationsToUser;
use App\Models\User;
use Storage;
use Pusher\Pusher;

class MessageController extends Controller
{


    public function sendMessage(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        $conversationId=$request->conversationId;

        $checkMessageMaster=MessageMaster::where('conversation_id',$conversationId)->first();

        $to_id=ConversationsToUser::where('conversation_id',$conversationId)->where('user_id','!=',auth()->user()->id)->pluck('user_id');

        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);
        $err_msg=0;
        $message=@$request->message;
        if($checkMessageMaster==null){
            $insMaster['conversation_id']=$conversationId;
            $createMaster=MessageMaster::create($insMaster);

            $ins['chat_master_id']=$createMaster->id;
            $ins['user_id']=auth()->user()->id;
            $ins['message']=@$message;
            if($request->file('file')) {
                $file = $request->file;
                $filename = time() . '-' . rand(1000, 9999) .'_'. $file->getClientOriginalName();
                Storage::putFileAs('public/message_files', $file, $filename);
                $ins['file_name']=$filename;
            }
            $messageCreate= MessageDetail::create($ins);
            $messageData= MessageDetail::where('id',$messageCreate->id)->with('getSender')->get();
            $cc= ConversationsToUser::where('conversation_id',$conversationId)->get();
            $value=array();
            foreach($cc as $item){
                $value[$item->user_id]=$item->display_name;
            }

            $new_registrationIds = User::whereIn('id',$to_id )->pluck('socket_id')->toArray();
            $response1 = $pusher->trigger('Ylanes', 'chat-event', [
                'to_id'         =>  $to_id,
                'from'          =>  @auth()->user()->profile_name,
                'online_status' =>  @$users->online_status,
                'from_id'       =>  @auth()->user()->id,
                'file'          =>  '',
                'conversation_id'    =>  $conversationId,
                'messageData'=>$messageData,
                'allUser'=>$value,
            ], $new_registrationIds);

            $response['result']=$messageData;


            return response()->json($response);

        }
        else{

            $ins['chat_master_id']=$checkMessageMaster->id;
            $ins['user_id']=auth()->user()->id;
            $ins['message']=@$message;
            if($request->file('file')) {
                $file = $request->file;
                $filename = time() . '-' . rand(1000, 9999) .'_'. $file->getClientOriginalName();
                Storage::putFileAs('public/message_files', $file, $filename);
                $ins['file_name']=$filename;
            }
            $messageCreate= MessageDetail::create($ins);
            $messageData= MessageDetail::where('id',$messageCreate->id)->with('getSender')->get();
            $cc= ConversationsToUser::where('conversation_id',$conversationId)->get();
            $value=array();
            foreach($cc as $item){
                $value[$item->user_id]=$item->display_name;
            }

            $new_registrationIds = User::whereIn('id',$to_id )->pluck('socket_id')->toArray();
            $response1 = $pusher->trigger('Ylanes', 'chat-event', [
                'to_id'         =>  $to_id,
                'from'          =>  @auth()->user()->profile_name,
                'online_status' =>  @$users->online_status,
                'from_id'       =>  @auth()->user()->id,
                'file'          =>  '',
                'conversation_id'    =>  $conversationId,
                'messageData'=>$messageData,
                'allUser'=>$value,
            ], $new_registrationIds);

            $response['result']=$messageData;
            return response()->json($response);

        }


    }


    public function fetchMessage(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];

        $conversationId=@$request->params['conversationId'];
        $checkMessageMaster=MessageMaster::where('conversation_id',$conversationId)->first();

        if($checkMessageMaster==null){
            $response['result']=[];
            return response()->json($response);
        }
        $messageData= MessageDetail::where('chat_master_id',@$checkMessageMaster->id)->with('getSender')->get();
        $cc= ConversationsToUser::where('conversation_id',$conversationId)->get();
        $value=array();
        foreach($cc as $item){
            $value[$item->user_id]=$item->display_name;
        }
        $response['result']=$messageData;
        $response['allUser']=$value;
        return response()->json($response);
    }



}
