<?php

namespace App\Http\Controllers\StaticPages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\ContactMail;

class StaticPagesController extends Controller
{

    public function contactUs()
    {
    	return view('modules.static_pages.contact_us');
    }

    public function sendmail(Request $request)
    {
        // return $request->all();

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            // 'mobile' => 'required',
            'message' => 'required',
        ]);
        $data = [
            'name'=>$request->name,
            'user_email'=>$request->email,
            'number'=>@$request->mobile,
            'message'=>$request->message,
        ];
            Mail::send(new ContactMail($data));
		return redirect()->back()->with('success',__('success.-4165'));
    }

    public function FAQ(){
        return view('modules.static_pages.faq');
    }
    public function aboutUs(){
        return view('modules.static_pages.about_us');
    }
    public function howItWorks(){
        return view('modules.static_pages.how_it_works');
    }


    public function termsOfService(){
        return view('modules.static_pages.terms_of_service');
    }
    public function privacyPolicy(){
        return view('modules.static_pages.privacy_policy');

    }
}
