<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use Mail;
use File;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Profession;
use App\Models\Language;
use App\Models\Country;
use App\Models\Category;
use App\Models\UserToCategories;
use App\Models\Holiday;
use App\Models\UserToCertificate;
use App\Models\MyConnect;
use App\Models\UserRef;
use App\Models\ConnectsGroup;
use Exception;
use App\Mail\UpdateEmailVerificationMail;
use App\Models\Conversation;
use App\Models\ConversationsToUser;
use Twilio\Rest\Client;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['updateTempEmail']]);
        $this->middleware('autologout');
    }
    public function editProfile() {
        $data['professions'] = Profession::orderBy('name','asc')->get();
        $data['languages'] = Language::orderBy('name','asc')->get();
        $data['countries'] = Country::orderBy('name','asc')->get();
        $data['sub_category'] = Category::where('parent_id','!=',0)->where('status','A')->orderBy('name','asc')->get();
        $data['holidays'] = Holiday::get();
        if(@auth::user()->is_login == '0'){
            return view('modules.dashboard.first_login_edit_profile',@$data);
        }
        return view('modules.dashboard.edit_profile',@$data);
    }
    public function viewDashboard() {
        $data['connect_request_users_from_user_id'] = MyConnect::orwhere(['from_user_id' => @auth::user()->id , 'to_user_id' => @auth::user()->id])
        ->whereHas('touser', function($q){
            $q->where('id','>',0);
        })
        ->whereHas('fromuser', function($q){
            $q->where('id','>',0);
        })->where('status','A')->pluck('from_user_id')->toArray();
        $data['connect_request_users_to_user_id'] = MyConnect::orwhere(['from_user_id' => @auth::user()->id , 'to_user_id' => @auth::user()->id])
        ->whereHas('touser', function($q){
            $q->where('id','>',0);
        })
        ->whereHas('fromuser', function($q){
            $q->where('id','>',0);
        })->where('status','A')->pluck('to_user_id')->toArray();
        $connectUsersIds = array_merge($data['connect_request_users_from_user_id'],$data['connect_request_users_to_user_id']);
        $finalconnectUsersIds = array_values(array_unique($connectUsersIds));
        $converStionIds = ConversationsToUser::whereIn('user_id',array_values($finalconnectUsersIds))
        ->whereIn('status',['C','U'])
        ->pluck('conversation_id')->toArray();
        $data['allConversation'] = Conversation::whereIn('id',array_unique(@$converStionIds))
        ->where('creator_user_id','!=',@auth::user()->id)
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i'))
        ->orderBy('date','desc')->take(20)->get();
        return view('modules.dashboard.dashboard',@$data);
    }
    public function storeUserDetails(Request $request) {
        if(empty(@$request->profile_picture)) {
            if(@$request->country){
                $user['country']    =   @$request->country;
            }
            if(@$request->gender){
                $user['gender']      =   @$request->gender;
            }
            $user['about_me']    =   @$request->about_me;
            $user['year_of_birth']    =   @$request->year_of_birth;
            if(!empty(@$request->profile_name)){
                $user['profile_name']    =   @$request->profile_name;
                $user['first_name']    =   @$request->profile_name;
                $user['last_name']    =   @$request->profile_name;
            }
            $user['describe_you']    =   @$request->describe_you;
            $user['problem_solve']    =   @$request->problem_solve;
            $user['qualities_you']    =   @$request->qualities_you;
            $user['marital_status']    =   @$request->marital_status;
            $user['profession']    =   @$request->profession;
            if(!empty(@$request->holidays)){
                $user['holidays']    =   json_encode(@$request->holidays);
                $user['holidays_other'] = !empty(@$request->holidays_other) ? @$request->holidays_other : null;
            }
            else{
                $user['holidays'] = null;
                $user['holidays_other'] = !empty(@$request->holidays_other) ? @$request->holidays_other : null;
            }
            if(!empty(@$request->interest)){
                $checkExistData = UserToCategories::where('user_id',@auth::user()->id)->where('type','R')->get();
                foreach (@$request->interest as $value) {
                    if(@$checkExistData->isNotEmpty()){
                        foreach ($checkExistData as $values) {
                            $checkExist = UserToCategories::where('user_id',@auth::user()->id)->where('type','R')->where('category_id',$value)->first();
                            if(!empty($checkExist)){
                                UserToCategories::where('user_id',@auth::user()->id)->where('type','R')->whereNotIn('category_id',@$request->interest)->delete();
                            }
                            else{
                                UserToCategories::create([
                                    'category_id' => $value,
                                    'user_id' => @auth::user()->id,
                                    'type' => 'R'
                                ]);
                            }
                        }
                    }
                    else{
                        UserToCategories::create([
                            'category_id' => $value,
                            'user_id' => @auth::user()->id,
                            'type' => 'R'
                        ]);
                    }
                }
            }
            else{
                UserToCategories::where('user_id',@auth::user()->id)->delete();
            }
            if(!empty(@$request->moderator_sub_category)){
                $checkExistDataM = UserToCategories::where('user_id',@auth::user()->id)->where('type','M')->get();
                foreach (@$request->moderator_sub_category as $value1) {
                    if(@$checkExistDataM->isNotEmpty()){
                        foreach ($checkExistDataM as $values2) {
                            $checkExist2 = UserToCategories::where('user_id',@auth::user()->id)->where('type','M')->where('category_id',$value1)->first();
                            if(!empty($checkExist2)){
                                UserToCategories::where('user_id',@auth::user()->id)->where('type','M')->whereNotIn('category_id',@$request->moderator_sub_category)->delete();
                            }
                            else{
                                UserToCategories::create([
                                    'category_id' => $value1,
                                    'user_id' => @auth::user()->id,
                                    'type' => 'M'
                                ]);
                            }
                        }
                    }
                    else{
                        UserToCategories::create([
                            'category_id' => $value1,
                            'user_id' => @auth::user()->id,
                            'type' => 'M'
                        ]);
                    }
                }
            }
            if(!empty(@$request->memories)){
                $user['memories']   =   @$request->memories;
            }
            $user['profile_headline']   =   @$request->profile_headline;
            $user['media_link']   =   @$request->media_link;
        }
        if(@$request->profile_picture) {
            $destinationPath = "storage/app/public/customer/profile_pics/";
            $base64_str = substr($request->profile_picture, strpos($request->profile_picture, ",")+1);
            $image_base64 = base64_decode($base64_str);

            $img = @Auth::user()->id.".".time().".".'jpg';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            $imgs = Image::make('storage/app/public/customer/profile_pics/' . $img);
            $imgs->resize(133, 133, function($constraint) {
                $constraint->aspectRatio();
            })->save('storage/app/public/customer/profile_pics/' . $img, 60);
            $user['image']     =   $img;
            $imagePath = 'storage/app/public/customer/profile_pics/'.@Auth::user()->image;
            if(File::exists($imagePath)) {
                File::delete($imagePath);
            }
            User::whereId(@Auth::user()->id)->update($user);
            return $img;
        }
        User::whereId(@Auth::user()->id)->update($user);
        $user = User::whereId(@Auth::user()->id)->first();

        // if(@$request->new_password) {
        //     $this->changePassword($request);
        // } else {
        //     session()->flash("success",__('success_user.-700'));
        // }
        session()->flash("success",__('success_user.-700'));
        // }
        // session()->flash("success",__('success_user.-700'));
        if(@auth::user()->is_login == '0'){
            @auth::user()->update(['is_login' => '1']);
        }
        if(!empty(@$user->country) && !empty(@$user->profile_name) && !empty(@$user->year_of_birth) && !empty(@$user->gender)){
            if(!empty(Session::get('url_redirect_after_purchase_balance')))
            {
                return redirect(Session::get('url_redirect_after_purchase_balance'));
            }
        }
        return redirect()->route('user.edit.profile');
    }
    public function addUserCertificate(Request $request){
        $checkExist = UserToCertificate::where('user_id',@auth::user()->id)->where('certification_name',@$request->certification_name)->first();
        if(empty($checkExist)){
            UserToCertificate::create([
                'user_id' => @auth::user()->id,
                'institute_name' => @$request->institute_name,
                'certification_name' => @$request->certification_name,
                'year' => @$request->year,
            ]);
            return 1;
        }
        else{
            return 0;
        }
    }
    public function getUserCertificate(){
        $UserToCertificate = UserToCertificate::where('user_id',@auth::user()->id)->orderBy('year','asc')->get();
        $result = '';
        foreach ($UserToCertificate as $datas) {
            $result .= '<li style="padding: 10px; background: #ffe47f; margin-bottom: 10px; float: left; margin-right: 10px; font-weight: 500;">'.@$datas->institute_name.' > '.@$datas->certification_name.' > '.@$datas->year.' <i class="fas fa-times" style="padding: 6px; cursor: pointer;" onclick="deleteCertification('.@$datas->id.');"></i></li>';
        }
        return $result;
    }
    public function deleteUserCertificate($ids){
        $UserToCertificate = UserToCertificate::where('user_id',@auth::user()->id)->where('id',@$ids)->delete();
        return 1;
    }
    public function refferandearnpage(){
        return view('modules.dashboard.referral_and_earn');
    }
    public function myConnectList(Request $request){
        $data['new_request_users'] = MyConnect::orwhere(['to_user_id' => @auth::user()->id])
        ->whereHas('touser', function($q){
            $q->where('id','>',0);
        })
        ->whereHas('fromuser', function($q){
            $q->where('id','>',0);
        })
        ->where('status','N')->latest()->paginate(15);
        $data['send_request_users'] = MyConnect::orwhere(['from_user_id' => @auth::user()->id])
        ->whereHas('touser', function($q){
            $q->where('id','>',0);
        })
        ->whereHas('fromuser', function($q){
            $q->where('id','>',0);
        })
        ->where('status','N')->latest()->paginate(15);
        $data['connect_request_users'] = MyConnect::orwhere(['from_user_id' => @auth::user()->id , 'to_user_id' => @auth::user()->id]);
        if($request->all()){
            if(!empty(@$request->group_id)){
                $data['connect_request_users'] = $data['connect_request_users']->where(['from_user_group_id' => @$request->group_id]);
                if($data['connect_request_users']->count() == 0){
                    $data['connect_request_users'] = MyConnect::orwhere(['from_user_id' => @auth::user()->id , 'to_user_id' => @auth::user()->id])->where(['to_user_group_id' => @$request->group_id]);
                }
            }
        }
        $data['connect_request_users'] = $data['connect_request_users']
        ->whereHas('touser', function($q){
            $q->where('id','>',0);
        })
        ->whereHas('fromuser', function($q){
            $q->where('id','>',0);
        })->where('status','A');
        $user=User::where('id',auth()->user()->id)->first();
        $user->update(['tot_connects'=>$data['connect_request_users']->count()]);
        Auth::setUser($user);
        $data['connect_request_users'] = $data['connect_request_users']->latest()->paginate(15);
        $data['group_list'] = ConnectsGroup::where('user_id',@auth::user()->id)->get();
        return view('modules.dashboard.my_connect',@$data);
    }
    public function myConnectStatusChange(Request $request,$id,$sta){
        $myConnectData = MyConnect::whereId($id)->first();
        if(!empty(@$myConnectData)){
            if(@$sta == 'N'){
                @$myConnectData->update(['status'=>'A']);
                @auth::user()->increment('tot_connects');
                session()->flash("success",__('success.-4145'));
                return redirect()->back();
            }
            elseif(@$sta == 'B'){
                @$myConnectData->delete();
                // @auth::user()->decrement('tot_connects');
                // @$myConnectData->update(['status'=>'B']);
                session()->flash("success",__('success.-4146'));
            }
            elseif(@$sta == 'S'){
                @$myConnectData->delete();
                // @auth::user()->decrement('tot_connects');
                // @$myConnectData->update(['status'=>'B']);
                session()->flash("success",__('success.-4146'));
            }
            elseif(@$sta == 'A'){
                @$myConnectData->delete();
                // @auth::user()->decrement('tot_connects');
                session()->flash("success",__('success.-4149'));
            }
            elseif(@$sta == 'BB'){
                @$myConnectData->delete();
                // @auth::user()->decrement('tot_connects');
                // @$myConnectData->update(['status'=>'B']);
                session()->flash("success",__('success.-4146'));
                return redirect()->back();
            }
        }
        else{
            session()->flash("error",__('errors.-5001'));
        }
        return redirect()->back()->with(['type' => @$sta]);
    }
    public function myBlockList(){
        $data['block_users'] = MyConnect::orwhere(['from_user_id' => @auth::user()->id , 'to_user_id' => @auth::user()->id])
        ->whereHas('touser', function($q){
            $q->where('id','>',0);
        })
        ->whereHas('fromuser', function($q){
            $q->where('id','>',0);
        })
        ->where('status','B')->latest()->paginate(15);
        return view('modules.dashboard.my_block_list',@$data);
    }
    public function createGroup(Request $request){
    	$myConnect = MyConnect::whereId(@$request->connect_id)->first();
    	if(!empty(@$myConnect)){
    		// if(empty(@$myConnect->from_user_group_id) || empty(@$myConnect->to_user_group_id)){
                if(!empty(@$request->group_name)){
    			    $save = ConnectsGroup::where('user_id',@auth::user()->id)->where('group_name',@$request->group_name)->first();
                }
                elseif(!empty(@$request->group_id)){
                    $save = ConnectsGroup::where('user_id',@auth::user()->id)->where('group_name',@$request->group_id)->first();
                }
                else{
                    session()->flash("error",__('errors.-5001'));
                    return redirect()->back();
                }
    			if(empty($save)){
	    			$save = ConnectsGroup::create([
	    				'user_id' => @auth::user()->id,
	    				'group_name' => @$request->group_name
	    			]);
	    		}
    			if($save){
    				if(@$myConnect->from_user_id == @auth::user()->id){
    					$myConnect->update(['from_user_group_id' => $save->id]);
    				}
    				elseif(@$myConnect->to_user_id == @auth::user()->id){
    					$myConnect->update(['to_user_group_id' => $save->id]);
    				}
    			}
    			session()->flash("success",__('success.-4150'));
    		// }
    		// else{
    		// 	session()->flash("error",__('errors.-5001'));
    		// }
    	}
    	else{
            session()->flash("error",__('errors.-5001'));
        }
        return redirect()->back();
    }
    public function refferUsersListspage(){
        $data['users'] = UserRef::where('ref_user_id',@auth::user()->id)
        ->whereHas('user', function($q){
            $q->where('id','>',0);
        })->latest()->paginate(15);
        return view('modules.dashboard.refered_users_lists',@$data);
    }
    public function settingsPage(){
        return view('modules.dashboard.setting');
    }
    public function updatechkEmailExist(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];

        $isPhone = is_numeric(trim($request->params['email']));
        if($isPhone)
        {
            $merchant = User::where(['phone' => trim($request->params['email'])]);
        }
        else{
            $merchant = User::where(['email' => trim($request->params['email'])]);
        }
        $merchant = $merchant->where(function($query) use($request){
            $query->Where('status','!=','D');
        });
        $merchant = $merchant->first();
        // if(empty($merchant)){
        //     $merchant = User::where(['tmp_email' => trim($request->params['email'])])->Where('status','!=','D')->first();
        // }
        if(@$merchant) {
            if($isPhone)
            {
                $response['error']['merchant'] = 'This mobile no already exists.';
            }
            else{
                $response['error']['merchant'] = 'This email id already exists.';
            }

        } else {
            $response['result']['status']  = false;
            if($isPhone)
            {
                $response['result']['message'] = 'This mobile no is available.';
            }
            else{
                $response['result']['message'] = 'This email is available.';
            }
        }
        return response()->json($response);
    }
    public function updatechkEmailExistPost(Request $request,$userIds){
        $user = User::where('email',@$request->temp_email)->where('status','!=','D')->first();
        if(empty(@$user) && !empty($userIds)){
            try{
                @$user = User::where('id',@$userIds)->first();
                $userCreate['email_vcode'] = mt_rand(100000, 999999);
                $userCreate['temp_email'] = @$request->temp_email;
                @$user->update($userCreate);
                Mail::send(new UpdateEmailVerificationMail(@$user));
                session()->flash("success",__('success.-4097'));
            }catch(\Exception $e){
                session()->flash('error',__('errors.-33108'));
                return redirect()->back();
            }
        }else{
            session()->flash('error',__('errors.-33108'));
        }
        return redirect()->back();
    }
    public function updateTempEmail($emailvcode,$id){
        $merchant = User::where('email_vcode', $emailvcode)->where('status','!=','D')->first();
        if(!empty($merchant)){
            if(@$merchant->email_vcode != null) {
                $update['email_vcode'] = NULL;
                $update['email'] = $merchant->temp_email;
                $update['temp_email'] = NULL;
                User::where('id', $merchant->id)->update($update);
                if(empty(@Auth::user()))
                {
                    session()->flash('success',__('success.-4116'));
                    return redirect()->route('login');
                }
                else{
                    session()->flash('success',__('success.-4116'));
                    return redirect()->route('user.settings');
                }
            } else {
                if(empty(@Auth::user()))
                {
                    session()->flash('success',__('success.-4095'));
                    return redirect()->route('login');
                }
                else{
                    session()->flash('success',__('success.-4095'));
                    return redirect()->route('user.settings');
                }
            }
        }
        else{
            if(empty(@Auth::user()))
            {
                session()->flash('success',__('success.-4095'));
                return redirect()->route('login');
            }
            else{
                session()->flash('success',__('success.-4095'));
                return redirect()->route('user.settings');
            }
        }
    }
    public function verifyUserUpdatePhone(Request $request,$userId){
        if(!empty(@$request->phone)){
            $userPhone = User::where('phone',$request->phone)->where('status','!=','D')->first();
            if(!empty(@$userPhone))
            {
                return 0;

            }
            else{
                $user = User::where('id',@$userId)->where('status','!=','D')->first();
                $six_digit_phone_vcode = mt_rand(100000, 999999);
                $update['phone_vcode'] = $six_digit_phone_vcode;
                $update['temp_phone'] = $request->phone;
                $user->update($update);
                $country=Country::where('id',$request->country_code)->first();
                $message= 'Welcome to ylanes ! '.'Your Verification OTP :' . $six_digit_phone_vcode;
                // $receiverPhoneNumber = '+919804559517';
                $receiverPhoneNumber = '+'.@$country->phonecode.$request->phone;
                // dd($receiverPhoneNumber);
                $this->sendMessage($message, $receiverPhoneNumber);
                return $user;
            }
        }
    }
    public function verifyUserUpdatePhoneByOtp(Request $request){
        $user = User::where('phone_vcode',@$request->phone_update_otp)->first();
        if(@$user->phone_vcode == $request->phone_update_otp){
            $update['phone'] = @$user->temp_phone;
            $update['temp_phone'] = null;
            $update['phone_vcode'] = null;
            if(!empty(@$request->country_code_popup))
            {
                $update['country'] = @$request->country_code_popup;
            }
            $user->update($update);
            session()->flash('success',__('success.-4117'));
            return redirect()->back();
        }
        else{
            session()->flash('success',__('success.-33108'));
            return redirect()->back();
        }
    }




    /*
     * Method : changeDash
     * Description : Used Ajax call in user dashboard when notification of web in user dashboard comes change the value evrything in dashboard required.
     * Author : Soumojit
	 * Date:2022-MAY-09
     */
    public function changeDash(Request $request) {
        if(@$request->all()){
			$up=array();
			$up['socket_id']=$request->socketId;
			User::where('id',auth()->user()->id)->update($up);
			$response['status']='success';
			return response()->json($response);
        }
    }
    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param  $recipients string or array of phone number of recepient
     */
    private function sendMessage($message, $recipients)
    {
        try{

            $account_sid = env("TWILIO_ACCOUNT_SID");
            $auth_token = env("TWILIO_ACCOUNT_TOKEN");
            $twilio_number = env("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create( $recipients, ['from' => $twilio_number, 'body' => $message]
            );
        }catch(Exception $e){
            // return $e;
        }
    }
}
