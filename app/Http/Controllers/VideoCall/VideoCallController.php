<?php

namespace App\Http\Controllers\VideoCall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\ConversationsToUser;
use App\Models\User;
use Auth;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client;
use TwilioRestClient;
use TwilioJwtAccessToken;
use TwilioJwtGrantsVideoGrant;
use Pusher\Pusher;
use App\Models\ConversationRequestJoin;
use App\Models\ConversationReports;
use App\Models\ConversationEviction;
use App\Models\YcoinHistory;

class VideoCallController extends Controller
{
    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct()
    {
        $this->middleware('auth');
        /*for twilio */
        $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
    }


    public function index($conversationId=null){
        $data['conversationDetails'] =  Conversation::whereId(@$conversationId)->first();
        $currentUser=ConversationsToUser::where('conversation_id',@$data['conversationDetails']->id)->where('user_id',@auth::user()->id)->first();
        if(@$currentUser==null){
            session()->flash("error",__('errors.-5001'));
            return redirect()->back();
        }
        if($currentUser->is_evicted=='Y'){
            session()->flash("error",__('errors.-33129'));
            return redirect()->back();
        }
		if(!empty($data['conversationDetails']))
		{
			$data['conversationJoinUsers'] = ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('status','!=','D')->get();
			if($data['conversationDetails']->creator_user_id == @auth::user()->id){
				$data['is_creator'] = 'yes';
			}
			else{
				$data['is_creator'] = 'no';
			}
            ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('user_id',@auth::user()->id)->update(['active_video_call'=>'Y']);
            $data['currentUser']=$currentUser;
            $userNameIdentity='';
            if($currentUser->is_anonymously=='Y'){
                $userNameIdentity=$currentUser->display_name;
            }else{
                $userNameIdentity=Auth::user()->profile_name;
            }
            $data['userNameIdentity']= $userNameIdentity;

            $data['report']=ConversationReports::where('conversation_master_id',$data['conversationDetails']->id)->where('reported_by_id',auth()->user()->id)->pluck('user_id')->toArray();
            $data['eviction']=ConversationEviction::where('conversation_master_id',$data['conversationDetails']->id)->where('reported_by_id',auth()->user()->id)->pluck('user_id')->toArray();
            $joinUser=ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('active_video_call','Y')->count();
            $data['callEndTime']=null;
            if(@$joinUser>1 &&  $data['conversationDetails']->call_end_time==null){
                $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
                    'cluster' => env('PUSHER_APP_CLUSTER')
                ]);
                $data['conversationDetails']->update(['call_end_time'=> date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').'+'.env('ExtendTimeMinute').' minutes'))]);
                $data['callEndTime']=env('ExtendTimeSecond');
                $to_id=ConversationsToUser::where('conversation_id',$conversationId)->where('user_id','!=',auth()->user()->id)->pluck('user_id');
                $new_registrationIds = User::whereIn('id',$to_id )->pluck('socket_id')->toArray();
                $response1 = $pusher->trigger('Ylanes', 'call-extend-event', [
                    'to_id'         =>  $to_id,
                    'from_id'         =>  Auth::user()->id,
                    'from'          =>  Auth::user()->profile_name,
                    'key1'          =>  "value1",
                    'room'          =>  $data['conversationDetails']->id,
                    'conversation_id'          =>  $data['conversationDetails']->id,
                    'time'          =>  env('ExtendTimeSecond'),
                ], $new_registrationIds);
            }elseif($data['conversationDetails']->call_end_time!=null){
                $tt=$data['conversationDetails']->no_of_extend*env('ExtendTimeMinute');
                $data['callEndTime']=strtotime(@$data['conversationDetails']->call_end_time)-strtotime(date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').'-'.$tt.' minutes')));
            }
			return view('modules.video_call.video_call',@$data);
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}
    }


    public function getToken(Request $request)
    {
        $roomName=$request->roomName;

        $data['conversationDetails'] =  Conversation::whereId(@$request->roomName)->first();
        $currentUser=ConversationsToUser::where('conversation_id',@$data['conversationDetails']->id)->where('user_id',auth::user()->id)->first();
        $currentUser->update(['join_time'=>date('Y-m-d H:i:s')]);
        $userNameIdentity='';
        if($currentUser->is_anonymously=='Y'){
            $userNameIdentity=$currentUser->display_name;
        }else{
            $userNameIdentity=Auth::user()->profile_name;
        }
        // A unique identifier for this user
        // $identity = Auth::user()->profile_name.'--'.@$currentUser->is_anonymously.'-'. Auth::user()->id;
        // $user['name']= Auth::user()->profile_name;
        $identity = $userNameIdentity.'--'.@$currentUser->is_anonymously.'-'. Auth::user()->id;
        $user['name']= $userNameIdentity;

        \Log::debug("joined with identity: $identity");
        $token = new AccessToken(
            $this->sid,
            $this->key,
            $this->secret, 3600,
            $identity,
        );

        $videoGrant = new VideoGrant();
        $videoGrant->setRoom($roomName);
        $token->addClaim('username', 'Soumojit Sadhukhan');

        $token->addGrant($videoGrant);
        $response = [
            'token'     => $token->toJWT(),
            'identity'  => $identity
        ];
        return response()->json($response, 200);

        // return view('room', [ 'accessToken' => $token->toJWT(), 'roomName' => $roomName ]);
    }

    /**
     * Method: removeUser
     * Description: This method is used to remove user from video call
     * Author: Soumojit
     * date:27-JULY-2021
     */
    public function removeUser(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];


        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $check=ConversationEviction::where('conversation_master_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->
        where('reported_by_id',auth()->user()->id)->first();
        if(@$check==null){
            $ins=[];
            $ins['conversation_master_id']=$request->params['roomId'];
            $ins['user_id']=$request->params['userId'];
            $ins['reported_by_id']=auth()->user()->id;
            ConversationEviction::create($ins);
            ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->increment('tot_eviction');
        }
        $datEv=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->first();
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);

        $new_registrationIds = User::where('id',$request->params['userId'] )->pluck('socket_id')->toArray();

        if($datEv->tot_eviction>1){
            $client = new Client($this->sid, $this->token);

            // $participant = $client->video->rooms($request->params['roomId'])
            // ->participants($request->params['sid'])
            // ->update(array("status" => "disconnected"));


            // ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->update(['is_evicted'=>'Y']);
            $response1 = $pusher->trigger('Ylanes', 'eviction-event', [
                'to_id'         =>  $request->params['userId'],
                'from_id'         =>  0,
                'from'          =>  ' ',
                'message'          =>  "You are evicted from the conversation",
                'room'          =>  $data['conversationDetails']->id,
                'evict_type'          =>  '2',
                'conversation_id'          =>  $data['conversationDetails']->id,
            ], $new_registrationIds);
            $response = [
                'conversationDetails'     => $data['conversationDetails']
            ];
        }else{
            $response1 = $pusher->trigger('Ylanes', 'eviction-event', [
                'to_id'         =>  $request->params['userId'],
                'from_id'         =>  0,
                'from'          =>  '',
                'message'          =>  "You are asked to leave this conversation",
                'room'          =>  $data['conversationDetails']->id,
                'evict_type'          =>  '1',
                'conversation_id'          =>  $data['conversationDetails']->id,
            ], $new_registrationIds);
            $response = [
                'conversationDetails'     => $data['conversationDetails']
            ];
        }
        $response['result']['message']='Success';
        return response()->json($response, 200);
    }

    public function createRequest(Request $request){

        $data['conversationDetails'] =  Conversation::whereId(@$request->conversionId)->first();
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);
        ConversationRequestJoin::where('from_user',Auth::user()->id)->where('conversation_id',$data['conversationDetails']->id)->delete();
        $data['conversationJoinUsers'] = ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('status','!=','D')->where('active_video_call','Y')->get();
        ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('user_id',Auth::user()->id)->where('join_type','R')->delete();
        $create=[];
        $create['conversation_id']=$data['conversationDetails']->id;
        $create['user_id']=Auth::user()->id;
        $create['join_type']='R';
        ConversationsToUser::create($create);
        foreach ($data['conversationJoinUsers'] as $item){
            $new_registrationIds = User::where('id',$item->user_id )->pluck('socket_id')->toArray();
            $ins=[];
            $ins['from_user']=Auth::user()->id;
            $ins['to_user']=$item->user_id;
            $ins['conversation_id']=$data['conversationDetails']->id;
            $ins['status']='S';
            ConversationRequestJoin::create($ins);
            $response1 = $pusher->trigger('Ylanes', 'receive-event', [
                'to_id'         =>  $item->user_id,
                'from_id'         =>  Auth::user()->id,
                'from'          =>  Auth::user()->profile_name,
                'key1'          =>  "value1",
                'room'          =>  $data['conversationDetails']->id,
                'conversation_id'          =>  $data['conversationDetails']->id,
                // 'order_id'          =>  $meetings->id,
                // 'time'          =>  $meetings->duration-$meetings->completed_call,
            ], $new_registrationIds);
        }
        $response = [
            'conversationDetails'     => $data['conversationDetails']
        ];
        return response()->json($response, 200);

    }

    public function requestCallStatus(Request $request){
        $data['conversationDetails'] =  Conversation::whereId(@$request->conversionId)->first();
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);

        ConversationRequestJoin::where('from_user',@$request->from_user)->where('conversation_id',@$request->conversionId)->where('to_user',@$request->to_user)->update(['status'=>@$request->status]);
        $checkAllAreRejected=ConversationRequestJoin::where('from_user',@$request->from_user)->where('conversation_id',@$request->conversionId)->whereIn('status',['R'])->count();
        $check=ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('user_id',$request->from_user)->where('join_type','R')->where('status','U')->first();

        if(@$request->status=='R'&&$checkAllAreRejected==1 && $check){
            ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('user_id',$request->from_user)->where('join_type','R')->update(['status'=>'R']);
            $new_registrationIds = User::where('id',$request->from_user )->pluck('socket_id')->toArray();
            $response1 = $pusher->trigger('Ylanes', 'join-event-reject', [
                'to_id'         =>  $request->from_user,
                'from'          =>  Auth::user()->profile_name,
                'key1'          =>  "value1",
                'room'          =>  $data['conversationDetails']->id,
                'conversation_id'          =>  $data['conversationDetails']->id,
            ], $new_registrationIds);
            $response = [
                'conversationDetails'     => $data['conversationDetails']
            ];
            return response()->json($response, 200);
        }

        $checkAllAreAccepted=ConversationRequestJoin::where('from_user',@$request->from_user)->where('conversation_id',@$request->conversionId)->whereIn('status',['R','S'])->count();
        if($checkAllAreAccepted==0){
            ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('user_id',$request->from_user)->where('join_type','R')->update(['status'=>'C']);
            $new_registrationIds = User::where('id',$request->from_user )->pluck('socket_id')->toArray();
            $response1 = $pusher->trigger('Ylanes', 'join-event-accept', [
                'to_id'         =>  $request->from_user,
                'from'          =>  Auth::user()->profile_name,
                'key1'          =>  "value1",
                'room'          =>  $data['conversationDetails']->id,
                'conversation_id'          =>  $data['conversationDetails']->id,
            ], $new_registrationIds);
            $response = [
                'conversationDetails'     => $data['conversationDetails']
            ];
            return response()->json($response, 200);
        }
        $response = [
            'conversationDetails'     => $data['conversationDetails']
        ];
        return response()->json($response, 200);

    }


    public function sendWarning(Request $request){

        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);

        $new_registrationIds = User::where('id',$request->params['userId'] )->pluck('socket_id')->toArray();
        $response1 = $pusher->trigger('Ylanes', 'warning-event', [
            'to_id'         =>  $request->params['userId'],
            'from_id'         =>  0,
            'from'          =>  ' ',
            'message'          =>  "You received an alert",
            'room'          =>  $data['conversationDetails']->id,
            'conversation_id'          =>  $data['conversationDetails']->id,
        ], $new_registrationIds);
        $response = [
            'conversationDetails'     => $data['conversationDetails']
        ];
        return response()->json($response, 200);
    }

    public function updateCallTime(Request $request){
        $currentUser=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->first();
        if(@$currentUser->join_time!=null){

            $date1Timestamp = strtotime($currentUser->join_time) ;
            $date2Timestamp = strtotime(date('Y-m-d H:i:s'));
            //Calculate the difference.
            $difference = $date2Timestamp - $date1Timestamp;
            $seconds = $difference;
            $currentUser->update(['join_time'=>null,'duration'=>$currentUser->duration+$seconds,'active_video_call'=>'N']);
        }
        $response['result']['message']='Success';
        return response()->json($response, 200);
    }


    public function AddReport(Request $request){
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $check=ConversationReports::where('conversation_master_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->
        where('reported_by_id',auth()->user()->id)->first();
        if(@$check==null){
            $ins=[];
            $ins['conversation_master_id']=$request->params['roomId'];
            $ins['user_id']=$request->params['userId'];
            $ins['reported_by_id']=auth()->user()->id;
            ConversationReports::create($ins);
            ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->increment('tot_report');
        }
        $response['result']['message']='Success';
        return response()->json($response, 200);
    }

    public function connectUserPublicProfile(Request $request){
        $userData = User::whereIn('status',['A','U'])
        ->where('id',$request->params['userId'])
        ->first();
        $conversationData =ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->first();
        $data['profile_name']=@$conversationData->is_anonymously=='Y'?@$conversationData->display_name:@$userData->profile_name;
        $data['image']= @$conversationData->is_anonymously=='Y'?null:@$userData->image;
        $data['gender']=@$userData->gender;
        $data['conversation_text']=@$conversationData->conversation_text;
        $data['is_anonymously']=@$conversationData->is_anonymously;
        $data['id']=@$userData->id;
        $response['result']['message']='Success';
        $response['result']['data']=$data;
        return response()->json($response, 200);

    }

    /**
     * Method: removeUser
     * Description: This method is used to remove user from video call
     * Author: Soumojit
     * date:27-JULY-2021
     */
    public function removeEvictionRequest(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];



        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $check=ConversationEviction::where('conversation_master_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->
        where('reported_by_id',auth()->user()->id)->first();
        if(@$check!=null){
            $check->delete();
            ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->decrement('tot_eviction');
        }
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);

        $new_registrationIds = User::where('id',$request->params['userId'] )->pluck('socket_id')->toArray();

        $datEv=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->first();
        if($datEv->tot_eviction>0){
            $response1 = $pusher->trigger('Ylanes', 'eviction-event-remove', [
                'to_id'         =>  $request->params['userId'],
                'from_id'         =>  0,
                'from'          =>  ' ',
                'message'          =>  "Asked to leave alert is removed by the user.",
                'room'          =>  $data['conversationDetails']->id,
                'evict_type'          =>  '1',
                'conversation_id'          =>  $data['conversationDetails']->id,
            ], $new_registrationIds);
            $response = [
                'conversationDetails'     => $data['conversationDetails']
            ];
        }else{
            $response1 = $pusher->trigger('Ylanes', 'eviction-event-remove', [
                'to_id'         =>  $request->params['userId'],
                'from_id'         =>  0,
                'from'          =>  '',
                'message'          =>  "Asked to leave alert is removed by the user.",
                'room'          =>  $data['conversationDetails']->id,
                'evict_type'          =>  '0',
                'conversation_id'          =>  $data['conversationDetails']->id,
            ], $new_registrationIds);
            $response = [
                'conversationDetails'     => $data['conversationDetails']
            ];
        }

        $response['result']['message']='Success';
        return response()->json($response, 200);
    }


    public function evictionUserLeave(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];


        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $client = new Client($this->sid, $this->token);

        // $participant = $client->video->rooms($request->params['roomId'])
        // ->participants($request->params['sid'])
        // ->update(array("status" => "disconnected"));
        $tot_person_talked=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id','!=',$request->params['userId'])->where('status','C')->count();
        User::where('id',auth()->user()->id)->update(['tot_person_talked'=>$tot_person_talked+@auth()->user()->tot_person_talked,'tot_conversation_completed'=>@auth()->user()->tot_conversation_completed+1]);
        ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->update(['is_evicted'=>'Y']);

        $response['result']['message']='Success';
        return response()->json($response, 200);
    }

    public function paramentLeave(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $client = new Client($this->sid, $this->token);
        $tot_person_talked=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id','!=',$request->params['userId'])->where('status','C')->count();
        User::where('id',auth()->user()->id)->update(['tot_person_talked'=>$tot_person_talked+@auth()->user()->tot_person_talked,'tot_conversation_completed'=>@auth()->user()->tot_conversation_completed+1]);

        ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->update(['is_leave_parament'=>'Y']);

        $tot_av_person=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('status','C')->where('is_leave_parament','N')->where('is_evicted','N')->count();
        if($tot_av_person==0){
            $data['conversationDetails']->update(['status'=>'CO']);
        }

        $response['result']['message']='Success';
        return response()->json($response, 200);
    }
    public function extendCall(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $client = new Client($this->sid, $this->token);
        // $response['error']['message']='Your YCoin balance is low';
        // return response()->json($response, 200);

        $ConversationData=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->first();
        $ConversationData->no_of_extend;
        $upd=[];
        $ins=[];
        if($ConversationData->no_of_extend>0){
            $ins['user_id']=auth()->user()->id;
            $ins['type']='OUT';
            $ins['category']='C';
            $ins['conversation_id']=$request->params['roomId'];
            $ins['conversation_id']=$request->params['roomId'];
            if(auth()->user()->conversation_balance_total<200){
                $response['error']['message']='Your YCoin balance is low';
                return response()->json($response, 200);

            }

            if(auth()->user()->conversation_balance_paid>=200){
                $paid_deduct_balance = 200;
                $free_deduct_balance = 0;
                $free_deduct_balance_user  = auth()->user()->conversation_balance_free-$free_deduct_balance;
                $paid_deduct_balance_user = auth()->user()->conversation_balance_paid-$paid_deduct_balance;

                $ins['coin_free']=$free_deduct_balance ;
                $ins['coin_paid']=$paid_deduct_balance;
                $ins['balance_coin_free']=$free_deduct_balance_user;
                $ins['balance_coin_paid']=$paid_deduct_balance_user;

                $upd['conversation_balance_free']=$free_deduct_balance_user;
                $upd['conversation_balance_paid']= $paid_deduct_balance_user;
                $upd['conversation_balance_total']=  $paid_deduct_balance_user+$free_deduct_balance_user;
            }else{
                $paid_deduct_balance = auth()->user()->conversation_balance_paid - 200;
                $free_deduct_balance = abs($paid_deduct_balance);

                $free_deduct_balance_user  = auth()->user()->conversation_balance_free-$free_deduct_balance;
                $paid_deduct_balance_user = auth()->user()->conversation_balance_paid-(200-abs($paid_deduct_balance));

                $ins['coin_free']=$free_deduct_balance ;
                $ins['coin_paid']=(200-abs($paid_deduct_balance));
                $ins['balance_coin_free']=$free_deduct_balance_user;
                $ins['balance_coin_paid']=$paid_deduct_balance_user;

                $upd['conversation_balance_free']=$free_deduct_balance_user;
                $upd['conversation_balance_paid']= $paid_deduct_balance_user;
                $upd['conversation_balance_total']=  $free_deduct_balance_user+$paid_deduct_balance_user;
            }
            YcoinHistory::create($ins);
            User::whereId(auth()->user()->id)->update($upd);
            ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->update(['no_of_extend'=>$ConversationData->no_of_extend+1]);
        }else{
            ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->update(['no_of_extend'=>$ConversationData->no_of_extend+1]);
        }
        $check=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id',$request->params['userId'])->first();
        $checkCount=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('no_of_extend',$check->no_of_extend)->count();
        if($checkCount>1){
            $data['conversationDetails']->update(['no_of_extend'=>$check->no_of_extend]);
        }
        $response['result']['message']='Success';
        return response()->json($response, 200);
    }

    public function callEndRefund(Request $request){

        $response = [
            'jsonrpc'   => '2.0'
        ];
        $data['conversationDetails'] =  Conversation::whereId(@$request->params['roomId'])->first();
        $to_id=ConversationsToUser::where('conversation_id',$request->params['roomId'])->where('user_id','!=',auth()->user()->id)->pluck('user_id');

        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);

        $new_registrationIds = User::whereIn('id',$to_id )->pluck('socket_id')->toArray();

        $response1 = $pusher->trigger('Ylanes', 'call-end-refund', [
            'to_id'         =>  $to_id,
            'from_id'         =>  0,
            'from'          =>  ' ',
            'message'          =>  "Asked to leave alert is removed by the user.",
            'room'          =>  $data['conversationDetails']->id,
            'conversation_id'          =>  $data['conversationDetails']->id,
        ], $new_registrationIds);

        @$totalUsers = ConversationsToUser::where('conversation_id',$data['conversationDetails']->id)->where('status','C')->get();
        if(!empty(@$totalUsers)){
            foreach ($totalUsers as $usersData) {
                $user_datas = User::whereId($usersData->user_id)->first();
                @$coinDetails = YcoinHistory::where('user_id',$user_datas->id)->where('conversation_id',@$data['conversationDetails']->id)->first();
                if(!empty(@$coinDetails)){
                    if(@$usersData->conv_balance_type == 'P')
                    {
                        $addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid) + $user_datas->conversation_balance_paid;
                        $user_datas->update(['conversation_balance_paid' => $addCoins]);
                        $totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                        User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
                        $paidBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid;
                        $freeBalance = '0';
                    }
                    else{
                        $addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid) + $user_datas->conversation_balance_free;
                        $user_datas->update(['conversation_balance_free' => $addCoins]);
                        $totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                        User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
                        $paidBalance = '0';
                        $freeBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid;
                    }
                    @$cUser = User::whereId(@$user_datas->id)->first();
                    YcoinHistory::create([
                        'user_id' => @$cUser->id,
                        'coin_free' => @$freeBalance,
                        'coin_paid' => @$paidBalance,
                        'balance_coin_free' => @$cUser->conversation_balance_free,
                        'balance_coin_paid' => @$cUser->conversation_balance_paid,
                        'type' => 'IN',
                        'category' => 'C',
                        'conversation_id' => @$data['conversationDetails']->id,
                    ]);
                }
            }
        }
        ConversationsToUser::where('conversation_id',@$data['conversationDetails']->id)->update(['status' => 'D']);
        @$data['conversationDetails']->update(['status'=>'C']);

        $response['result']['message']='Success';
        return response()->json($response, 200);
    }

}
