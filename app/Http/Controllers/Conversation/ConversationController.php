<?php

namespace App\Http\Controllers\Conversation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Topic;
use App\Models\Conversation;
use App\Models\ConversationReports;
use App\Models\ConversationsToUser;
use App\Models\User;
use App\Models\ConversationUserReview;
use App\Models\ConversationReviews;
use App\Models\YcoinHistory;
use Auth;
use DB;
class ConversationController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth',['except' => ['getSubCategory','getSubCategoryTopic']]);
		$this->middleware('autologout');
	}
	public function createConversationPage(){
		$data['category'] = Category::where('parent_id','0')->orderBy('name','asc')->where('status','A')->get();
		return view('modules.conversation.create_conversation',@$data);
	}
	public function getSubCategory($catgoryId=null){
		$subCategories = Category::where(['parent_id'=>@$catgoryId])->orderBy('name','asc')->whereIn('status',['A'])->select('id','parent_id','status','name')->get();
		$html = '';
		if(@$subCategories->count() > 0){
			$html = $html.'<option value="">Select Sub Category</option>';

			foreach($subCategories as $subCategory){
				$html = $html.'<option value="'.$subCategory->id.'">'.$subCategory->name.'</option>';
			}
			$response['result']['get_sub_categories'] = $html;
		}else{
			$response['result']['get_sub_categories'] = '<option value="">Select Sub Category</option>';
		}
		return $response;
	}
	public function getSubCategoryTopic($subcatIds=null){
		$subCategoryTopic = Topic::where(['sub_category_id'=>@$subcatIds])->orderBy('topic_line_1','asc')->whereIn('status',['A'])->get();
		$html = '';
		if(@$subCategoryTopic->count() > 0){
			$html = $html.'<option value="">Select Topic</option>';

			foreach($subCategoryTopic as $topic){
				$html = $html.'<option value="'.$topic->id.'">'.$topic->topic_line_1.'</option>';
			}
			$response['result']['get_topic'] = $html;
		}else{
			$response['result']['get_topic'] = '<option value="">Select Topic</option>';
		}
		return $response;
	}
	public function createConversationPost(Request $request){
		$request->validate([
			'category' =>['required','numeric'],
			'sub_category' =>['required','numeric'],
			'topic' =>['required','numeric'],
			'date' =>['required'],
			'time' =>['required'],
			'type' =>['required'],
			'is_anonymously' =>['required'],
			'conversation_text' =>['required','max:300'],
		],[
			'category.required'=>'Please select a category!',
			'sub_category.required'=>'Please select a sub category!',
			'topic.required'=>'Please select a topic!',
			'date.required'=>'Please select a date!',
			'time.required'=>'Please select a time!',
			'conversation_text.required'=>'Please enter conversation',
			'conversation_text.max'=>'Maximun enter 300 charcatres for conversation',
		]);
		if(date('Y-m-d', strtotime(@$request->date)) < date('Y-m-d')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		elseif(date('Y-m-d', strtotime(@$request->date)) == date('Y-m-d') && date('H:i A', strtotime(@$request->time)) < date('H:i A')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		if(@auth::user()->conversation_balance_total < 200){
			session()->flash("error",__('errors.-33127'));
			return redirect()->back();
		}
		elseif(empty(@auth::user()->conversation_balance_free) && @auth::user()->conversation_balance_free < 200){
			session()->flash("error",__('errors.-33127'));
			return redirect()->back();
		}
		$createCon['creator_user_id'] = @auth::user()->id;
		$createCon['category_id'] = @$request->category;
		$createCon['sub_category_id'] = @$request->sub_category;
		$createCon['topic_id'] = @$request->topic;
		$createCon['date'] = date('Y-m-d', strtotime(@$request->date));
		$createCon['time'] = date('H:i:s', strtotime(@$request->time));
		if(@$request->type == 'ON'){
			$createCon['type'] = 'ON';
			$createCon['no_of_participants'] = '1';
		}
		else if(@$request->type == 'OP'){
			$createCon['type'] = 'OP';
			$createCon['no_of_participants'] = '1';
		}
		else{
			$createCon['type'] = 'CO';
			$createCon['no_of_participants'] = '1';
		}
		$createCon['status'] = 'A';
		$createCon['created_by'] = 'U';
		$save = Conversation::create(@$createCon);
		if(!empty(@$save)){
			if(@auth::user()->conversation_balance_paid > 0){
				$is_paid = 'P';
				$paid_deduct_balance = @auth::user()->conversation_balance_paid - 200;
				if($paid_deduct_balance >= 0){
					User::whereId(@auth::user()->id)->update(['conversation_balance_paid' => @$paid_deduct_balance]);
				}
				else{
					$free_deduct_balance = @auth::user()->conversation_balance_free - abs($paid_deduct_balance);
					if($free_deduct_balance >= 0){
						User::whereId(@auth::user()->id)->update(['conversation_balance_paid'=>'0' ,'conversation_balance_free' => @$free_deduct_balance]);
					}
					else{
						Conversation::whereId(@$save->id)->delete();
						session()->flash("error",__('errors.-33127'));
						return redirect()->back();
					}
				}
			}
			else{
				$is_paid = 'F';
				$free_deduct_balance = @auth::user()->conversation_balance_free - 200;
				if($free_deduct_balance >= 0){
					User::whereId(@auth::user()->id)->update(['conversation_balance_free' => @$free_deduct_balance]);
				}
				else
				{
					Conversation::whereId(@$save->id)->delete();
					session()->flash("error",__('errors.-33127'));
					return redirect()->back();
				}
			}
			ConversationsToUser::create([
				'user_id' => @auth::user()->id,
				'conversation_id' => @$save->id,
				'is_creator' => 'Y',
				'is_anonymously' => @$request->is_anonymously,
				'display_name' => @$request->display_name,
				'conversation_text' => @$request->conversation_text,
				'status' => 'C',
				'conv_balance_type' => $is_paid,
			]);
			$totalBalance = User::whereId(@auth::user()->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
			User::whereId(@auth::user()->id)->update(['conversation_balance_total' => @$totalBalance]);
			if($is_paid == 'P'){
				$paidBalance = '200';
				$freeBalance = '0';
			}
			else{
				$paidBalance = '0';
				$freeBalance = '200';
			}
			@$cUser = User::whereId(@auth::user()->id)->first();
			YcoinHistory::create([
				'user_id' => @auth::user()->id,
				'coin_free' => @$freeBalance,
				'coin_paid' => @$paidBalance,
				'balance_coin_free' => @$cUser->conversation_balance_free,
				'balance_coin_paid' => @$cUser->conversation_balance_paid,
				'type' => 'OUT',
				'category' => 'C',
				'conversation_id' => @$save->id,
			]);
			session()->flash("success",__('success.-4141'));
		}
		else{
			session()->flash("error",__('errors.-5001'));
		}
		return redirect()->route('upcoming.conversation');
	}
	public function upcomingConversationPage(Request $request){
		$converStionIds = ConversationsToUser::where('user_id',@auth::user()->id)
		->whereIn('status',['C','U'])
		->pluck('conversation_id')->toArray();
		$data['allConversation'] = Conversation::whereIn('id',array_unique(@$converStionIds))
        ->with(['conversationstousers'=>function($q){
            $q->where('user_id',@auth::user()->id);
        }])
		->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i',strtotime(date('Y-m-d H:i').'-15 minutes')))
		->orderBy('date','desc');
		if($request->all()){
			if(!empty(@$request->type)){
				$data['allConversation'] = $data['allConversation']->where('type',@$request->type);
			}
		}
		$data['allConversation'] = $data['allConversation']->paginate(4);
		return view('modules.conversation.upcoming_conversation',@$data);
	}
	public function pastConversationPage(Request $request){
		$converStionIds = ConversationsToUser::where('user_id',@auth::user()->id)
		->whereIn('status',['C'])
		->pluck('conversation_id')->toArray();
		$data['allConversation'] = Conversation::whereIn('id',array_unique(@$converStionIds))
		->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'<',date('Y-m-d H:i'))
		->orderBy('date','desc');
		if($request->all()){
			if(!empty(@$request->type)){
				$data['allConversation'] = $data['allConversation']->where('type',@$request->type);
			}
			if(!empty(@$request->topic)){
				$data['allConversation'] = $data['allConversation']->where('topic_id',@$request->topic);
			}
			if(!empty(@$request->from_date1)){
				$data['allConversation'] =  $data['allConversation']
				->where(DB::raw("DATE(date)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
			}
			if(!empty(@$request->to_date1)){
				$data['allConversation'] =  $data['allConversation']->where(DB::raw("DATE(date)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
			}
		}
		$data['allConversation'] = $data['allConversation']->paginate(4);
		$converTopicids = Conversation::whereIn('id',array_unique(@$converStionIds))->where('date','<',date('Y-m-d'))->pluck('topic_id')->toArray();
		$data['topics'] = Topic::whereIn('status',['A','I'])->whereIn('id',@$converTopicids)->orderBy('topic_line_1','asc')->get();
		return view('modules.conversation.past_conversation',@$data);
	}
	public function viewConversationDetails($conversationIds){
		$data['converstion_details'] =  Conversation::whereId(@$conversationIds)->first();
		if(!empty($data['converstion_details']))
		{
			$data['converstion_join_users'] = ConversationsToUser::where('conversation_id',$data['converstion_details']->id)->where('status','!=','D')->get();
			if($data['converstion_details']->creator_user_id == @auth::user()->id){
				$data['is_creator'] = 'yes';
			}
			else{
				$data['is_creator'] = 'no';
			}
            $data['converstion_join_current_users'] = ConversationsToUser::where('conversation_id',$data['converstion_details']->id)->where('user_id',auth::user()->id)->where('status','!=','D')->first();
			return view('modules.conversation.conversations_details',@$data);
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}
	}
	public function approveRejectConversationUserRequest($conversationUsersIds,$conversationid,$type){
		$checkConverstion = Conversation::whereId(@$conversationid)->first();
		if(@$checkConverstion->creator_user_id == @auth::user()->id){
			$conusersdata = ConversationsToUser::whereId(@$conversationUsersIds)->first();
			$checkParti = ConversationsToUser::where('conversation_id',@$checkConverstion->id)->where('is_creator','!=','Y')->where('status','C')->count();
			if(!empty(@$conusersdata)){
				if(@$type == 'A'){
					$userData = User::whereId(@$conusersdata->user_id)->first();
					if($checkConverstion->type == 'ON'){
						@$checkConverstion->update(['is_full' => 'Y']);
						if($checkParti >= 1){
							session()->flash("success",__('success.-4148'));
							return redirect()->back();
						}
					}
					else{
						if($checkParti >= 4){
							@$checkConverstion->update(['is_full' => 'Y']);
							session()->flash("success",__('success.-4148'));
							return redirect()->back();
						}
					}
					$conusersdata->update(['status'=>'C']);
					@$checkConverstion->increment('no_of_participants');
					@$converstionCheck = Conversation::whereId(@$checkConverstion->id)->first();
					if (@$converstionCheck->is_full == 'Y') {
						$this->rejectUserRefundYcoin(@$converstionCheck);
					}
					session()->flash("success",__('success.-4147'));
				}
				elseif(@$type == 'R'){
					$conusersdata->update(['status'=>'R']);
					$user_datas = User::whereId($conusersdata->user_id)->first();
					@$coinDetails = YcoinHistory::where('user_id',$user_datas->id)->where('conversation_id',@$checkConverstion->id)->first();
					if(!empty(@$coinDetails)){
						if(@$conusersdata->conv_balance_type == 'P')
						{
							$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid) + $user_datas->conversation_balance_paid;
							$user_datas->update(['conversation_balance_paid' => $addCoins]);
							$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
							User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
							$paidBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid;
							$freeBalance = '0';
						}
						else{
							$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid) + $user_datas->conversation_balance_free;
							$user_datas->update(['conversation_balance_free' => $addCoins]);
							$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
							User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
							$paidBalance = '0';
							$freeBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid;
						}
						@$cUser = User::whereId(@$user_datas->id)->first();
						YcoinHistory::create([
							'user_id' => @$cUser->id,
							'coin_free' => @$freeBalance,
							'coin_paid' => @$paidBalance,
							'balance_coin_free' => @$cUser->conversation_balance_free,
							'balance_coin_paid' => @$cUser->conversation_balance_paid,
							'type' => 'IN',
							'category' => 'C',
							'conversation_id' => @$checkConverstion->id,
						]);
					}
					session()->flash("success",__('success.-4146'));
				}
				return redirect()->back();
			}
			else{
				session()->flash("error",__('errors.-5001'));
				return redirect()->back();
			}
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}
	}
	private function rejectUserRefundYcoin($converstion){
		$conusersdata = ConversationsToUser::where('conversation_id',@$converstion->id)->where('status','U')->get();
		foreach ($conusersdata as $c_user) {
			@$c_user->update(['status'=>'R']);
			$user_datas = User::whereId(@$c_user->user_id)->first();
			@$coinDetails = YcoinHistory::where('user_id',$user_datas->id)->where('conversation_id',@$converstion->id)->first();
			if(!empty(@$coinDetails)){
				if(@$c_user->conv_balance_type == 'P')
				{
					$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid) + $user_datas->conversation_balance_paid;
					$user_datas->update(['conversation_balance_paid' => $addCoins]);
					$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
					User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
					$paidBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid;
					$freeBalance = '0';
				}
				else{
					$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid) + $user_datas->conversation_balance_free;
					$user_datas->update(['conversation_balance_free' => $addCoins]);
					$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
					User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
					$paidBalance = '0';
					$freeBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid;
				}
				@$cUser = User::whereId(@$user_datas->id)->first();
				YcoinHistory::create([
					'user_id' => @$cUser->id,
					'coin_free' => @$freeBalance,
					'coin_paid' => @$paidBalance,
					'balance_coin_free' => @$cUser->conversation_balance_free,
					'balance_coin_paid' => @$cUser->conversation_balance_paid,
					'type' => 'IN',
					'category' => 'C',
					'conversation_id' => @$converstion->id,
				]);
			}
		}
	}
	public function cancelConversationByUser($converIds=null){
		if(!empty($converIds)){
			$checkConverstion = Conversation::whereId(@$converIds)->whereNotIn('status',['D','C'])->first();
            $currentTime=strtotime(date('Y-m-d H:i:s'));
            $orderTime=strtotime(date('Y-m-d H:i:s',strtotime($checkConverstion->date.$checkConverstion->time)));
            $orderTimeBefore=strtotime(date('Y-m-d H:i:s',strtotime($checkConverstion->date.$checkConverstion->time.'-15 minutes')));
            // return $orderTime-$currentTime;
            $charge=0;

            if($orderTimeBefore-$currentTime>0){
                $charge=0;
            }
            elseif($orderTime-$currentTime>0){
                $charge=50;
            }else{
                session()->flash("error",__('errors.-5001'));
                return redirect()->back();
            }

			if(!empty(@$checkConverstion)){

				if(@$checkConverstion->type == 'CO' || @$checkConverstion->type == 'ON'){
					if(@$checkConverstion->creator_user_id == @auth::user()->id)
					{
						@$totalUsers = ConversationsToUser::where('conversation_id',@$checkConverstion->id)->where('status','C')->get();
						if(!empty(@$totalUsers)){
							foreach ($totalUsers as $usersData) {
								$user_datas = User::whereId($usersData->user_id)->first();
								@$coinDetails = YcoinHistory::where('user_id',$user_datas->id)->where('conversation_id',@$checkConverstion->id)->first();
								if(!empty(@$coinDetails)){
									if(@$usersData->conv_balance_type == 'P')
									{
										$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid-$charge) + $user_datas->conversation_balance_paid;
										$user_datas->update(['conversation_balance_paid' => $addCoins]);
										$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
										User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
										$paidBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid-$charge;
										$freeBalance = '0';
									}
									else{
										$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid-$charge) + $user_datas->conversation_balance_free;
										$user_datas->update(['conversation_balance_free' => $addCoins]);
										$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
										User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
										$paidBalance = '0';
										$freeBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid-$charge;
									}
									@$cUser = User::whereId(@$user_datas->id)->first();
									YcoinHistory::create([
										'user_id' => @$cUser->id,
										'coin_free' => @$freeBalance,
										'coin_paid' => @$paidBalance,
										'balance_coin_free' => @$cUser->conversation_balance_free,
										'balance_coin_paid' => @$cUser->conversation_balance_paid,
										'type' => 'IN',
										'category' => 'C',
										'conversation_id' => @$checkConverstion->id,
									]);
								}
							}
						}
						ConversationsToUser::where('conversation_id',@$checkConverstion->id)->update(['status' => 'D']);
						@$checkConverstion->update(['status'=>'C']);
						session()->flash("success",__('success.-4151'));
						return redirect()->back();
					}
					else{
						$checkConUser = ConversationsToUser::where('conversation_id',@$checkConverstion->id)->where('user_id',@auth::user()->id)->whereNotIn('status',['D'])->first();
						if(!empty(@$checkConUser)){
							if(@$checkConUser->status == 'C'){
								$user_datas = User::whereId($checkConUser->user_id)->first();
								@$coinDetails = YcoinHistory::where('user_id',$user_datas->id)->where('conversation_id',@$checkConverstion->id)->first();
								if(!empty(@$coinDetails)){
									if(@$checkConUser->conv_balance_type == 'P')
									{
										$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid-$charge) + $user_datas->conversation_balance_paid;
										$user_datas->update(['conversation_balance_paid' => $addCoins]);
										$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
										User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
										$paidBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid-$charge;
										$freeBalance = '0';
									}
									else{
										$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid-$charge) + $user_datas->conversation_balance_free;
										$user_datas->update(['conversation_balance_free' => $addCoins]);
										$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
										User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
										$paidBalance = '0';
										$freeBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid-$charge;
									}
									@$cUser = User::whereId(@$user_datas->id)->first();
									YcoinHistory::create([
										'user_id' => @$cUser->id,
										'coin_free' => @$freeBalance,
										'coin_paid' => @$paidBalance,
										'balance_coin_free' => @$cUser->conversation_balance_free,
										'balance_coin_paid' => @$cUser->conversation_balance_paid,
										'type' => 'IN',
										'category' => 'C',
										'conversation_id' => @$checkConverstion->id,
									]);
								}
								@$checkConverstion->decrement('no_of_participants');
								@$checkConverstion->update(['is_full' => 'N']);
								@$checkConUser->update(['status'=>'D']);
								session()->flash("success",__('success.-4151'));
								return redirect()->back();
							}
							else{
								$checkConUser->update(['status'=>'D']);
								session()->flash("success",__('success.-4151'));
								return redirect()->back();
							}
						}
						else{
							session()->flash("error",__('errors.-5001'));
							return redirect()->back();
						}
					}
				}
				else{
					$checkConUser = ConversationsToUser::where('conversation_id',@$checkConverstion->id)->where('user_id',@auth::user()->id)->whereNotIn('status',['D'])->first();
					if(!empty(@$checkConUser)){
						if(@$checkConUser->status == 'C'){
							$user_datas = User::whereId($checkConUser->user_id)->first();
							@$coinDetails = YcoinHistory::where('user_id',$user_datas->id)->where('conversation_id',@$checkConverstion->id)->first();
							if(!empty(@$coinDetails)){
								if(@$checkConUser->conv_balance_type == 'P')
								{
									$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid-$charge) + $user_datas->conversation_balance_paid;
									$user_datas->update(['conversation_balance_paid' => $addCoins]);
									$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
									User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
									$paidBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid-$charge;
									$freeBalance = '0';
								}
								else{
									$addCoins = (@$coinDetails->coin_free + @$coinDetails->coin_paid-$charge) + $user_datas->conversation_balance_free;
									$user_datas->update(['conversation_balance_free' => $addCoins]);
									$totalBalance = User::whereId(@$user_datas->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
									User::whereId(@$user_datas->id)->update(['conversation_balance_total' => @$totalBalance]);
									$paidBalance = '0';
									$freeBalance = @$coinDetails->coin_free + @$coinDetails->coin_paid-$charge;
								}
								@$cUser = User::whereId(@$user_datas->id)->first();
								YcoinHistory::create([
									'user_id' => @$cUser->id,
									'coin_free' => @$freeBalance,
									'coin_paid' => @$paidBalance,
									'balance_coin_free' => @$cUser->conversation_balance_free,
									'balance_coin_paid' => @$cUser->conversation_balance_paid,
									'type' => 'IN',
									'category' => 'C',
									'conversation_id' => @$checkConverstion->id,
								]);
							}
							@$checkConverstion->decrement('no_of_participants');
							@$checkConverstion->update(['is_full' => 'N']);
							@$checkConUser->update(['status'=>'D']);
							session()->flash("success",__('success.-4151'));
							return redirect()->back();
						}
						else{
							$checkConUser->update(['status'=>'D']);
							session()->flash("success",__('success.-4151'));
							return redirect()->back();
						}
					}
					else{
						session()->flash("error",__('errors.-5001'));
						return redirect()->back();
					}
				}
			}
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}
	}
	public function viewFeedbackPage($conversids=null){
		if(!empty($conversids)){
			$data['conversation'] = Conversation::whereId(@$conversids)->whereNotIn('status',['D','C'])->first();
			if(!empty(@$data['conversation']))
			{
                $data['report']=ConversationReports::where('conversation_master_id',$data['conversation']->id)->where('reported_by_id',auth()->user()->id)->pluck('user_id')->toArray();
				$data['conversation_to_user'] = ConversationsToUser::where('conversation_id',@$data['conversation']->id)->where('user_id','!=',@auth::user()->id)->where('status','C')->get();
				$data['user_reviews'] = ConversationUserReview::where('conversation_id',@$data['conversation']->id)->where('from_user_id',@auth::user()->id)->pluck('to_user_id')->toArray();
				$data['my_reviews'] = ConversationUserReview::where('conversation_id',@$data['conversation']->id)->where('to_user_id',@auth::user()->id)->latest()->get();
                $data['converstion_join_current_users'] = ConversationsToUser::where('conversation_id',$data['conversation']->id)->where('user_id',auth::user()->id)->where('status','!=','D')->first();
				return view('modules.conversation.view_feedback',@$data);
			}
			else{
				session()->flash("error",__('errors.-5001'));
				return redirect()->back();
			}
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}

	}
	public function reviewConversationPost(Request $request){
        $value=$request->all();
        // return $value;
		$conversation = Conversation::whereId(@$request->conversation_id)->whereNotIn('status',['D','C'])->first();
		if(!empty(@$conversation))
		{
            $upd=[];
            foreach($request->to_user_id as $key=>$userList){
                // $upd[$key]=$value['review'.$userList];

                $conversation_to_user = ConversationsToUser::where('conversation_id',@$conversation->id)->where('user_id',@$userList)->where('status','C')->first();
                if(!empty($conversation_to_user))
                {
                    $existReview = ConversationUserReview::where('conversation_id',@$conversation->id)->where('from_user_id',@auth::user()->id)->where('to_user_id',@$conversation_to_user->user_id)->first();
                    if (!empty(@$existReview)) {
                        session()->flash("success",__('success.-4155'));
                        return redirect()->back();
                    }
                    else{
                        ConversationUserReview::create([
                            'conversation_id' => @$conversation->id,
                            'from_user_id' => @auth::user()->id,
                            'to_user_id' => @$conversation_to_user->user_id,
                            'hearts' => @$value['rew_rating'.$userList],
                            'reviews_text' => @$value['review'.$userList]
                        ]);
                        $userTotalReview = @$conversation_to_user->hearts_received + @$value['rew_rating'.$userList];
                        $conversation_to_user->update(['hearts_received' => $userTotalReview]);
                        $userData = User::whereId(@$conversation_to_user->user_id)->first();
                        $userTotalReviews = $userData->tot_hearts + @$value['rew_rating'.$userList];
                        $userData->update(['tot_hearts' => @$userTotalReviews]);

                    }
                }
                else{
                    session()->flash("error",__('errors.-5001'));
                    return redirect()->back();
                }

            }
            $CheckReviews = ConversationReviews::where('conversation_id',@$conversation->id)->where('user_id',@auth::user()->id)->first();
			if (!empty(@$CheckReviews)) {

			}
			else{
				ConversationReviews::create([
					'conversation_id' => @$conversation->id,
					'user_id' => @auth::user()->id,
					'rating' => @$request->conversation_rate,
				]);
				session()->flash("success",__('success.-4156'));
                return redirect()->back();
			}


            session()->flash("success",__('success.-4156'));
            return redirect()->back();
            // return $upd;
			// $conversation_to_user = ConversationsToUser::where('conversation_id',@$conversation->id)->where('user_id',@$request->to_user_id)->where('status','C')->first();
			// if(!empty($conversation_to_user))
			// {
			// 	$existReview = ConversationUserReview::where('conversation_id',@$conversation->id)->where('from_user_id',@auth::user()->id)->where('to_user_id',@$conversation_to_user->user_id)->first();
			// 	if (!empty(@$existReview)) {
			// 		session()->flash("success",__('success.-4155'));
			// 		return redirect()->back();
			// 	}
			// 	else{
			// 		ConversationUserReview::create([
			// 			'conversation_id' => @$conversation->id,
			// 			'from_user_id' => @auth::user()->id,
			// 			'to_user_id' => @$conversation_to_user->user_id,
			// 			'hearts' => @$request->rew_rating,
			// 			'reviews_text' => @$request->review
			// 		]);
			// 		$userTotalReview = @$conversation_to_user->hearts_received + @$request->rew_rating;
			// 		$conversation_to_user->update(['hearts_received' => $userTotalReview]);
			// 		$userData = User::whereId(@$conversation_to_user->user_id)->first();
			// 		$userTotalReviews = $userData->tot_hearts + @$request->rew_rating;
			// 		$userData->update(['tot_hearts' => @$userTotalReviews]);
			// 		session()->flash("success",__('success.-4156'));
			// 		return redirect()->back();
			// 	}
			// }
			// else{
			// 	session()->flash("error",__('errors.-5001'));
			// 	return redirect()->back();
			// }

		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}
	}
	public function reviewPostConversations($conversIds=null,$ratetype=null){
		$conversation = Conversation::whereId(@$conversIds)->whereNotIn('status',['D','C'])->first();
		if(!empty(@$conversation)){
			$CheckReviews = ConversationReviews::where('conversation_id',@$conversation->id)->where('user_id',@auth::user()->id)->first();
			if (!empty(@$CheckReviews)) {
				session()->flash("success",__('success.-4157'));
				return redirect()->back();
			}
			else{
				ConversationReviews::create([
					'conversation_id' => @$conversation->id,
					'user_id' => @auth::user()->id,
					'rating' => @$ratetype,
				]);
				session()->flash("success",__('success.-4158'));
				return redirect()->back();
			}
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}
	}
}
