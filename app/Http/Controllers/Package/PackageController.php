<?php

namespace App\Http\Controllers\Package;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use App\Models\User;
use App\Models\PackageMaster;
use App\Models\Payment;
use App\Models\UserPackagePurchase;
use App\Models\YcoinHistory;
use Illuminate\Support\Str;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('autologout');
    }
    public function packagePurchasePage(){
    	$data['packages'] = PackageMaster::orderBy('price','asc')->get();
    	return view('modules.dashboard.purchase_package',@$data);
    }
    public function packagePurchaseByUser($packageIds=null){
    	$packages = PackageMaster::whereId($packageIds)->first();
    	if(!empty($packages)){
    		$save = UserPackagePurchase::create([
    			'user_id' => @auth::user()->id,
    			'package_id' => @$packages->id,
    			'amount' => @$packages->price,
    			'conv_free' => @$packages->coin_free,
    			'conv_paid' => @$packages->coin_paid
    		]);
    		if(!empty($save)){
    			$update['conversation_balance_total'] = @auth::user()->conversation_balance_total + $packages->coin_total;
    			$update['conversation_balance_free'] = @auth::user()->conversation_balance_free + $save->conv_free;
    			$update['conversation_balance_paid'] = @auth::user()->conversation_balance_paid + $save->conv_paid;
    			@auth::user()->update($update);
                @$userData = User::whereId(@auth::user()->id)->first();
                YcoinHistory::create([
                    'user_id' => @$userData->id,
                    'coin_free' => @$save->conv_free,
                    'coin_paid' => @$save->conv_paid,
                    'balance_coin_free' => @$userData->conversation_balance_free,
                    'balance_coin_paid' => @$userData->conversation_balance_paid,
                    'type' => 'IN',
                    'category' => 'P'
                ]);
                if(!empty(Session::get('url_redirect_after_purchase_balance')))
                {
                    return redirect(Session::get('url_redirect_after_purchase_balance'));
                }
    			session()->flash('success',__('success.-4160'));
            	return redirect()->back();
    		}
    		else{
    			session()->flash('error',__('errors.-33108'));
            	return redirect()->back();
    		}
    	}
    	else{
    		session()->flash('error',__('errors.-33108'));
            return redirect()->back();
    	}
    }
    public function packagePurchaseListUser(Request $request){
    	$data['purchase_packages'] = UserPackagePurchase::where('user_id',@auth::user()->id)->with(['getPayment']);
    	if (!empty($request->all())) {
			if(!empty(@$request->from_date1)){
				$data['purchase_packages'] =  $data['purchase_packages']
				->where(DB::raw("DATE(created_at)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
			}
			if(!empty(@$request->to_date1)){
				$data['purchase_packages'] =  $data['purchase_packages']->where(DB::raw("DATE(created_at)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
			}
		}
    	$data['purchase_packages'] = $data['purchase_packages']->latest()->paginate(10);
    	return view('modules.dashboard.my_package',@$data);
    }
    public function YcoinHistoryList(Request $request){
        $data['ycoin_history'] = YcoinHistory::where('user_id',@auth::user()->id)->orderBy('id','desc');
        if (!empty($request->all())) {
            if(!empty(@$request->type)){
                $data['ycoin_history'] =  $data['ycoin_history']
                ->where('type',@$request->type);
            }
            if(!empty(@$request->from_date1)){
                $data['ycoin_history'] =  $data['ycoin_history']
                ->where(DB::raw("DATE(created_at)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
            }
            if(!empty(@$request->to_date1)){
                $data['ycoin_history'] =  $data['ycoin_history']->where(DB::raw("DATE(created_at)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
            }
        }
        $data['ycoin_history'] = $data['ycoin_history']->latest()->paginate(10);
        return view('modules.dashboard.ycoin_usage',@$data);
    }

    /*
    *Method: purchasePackageConfirmation
    *Description: To purchase Package Confirmation page
    *Author:Soumojit
    *Date: 26-MAY-2022
    */
    public function purchasePackageConfirmation($packageIds=null){
        $data['package'] = PackageMaster::whereId($packageIds)->first();
        $data['packages'] = PackageMaster::orderBy('price','asc')->get();
        return view('modules.payment.purchase_package')->with($data);

    }

    /*
    *Method: savePayment
    *Description: To create Order and return checkout Page
    *Author:Soumojit
    *Date: 26-MAY-2022
    */
    public function savePayment($packageIds=null)
    {
        $packages = PackageMaster::whereId($packageIds)->first();
    	if(!empty($packages)){
            $request_array = [
				'amount' => @$packages->price * 100,
				'currency' =>"INR"
			];
			$header = array(
				'Content-Type: application/json',
				'Authorization: Basic '. base64_encode(env('RAZORPAY_KEY').":".env('RAZORPAY_SECRET'))
			);
			$url = "https://api.razorpay.com/v1/orders";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
			$result = curl_exec($ch);
			curl_close($ch);
			$res = json_decode($result);
            if($res==null){
                session()->flash('error',__('errors.-33108'));
                return redirect()->back();
            }
            $create['amount'] = $packages->price;
            $create['user_id'] = auth()->user()->id;
            $create['packages_id'] = @$packages->id;
            $create['order_id'] = @$res->id;
        	$create['status']  = "IP";
        	$create['transaction_id'] = $this->transactionGenerate();
            $pay = Payment::create($create);
			return redirect()->route('pay.now',['orderID'=>$pay->transaction_id]);
        }
        else{
    		session()->flash('error',__('errors.-33108'));
            return redirect()->back();
    	}

    }
	/*
    *Method: paymentProcess
    *Description: To show the pay now page
    *Author:Soumojit
    *Date: 26-MAY-2022
    */
	public function paymentProcess($id)
	{
        $data['packages'] = PackageMaster::orderBy('price','asc')->get();
		$payment_details=Payment::where('transaction_id',$id)->first();
        $data['package'] = PackageMaster::whereId($payment_details->packages_id)->first();
        if($payment_details->status=='P'|| $payment_details->status=='F'){
            session()->flash('error',__('errors.-33130'));
            return redirect()->route('purchase.package');
        }
        $payment_details->update(['status'=>'I']);
		$user_details=User::where('id',auth()->user()->id)->first();
		$data['amount']=$payment_details->amount;
		$data['order_id']=$payment_details->order_id;
		$data['token_id']=@$payment_details->transaction_id;
		$data['pay_id']=@$id;
		$data['user_details']=$user_details;
		return view('modules.payment.checkout')->with($data);
	}
	/*
    *Method: paymentConfirmation
    *Description: To update payment data after payment confirmation from the payment gateway
    *Author:Soumojit
    *Date: 26-MAY-2022
    */
	public function paymentConfirmation(Request $request,$id){

	    if($request['razorpay_payment_id'] && $request['razorpay_order_id'] && $request['razorpay_signature'])
        {
            $header = array(
				'Content-Type: application/json',
				'Authorization: Basic '. base64_encode(env('RAZORPAY_KEY').":".env('RAZORPAY_SECRET'))
			);
            $payment_details=Payment::where('transaction_id',$id)->first();
            if($payment_details->status=='P'||$payment_details->status=='F'){
                session()->flash('error',__('errors.-33130'));
                return redirect()->route('purchase.package');
            }
            $request_array = [
				'amount' => @$payment_details->amount * 100,
				'currency' =>"INR"
			];
            $url = "https://api.razorpay.com/v1/payments/".$request['razorpay_payment_id']."/capture";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
			$result = curl_exec($ch);
			curl_close($ch);
			$url = "https://api.razorpay.com/v1/payments/".$request['razorpay_payment_id'];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			$res = json_decode($result);

            $generated_signature = hash_hmac('sha256', $payment_details->order_id."|".$request['razorpay_payment_id'], env('RAZORPAY_SECRET'));
            if($generated_signature==$request['razorpay_signature'] && $res->status=='captured'){
                $packages = PackageMaster::whereId($payment_details->packages_id)->first();
                $upd=[];
                $upd['status']  = 'P';
                $upd['transaction_key'] = $request['razorpay_payment_id'];
                $upd['payment_signature'] = $request['razorpay_signature'];
                $upd['response'] = $result;
                Payment::where('transaction_id',$id)->update($upd);
                $pay=Payment::where('id',$payment_details->id)->first();
                if($pay){
                    $save = UserPackagePurchase::create([
                        'user_id' => @auth::user()->id,
                        'package_id' => @$packages->id,
                        'amount' => @$packages->price,
                        'conv_free' => @$packages->coin_free,
                        'conv_paid' => @$packages->coin_paid,
                        'payment_master_id'=>$pay->id,
                    ]);
                    if(!empty($save)){
                        $update['conversation_balance_total'] = @auth::user()->conversation_balance_total + $packages->coin_total;
                        $update['conversation_balance_free'] = @auth::user()->conversation_balance_free + $save->conv_free;
                        $update['conversation_balance_paid'] = @auth::user()->conversation_balance_paid + $save->conv_paid;
                        @auth::user()->update($update);
                        @$userData = User::whereId(@auth::user()->id)->first();
                        YcoinHistory::create([
                            'user_id' => @$userData->id,
                            'coin_free' => @$save->conv_free,
                            'coin_paid' => @$save->conv_paid,
                            'balance_coin_free' => @$userData->conversation_balance_free,
                            'balance_coin_paid' => @$userData->conversation_balance_paid,
                            'type' => 'IN',
                            'category' => 'P'
                        ]);
                    }
                    if(!empty(Session::get('url_redirect_after_purchase_balance')))
                    {
                        return redirect(Session::get('url_redirect_after_purchase_balance'));
                    }
                    session()->flash('success',__('success.-4160'));
            	    return redirect()->route('user.package');
                }else{
                    session()->flash('error',__('errors.-33108'));
                    return redirect()->route('purchase.package');
                }
            }else{
                $upd=[];
                $upd['status']  = 'F';
                Payment::where('transaction_id',$id)->where('status','I')->update($upd);
                session()->flash('error',__('errors.-33108'));
                return redirect()->route('purchase.package');
            }
        } else {
            $upd=[];
            $upd['status']  = 'F';
            Payment::where('transaction_id',$id)->where('status','I')->update($upd);
            session()->flash('error',__('errors.-33108'));
            return redirect()->route('purchase.package');
        }
    }
    /*
    *Method: transactionGenerate
    *Description: To Generate transaction id
    *Author:Soumojit
    *Date: 26-MAY-2022
    */

    private function transactionGenerate(){
        $transaction = Str::random(10);
        $payment_details=Payment::where('transaction_id',$transaction)->first();
        if(@$payment_details){
            $this->transactionGenerate();
        }
        else{
            return $transaction;
        }
    }
}
