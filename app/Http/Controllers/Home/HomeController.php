<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Session;
use App\Models\Topic;
class HomeController extends Controller
{
    public function index($ref_id=null){
    	if(!empty($ref_id)){
    		$this->storeSession($ref_id);
    	}
        // $data['topics'] = Topic::where('status','A')->latest()->take(8)->get();
        $topicArr=[11,48,54,26,108,89,33,61];
        // $data['topics'] = Topic::where('status','A')->whereIn('id',[11,48,54,26,108,89,33,61])->get();
        $topics=[];
        for($i=0;$i<8;$i++){
            $topic=Topic::where('status','A')->where('id',$topicArr[$i])->first();
            array_push($topics,$topic);
        }
        $data['topics']=$topics;

        // $allUser= User::get();
        // foreach($allUser as $user){
        //     $upd=[];
        //     $code='US';
        //     $sum=str_pad(@$user->id, 7, '0', STR_PAD_LEFT);
        //     $upd['user_id']=$code.$sum;
        //     User::where('id',$user->id)->update($upd);
        // }
    	return view('welcome',@$data);
    }
    private function storeSession($ref_id){
    	$refUser = User::where('status','!=','D')->where('ref_code',@$ref_id)->first();
    	if(!empty($refUser)){
    		Session::put('ref_user_id', @$refUser->id);
    	}
    }
}
