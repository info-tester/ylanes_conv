<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Conversation;
use App\Models\Category;
use App\Models\Topic;
use App\Models\User;
use App\Models\MyConnect;
use DB;
use Carbon\Carbon;
use Auth;
use App\Models\ConversationsToUser;
use Session;
use App\Models\YcoinHistory;
use Pusher\Pusher;
use App\Models\ConversationRequestJoin;

class SearchController extends Controller
{
    public function searchPage(Request $request,$subCat=null){
    	$data['category_search'] = Category::where('slug',@$subCat)->where('parent_id',0)->first();
        $data['sub_category'] = Category::where('slug',@$subCat)->where('parent_id','!=','0')->first();
    	return view('modules.search.search',@$data);
    }
    public function searchResult(Request $request){
    	$data['conversations'] = Conversation::where('status','A')
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i',strtotime(date('Y-m-d H:i').'-30 minutes')))
        ->whereHas('topic',function($q){
            $q->where('status','A');
        })
        ->orderBy('date','asc')->groupBy('topic_id');
        $data['topicData'] =[];
    	if(!empty($request->all())){
            if(@$request->keywords){
                $topicIds = Topic::where(function($q) use($request){
                    $q->Where('topic_line_1','like','%'.@$request->keywords.'%')
                    ->OrWhere('topic_line_2','like','%'.@$request->keywords.'%');
                })->pluck('id')->toArray();
                if(!empty($topicIds)){
                    $data['conversations'] = $data['conversations']->whereIn('topic_id',@$topicIds);
                }
                else{
                    $parentCategory = Category::where(['parent_id' => 0,'status' => 'A'])
                    ->where(function($q) use($request){
                        $q->Where('name','like','%'.@$request->keywords.'%')
                        ->OrWhere('description','like','%'.@$request->keywords.'%');
                    })->pluck('id')->toArray();
                    if(!empty($parentCategory)){
                        $data['conversations'] = $data['conversations']->whereIn('category_id',@$parentCategory);
                    }
                    else{
                        $parentSubCategory = Category::where('status','A')->where('parent_id','!=',0)
                        ->where(function($q) use($request){
                            $q->Where('name','like','%'.@$request->keywords.'%')
                            ->OrWhere('description','like','%'.@$request->keywords.'%');
                        })->pluck('id')->toArray();
                        $data['conversations'] = $data['conversations']->whereIn('sub_category_id',@$parentSubCategory);
                    }
                }
            }
    		if(!empty(@$request->category))
    		{
    			$data['conversations'] = $data['conversations']->where('category_id',@$request->category);
    		}
            if(!empty(@$request->sub_category))
            {
                $data['conversations'] = $data['conversations']->where('sub_category_id',@$request->sub_category);
                $data['topicData'] = Topic::where('sub_category_id',@$request->sub_category)->where('status','A')->get();
            }
    		if(!empty(@$request->order_by))
    		{
    		    if(@$request->order_by == 'today'){
    		    	$data['conversations'] = $data['conversations']->where('date',date('Y-m-d'));
    		    }
    		    elseif(@$request->order_by == 'towmorrow'){
    		    	$data['conversations'] = $data['conversations']->where('date',date('Y-m-d', strtotime( date('Y-m-d') . " +1 days")));
    		    }
    		    elseif(@$request->order_by == 'next'){
    		    	$data['conversations'] = $data['conversations']->whereBetween('date',[date('Y-m-d'),date('Y-m-d', strtotime(date('Y-m-d') . " +7 days"))]);
    		    }
    		}
    	}
    	if(!empty(@$request->per_page)){
    		@$per_page = @$request->per_page;
    	}
    	else{
    		@$per_page = 9;
    	}
    	$data['conversations'] = $data['conversations']->paginate(@$per_page);
    	return view('modules.search.search-result',$data);
    }
    public function searchSingleTopic(Request $request,$topicIds=null,$contype=null){
        $data['conversations'] = Conversation::where('status','A')
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i'))
        ->where('topic_id',$topicIds)->orderBy('date','asc');
        if(!empty(@$request->all())){
            if(!empty(@$request->order_by))
            {
                if(@$request->order_by == 'today'){
                    $data['conversations'] = $data['conversations']->where('date',date('Y-m-d'));
                }
                elseif(@$request->order_by == 'towmorrow'){
                    $data['conversations'] = $data['conversations']->where('date',date('Y-m-d', strtotime( date('Y-m-d') . " +1 days")));
                }
                elseif(@$request->order_by == 'next'){
                    $data['conversations'] = $data['conversations']->whereBetween('date',[date('Y-m-d'),date('Y-m-d', strtotime(date('Y-m-d') . " +7 days"))]);
                }
            }
        }
        if(!empty(@$contype)){
            $data['conversations'] = $data['conversations']->where('type',@$contype);
        }
        $data['conversations'] = $data['conversations']->get();
        $data['topic'] = Topic::whereId(@$topicIds)->where('status','A')->first();
        if(!empty(@$data['topic'])){
            return view('modules.search.search-single-topic',$data);
        }
        else{
            abort(404);
        }
    }
    public function joiningConversation(Request $request){
        if(!empty(@auth::user())){
            if(empty(@auth::user()->conversation_balance_total) && @auth::user()->conversation_balance_total == 0){
                return 7;
            }
            $checkConver = Conversation::where('status','A')->where('date','>=',date('Y-m-d'))->where('id',@$request->conversation_id)->first();
            if(!empty($checkConver)){
                if (@$checkConver->is_full == 'N') {
                    if (@$checkConver->type == 'ON') {
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@auth::user()->id)->first();
                        if(empty(@$chechConverStionUsers)){
                            if(@auth::user()->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                $paid_deduct_balance = @auth::user()->conversation_balance_paid - 200;
                                if($paid_deduct_balance >= 0){
                                    User::whereId(@auth::user()->id)->update(['conversation_balance_paid' => @$paid_deduct_balance]);
                                }
                                else{
                                    $free_deduct_balance = @auth::user()->conversation_balance_free - abs($paid_deduct_balance);
                                    if($free_deduct_balance >= 0){
                                        User::whereId(@auth::user()->id)->update(['conversation_balance_paid'=>'0' ,'conversation_balance_free' => @$free_deduct_balance]);
                                    }
                                    else{
                                        return 7;
                                    }
                                }
                            }
                            else{
                                $is_paid = 'F';
                                $free_deduct_balance = @auth::user()->conversation_balance_free - 200;
                                if($free_deduct_balance >= 0){
                                    User::whereId(@auth::user()->id)->update(['conversation_balance_free' => @$free_deduct_balance]);
                                }
                                else{
                                    return 7;
                                }
                            }
                            ConversationsToUser::create([
                                'user_id' => @auth::user()->id,
                                'conversation_id' => @$checkConver->id,
                                'is_creator' => 'N',
                                'is_anonymously' => @$request->join_type,
                                'display_name' => @$request->display_name,
                                'conversation_text' => @$request->conversation_text,
                                'status' => 'U',
                                'conv_balance_type' => $is_paid,
                            ]);
                            // @$checkConver->increment('no_of_participants');
                            // @$checkConver->update(['is_full' => 'Y']);
                            $totalBalance = User::whereId(@auth::user()->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@auth::user()->id)->update(['conversation_balance_total' => @$totalBalance]);
                            if($is_paid == 'P'){
                                $paidBalance = '200';
                                $freeBalance = '0';
                            }
                            else{
                                $paidBalance = '0';
                                $freeBalance = '200';
                            }
                            @$cUser = User::whereId(@auth::user()->id)->first();
                            YcoinHistory::create([
                                'user_id' => @auth::user()->id,
                                'coin_free' => @$freeBalance,
                                'coin_paid' => @$paidBalance,
                                'balance_coin_free' => @$cUser->conversation_balance_free,
                                'balance_coin_paid' => @$cUser->conversation_balance_paid,
                                'type' => 'OUT',
                                'category' => 'C',
                                'conversation_id' => @$checkConver->id,
                            ]);
                            return 'yes';
                        }
                        else{
                            if(@$chechConverStionUsers->status == 'D'){
                                return 6;
                            }
                            else{
                                return 3;
                            }
                        }
                    }
                    elseif(@$checkConver->type == 'OP'){
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@auth::user()->id)->first();
                        if(empty(@$chechConverStionUsers)){
                            if(@auth::user()->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                $paid_deduct_balance = @auth::user()->conversation_balance_paid - 200;
                                if($paid_deduct_balance >= 0){
                                    User::whereId(@auth::user()->id)->update(['conversation_balance_paid' => @$paid_deduct_balance]);
                                }
                                else{
                                    $free_deduct_balance = @auth::user()->conversation_balance_free - abs($paid_deduct_balance);
                                    if($free_deduct_balance >= 0){
                                        User::whereId(@auth::user()->id)->update(['conversation_balance_paid'=>'0' ,'conversation_balance_free' => @$free_deduct_balance]);
                                    }
                                    else{
                                        return 7;
                                    }
                                }
                            }
                            else{
                                $is_paid = 'F';
                                $free_deduct_balance = @auth::user()->conversation_balance_free - 200;
                                if($free_deduct_balance >= 0){
                                    User::whereId(@auth::user()->id)->update(['conversation_balance_free' => @$free_deduct_balance]);
                                }
                                else{
                                    return 7;
                                }
                            }
                            $jonType='N';
                            $checkCountUser =ConversationsToUser::where('conversation_id',@$checkConver->id)->where('status','!=','D')->where('active_video_call','Y')->count();

                            if(strtotime(date('Y-m-d H:i:s'))>strtotime($checkConver->date.$checkConver->time) && $checkCountUser>0){
                                $jonType='R';
                                ConversationsToUser::create([
                                    'user_id' => @auth::user()->id,
                                    'conversation_id' => @$checkConver->id,
                                    'is_creator' => 'N',
                                    'is_anonymously' => @$request->join_type,
                                    'display_name' => @$request->display_name,
                                    'conversation_text' => @$request->conversation_text,
                                    'status' => 'U',
                                    'join_type' => 'R',
                                    'conv_balance_type' => $is_paid,
                                ]);
                                $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
                                    'cluster' => env('PUSHER_APP_CLUSTER')
                                ]);
                                $conversationJoinUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('status','!=','D')->where('active_video_call','Y')->get();
                                foreach ($conversationJoinUsers as $item){
                                    $new_registrationIds = User::where('id',$item->user_id )->pluck('socket_id')->toArray();
                                    $ins=[];
                                    $ins['from_user']=Auth::user()->id;
                                    $ins['to_user']=$item->user_id;
                                    $ins['conversation_id']=$checkConver->id;
                                    $ins['status']='S';
                                    ConversationRequestJoin::create($ins);
                                    $response1 = $pusher->trigger('Ylanes', 'receive-event', [
                                        'to_id'         =>  $item->user_id,
                                        'from_id'         =>  Auth::user()->id,
                                        'from'          =>  @$request->join_type=='Y'?@$request->display_name:Auth::user()->profile_name,
                                        'key1'          =>  "value1",
                                        'room'          =>  $checkConver->id,
                                        'conversation_id'          =>  $checkConver->id,
                                        'conversation_text'=>@$request->conversation_text
                                    ], $new_registrationIds);
                                }
                            }else{

                                ConversationsToUser::create([
                                    'user_id' => @auth::user()->id,
                                    'conversation_id' => @$checkConver->id,
                                    'is_creator' => 'N',
                                    'is_anonymously' => @$request->join_type,
                                    'display_name' => @$request->display_name,
                                    'conversation_text' => @$request->conversation_text,
                                    'status' => 'C',
                                    'conv_balance_type' => $is_paid,
                                ]);
                            }

                            // ConversationsToUser::create([
                            //     'user_id' => @auth::user()->id,
                            //     'conversation_id' => @$checkConver->id,
                            //     'is_creator' => 'N',
                            //     'is_anonymously' => @$request->join_type,
                            //     'display_name' => @$request->display_name,
                            //     'conversation_text' => @$request->conversation_text,
                            //     'status' => 'C',
                            //     'conv_balance_type' => $is_paid,
                            // ]);
                            @$checkConver->increment('no_of_participants');
                            if(@$checkConver->conversationstousers->count() > 4){
                                @$checkConver->update(['is_full' => 'Y']);
                            }
                            $totalBalance = User::whereId(@auth::user()->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@auth::user()->id)->update(['conversation_balance_total' => @$totalBalance]);
                            if($is_paid == 'P'){
                                $paidBalance = '200';
                                $freeBalance = '0';
                            }
                            else{
                                $paidBalance = '0';
                                $freeBalance = '200';
                            }
                            @$cUser = User::whereId(@auth::user()->id)->first();
                            YcoinHistory::create([
                                'user_id' => @auth::user()->id,
                                'coin_free' => @$freeBalance,
                                'coin_paid' => @$paidBalance,
                                'balance_coin_free' => @$cUser->conversation_balance_free,
                                'balance_coin_paid' => @$cUser->conversation_balance_paid,
                                'type' => 'OUT',
                                'category' => 'C',
                                'conversation_id' => @$checkConver->id,
                            ]);
                            if($jonType=='R'){
                                return 'request-send';
                            }else{

                                return 'yes';
                            }
                        }
                        else{
                            if(@$chechConverStionUsers->status == 'D'){
                                return 6;
                            }
                            else{
                                return 3;
                            }
                        }
                    }
                    elseif(@$checkConver->type == 'CO'){
                        $chechConverStionUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('user_id',@auth::user()->id)->first();
                        if(empty(@$chechConverStionUsers)){
                            if(@auth::user()->conversation_balance_paid > 0){
                                $is_paid = 'P';
                                $paid_deduct_balance = @auth::user()->conversation_balance_paid - 200;
                                if($paid_deduct_balance >= 0){
                                    User::whereId(@auth::user()->id)->update(['conversation_balance_paid' => @$paid_deduct_balance]);
                                }
                                else{
                                    $free_deduct_balance = @auth::user()->conversation_balance_free - abs($paid_deduct_balance);
                                    if($free_deduct_balance >= 0){
                                        User::whereId(@auth::user()->id)->update(['conversation_balance_paid'=>'0' ,'conversation_balance_free' => @$free_deduct_balance]);
                                    }
                                    else{
                                        return 7;
                                    }
                                }
                            }
                            else{
                                $is_paid = 'F';
                                $free_deduct_balance = @auth::user()->conversation_balance_free - 200;
                                if($free_deduct_balance >= 0){
                                    User::whereId(@auth::user()->id)->update(['conversation_balance_free' => @$free_deduct_balance]);
                                }
                                else{
                                    return 7;
                                }
                            }
                            $checkCountUser =ConversationsToUser::where('conversation_id',@$checkConver->id)->where('status','!=','D')->where('active_video_call','Y')->count();
                            if(strtotime(date('Y-m-d H:i:s'))>strtotime($checkConver->date.$checkConver->time)&& $checkCountUser>0){
                                $jonType='R';
                                ConversationsToUser::create([
                                    'user_id' => @auth::user()->id,
                                    'conversation_id' => @$checkConver->id,
                                    'is_creator' => 'N',
                                    'is_anonymously' => @$request->join_type,
                                    'display_name' => @$request->display_name,
                                    'conversation_text' => @$request->conversation_text,
                                    'status' => 'U',
                                    'join_type' => 'R',
                                    'conv_balance_type' => $is_paid,
                                ]);
                                $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
                                    'cluster' => env('PUSHER_APP_CLUSTER')
                                ]);
                                $conversationJoinUsers = ConversationsToUser::where('conversation_id',@$checkConver->id)->where('status','!=','D')->where('active_video_call','Y')->get();
                                foreach ($conversationJoinUsers as $item){
                                    $new_registrationIds = User::where('id',$item->user_id )->pluck('socket_id')->toArray();
                                    $ins=[];
                                    $ins['from_user']=Auth::user()->id;
                                    $ins['to_user']=$item->user_id;
                                    $ins['conversation_id']=$checkConver->id;
                                    $ins['status']='S';
                                    ConversationRequestJoin::create($ins);
                                    $response1 = $pusher->trigger('Ylanes', 'receive-event', [
                                        'to_id'         =>  $item->user_id,
                                        'from_id'         =>  Auth::user()->id,
                                        'from'          =>  @$request->join_type=='Y'?@$request->display_name:Auth::user()->profile_name,
                                        'key1'          =>  "value1",
                                        'room'          =>  $checkConver->id,
                                        'conversation_id'          =>  $checkConver->id,
                                        'conversation_text'=>@$request->conversation_text
                                    ], $new_registrationIds);
                                }
                            }else{

                                ConversationsToUser::create([
                                    'user_id' => @auth::user()->id,
                                    'conversation_id' => @$checkConver->id,
                                    'is_creator' => 'N',
                                    'is_anonymously' => @$request->join_type,
                                    'display_name' => @$request->display_name,
                                    'conversation_text' => @$request->conversation_text,
                                    'status' => 'U',
                                    'conv_balance_type' => @$is_paid,
                                ]);
                            }

                            // ConversationsToUser::create([
                            //     'user_id' => @auth::user()->id,
                            //     'conversation_id' => @$checkConver->id,
                            //     'is_creator' => 'N',
                            //     'is_anonymously' => @$request->join_type,
                            //     'display_name' => @$request->display_name,
                            //     'conversation_text' => @$request->conversation_text,
                            //     'status' => 'U',
                            //     'conv_balance_type' => @$is_paid,
                            // ]);
                            // @$checkConver->increment('no_of_participants');
                            $totalBalance = User::whereId(@auth::user()->id)->sum(\DB::raw('conversation_balance_paid + conversation_balance_free'));
                            User::whereId(@auth::user()->id)->update(['conversation_balance_total' => @$totalBalance]);
                            if($is_paid == 'P'){
                                $paidBalance = '200';
                                $freeBalance = '0';
                            }
                            else{
                                $paidBalance = '0';
                                $freeBalance = '200';
                            }
                            @$cUser = User::whereId(@auth::user()->id)->first();
                            YcoinHistory::create([
                                'user_id' => @auth::user()->id,
                                'coin_free' => @$freeBalance,
                                'coin_paid' => @$paidBalance,
                                'balance_coin_free' => @$cUser->conversation_balance_free,
                                'balance_coin_paid' => @$cUser->conversation_balance_paid,
                                'type' => 'OUT',
                                'category' => 'C',
                                'conversation_id' => @$checkConver->id,
                            ]);
                            return 'yes';
                        }
                        else{
                            if(@$chechConverStionUsers->status == 'U'){
                                return 4;
                            }
                            else if(@$chechConverStionUsers->status == 'C'){
                                return 3;
                            }
                            else if(@$chechConverStionUsers->status == 'R'){
                                return 5;
                            }
                            else{
                                return 6;
                            }
                        }
                    }
                }
                else{
                    return 2;
                }
            }
            else{
                return 0;
            }
        }
        else{
            Session::put('is_anonymously',@$request->join_type);
            Session::put('joining_conversation_id',@$request->conversation_id);
            Session::put('joining_display_name',@$request->display_name);
            Session::put('joining_conversation_text',@$request->conversation_text);
            return 1;
        }
    }
    public function autoComplete(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        $response['result']['sub_categories'] = [];
        $keyword = $request->params['keyword'];
        if(@$keyword) {
            $parentCategory = Category::with(['subCategories'])->where(['parent_id' => 0,'status' => 'A']);
            $parentCategory = $parentCategory->where(function($q) use($keyword){
                    $q->Where('name','like','%'.@$keyword.'%')
                    ->OrWhere('description','like','%'.@$keyword.'%');
                })->take(4)->get();
            if($parentCategory->count() == 0) {
                $subCategory = Category::with(['immediateParent'])->where('parent_id',"!=",0)->where(['status' => 'A']);
                $subCategory = $subCategory->where(function($q) use($keyword){
                    $q->Where('name','like','%'.@$keyword.'%');
                })->get();
                $response['result']['sub_categories'] = @$subCategory;
            }

            $response['result']['topic'] = Topic::with(['subcategory'])->where(function($q) use($keyword){
                $q->Where('topic_line_1','like','%'.@$keyword.'%')
                ->OrWhere('topic_line_2','like','%'.@$keyword.'%');
            })->take(4)->orderBy('id', 'DESC')->get();

            $response['result']['parent_categories'] = $parentCategory;
        } else {
            $response['error']['message'] = 'ERROR';
        }
        return response()->json($response);
    }
    public function landingPage(Request $request){
        $data['conversations_open'] = Conversation::where('status','A')
        ->where('type','OP')
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i',strtotime(date('Y-m-d H:i').'-15 minutes')))
        ->whereHas('topic',function($q){
            $q->where('status','A');
        })
        ->orderBy('date','asc')->groupBy('topic_id');
        if(@$request->all()){
            if(!empty(@$request->order_by))
            {
                if(@$request->order_by == 'today'){
                    $data['conversations_open'] = $data['conversations_open']->where('date',date('Y-m-d'));
                }
                elseif(@$request->order_by == 'towmorrow'){
                    $data['conversations_open'] = $data['conversations_open']->where('date',date('Y-m-d', strtotime( date('Y-m-d') . " +1 days")));
                }
                elseif(@$request->order_by == 'next'){
                    $data['conversations_open'] = $data['conversations_open']->whereBetween('date',[date('Y-m-d'),date('Y-m-d', strtotime(date('Y-m-d') . " +7 days"))]);
                }
            }
        }
        $data['conversations_open'] = $data['conversations_open']->take(15)->get();
        $data['conversations_one'] = Conversation::where('status','A')
        ->where('type','ON')
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i',strtotime(date('Y-m-d H:i').'-15 minutes')))
        ->whereHas('topic',function($q){
            $q->where('status','A');
        })
        ->orderBy('date','asc')->groupBy('topic_id');
        if(@$request->all()){
            if(!empty(@$request->order_by))
            {
                if(@$request->order_by == 'today'){
                    $data['conversations_one'] = $data['conversations_one']->where('date',date('Y-m-d'));
                }
                elseif(@$request->order_by == 'towmorrow'){
                    $data['conversations_one'] = $data['conversations_one']->where('date',date('Y-m-d', strtotime( date('Y-m-d') . " +1 days")));
                }
                elseif(@$request->order_by == 'next'){
                    $data['conversations_one'] = $data['conversations_one']->whereBetween('date',[date('Y-m-d'),date('Y-m-d', strtotime(date('Y-m-d') . " +7 days"))]);
                }
            }
        }
        $data['conversations_one'] = $data['conversations_one']->take(15)->get();
        $data['conversations_cond'] = Conversation::where('status','A')
        ->where('type','CO')
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i',strtotime(date('Y-m-d H:i').'-15 minutes')))
        ->whereHas('topic',function($q){
            $q->where('status','A');
        })
        ->orderBy('date','asc')->groupBy('topic_id');
        if(@$request->all()){
            if(!empty(@$request->order_by))
            {
                if(@$request->order_by == 'today'){
                    $data['conversations_cond'] = $data['conversations_cond']->where('date',date('Y-m-d'));
                }
                elseif(@$request->order_by == 'towmorrow'){
                    $data['conversations_cond'] = $data['conversations_cond']->where('date',date('Y-m-d', strtotime( date('Y-m-d') . " +1 days")));
                }
                elseif(@$request->order_by == 'next'){
                    $data['conversations_cond'] = $data['conversations_cond']->whereBetween('date',[date('Y-m-d'),date('Y-m-d', strtotime(date('Y-m-d') . " +7 days"))]);
                }
            }
        }
        $data['conversations_cond'] = $data['conversations_cond']->take(15)->get();
        return view('modules.search.conversation_landing_page',$data);
    }
    public function conversationDetailsPage($conversationIds=null){
        if(!empty(@$conversationIds)){
            if(!empty(Session::get('url_redirect_after_purchase_balance')))
            {
                Session::forget('url_redirect_after_purchase_balance');
            }
            $data['converstion_details'] =  Conversation::whereId(@$conversationIds)->first();
            $data['converstion_join_users'] = ConversationsToUser::where('conversation_id',$data['converstion_details']->id)->where('status','C')->get();
            $data['conversations_open'] = Conversation::where('status','A')
            ->where('type','OP')
            ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i'))
            ->whereHas('topic',function($q){
                $q->where('status','A');
            })
            ->orderBy('date','asc')
            ->where('id','!=',@$data['converstion_details']->id)
            ->where('topic_id',@$data['converstion_details']->topic_id)
            ->take(15)->get();
                return view('modules.search.conversation_detail',@$data);
        }
        else{
            return redirect()->route('welcome');
        }
    }
    public function publicProfile($hostId=null){
        $data['host_data'] = User::whereIn('status',['A','U'])
        ->where('id',@$hostId)
        ->first();
        if(empty($data['host_data'])){
            abort('404');
        }
        return view('modules.search.public_profile',@$data);
    }
    public function requestSendUsers(Request $request){
        $dataConIds = ConversationsToUser::where('user_id',@auth::user()->id)->where('status','C')->pluck('conversation_id')->toArray();
        $dataToUserConId = ConversationsToUser::where('user_id',@$request->to_user_id)->whereIn('conversation_id',$dataConIds)->pluck('conversation_id')->toArray();
        $topicIds = Conversation::whereIn('id',$dataToUserConId)->where('status','A')->latest()->first();
        $checkData1 = MyConnect::where('from_user_id',@auth::user()->id)->where('to_user_id',@$request->to_user_id)->first();
        if(empty(@$checkData1)){
            $checkData2 = MyConnect::where('from_user_id',@$request->to_user_id)->where('to_user_id',@auth::user()->id)->first();
            if(empty($checkData2)){
                $create['from_user_id'] = @auth::user()->id;
                $create['to_user_id'] = @$request->to_user_id;
                if(!empty(@$topicIds)){
                    $create['topic_id'] = @$topicIds->topic_id;
                }
                $create['status'] = 'N';
                MyConnect::create(@$create);
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function setSessions($convIds){
        Session::put('url_redirect_after_purchase_balance',url('conversation-'.@$convIds));
        return 1;
    }
    public function seeAllTopicPage(Request $request,$convType=null){
        $data['conversations'] = Conversation::where('status','A')
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i',strtotime(date('Y-m-d H:i').'-30 minutes')))
        ->whereHas('topic',function($q){
            $q->where('status','A');
        })
        ->orderBy('date','asc');
        if(!empty(@$convType)){
            $data['conversations'] = $data['conversations']->where('type',@$convType);
            $data['con_type'] = @$convType;
        }
        if(!empty($request->all())){
            if(!empty(@$request->order_by))
            {
                if(@$request->order_by == 'today'){
                    $data['conversations'] = $data['conversations']->where('date',date('Y-m-d'));
                }
                elseif(@$request->order_by == 'towmorrow'){
                    $data['conversations'] = $data['conversations']->where('date',date('Y-m-d', strtotime( date('Y-m-d') . " +1 days")));
                }
                elseif(@$request->order_by == 'next'){
                    $data['conversations'] = $data['conversations']->whereBetween('date',[date('Y-m-d'),date('Y-m-d', strtotime(date('Y-m-d') . " +7 days"))]);
                }
            }
        }
        if(!empty(@$request->per_page)){
            @$per_page = @$request->per_page;
        }
        else{
            @$per_page = 9;
        }
        $data['conversations'] = $data['conversations']->paginate(@$per_page);
        return view('modules.search.see-all-topic',$data);
    }
}
