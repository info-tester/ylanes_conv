<?php

namespace App\Http\Controllers\Admin\Auth;

use Mail;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Mail\ResetPasswordAdminMail;
use Str;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your admins. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest:admin');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('admin.auth.passwords.email');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }
    public function sendResetLinkEmail(Request $request)
    {
        $request->validate([
                'email' => 'required|email'
            ]);

            $user = Admin::where('email', $request->email)->first();
            //dd($user);
            if($user)
            {
                $token = Str::random(60);
                $user->update(['remember_token' => $token]);

                $data['token'] = $token;
                $data['email'] = $request->email;
                $data['name'] = $user->name;
                Mail::to($request->email)->send(new ResetPasswordAdminMail($data));
                session()->flash("success",__('success.-4109'));
                return redirect()->route('admin.login');
            }
            session()->flash("error",__('errors.-33108'));
            return redirect()->route('admin.login');
    }
}
