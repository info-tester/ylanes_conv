<?php

namespace App\Http\Controllers\Admin\PackagePurchase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use App\Models\User;
use App\Models\PackageMaster;
use App\Models\UserPackagePurchase;
class PackagePurchaseController extends Controller
{
    public function __construct()
	{
		$this->middleware('admin.auth:admin');
	}


	public function index(Request $request){
		$data['purchase_packages'] = UserPackagePurchase::with(['getPayment','getUser']);
    	if (!empty($request->all())) {
            if(@$request->keyword){
                $data['purchase_packages'] =  $data['purchase_packages']
				->where(function($q1)use ($request){
                    $q1->whereHas('getUser',function($q2)use ($request){
                        $q2->where('profile_name','like','%'.@$request->keyword.'%')
                        ->orWhere('phone','like','%'.@$request->keyword.'%')
                        ->orWhere('email','like','%'.@$request->keyword.'%');
                    });
                });
            }
			if(!empty(@$request->from_date1)){
				$data['purchase_packages'] =  $data['purchase_packages']
				->where(DB::raw("DATE(created_at)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
			}
			if(!empty(@$request->to_date1)){
				$data['purchase_packages'] =  $data['purchase_packages']->where(DB::raw("DATE(created_at)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
			}
		}
    	$data['purchase_packages'] = $data['purchase_packages']->latest()->get();
    	$data['key'] = @$request->all();
		return view('admin.modules.package_purchase.package_purchase',@$data);
	}


}
