<?php

namespace App\Http\Controllers\Admin\MyProfile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;


class MyProfileController extends Controller
{
    public function __construct()
	{
		$this->middleware('admin.auth:admin');
		
	}
	public function index(Request $request){
		if(@$request->all()){
			$admin = Admin::where('id',Auth::guard('admin')->user()->id)->first();
			if(@$admin){


			$update['name'] = @$request->name;
			$update['email'] = @$request->email;
			$update['phone'] = @$request->phone;
			if(@$request->profile_pic){
                $pic   = $request->profile_pic;
                $name  = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/sub_admin_pics/', $name);
                $update['profile_pic'] = $name;
            }
            if(@$request->nw_password){
            	$password = @$request->nw_password;
            	$update['password'] = \Hash::make($password);	
            }
            

			Admin::where('id',Auth::guard('admin')->user()->id)->update($update);
			session()->flash("success",__('success.-4059'));
			
			}else{
				session()->flash("error",__('errors.-5057'));
			}
			return redirect()->back();
		}else{
			return view('admin.modules.my_profile.my_profile');	
		}
		
	}
	public function checkAdminDuplicateEmail(Request $request){
		if(@$request->all()){
			$admin = $this->admins->where('email','!=',@$request->old_email);
			$admin = $admin->where('email',@$request->email)->first();
			if(@$admin){
				return 0;
			}else{
				return 1;
			}
		}else{
			return 0;
		}
		
	}
	public function checkAdminDuplicatePhone(Request $request){
		if(@$request->all()){
			
			$admin = $this->admins->where('phone','!=',@$request->old_phone);
			$admin = $admin->where('phone',@$request->phone)->first();
			if(@$admin){
				return 0;
			}else{
				return 1;
			}
		}else{
			return 0;
		}
		
	}
}
