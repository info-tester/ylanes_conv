<?php

namespace App\Http\Controllers\Admin\Conversation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Topic;
use App\Models\Conversation;
use App\Models\ConversationsToUser;
use Auth;
use DB;
class ConversationController extends Controller
{
	public function manageConversation(Request $request){
		$data['topics'] = Topic::whereIn('status',['A','I'])->orderBy('topic_line_1','asc')->get();
		$data['category'] = Category::where('parent_id','0')->orderBy('name','asc')->where('status','A')->get();
		$data['conversations'] = Conversation::whereIn('status',['A','I','C','CO'])->orderBy('date','desc');
		if (!empty(@$request->all())) {
			if(!empty(@$request->category)){
				$data['conversations'] = $data['conversations']->where('category_id',@$request->category);
			}
			if(!empty(@$request->sub_category)){
				$data['conversations'] = $data['conversations']->where('sub_category_id',@$request->sub_category);
			}
			if(!empty(@$request->topic)){
				$data['conversations'] = $data['conversations']->where('topic_id',@$request->topic);
			}
			if(!empty(@$request->type)){
				$data['conversations'] = $data['conversations']->where('type',@$request->type);
			}
			if(!empty(@$request->from_date1)){
				$data['conversations'] =  $data['conversations']
				->where(DB::raw("DATE(date)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
			}
			if(!empty(@$request->to_date1)){
				$data['conversations'] =  $data['conversations']->where(DB::raw("DATE(date)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
			}
			if(!empty(@$request->created_by)){
				$data['conversations'] = $data['conversations']->where('created_by',@$request->created_by);
			}
			if(!empty(@$request->status)){
				$data['conversations'] = $data['conversations']->where('status',@$request->status);
			}
		}
		$data['conversations'] = $data['conversations']->get();
		return view('admin.modules.conversation.manage_conversation',@$data);
	}
	public function createConversationPage(){
		$data['category'] = Category::where('parent_id','0')->orderBy('name','asc')->where('status','A')->get();
		return view('admin.modules.conversation.add_conversation',@$data);
	}
	public function getSubCategory($catgoryId=null,$sub_category=null){
		$subCategories = Category::where(['parent_id'=>@$catgoryId])->orderBy('name','asc')->whereIn('status',['A'])->select('id','parent_id','status','name')->get();
		$html = '';
		if(@$subCategories->count() > 0){
			$html = $html.'<option value="">Select Sub Category</option>';

			foreach($subCategories as $subCategory){
                if($sub_category==$subCategory->id){
                    $html = $html.'<option value="'.$subCategory->id.'" selected >'.$subCategory->name.'</option>';

                }else{

                    $html = $html.'<option value="'.$subCategory->id.'">'.$subCategory->name.'</option>';
                }
			}
			$response['result']['get_sub_categories'] = $html;
		}else{
			$response['result']['get_sub_categories'] = '<option value="">Select Sub Category</option>';
		}
		return $response;
	}
	public function getSubCategoryTopic($subcatIds=null){
		$subCategoryTopic = Topic::where(['category_id'=>@$subcatIds])->orderBy('topic_line_1','asc')->whereIn('status',['A'])->get();
		$html = '';
		if(@$subCategoryTopic->count() > 0){
			$html = $html.'<option value="">Select Topic</option>';

			foreach($subCategoryTopic as $topic){
				$html = $html.'<option value="'.$topic->id.'">'.$topic->topic_line_1.'</option>';
			}
			$response['result']['get_topic'] = $html;
		}else{
			$response['result']['get_topic'] = '<option value="">Select Topic</option>';
		}
		return $response;
	}
	public function createConversationPost(Request $request){
		$request->validate([
			'category' =>['required','numeric'],
			'sub_category' =>['required','numeric'],
			'topic' =>['required','numeric'],
			'date' =>['required'],
			'time' =>['required']
		],[
			'category.required'=>'Please select a category!',
			'sub_category.required'=>'Please select a sub category!',
			'topic.required'=>'Please select a topic!',
			'date.required'=>'Please select a date!',
			'time.required'=>'Please select a time!'
		]);
		if(date('Y-m-d', strtotime(@$request->date)) < date('Y-m-d')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		elseif(date('Y-m-d', strtotime(@$request->date)) == date('Y-m-d') && date('H:i A', strtotime(@$request->time)) < date('H:i A')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		$createCon['creator_user_id'] = '0';
		$createCon['category_id'] = @$request->category;
		$createCon['sub_category_id'] = @$request->sub_category;
		$createCon['topic_id'] = @$request->topic;
		$createCon['date'] = date('Y-m-d', strtotime(@$request->date));
		$createCon['time'] = date('H:i:s', strtotime(@$request->time));
		$createCon['type'] = 'OP';
		$createCon['no_of_participants'] = '0';
		$createCon['status'] = 'A';
		$createCon['created_by'] = 'A';
		$save = Conversation::create(@$createCon);
		if(!empty(@$save)){
			session()->flash("success",__('success.-4141'));
		}
		else{
			session()->flash("error",__('errors.-5001'));
		}
		return redirect()->back();
	}
	public function ConversationStatusChange($id){
		$conversation = Conversation::where('id',@$id)->first();
		if(@$conversation){
			if(@$conversation->status == 'A'){
				$update['status'] = 'I';

			}else if(@$conversation->status == 'I'){
				$update['status'] = 'A';

			}
			$conversation->update($update);
			session()->flash("success",__('success.-4142'));
			return redirect()->back();

		}else{
			session()->flash("success",__('success.-4142'));
			return redirect()->back();
		}
	}
	public function viewConversationPage($id){
		$data['conversation'] = Conversation::where('id',@$id)->first();
		if(!empty($data['conversation'])){
			return view('admin.modules.conversation.view_conversation',@$data);
		}else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.conversation');
		}
	}
	public function editConversationPage($conIds){
		$data['category'] = Category::where('parent_id','0')->where('status','A')->get();
		$data['conversation'] = Conversation::whereId(@$conIds)->first();
		if(!empty($data['conversation'])){
			if($data['conversation']->conversationstousers->isEmpty()){
				return view('admin.modules.conversation.edit_conversation',@$data);
			}
			else{
				session()->flash("error",__('errors.-5001'));
				return redirect()->route('admin.manage.conversation');
			}
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.conversation');
		}
	}
	public function updateConversationPost(Request $request,$conIds){
		$request->validate([
			'category' =>['required','numeric'],
			'sub_category' =>['required','numeric'],
			'topic' =>['required','numeric'],
			'date' =>['required'],
			'time' =>['required']
		],[
			'category.required'=>'Please select a category!',
			'sub_category.required'=>'Please select a sub category!',
			'topic.required'=>'Please select a topic!',
			'date.required'=>'Please select a date!',
			'time.required'=>'Please select a time!'
		]);
		if(date('Y-m-d', strtotime(@$request->date)) < date('Y-m-d')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		elseif(date('Y-m-d', strtotime(@$request->date)) == date('Y-m-d') && date('H:i A', strtotime(@$request->time)) < date('H:i A')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		$updateCon['category_id'] = @$request->category;
		$updateCon['sub_category_id'] = @$request->sub_category;
		$updateCon['topic_id'] = @$request->topic;
		$updateCon['date'] = date('Y-m-d', strtotime(@$request->date));
		$updateCon['time'] = date('H:i:s', strtotime(@$request->time));
		$updateCon['type'] = 'OP';
		$save = Conversation::where('id',@$conIds)->update(@$updateCon);
		if(!empty(@$save)){
			session()->flash("success",__('success.-4143'));
		}
		else{
			session()->flash("error",__('errors.-5001'));
		}
		return redirect()->back();
	}
}
