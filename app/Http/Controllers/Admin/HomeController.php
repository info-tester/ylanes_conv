<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Conversation;
use App\Models\Topic;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Show the Admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $data['users_count'] = User::where('status','!=','D')->count();
        $data['up_conversations'] = Conversation::whereIn('status',['A','I','C'])
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'>=',date('Y-m-d H:i'))->count();
        $data['past_conversations'] = Conversation::whereIn('status',['A','I','C'])
        ->where(DB::raw("DATE_FORMAT(CONCAT(conversations.date,' ',conversations.time),'%Y-%m-%d %H:%i')"),'<=',date('Y-m-d H:i'))->count();
        $data['topics'] = Topic::where('status','!=','D')->count();
        return view('admin.modules.dashboard.home',@$data);
    }
}
