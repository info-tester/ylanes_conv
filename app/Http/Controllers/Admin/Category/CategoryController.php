<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Topic;
use App\Models\UserToCategories;
use Intervention\Image\ImageManagerStatic as Image;
use Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
class CategoryController extends Controller
{
	public function __construct()
	{
		$this->middleware('admin.auth:admin');
		
	}
	public function manageCategory(Request $request){
		$data['categories'] = Category::whereIn('status',['I','A']);
		if($request->all()){
			if(!empty(@$request->category_id))
			{
				$data['categories'] = $data['categories']->where(function($q) use($request){
					$q->where('id',@$request->category_id)
					->orwhere('parent_id',@$request->category_id);
				}); 
			}
			if(@$request->keyword) {
				$data['categories'] = $data['categories']->where(function($q) use($request){
					$q->Where('name','like','%'.@$request->keyword.'%');
				});                                 
			}
			if (@$request->show_header) {
				$data['categories'] = $data['categories']->where(function($q) use($request){
					$q->where('show_header',@$request->show_header)
					->where('parent_id',0);
				});
			}
		}
		$data['categories'] = $data['categories']->orderBy('name','asc')->get();
		$data['parent_cat'] = Category::whereIn('status',['A'])->where('parent_id',0)->orderBy('name','asc')->get();
		return view('admin.modules.category.manage-category',@$data);
	}
	public function AddCategory(Request $request){
		$data['categories'] = Category::where('parent_id',0)->whereIn('status',['I','A'])->latest()->orderBy('name','asc')->get();
		if(@$request->all()){
			$file = $request->file('uploaded_file');
			if ($file) {
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); 
				$tempPath = $file->getRealPath();
				$fileSize = $file->getSize();
				if($extension != 'csv' && $extension != 'CSV')
				{
					session()->flash("error",__('errors.-33121'));
					return redirect()->back();
				}
				$this->checkUploadedFileProperties($extension, $fileSize);
				$location = 'public/upload'; 
				$file->move($location, $filename); 
				$filepath = public_path('upload' . "/" . $filename);
				$file = fopen($filepath, "r");
				$importData_arr = array(); 
				$i = 0;
				while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
					$num = count($filedata);
					if ($i == 0) {
						$i++;
						continue;
					}
					for ($c = 0; $c < $num; $c++) {
						$importData_arr[$i][] = $filedata[$c];
					}
					$i++;
				}
				fclose($file);
				$j = 0;
				foreach ($importData_arr as $importData) {
					$j++;
					try {
						DB::beginTransaction();
						if(!empty(@$importData[0])){
							$checkExistCat = Category::where('name',@$importData[0])->where('status','!=','D')->first();
							if (empty($checkExistCat)) {
								$addCategory['name'] = @$importData[0];
								$addCategory['slug'] = str::slug(@$importData[0]);
								$addCategory['description'] = null;
								$addCategory['status'] = 'A';
								$addCategory['parent_id'] = 0;
								$category = Category::create($addCategory);
								$new['slug'] = str::slug(@$importData[0]).'-'.$category->id;
								Category::where('id',$category->id)->update($new);
								$show_data[] = ['category_name' => @$importData[0], 'sub_category_name' => null, 'is_upload' => 1];
							}
							else{
								$show_data[] = ['category_name' => @$importData[0], 'sub_category_name' => null, 'is_upload' => 0];
							}
						}
						if(!empty(@$importData[1])){
							$checkExistCat = Category::where('name',@$importData[0])->where('status','!=','D')->first();
							if (!empty($checkExistCat)) {
								$checkExistCatSub = Category::where('name',@$importData[1])->where('status','!=','D')->first();
								if (empty($checkExistCatSub)) {
									$addCategory['name'] = @$importData[1];
									$addCategory['slug'] = str::slug(@$importData[1]);
									$addCategory['status'] = 'A';
									$addCategory['description'] = null;
									$addCategory['parent_id'] = $checkExistCat->id;
									$category = Category::create($addCategory);
									$new['slug'] = str::slug(@$importData[1]).'-'.$category->id;
									Category::where('id',$category->id)->update($new);
									$show_data[] = ['category_name' => @$importData[0], 'sub_category_name' => @$importData[1], 'is_upload' => 1];
								}
								else{
									$show_data[] = ['category_name' => @$importData[0], 'sub_category_name' => @$importData[1], 'is_upload' => 0];
								}
							}
							else{
								$show_data[] = ['category_name' => @$importData[0], 'sub_category_name' => @$importData[1], 'is_upload' => 0];
							}
						}
						
						DB::commit();
					} catch (\Exception $e) {
						//throw $th;
						DB::rollBack();
					}
				}
				$insert_datas = array_merge($show_data);
				$catname = '';
				foreach ($insert_datas as $keys => $values) {
					if(str_replace(' ', '',strtolower($values['category_name'])) == @$catname && $values['sub_category_name'] == null)
					{
						$insert_data[] = null;
					}
					else{
						$insert_data[] = $values;
					}
					$catname = str_replace(' ', '',strtolower($values['category_name']));
				}
				return redirect()->back()->with('datas', array_filter($insert_data));
			} 
			else {
				$request->validate([
					'name'=>[
						'required',
						'max:255',
						Rule::unique('categories')->where(function($query) {
							$query->where('status', '!=', 'D');
						})
					],
				],[
					'name.required'=>'Please provide title of the category!'
				]);

				$addCategory['name'] = $request->name;
				$addCategory['slug'] = str::slug($request->name);
				$addCategory['description'] = nl2br($request->desc);
				$addCategory['status'] = 'A';
				if(!empty(@$request->parent_id)){
					$addCategory['parent_id'] = $request->parent_id;
				}
				else{
					$addCategory['parent_id'] = 0;
				}
				$category = Category::create($addCategory);
				if(@$request->categoryPicture){
					$pic   = $request->categoryPicture;
					$name = $category->id.'.'.$pic->getClientOriginalExtension();
					$pic->move('storage/app/public/category_pics/', $name);

					$img = Image::make('storage/app/public/category_pics/' . $name);
					$img->resize(158, 158)->save('storage/app/public/category_pics/' . $name, 60);

					$new['picture'] = $name;
				}
				$new['slug'] = str::slug($request->name).'-'.$category->id;
				Category::where('id',$category->id)->update($new);
				session()->flash("success",__('success.-1000'));
				return redirect()->back();
			}
		}
		else{
			return view('admin.modules.category.add-category',@$data);
		}
	}
	public function checkUploadedFileProperties($extension, $fileSize)
	{
		$valid_extension = array("csv"); //Only want csv and excel files
		$maxFileSize = 2097152; // Uploaded file size limit is 2mb
		if (in_array(strtolower($extension), $valid_extension)) {
			if ($fileSize <= $maxFileSize) {
			} else {
				throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
			}
		} else {
			throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
		}
	}
	public function editCategory(Request $request,$id){
		$data['categories'] = Category::where(['parent_id'=>0])->whereIn('status',['I','A'])->orderBy('name','asc')->get();
		$data['categoryInfo'] = Category::where(['id'=>$id])->whereIn('status',['I','A'])->first();
		if(@$request->all()){       
			$request->validate([
				'name'=>[
					'required',
					'max:255',
					Rule::unique('categories')->where(function($query) use ($id) {
						$query->where('status', '!=', 'D')->where('id','!=',@$id);
					})
				],
			],[
				'name.required'=>'Please provide name of the category!'
			]);
			$addCategory['name'] = $request->name;
			$addCategory['slug'] = str::slug($request->name).'-'.$id;
			$addCategory['description'] = nl2br($request->desc);
			$addCategory['status'] = 'A';
			if(@$request->categoryPicture){
				$pic   = $request->categoryPicture;
				$name = $id.'.'.$pic->getClientOriginalExtension();
				$pic->move('storage/app/public/category_pics/', $name);

				$img = Image::make('storage/app/public/category_pics/' . $name);
				$img->resize(158, 158)->save('storage/app/public/category_pics/' . $name, 60);
				$addCategory['picture'] = $name;
			}

			Category::where('id',$id)->update($addCategory);
			session()->flash("success",__('success.-4002'));
			return redirect()->back();



		}else{
                // dd($data);
			return view('admin.modules.category.edit-category',@$data);
		}

	}
	public function updateStatus($slug){
		$category = Category::where('slug',$slug)->first();
		if(@$category){
			if(@$category->status == 'A'){
				$update['status'] = 'I';

			}else if(@$category->status == 'I'){
				$update['status'] = 'A';

			}
			Category::where('id',$category->id)->update($update);
			session()->flash("success",__('success.-4111'));
			return redirect()->back();

		}else{
			session()->flash("success",__('success.-4111'));
			return redirect()->back();
		}
	}
	public function categoryDelete($id){
		$category = Category::where(['id'=>@$id])->whereIn('status',['I','A'])->first();
		if(@$category){
			$noOfChild = Category::where(['parent_id'=>@$id])->whereIn('status',['I','A'])->count();
			$noOfTopic = Topic::where(['category_id'=>@$id])->orwhere(['sub_category_id'=>@$id])->count();
			
			$noOfuserCat = UserToCategories::where(['category_id'=>@$id])->count();

			if($noOfChild <= 0 && $noOfTopic <=0 && $noOfuserCat <=0){
				$update['status'] = 'D';
				Category::where(['id'=>@$id])->update($update);
				session()->flash("success",__('success.-800'));
				return redirect()->back();

			}else{
				if($noOfTopic > 0){
					session()->flash("error",__('errors.-33122'));
				}
				elseif($noOfChild > 0){
					session()->flash("error",__('errors.-5084'));
				}
				else{
					session()->flash("error",__('errors.-33124'));
				}
				return redirect()->back();
			}
		}else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->back();
		}

	}
	public function updateHeaderStatus($slug){
		$category = Category::where('slug',$slug)->first();
		if(@$category){
			if(@$category->show_header == 'Y'){
				$update['show_header'] = 'N';
				session()->flash("success",__('success.-4133'));
			}else if(@$category->show_header == 'N'){
				$update['show_header'] = 'Y';
				session()->flash("success",__('success.-4132'));
			}
			Category::where('id',$category->id)->update($update);
			return redirect()->back();

		}else{
			session()->flash("success",__('success.-4111'));
			return redirect()->back();
		}
	}
	public function addBulkCategorypage(){
		return view('admin.modules.category.add-bulk-category');
	}
}
