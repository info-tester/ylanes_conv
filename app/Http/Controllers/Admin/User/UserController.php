<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\YcoinHistory;
use Illuminate\Validation\Rule;
use Auth;
use Session;
use Mail;
use File;
use DB;
use Exception;
use App\Models\ConversationsToUser;
class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('admin.auth:admin');

	}
	public function manageUser(Request $request){
		$data['allUser'] = User::where('status','!=','D');
		if($request->all()){
			if(@$request->keyword) {
				$data['allUser'] = $data['allUser']->where(function($q) use($request){
					$q->Where('first_name','like','%'.@$request->keyword.'%')
					->orWhere('last_name','like','%'.@$request->keyword.'%')
					->orWhere('phone','like','%'.@$request->keyword.'%')
					->orWhere('email','like','%'.@$request->keyword.'%')
					->orWhere(\DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%".@$request->keyword."%")
					->OrWhere('profile_name','like','%'.@$request->keyword.'%');
				});
			}
			if (@$request->status) {
				$data['allUser'] = $data['allUser']->where(function($q) use($request){
					$q->where('status',@$request->status);
				});
			}
			if (@$request->signup_from) {
				$data['allUser'] = $data['allUser']->where(function($q) use($request){
					$q->where('signup_from',@$request->signup_from);
				});
			}
			if(@$request->from_date1){

				$data['allUser'] =  $data['allUser']
				->where(DB::raw("DATE(created_at)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
			}
			if(@$request->to_date1){

				$data['allUser'] =  $data['allUser']->where(DB::raw("DATE(created_at)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
			}
		}
		$data['allUser'] = $data['allUser']->orderBy('created_at','desc')->get();
		$data['key'] = $request->all();
		return view('admin.modules.user.manage-user',@$data);
	}
	public function UserStatus($id){
		$user = User::where('id',@$id)->first();
		if($user){
			if($user->status == 'A')
			{
				$data['status'] = 'I';
			}
			else{
				$data['status'] = 'A';
			}
			$user->update($data);
			session()->flash("success",__('success.-4114'));
			return redirect()->back();
		}
		else{
			session()->flash("error",__('errors.-5057'));
			return redirect()->back();
		}
	}
	public function UserDelete($ids){
		$existUserEdu = User::where('id',@$ids)->where('status','!=','D')->first();
		if(!empty(@$existUserEdu)){
			$existUserEdu->update(['status'=>'D']);
			session()->flash("success",__('success_user.-737'));
		}
		else{
			session()->flash('error',__('errors.-33119'));
		}
		return redirect()->route('admin.manage.users');
	}
	public function viewUser($id){
		$data['user'] = User::where('id',@$id)->where('status','!=','D')->first();
		if(!empty($data['user']))
		{
			return view('admin.modules.user.view-user',@$data);
		}
		else{
			session()->flash("error",__('errors.-33108'));
			return redirect()->back();
		}
	}
	public function sendYcoins(Request $request){
		try{
			$userData = User::where('id',@$request->popup_user_id)->where('status','!=','D')->first();
			if(!empty($userData)){
				$update['conversation_balance_total'] = @$userData->conversation_balance_total + @$request->popup_y_coin;
				$update['conversation_balance_free'] = @$userData->conversation_balance_free + @$request->popup_y_coin;
				@$userData->update($update);
				@$userData = User::whereId(@$userData->id)->first();
				YcoinHistory::create([
					'user_id' => @$userData->id,
					'coin_free' => @$request->popup_y_coin,
					'coin_paid' => '0',
					'balance_coin_free' => @$userData->conversation_balance_free,
					'balance_coin_paid' => @$userData->conversation_balance_paid,
					'type' => 'IN',
					'category' => 'A'
				]);
				$userMail = @$userData->email;
				$data['subject'] = @$subject = "Admin sent you ".@$request->popup_y_coin." Ycoins";
				$data['ycoin'] = @$request->popup_y_coin;
				if(!empty(@$userMail)){
					Mail::send('mail.admin_ycoin_email', @$data , function($message) use ($userMail,$subject)
	                {
	                    $message->to(@$userMail)->subject(@$subject)->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
	                });
				}
				session()->flash("success",__('success.-4164'));
				return redirect()->back();
			}
			else{
				session()->flash("error",__('errors.-33119'));
				return redirect()->back();
			}
		}
		catch (Exception $e) {
			// dd($e->getMessage());
			return redirect()->back();
		}
	}

    public function viewUserConversation($id){
        $data['user'] = User::where('id',@$id)->where('status','!=','D')->first();
		if(!empty($data['user']))
		{
            $data['conversations']=ConversationsToUser::with(['getConversation.users'])->where('user_id', $data['user']->id)->whereIn('status',['C','U'])->get();
			return view('admin.modules.user.view_user_conversation',@$data);
		}
		else{
			session()->flash("error",__('errors.-33108'));
			return redirect()->back();
		}
    }
}
