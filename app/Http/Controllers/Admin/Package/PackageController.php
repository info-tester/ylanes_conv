<?php

namespace App\Http\Controllers\Admin\Package;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use App\Models\User;
use App\Models\PackageMaster;
use App\Models\UserPackagePurchase;
class PackageController extends Controller
{
    public function __construct()
	{
		$this->middleware('admin.auth:admin');
	}
	public function managePackage(){
		$data['packages'] = PackageMaster::latest()->get();
		return view('admin.modules.package.manage-package',@$data);
	}
	public function addPackagePage(){
		return view('admin.modules.package.add-package');
	}
	public function addPackagePost(Request $request){
		$request->validate([
			'price' =>['required','numeric'],
			'conv_paid' =>['required','numeric'],
			'conv_free' =>['required','numeric']
		],[
			'conv_paid.required'=>'Paid coin field are required.',
			'conv_paid.numeric'=>'Paid coin field enter a number.',
			'conv_free.required'=>'Free coin field are required.',
			'conv_free.numeric'=>'Free coin field enter a number.'
		]);
		PackageMaster::create([
			'price' => @$request->price,
			'coin_paid' => @$request->conv_paid,
			'coin_free' => @$request->conv_free,
			'coin_total' => @$request->conv_paid + @$request->conv_free,
		]);
		session()->flash('success',__('success.-4161'));
        return redirect()->back();
	}
	public function editPackagePage($pIds){
		$data['package_data'] = PackageMaster::whereId($pIds)->first();
		if(!empty($data['package_data'])){
			return view('admin.modules.package.edit-package',@$data);
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.package');
		
		}
	}
	public function updatePackage(Request $request,$pids){
		$request->validate([
			'price' =>['required','numeric'],
			'conv_paid' =>['required','numeric'],
			'conv_free' =>['required','numeric']
		],[
			'conv_paid.required'=>'Paid coin field are required.',
			'conv_paid.numeric'=>'Paid coin field enter a number.',
			'conv_free.required'=>'Free coin field are required.',
			'conv_free.numeric'=>'Free coin field enter a number.'
		]);
		PackageMaster::whereId($pids)->update([
			'price' => @$request->price,
			'coin_paid' => @$request->conv_paid,
			'coin_free' => @$request->conv_free,
			'coin_total' => @$request->conv_paid + @$request->conv_free,
		]);
		session()->flash('success',__('success.-4162'));
        return redirect()->back();
	}
	public function deletePackage($pids){
		PackageMaster::whereId($pids)->delete();
		session()->flash('success',__('success.-4163'));
        return redirect()->back();
	}
}
