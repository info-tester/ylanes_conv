<?php

namespace App\Http\Controllers\Admin\Topic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Category;
use App\Models\Topic;
class TopicController extends Controller
{
	public function __construct()
	{
		$this->middleware('admin.auth:admin');
	}
	public function manageTopic(Request $request){
		$data['category'] = Category::where('parent_id','0')->where('status','A')->orderBy('name','asc')->get();
		$data['all_topics'] = Topic::latest();
		if($request->all()){
			if(!empty(@$request->category_id))
			{
				$data['all_topics'] = $data['all_topics']->where(function($q) use($request){
					$q->where('category_id',@$request->category_id);
				});
			}
			if(!empty(@$request->sub_category_id))
			{
				$data['all_topics'] = $data['all_topics']->where(function($q) use($request){
					$q->where('sub_category_id',@$request->sub_category_id);
				});
			}
			if(@$request->keyword) {
				$data['all_topics'] = $data['all_topics']->where(function($q) use($request){
					$q->Where('topic_line_1','like','%'.@$request->keyword.'%')->orWhere('topic_line_2','like','%'.@$request->keyword.'%');
				});
			}
		}
		$data['all_topics'] = $data['all_topics']->get();
		return view('admin.modules.topic.manage-topic',@$data);
	}
	public function addTopic(){
		$data['category'] = Category::where('parent_id','0')->where('status','A')->get();
		return view('admin.modules.topic.add-topic',@$data);
	}
	public function addTopicPost(Request $request){
		$request->validate([
			'category_id' =>['required','numeric'],
			'sub_category_id' =>['required','numeric'],
			'topic_line_1' =>[
				'required',
				Rule::unique('topics')->where(function($query) use ($request) {
					$query->where('sub_category_id',@$request->sub_category_id);
				})
			],
			'topic_line_2' =>['required'],
		],[
			'category_id.required'=>'Please select a category!',
			'sub_category_id.required'=>'Please select a sub category!',
			'topic_line_1.required'=>'Topic Line 1 field are required.',
			'topic_line_2.required'=>'Topic Line 2 field are required.'
		]);
		$addTopic['category_id'] = $request->category_id;
		$addTopic['sub_category_id'] = $request->sub_category_id;
		$addTopic['topic_line_1'] = $request->topic_line_1;
		$addTopic['topic_line_2'] = $request->topic_line_2;
		$addTopic['status'] = 'A';
		Topic::create($addTopic);
		session()->flash("success",__('success.-4137'));
		return redirect()->back();
	}
	public function updateTopicStatus($id){
		$topics = Topic::where('id',$id)->first();
		if(@$topics){
			if(@$topics->status == 'A'){
				$update['status'] = 'I';

			}else if(@$topics->status == 'I'){
				$update['status'] = 'A';

			}
			Topic::where('id',$topics->id)->update($update);
			session()->flash("success",__('success.-4138'));
			return redirect()->back();

		}else{
			session()->flash("success",__('success.-4138'));
			return redirect()->back();
		}
	}
	public function editTopic($id){
		$data['topic_data'] = Topic::where('id',@$id)->first();
		if(!empty($data['topic_data'])){
			return view('admin.modules.topic.edit-topic',@$data);
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.topic');
		}
	}
	public function editTopicPost(Request $request,$id){
		$topicData = Topic::where('id',@$id)->first();
		if(!empty(@$topicData)){
			$request->validate([
				'topic_line_1' =>[
					'required',
					Rule::unique('topics')->where(function($query) use ($id,$topicData) {
						$query->where('id','!=',@$id)
						->where('sub_category_id',@$topicData->sub_category_id);
					})
				],
				'topic_line_2' =>['required'],
			],[
				'topic_line_1.required'=>'Topic Line 1 field are required.',
				'topic_line_2.required'=>'Topic Line 2 field are required.'
			]);
			$upTopic['topic_line_1'] = $request->topic_line_1;
			$upTopic['topic_line_2'] = $request->topic_line_2;
			@$topicData->update($upTopic);
			session()->flash("success",__('success.-4139'));
			return redirect()->back();
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.topic');
		}
	}
	public function topicDeletePost($id){
		$topic = Topic::where('id',@$id)->first();
		if($topic->conversation->isEmpty())
		{
			Topic::where('id',@$id)->delete();
			session()->flash("success",__('success.-4140'));
		}
		else{
			session()->flash("error",__('errors.-33123'));
		}
		return redirect()->route('admin.manage.topic');
	}
}
