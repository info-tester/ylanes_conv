<?php

namespace App\Http\Controllers\Admin\Schedule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Topic;
use App\Models\Conversation;
use App\Models\ConversationScheduleMaster;
use App\Models\ConversationScheduleDetail;
use App\Models\Category;
use Carbon\Carbon;
use DB;
use Illuminate\Validation\Rule;
use Exception;
use Mail;
class ScheduleController extends Controller
{
	public function __construct()
	{
		$this->middleware('admin.auth:admin',['except' => ['cronJobsScheduleUpdate']]);
	}
	public function manageSchedule(Request $request){
		$data['schedule_master'] = ConversationScheduleMaster::orderBy('start_date','desc');
		if (!empty($request->all())) {
			if(!empty(@$request->topic_id)){
				$masterShuduleIds = ConversationScheduleDetail::where('topic_id',@$request->topic_id)->pluck('conv_schedule_master_id')->toArray();
				$data['schedule_master'] = $data['schedule_master']->whereIn('id',@$masterShuduleIds);
			}
			if(!empty(@$request->from_date1)){
				$data['schedule_master'] =  $data['schedule_master']
				->where(DB::raw("DATE(start_date)"),'>=',date('Y-m-d',strtotime(@$request->from_date1)));
			}
			if(!empty(@$request->to_date1)){
				$data['schedule_master'] =  $data['schedule_master']->where(DB::raw("DATE(start_date)"),'<=',date('Y-m-d',strtotime($request->to_date1)));
			}
		}
		$data['schedule_master'] = $data['schedule_master']->get();
		@$data['topics'] = Topic::whereIn('status',['A'])->orderBy('topic_line_1','asc')->get();
		return view('admin.modules.schedule.manage-schedule',@$data);
	}
	public function addSchedulePage(){
		$data['sub_categories'] = Category::whereIn('status',['A'])
		->whereHas('allsubCategoriesTopics', function($query){
			$query->where('id','>',0);
		})
		->where('parent_id','!=',0)->get();
		foreach ($data['sub_categories'] as $key => $value) {
			@$topics = Topic::whereIn('status',['A'])->where('sub_category_id',@$value->id)->get();
			$value->topics = @$topics;
		}
		return view('admin.modules.schedule.add-schedule',@$data);
	}
	public function addSchedulePost(Request $request){
		$request->validate([
			'topic_id.*' =>['required','numeric'],
			'freq_hour' =>['required','numeric'],
			'repeat_after_days' =>['required','numeric'],
			'start_date' =>['required'],
			'time' =>['required']
		],[
			'topic_id.*.required'=>'Please choose a Topic!',
			'freq_hour.required'=>'Please select a frequency hour!',
			'repeat_after_days.required'=>'Please enter a repeat after days!',
			'start_date.required'=>'Please select a start date!',
			'time.required'=>'Please select a start time!'
		]);
		if(date('Y-m-d', strtotime(@$request->start_date)) < date('Y-m-d')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		elseif(date('Y-m-d', strtotime(@$request->start_date)) == date('Y-m-d') && date('H:i A', strtotime(@$request->time)) < date('H:i A')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		// $schedule_master_exists = ConversationScheduleMaster::where('start_date',date('Y-m-d', strtotime(@$request->start_date)))->first();
		// if (!empty(@$schedule_master_exists)) {
		// 	session()->flash("error",__('errors.-33126'));
		// 	return redirect()->back();
		// }

		if(!empty(@$request->repeat_after_days))
		{
			$crondays = @$request->repeat_after_days - 3;
			if($crondays <= 0){
				if(@$request->repeat_after_days == 0)
				{
					$crondays = null;
				}
				else{
					$crondays = @$request->repeat_after_days;
				}
			}
		}
		// $data[] = $d->toDateTimeString();
		if(!empty(@$request->topic_id)){
			foreach (@$request->topic_id as $value) {
				$save = ConversationScheduleMaster::create([
					'freq_hour' => @$request->freq_hour,
					'repeat_after_days' => @$request->repeat_after_days,
					'start_date' => date('Y-m-d', strtotime(@$request->start_date)),
					'start_time' => date('H:i:s', strtotime(@$request->time)),
					'next_cron_date' => !empty(@$crondays) ? Carbon::parse(@$request->start_date)->addDays(@$crondays)->format('Y-m-d') : null,
					'is_updated' => 'N',
					'topics_json' => json_encode(@$request->topic_id)
				]);
				if(!empty($save)){
					ConversationScheduleDetail::create([
						'conv_schedule_master_id' => @$save->id,
						'topic_id' => @$value,
					]);
					$start = Carbon::parse(@$save->start_date.' '.date('H:i:s', strtotime(@$save->start_time)));
					$end = Carbon::parse(@$save->start_date)->addDays(1);
					$data = [];
					for($d = $start;$d < $end;$d->addHour(@$request->freq_hour)){
						@$topicsdata = Topic::whereId(@$value)->first();
						if (!empty(@$topicsdata)) {
							$createCon['creator_user_id'] = '0';
							$createCon['category_id'] = @$topicsdata->category_id;
							$createCon['sub_category_id'] = @$topicsdata->sub_category_id;
							$createCon['topic_id'] = @$topicsdata->id;
							$createCon['date'] = @$d->format('Y-m-d');
							$createCon['time'] = @$d->format('H:i:s');
							$createCon['type'] = 'OP';
							$createCon['no_of_participants'] = '0';
							$createCon['status'] = 'A';
							$createCon['created_by'] = 'A';
							$createCon['schedule_id'] = @$save->id;
							Conversation::create(@$createCon);
						}
					}
				}
			}
		}
		session()->flash("success",__('success.-4152'));
		return redirect()->back();
	}
	public function editSchedulePage($masterIds=null){
		$data['schedule_master'] = ConversationScheduleMaster::whereId(@$masterIds)->where('is_updated','N')->first();
		if(!empty(@$data['schedule_master'])){
			$data['sub_categories'] = Category::whereIn('status',['A'])
			->whereHas('allsubCategoriesTopics', function($query){
				$query->where('id','>',0);
			})
			->where('parent_id','!=',0)->get();
			foreach ($data['sub_categories'] as $key => $value) {
				@$topics = Topic::whereIn('status',['A'])->where('sub_category_id',@$value->id)->get();
				$value->topics = @$topics;
			}
			return view('admin.modules.schedule.edit-schedule',@$data);
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.schedule');
		}
	}
	public function editSchedulePost(Request $request,$id){
		$request->validate([
			'topic_id.*' =>['required','numeric'],
			// 'freq_hour' =>['required','numeric'],
			'repeat_after_days' =>['required','numeric'],
			'start_date' =>['required'],
			'time' =>['required']
		],[
			'topic_id.*.required'=>'Please choose a Topic!',
			// 'freq_hour.required'=>'Please select a frequency hour!',
			'repeat_after_days.required'=>'Please enter a repeat after days!',
			'start_date.required'=>'Please select a start date!',
			'time.required'=>'Please select a start time!'
		]);
		if(date('Y-m-d', strtotime(@$request->start_date)) < date('Y-m-d')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		elseif(date('Y-m-d', strtotime(@$request->start_date)) == date('Y-m-d') && date('H:i A', strtotime(@$request->time)) < date('H:i A')){
			session()->flash("error",__('errors.-33125'));
			return redirect()->back();
		}
		$start = Carbon::parse(@$request->start_date.' '.date('H:i:s', strtotime(@$request->time)));
		$end = Carbon::parse(@$request->start_date)->addDays(1);
		$data['schedule_master'] = ConversationScheduleMaster::whereId(@$id)->where('is_updated','N')->first();
		if (!empty($data['schedule_master'])) {
			if(!empty(@$request->repeat_after_days))
			{
				$crondays = @$request->repeat_after_days - 3;
				if($crondays <= 0){
					if(@$request->repeat_after_days == 0)
					{
						$crondays = null;
					}
					else{
						$crondays = @$request->repeat_after_days;
					}
				}
			}
			$topicsJson = $data['schedule_master']->topics_json;
			$update['repeat_after_days'] = @$request->repeat_after_days;
			$update['start_date'] = date('Y-m-d', strtotime(@$request->start_date));
			$update['start_time'] = date('H:i:s', strtotime(@$request->time));
			$update['next_cron_date'] = !empty(@$crondays) ? Carbon::parse(@$request->start_date)->addDays(@$crondays)->format('Y-m-d') : null;
			$update['topics_json'] = json_encode(@$request->topic_id);
			$data['schedule_master']->update(@$update);
			$checkExistData = ConversationScheduleDetail::where('conv_schedule_master_id',@$data['schedule_master']->id)->get();
			Conversation::where('schedule_id',@$data['schedule_master']->id)->where('no_of_participants','0')->delete();
			if(!empty(@$topicsJson) && !empty(@$request->topic_id)){
				$result = array_diff(@$request->topic_id,json_decode($topicsJson));
				ConversationScheduleMaster::where('topics_json',@$topicsJson)->update(['topics_json'=>json_encode(@$request->topic_id)]);
			}
			if(!empty(@$request->topic_id)){
				$i=0;
				foreach (@$request->topic_id as $values) {
					if (in_array($values, $result)){
						$result = array_values($result);
						$checkDelShuMasterIds = ConversationScheduleDetail::where('topic_id',@$result[@$i])->pluck('conv_schedule_master_id')->toArray();
						if(!empty(@$checkDelShuMasterIds)){
							Conversation::whereIn('schedule_id',@$checkDelShuMasterIds)->where('no_of_participants','0')->delete();
							ConversationScheduleMaster::whereIn('id',@$checkDelShuMasterIds)->delete();
							ConversationScheduleDetail::whereIn('conv_schedule_master_id',@$checkDelShuMasterIds)->delete();
						}
						$save = ConversationScheduleMaster::create([
							'freq_hour' => @$data['schedule_master']->freq_hour,
							'repeat_after_days' => @$request->repeat_after_days,
							'start_date' => date('Y-m-d', strtotime(@$request->start_date)),
							'start_time' => date('H:i:s', strtotime(@$request->time)),
							'next_cron_date' => !empty(@$crondays) ? Carbon::parse(@$request->start_date)->addDays(@$crondays)->format('Y-m-d') : null,
							'is_updated' => 'N',
							'topics_json' => json_encode(@$request->topic_id)
						]);
						if(!empty(@$save)){
							ConversationScheduleDetail::create([
								'conv_schedule_master_id' => @$save->id,
								'topic_id' => @$result[@$i],
							]);
						}
						$i++;
					}
					else{
						if(!empty(@$topicsJson)){
							foreach (json_decode($topicsJson) as $valuess) {
								if(!in_array($valuess,@$request->topic_id)){
									$checkDelShuMasterIds = ConversationScheduleDetail::where('topic_id',$valuess)->pluck('conv_schedule_master_id')->toArray();
									if(!empty(@$checkDelShuMasterIds)){
										Conversation::whereIn('schedule_id',@$checkDelShuMasterIds)->where('no_of_participants','0')->delete();
										ConversationScheduleMaster::whereIn('id',@$checkDelShuMasterIds)->delete();
										ConversationScheduleDetail::whereIn('conv_schedule_master_id',@$checkDelShuMasterIds)->delete();
									}
								}
							}
						}
					}
				}
			}
			for($d = $start;$d < $end;$d->addHour(@$data['schedule_master']->freq_hour)){
				if(!empty(@$request->topic_id)){
					foreach (@$request->topic_id as $value) {
						if(@$checkExistData->isNotEmpty()){
							foreach ($checkExistData as $values) {
								$checkExist = ConversationScheduleDetail::where('conv_schedule_master_id',@$data['schedule_master']->id)->where('topic_id',$value)->first();
								if(!empty($checkExist)){
									ConversationScheduleDetail::where('conv_schedule_master_id',@$data['schedule_master']->id)->whereNotIn('topic_id',@$request->topic_id)->delete();
								}
								else{
									// ConversationScheduleDetail::create([
									// 	'conv_schedule_master_id' => $data['schedule_master']->id,
									// 	'topic_id' => @$value,
									// ]);
								}
							}
						}
						else{
							// ConversationScheduleDetail::create([
							// 	'conv_schedule_master_id' => $data['schedule_master']->id,
							// 	'topic_id' => @$value,
							// ]);
						}
						@$topicsdata = Topic::whereId(@$value)->first();
						if (!empty(@$topicsdata)) {
							$createCon['creator_user_id'] = '0';
							$createCon['category_id'] = @$topicsdata->category_id;
							$createCon['sub_category_id'] = @$topicsdata->sub_category_id;
							$createCon['topic_id'] = @$topicsdata->id;
							$createCon['date'] = @$d->format('Y-m-d');
							$createCon['time'] = @$d->format('H:i:s');
							$createCon['type'] = 'OP';
							$createCon['no_of_participants'] = '0';
							$createCon['status'] = 'A';
							$createCon['created_by'] = 'A';
							$createCon['schedule_id'] = @$data['schedule_master']->id;
							Conversation::create(@$createCon);
						}
					}
				}
			}
			session()->flash("success",__('success.-4153'));
			return redirect()->route('admin.manage.schedule');
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.schedule');
		}
	}
	public function cronJobsScheduleUpdate(){
		$data_schedule_master = ConversationScheduleMaster::where('is_updated','N')
		->where('next_cron_date',date('Y-m-d'))
		->get();
		if(!empty(@$data_schedule_master))
		{
			foreach (@$data_schedule_master as $value) {
				if(!empty(@$value->repeat_after_days))
				{
					$crondays = @$value->repeat_after_days - 3;
					if($crondays <= 0){
						if(@$value->repeat_after_days == 0)
						{
							$crondays = null;
						}
						else{
							$crondays = @$value->repeat_after_days;
						}
					}
				}
				$save = ConversationScheduleMaster::create([
					'freq_hour' => @$value->freq_hour,
					'repeat_after_days' => @$value->repeat_after_days,
					'start_date' => @$value->next_cron_date,
					'start_time' => @$value->start_time,
					'next_cron_date' => !empty(@$crondays) ? Carbon::parse(@$value->next_cron_date)->addDays(@$crondays)->format('Y-m-d') : null,
					'is_updated' => 'N',
					'topics_json' => @$value->topics_json
				]);

				if(!empty($save)){
					if (!empty(@$value->ConversationScheduleDetails)) {
						foreach (@$value->ConversationScheduleDetails as $values) {
							ConversationScheduleDetail::create([
								'conv_schedule_master_id' => @$save->id,
								'topic_id' => @$values->topic_id,
							]);
							$start = Carbon::parse(@$value->next_cron_date.' '.date('H:i:s', strtotime(@$value->start_time)));
							$end = Carbon::parse(@$value->next_cron_date)->addDays(1);
							$data = [];
							for($d = $start;$d < $end;$d->addHour(@$value->freq_hour)){
								@$topicsdata = Topic::whereId(@$values->topic_id)->first();
								if (!empty(@$topicsdata)) {
									$createCon['creator_user_id'] = '0';
									$createCon['category_id'] = @$topicsdata->category_id;
									$createCon['sub_category_id'] = @$topicsdata->sub_category_id;
									$createCon['topic_id'] = @$topicsdata->id;
									$createCon['date'] = @$d->format('Y-m-d');
									$createCon['time'] = @$d->format('H:i:s');
									$createCon['type'] = 'OP';
									$createCon['no_of_participants'] = '0';
									$createCon['status'] = 'A';
									$createCon['created_by'] = 'A';
									$createCon['schedule_id'] = @$save->id;
									Conversation::create(@$createCon);
								}
							}
						}
					}
				}
				$value->update(['is_updated' => 'Y']);
			}
		}
		try {
			@$data = [];
			Mail::send('mail.test_mail', @$data , function($message)
			{
				$message->to('infoware.solutions1@gmail.com')->subject('Ylanes Conversation Schedule cron is running')->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
			});
		}
		catch (Exception $e) {
            // dd($e);
		}
		return 'Yes';
	}
	public function deleteSchedulePost($id=null){
		$data['schedule_master'] = ConversationScheduleMaster::whereId(@$id)->first();
		if(!empty($data['schedule_master'])){
			$checkExistData = ConversationScheduleDetail::where('conv_schedule_master_id',@$data['schedule_master']->id)->first();
			$typeJson = json_decode($data['schedule_master']->topics_json);
			if(!empty($typeJson)){
				if (($key = array_search($checkExistData->topic_id, $typeJson)) !== false) {
				    unset($typeJson[$key]);
				}
				$upTypeJson = array_values($typeJson);
				ConversationScheduleMaster::where('topics_json',$data['schedule_master']->topics_json)->update(['topics_json'=>json_encode(@$upTypeJson)]);
			 	Conversation::where('schedule_id',@$data['schedule_master']->id)->where('no_of_participants','0')->delete();
			 	$checkExistData->delete();
				$data['schedule_master']->delete();
			}
			session()->flash("success",__('success.-4154'));
			return redirect()->back();
		}
		else{
			session()->flash("error",__('errors.-5001'));
			return redirect()->route('admin.manage.schedule');
		}
	}
}
