<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;
use Session;
class AutoLogout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('web')->check()){
            $user = Auth::user();
            if(!empty($user)){
                if ($user->status == 'I' || $user->status == 'D') {
                    Auth::guard('web')->logout();
                    $request->session()->invalidate();
                    return redirect()->route('login');
                }
                else if(!empty(@$request->session()->get('a_url'))){
                    $user->update(['user_type' => 'G']);
                    return redirect(@$request->session()->get('a_url'));
                }
            }
        }
        return $next($request);
    }
}
