<?php

// use Config;
namespace App\Helpers;
use App\Models\Currency;
use App\Models\Category;
use App\Models\Country;
use Carbon\Carbon;
use DB;

class CustomHelper
{
	static public function getCategory(){
		$category = Category::where('status','A')->orderBy('name','asc')->where('parent_id','0')->get();
		return $category;
	}
	static public function getSubCategory($catId){
		$subCategory = Category::where('status','A')->orderBy('name','asc')->where('parent_id',@$catId)->get();
		return $subCategory;
	}
	static public function number_format_short( $n, $precision = 1 ) {
		if ($n < 900) {
			$n_format = number_format($n, $precision);
			$suffix = '';
		} else if ($n < 900000) {
			$n_format = number_format($n / 1000, $precision);
			$suffix = 'K';
		} else if ($n < 900000000) {
			$n_format = number_format($n / 1000000, $precision);
			$suffix = 'M';
		} else if ($n < 900000000000) {
			$n_format = number_format($n / 1000000000, $precision);
			$suffix = 'B';
		} else {
			$n_format = number_format($n / 1000000000000, $precision);
			$suffix = 'T';
		}
		if ( $precision > 0 ) {
			$dotzero = '.' . str_repeat( '0', $precision );
			$n_format = str_replace( $dotzero, '', $n_format );
		}
		return $n_format . $suffix;
	}
	static public function getCountrycode(){
		$CountryCode = Country::select('id','phonecode','name')->orderBy('name','asc')->where('phonecode','>',0)->get();
		return $CountryCode;
	}
	static public function footergetCategory(){
		$category = Category::where('status','A')->where('parent_id','0')->take(6)->get();
		return $category;
	}
    static public function timeDifference($time=null) {

        $currentTime=$time;
        if($currentTime==0||null){
            return '0 sec';
        }
        $totalTime='';
        $hours = floor($time / (60*60));
        $rem = floor($time % (60*60));
        if($hours>0){
            $totalTime .= $hours.' Hour ';
        }
        $minutes = floor($rem / 60);
        $rem= floor($rem % 60);
        if($minutes>0){
            $totalTime .= $minutes.' Min ';
        }
        if($rem>0){
            $totalTime .= $rem.' Sec ';
        }
        return $totalTime;
    }

}
