<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Validator;
use db;

class UpdateEmailVerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user_data = [];
    public function __construct($data = [])
    {
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build(){
        $user = $this->user_data;
        $email = @$user->temp_email;      
        $subject = "Verify Your email-id! Request for email-id change!"; 
        return $this->view('mail.verification_mail_update_email',['user' => $user])
            ->to($email)
            ->subject($subject)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
