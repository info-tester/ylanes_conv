<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Validator;
use db;

class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user_data = [];
    public function __construct($data = [])
    {
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build(){
        $user = $this->user_data;
        $email = $user->email;
        $subject = "Verification Email";
        return $this->view('mail.verification_mail',['user' => $user])
            ->to($email)
            ->subject($subject)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
