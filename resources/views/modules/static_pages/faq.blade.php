@extends('layouts.app')
@section('title','FAQs')
@section('content')

<div class="about_us_ban banner_nn01">
   <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>



<section class="abt_dose_work cont_us_pg">
<div class="container">
  <div class="pg_hed">
      <h1>Frequently Asked Questions</h1>
      {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua enim</p> --}}
  </div>


</div>
</section>

<section class="faq_tabs_section">
<div class="container">
  <div class="row">
      <div class="faq_nes">
      <div class="help_left sticky_new">
          <div class="faq_tbs">
              <ul class="nav nav-tabs">
                  <li>
                      <a class="active" data-toggle="tab" href="#tab1"><img src="{{url('public/frontend/images/cats.png')}}"> Our Currency</a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#tab2"><img src="{{url('public/frontend/images/cats.png')}}"> Rooms</a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#tab3"><img src="{{url('public/frontend/images/cats.png')}}"> Late Entry and Cancellations</a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#tab4"><img src="{{url('public/frontend/images/cats.png')}}"> Users and uses of the platform</a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#tab5"><img src="{{url('public/frontend/images/cats.png')}}"> Rules of Engagement </a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#tab6"><img src="{{url('public/frontend/images/cats.png')}}"> Invitations, Referrals and Making Connections</a>
                  </li>
              </ul>
          </div>
      </div>

      <div class="help_right">
          <div class="tab-content">
              <div class="tab-pane active" id="tab1">
                  <div class="accordian_sec">
                      <div class="accordion" id="faq">
                          <div class="card">
                              <div class="card-header" id="faqhead1">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">
                                      <p>What are YCoins?</p>
                                  </a>
                              </div>
                              <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                                  <div class="card-body">
                                      <p>YCoins are the digital currency on our platform to be used for creating and joining various rooms. </p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead2">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">
                                      <p>How to purchase YCoins?</p>
                                  </a>
                              </div>
                              <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                                  <div class="card-body">
                                      <p>You can purchase YCoins as per the packages  <a href="{{route('purchase.package')}}">here</a> </p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead3">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">
                                      <p>Are the YCoins transferable?</p>
                                  </a>
                              </div>
                              <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                                  <div class="card-body">
                                      <p>No, the YCoins cannot be transferred to other users.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead4">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">
                                      <p>Can we encash our YCoins?</p>
                                  </a>
                              </div>
                              <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                                  <div class="card-body">
                                      <p>No you can not encash Ycoins.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead5">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5">
                                      <p> Can I get a refund of the money I spent on purchasing YCoins?</p>
                                  </a>
                              </div>
                              <div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">
                                  <div class="card-body">
                                      <p>No, we do not refund the money spent by a user on the purchase of YCoins on our platform. However, YCoins can be refunded under special circumstances, such as; rooms getting cancelled or registrations not being accepted by a host.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead6">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6">
                                      <p>Do you have a Subscription model?</p>
                                  </a>
                              </div>
                              <div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">
                                  <div class="card-body">
                                      <p>No, we currently do not have a Subscription model. However, interested users can buy a suitable YCoins package and use them at their convenience. </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab2">
                  <div class="accordian_sec">
                      <div class="accordion" id="faqnews">
                          <div class="card">
                              <div class="card-header" id="faqhead8">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq8" aria-expanded="true" aria-controls="faq8">
                                      <p> Who can create a room?</p>
                                  </a>
                              </div>
                              <div id="faq8" class="collapse" aria-labelledby="faqhead8" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>Anyone can create a room.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead9">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq9" aria-expanded="true" aria-controls="faq9">
                                      <p>How many users can join a room?</p>
                                  </a>
                              </div>
                              <div id="faq9" class="collapse" aria-labelledby="faqhead9" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>All other group rooms can have 5 participants. A one-on-one room can have only 2 participants.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead10">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq10" aria-expanded="true" aria-controls="faq10">
                                      <p>How many kinds of rooms are there?</p>
                                  </a>
                              </div>
                              <div id="faq10" class="collapse" aria-labelledby="faqhead10" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>There are 3 kinds of rooms; Standard, Premium,  and One-on-One. To know more about them,<!-- <a href="#">click here</a> -->.</p>

                                      <div class="custom-table new_custab">
                                            <div class="new-table-mr">
                                               <div class="table">
                                                  <div class="one_row1 hidden-sm-down only_shawo">
                                                     <div class="cell1 tab_head_sheet"> Room Type</div>
                                                     <div class="cell1 tab_head_sheet">Description</div>
                                                  </div>
                                                  <!--row 1-->
                                                  <div class="one_row1 small_screen31">
                                                     <div class="cell1 tab_head_sheet_1 newtcell">
                                                        <span class="W55_1">Room Type</span>
                                                        <p class="add_ttrr">Standard</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Description</span>
                                                        <p class="add_ttrr">Anyone can register for this room, and it is filled up on a first come, first serve basis.</p>
                                                     </div>
                                                  </div>

                                                  <div class="one_row1 small_screen31 newtcell">
                                                     <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Room Type</span>
                                                        <p class="add_ttrr">Premium</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Description</span>
                                                        <p class="add_ttrr">Host (user who created the room) has to approve which of the registered participants can join the room based on their profile and ‘Your Take’ field.</p>
                                                     </div>
                                                  </div>

                                                  <div class="one_row1 small_screen31 newtcell">
                                                     <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Room Type</span>
                                                        <p class="add_ttrr">One-on-one</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Description</span>
                                                        <p class="add_ttrr">A one-on-one room can be opened by any user. The host must select the fellow participant from the list of users who have registered for that room.</p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>



                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead11">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq11" aria-expanded="true" aria-controls="faq11">
                                      <p> How to enter a room?</p>
                                  </a>
                              </div>
                              <div id="faq11" class="collapse" aria-labelledby="faqhead11" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>To enter a room, one must register for the room by paying the room fee. The rights to admission to the Premium and One-on-One rooms lie with the host; other rooms are open for all. The user has a choice of entering a room anonymously if they so choose.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead12">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq12" aria-expanded="true" aria-controls="faq12">
                                      <p>  Can we enter a room anonymously?</p>
                                  </a>
                              </div>
                              <div id="faq12" class="collapse" aria-labelledby="faqhead12" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>Yes, a user has the option of registering anonymously. In this case, your profile name and page will not be accessible to the participants of this room. However, your age group, your gender, the number of hearts you have earned and Your Take on the topic will be visible to the other users. </p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead13">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq13" aria-expanded="true" aria-controls="faq13">
                                      <p>   How to create a room?</p>
                                  </a>
                              </div>
                              <div id="faq13" class="collapse" aria-labelledby="faqhead13" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>Any user can create a room by clicking on the “Create Room” button. The users can choose the type of room, a topic of interest, a convenient schedule, mode of entry (public profile vs anonymous) and visibility of the room (Host country or across the globe) while creating a room.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead14">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq14" aria-expanded="true" aria-controls="faq14">
                                      <p> Can multiple rooms be created on the same topic at the same time?</p>
                                  </a>
                              </div>
                              <div id="faq14" class="collapse" aria-labelledby="faqhead14" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>Yes, multiple rooms can be created for the same topic at the same time.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead114">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq114" aria-expanded="true" aria-controls="faq114">
                                      <p>  What is the duration of each conversation?</p>
                                  </a>
                              </div>
                              <div id="faq114" class="collapse" aria-labelledby="faqhead114" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>All conversations are scheduled for 1hr, however even if two people in a room decide to continue, the duration can be extended to 2 hrs at no additional cost. However, if the participants wish to continue beyond 2 hrs, it will be treated as a new conversation for which additional fee appropriate to the room needs to be paid.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead124">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq124" aria-expanded="true" aria-controls="faq124">
                                      <p>  What is ‘Your Take’?</p>
                                  </a>
                              </div>
                              <div id="faq124" class="collapse" aria-labelledby="faqhead124" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>At YLanes our endeavour is to enable meaningful conversations among our guests and we appreciate that each guest may have a different take on a topic. The section, “Your Take” encourages the guests to highlight what they would be bringing to a conversation, such as a sub-topic, a question, a thought, an experience, some perspectives or ideas on the topic.  We believe that this would enhance the overall experience for all our guests in a conversation.</p>
                                  </div>
                              </div>
                          </div>
                          {{-- <div class="card">
                              <div class="card-header" id="faqhead134">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq134" aria-expanded="true" aria-controls="faq134">
                                      <p> What are Resources?</p>
                                  </a>
                              </div>
                              <div id="faq134" class="collapse" aria-labelledby="faqhead134" data-parent="#faqnews">
                                  <div class="card-body">
                                      <p>At YLanes we understand that guests passionate about a topic, may have explored it outside the platform as well and benefited from it. We encourage our guests to share such interesting Resources from other platforms such as YouTube videos, Podcasts, Instagram handles, blogs etc. using the “Resources” link, for the benefit of other guests on the platform.</p>
                                  </div>
                              </div>
                          </div> --}}



                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab3">
                  <div class="accordian_sec">
                      <div class="accordion" id="faqnest">
                          <div class="card">
                              <div class="card-header" id="faqhead15">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq15" aria-expanded="true" aria-controls="faq15">
                                      <p><span> Can a registration be cancelled?</p>
                                  </a>
                              </div>
                              <div id="faq15" class="collapse" aria-labelledby="faqhead15" data-parent="#faqnest">
                                  <div class="card-body">
                                      <p>Yes, anyone can cancel their registration upto 30 minutes before the start of the room without any penalty. </p>
                                      <p>If a host cancels their registration, the next participant will become the host of the room.</p>
                                       <p>If the host of a one-on-one room cancels their registration, the room gets cancelled, and the participants will get a full refund.
                                      </p>
                                      <p><a href="#url" id="moreless-button3">Click here</a>  to know the details of the penalties under various scenarios.</p>
                                      <div class="moretext3">
                                          <div class="custom-table new_custab">
                                            <div class="new-table-mr">
                                               <div class="table">
                                                  <div class="one_row1 hidden-sm-down only_shawo">
                                                     <div class="cell1 tab_head_sheet"> Scenarios</div>
                                                     <div class="cell1 tab_head_sheet">Penalties</div>
                                                  </div>
                                                  <!--row 1-->
                                                  <div class="one_row1 small_screen31">
                                                     <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Scenarios</span>
                                                        <p class="add_ttrr">A user cancels the call less than 30 minutes before the scheduled time.</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Penalties</span>
                                                        <p class="add_ttrr">50 YCoins will be deducted from the user.</p>
                                                     </div>
                                                  </div>

                                                  {{-- <div class="one_row1 small_screen31">
                                                     <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Scenarios</span>
                                                        <p class="add_ttrr">A <b>Moderator</b> cancels a call less than 30 minutes from the start time.</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Penalties</span>
                                                        <p class="add_ttrr">10 hearts and 100 YCoins will be deducted from the Moderator.</p>
                                                     </div>
                                                  </div>

                                                  <div class="one_row1 small_screen31">
                                                     <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Scenarios</span>
                                                        <p class="add_ttrr">A <b>Moderator</b> does not show up within 15 minutes into a scheduled room.</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Penalties</span>
                                                        <p class="add_ttrr">20 hearts and 300 YCoins will be deducted from the Moderator. The room automatically gets cancelled.
</p>
                                                     </div>
                                                  </div> --}}

                                                  <div class="one_row1 small_screen31">
                                                     <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Scenarios</span>
                                                        <p class="add_ttrr">A user does not join within 15 minutes into a scheduled room</p>
                                                     </div>
                                                     <div class="cell1 tab_head_sheet_1 half-boxes">
                                                        <span class="W55_1">Penalties</span>
                                                        <p class="add_ttrr">The user will not be allowed to join the conversation room and they will forfeit all the YCoins paid while registering for the conversation</p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead16">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq16" aria-expanded="true" aria-controls="faq16">
                                      <p> What is a late entry into a room?</p>
                                  </a>
                              </div>
                              <div id="faq16" class="collapse" aria-labelledby="faqhead16" data-parent="#faqnest">
                                  <div class="card-body">
                                      <p>We value the time taken out by our guests for meaningful conversations and we expect our guests too to respect each other's time. Hence, all conversation rooms are shut at 15 minutes after the scheduled start time and registered users who couldn't join by then would be treated as no-show guests. The users will not be eligible for any refund of their YCoins in such scenarios. </p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead17">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq17" aria-expanded="true" aria-controls="faq17">
                                      <p>Can the calls be rescheduled by the host ?</p>
                                  </a>
                              </div>
                              <div id="faq17" class="collapse" aria-labelledby="faqhead17" data-parent="#faqnest">
                                  <div class="card-body">
                                      <p>We value the time taken out by our guests for meaningful conversations and we expect our guests to also respect each other's time. Hence, rescheduling of the calls by a host will be considered a cancellation.</p>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab4">
                  <div class="accordian_sec">
                      <div class="accordion" id="faqnwfour">
                          {{-- <div class="card">
                              <div class="card-header" id="faqhead22">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq22" aria-expanded="true" aria-controls="faq22">
                                      <p>  How many kinds of users are there?</p>
                                  </a>
                              </div>
                              <div id="faq22" class="collapse" aria-labelledby="faqhead22" data-parent="#faqnwfour">
                                  <div class="card-body">
                                      <p>There are 3 kinds of users on our platform, Regular Users, Superhosts and Moderators. <a href="#"> Click here</a>  to know more about them. </p>
                                  </div>
                              </div>
                          </div> --}}
                          {{-- <div class="card">
                              <div class="card-header" id="faqhead16">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq23" aria-expanded="true" aria-controls="faq23">
                                      <p> Can a regular user create a Moderated room?</p>
                                  </a>
                              </div>
                              <div id="faq23" class="collapse" aria-labelledby="faqhead23" data-parent="#faqnwfour">
                                  <div class="card-body">
                                      <p>No, only a Moderator can create a Moderated room.</p>
                                  </div>
                              </div>
                          </div> --}}
                          <div class="card">
                              <div class="card-header" id="faqhead24">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq24" aria-expanded="true" aria-controls="faq24">
                                      <p> Does the platform support the mentally ill?</p>
                                  </a>
                              </div>
                              <div id="faq24" class="collapse" aria-labelledby="faqhead24" data-parent="#faqnwfour">
                                  <div class="card-body">
                                      <p>YLanes is a conversations-only platform designed to connect like-minded individuals for meaningful conversations. This platform is not designed to support guests with severe mental illnesses. Please go through our detailed terms and conditions <a href="{{route('user.privacy.terms')}}">here</a> </p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead25">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq25" aria-expanded="true" aria-controls="faq25">
                                      <p> Does this platform provide solutions to people’s challenges?</p>
                                  </a>
                              </div>
                              <div id="faq25" class="collapse" aria-labelledby="faqhead25" data-parent="#faqnwfour">
                                  <div class="card-body">
                                      <p>YLanes is a conversations-only platform designed to connect like-minded individuals for meaningful conversations. This platform enables conversations on topics of mutual interest among the user and it is not designed to offer solutions to people’s challenges.</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab5">
                  <div class="accordian_sec">
                      <div class="accordion" id="faqfives">
                          <div class="card">
                              <div class="card-header" id="faqhead29">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq29" aria-expanded="true" aria-controls="faq29">
                                      <p>What are the Rules of Engagement on this platform?</p>
                                  </a>
                              </div>
                              <div id="faq29" class="collapse" aria-labelledby="faqhead29" data-parent="#faqfives">
                                  <div class="card-body">
                                      <p>We value the time taken out by our guests to enjoy authentic and meaningful conversations. Hence, we expect all our guests to follow our rules of engagement: </p>
                                      <ul>
                                          <li> Speak with authenticity (Try to speak in  “I” statements)</li>
                                          <li> Listen with intent (Try to understand; avoid judgements)</li>
                                          <li> Engage with respect</li>
                                          <li> Maintain confidentiality</li>
                                          <li> Avoid advising (Share relevant  experiences, if any)</li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="tab6">
                  <div class="accordian_sec">
                      <div class="accordion" id="faqsix">
                          <div class="card">
                              <div class="card-header" id="faqhead36">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq36" aria-expanded="true" aria-controls="faq36">
                                      <p> How can we invite people to register for conversation rooms?</p>
                                  </a>
                              </div>
                              <div id="faq36" class="collapse" aria-labelledby="faqhead36" data-parent="#faqsix">
                                  <div class="card-body">
                                      <p>We can invite people within and outside the platform to register for conversations by clicking the “share” button and sharing the room link on various social media and messaging applications. Users can also share the links for rooms that they are not registering for. </p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead37">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq37" aria-expanded="true" aria-controls="faq37">
                                      <p> Can we refer people to the platform?</p>
                                  </a>
                              </div>
                              <div id="faq37" class="collapse" aria-labelledby="faqhead37" data-parent="#faqsix">
                                  <div class="card-body">
                                      <p>Yes, users can invite their friends to join the platform by clicking on the “refer a friend” button and earn 100 YCoins for each successful referral.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead38">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq38" aria-expanded="true" aria-controls="faq38">
                                      <p> Can we connect with our fellow users on the platform?</p>
                                  </a>
                              </div>
                              <div id="faq38" class="collapse" aria-labelledby="faqhead38" data-parent="#faqsix">
                                  <div class="card-body">
                                      <p>Yes, we can send Connection requests to other users on the platform. Once they accept your request, you will be able to see the rooms being created by them, and also chat with them.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead39">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq39" aria-expanded="true" aria-controls="faq39">
                                      <p> Can I chat with other users on the platform?</p>
                                  </a>
                              </div>
                              <div id="faq39" class="collapse" aria-labelledby="faqhead39" data-parent="#faqsix">
                                  <div class="card-body">
                                      <p>Yes, you can chat only with your Connections on the platform.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="faqhead40">
                                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#faq40" aria-expanded="true" aria-controls="faq40">
                                      <p> What happens when a profile gets reported?</p>
                                  </a>
                              </div>
                              <div id="faq40" class="collapse" aria-labelledby="faqhead40" data-parent="#faqsix">
                                  <div class="card-body">
                                      <p>If a profile gets reported 5 times, the user may be permanently debarred from the platform. In such a scenario, the user would lose all the YCoins in their wallet and there would be no recourse for any refund.</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </div>
  </div>
</div>
</section>
@endsection
@push('js')

@endpush
