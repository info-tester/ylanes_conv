@extends('layouts.app')
@section('title','How It Works')
@section('content')

<div class="about_us_ban banner_nn01">
   <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>

<section class="abt_dose_work cont_us_pg">
    <div class="container">
       <div class="pg_hed">
          <h1>How does it work</h1>
       </div>
    </div>
 </section>
 <section class="faq_tabs_section pab85">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="how_fr">
                <div class="row align-items-center">
                   <div class="col-md-8">
                      <div class="how_bx_new">
                         <p>YLanes enables Meaningful Conversations with like-minded people in an Audio-Visual format.  We encourage our guests to follow 3 core principles of a Meaningful Conversation</p>
                         <ul>
                            <li><span> 1. </span> Speak with authenticity</li>
                            <li><span> 2. </span> Listen with intent</li>
                            <li><span> 3. </span> Engage with respect </li>
                         </ul>
                      </div>
                   </div>
                   <div class="col-md-4">
                      <div class="hw_imgs">
                         <img src="{{url('public/frontend/images/ylan_hw1.png')}}" >
                      </div>
                   </div>
                </div>
             </div>
          </div>


          <div class="col-12">
             <div class="how_fr">
                <div class="row align-items-center">
                   <div class="col-md-4">
                      <div class="hw_imgs">
                         <img src="{{url('public/frontend/images/ylan_hw.png')}}" >
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="how_bx_new">
                         <p>You can choose from a list of 75+ conversation topics covering </p>
                         <ul>
                            <li><span> 1. </span> Relationships </li>
                            <li><span> 2. </span>  Reflections </li>
                            <li><span> 3. </span> Self-Exploration  </li>
                         </ul>
                      </div>
                   </div>

                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
                      <div class="how_bx_new">
                         <p class=" mb-0"> You can either schedule a conversation on a topic of interest by creating a new room or register for a conversation scheduled by another user on a topic of interest</p>


                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
               <div class="row align-items-center">
                   <div class="col-md-8">
                      <div class="how_bx_new">
                         <p>While scheduling a conversation you could choose to chat with a small 5-person group or one-on-one</p>
                         <ul>
                            <li><span> 1. </span>  Standard Rooms are open for all - rooms get filled on a first come, first serve basis. </li>
                            <li><span> 2. </span>  Premium Rooms have conditional entry - a host can choose who to allow into the room. </li>

                            <li><span> 3. </span>  One-on-one rooms enable intimate conversations with another guest.  </li>
                         </ul>
                     </div>
                 </div>
                   <div class="col-md-4">
                      <div class="hw_imgs">
                         <img src="{{url('public/frontend/images/ylan_hw2.png')}}" >
                      </div>
                   </div>




                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
                      <div class="how_bx_new">
                         <p class=" mb-0">  Both the hosts and the guests registering for a room must fill-up a text box to highlight what they would be bringing to the conversation.</p>


                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
                      <div class="how_bx_new">
                         <p class=" mb-0">  Participants need to pay  for conversations using YCoins. The guests will have a credit of 800 YCoins when they sign-up. Guests could earn further YCoins by referring their friends to YLanes, through their behaviour   on the platform or by purchasing them on the platform.</p>


                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
                <div class="row align-items-center">
                   <div class="col-md-4">
                      <div class="hw_imgs">
                         <img src="{{url('public/frontend/images/ylan_hw3.png')}}" >
                      </div>
                   </div>
                   <div class="col-md-8">
                      <div class="how_bx_new">
                         <p class="">  Choices while entering a room:</p>
                         <ul>
                            <li><span> 1. </span> Enter anonymously or by using your public profile </li>
                            <li><span> 2. </span> Choose to keep your video on or off. </li>
                         </ul>
                     </div>
                 </div>

                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
                      <div class="how_bx_new">
                         <p class=" mb-0">  You can “Connect” with like-minded people across the world on the platform and build your own communities by topic as you go along.</p>


                </div>
             </div>
          </div>

          <div class="col-12">
             <div class="how_fr">
                      <div class="how_bx_new">
                         <p class=" mb-0">YLanes is a conversations-only platform designed to connect like-minded individuals for meaningful conversations. This platform is not designed to support guests with severe mental illnesses. Please go through our terms and conditions here.</p>


                </div>
             </div>
          </div>

       </div>
    </div>
 </section>
@endsection
@push('js')

@endpush
