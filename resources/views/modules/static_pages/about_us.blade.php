@extends('layouts.app')
@section('title','About Us Ylanes')
@section('content')

<div class="about_us_ban banner_nn01">
   <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>

<section class="abt_dose_work cont_us_pg">
	<div class="container">
		<div class="pg_hed">
			<h1> About Us Ylanes</h1>
			<p>YLanes is a conversations-only platform designed to connect like-minded individuals for meaningful conversations.YLanes does not offer any solutions / counselling or therapy on its platform and it strongly discourages its users too from doing so. This platform is not designed to support guests with severe mental illnesses.YLanes has a zero tolerance policy towards bad behavior (bullying, harassment, nudity, offensive language, ridiculing etc.); offenders would be permanently debarred from the platform. To go through our Terms and Conditions in detail please click here</p>
		</div>
	</div>
</section>

<section class="new_abouts">
    {{-- <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_secs_headings">
                    <h1>About YLanes</h1>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="abut_set">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-lg-5">
                    <div class="abt_mob_imgs">
                    <img src="{{url('public/frontend/images/abt_mobs.png')}}">
                    </div>
                </div>
                <div class="col-md-6 col-lg-7">
                    <div class="abuts_contents">
                        <p class="abts_cont">YLanes enables Meaningful Conversations with like-minded people </p>
                        <div class="cont_lb1">
                            <em>
                                <img src="{{url('public/frontend/images/tt.png')}}">
                            </em>
                            <div class="cnt_ab">
                                <h3>Share your stories & ideas</h3>
                                <p>on topics that matter to you (anonymously if you wish) </p>
                            </div>
                        </div>
                        <div class="cont_lb1">
                            <em>
                                <img src="{{url('public/frontend/images/tt.png')}}">
                            </em>
                            <div class="cnt_ab">
                                <h3>Connect and gain perspective </h3>
                                <p>from like-minded individuals from across the world on topics of interest </p>
                            </div>
                        </div>
                        <div class="cont_lb1">
                            <em>
                                <img src="{{url('public/frontend/images/tt.png')}}">
                            </em>
                            <div class="cnt_ab">
                                <h3>Hold a safe space for others</h3>
                                <p>to share their thoughts, on topics of your experience or expertise. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="spas_5">
            <img src="{{url('public/frontend/images/sha5.png')}}">
        </div>
    </div>
</section>
<section class="abt_teams_sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_secs_headings">
                    <h1>Ylanes Founders</h1>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="team_class_boxs">
                    <div class="pro_abt_img">
                        <span>
                            <img src="public/frontend/images/rajesh.jpg">
                        </span>
                    </div>
                    <div class="all_nfo_abts">
                        <h3>Rajesh Ivaturi</h3>
                        <h6>Co-Founder, CEO</h6>
                        <p>IIM-A, Ex-Partner EY</p>
                        <a href="https://www.linkedin.com/in/rajeshivaturi/" target="_blank">View Linkdin Profile</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="team_class_boxs">
                    <div class="pro_abt_img">
                        <span>
                            <img src="public/frontend/images/deepti.png">
                        </span>
                    </div>
                    <div class="all_nfo_abts">
                        <h3>Deepti Punjabi</h3>
                        <h6>Co-Founder, COO</h6>
                        <p>IIM-C, Deutsche Bank</p>
                        <a href="https://www.linkedin.com/in/deepti-punjabi-43a02418/" target="_blank">View Linkdin Profile</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
@push('js')

@endpush
