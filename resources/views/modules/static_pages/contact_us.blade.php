@extends('layouts.app')
@section('title','Contact Us')
@section('content')

<div class="mar_top"></div>
<div class="about_us_ban banner_nn01">
   <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>



<section class="how_dose_work cont_us_pg">
<div class="container">
  <div class="pg_hed">
      <h1>Have A Questions Drop Us Line!</h1>
      {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua enim</p> --}}
  </div>

  <div class="row">
      <div class="col-md-4 order-8 col-sm-12">
          <div class="contact-from">
              <div class="cont-box">
                  <img src="{{url('public/frontend/images/hou.png')}}">
                  <div>
                      <h3>Reach Us On</h3>
                      <p>Madivam Connects Pvt Ltd
                        <br>
                        HD - 088, WeWork Enam Sambhav C - 20
                        <br>
                        BKC G Block Road
                        <br>
                        Mumbai 400051
                        <br>
                        Maharashtra
                        <br>
                        India</p>
                  </div>
              </div>
              <div class="cont-box">
                  <img src="{{url('public/frontend/images/ema.png')}}">
                  <div>
                      <h3>Email Us On</h3>
                      <p><a href="mailto: grievances@ylanes.com"> grievances@ylanes.com</a> </p>
                      {{-- <p><a href="mailto:info@ylanes.com">info@ylanes.com</a></p> --}}
                  </div>
              </div>
              {{-- <div class="cont-box">
                  <img src="{{url('public/frontend/images/call.png')}}">
                  <div>
                      <h3>Call Us On</h3>
                      <p><a href="tel:0123-567890">0123-567890</a></p>
                      <p><a href="tel:+91-9876543210">+91-9876543210</a></p>
                  </div>
              </div> --}}

              <div class="social-icons-cont text-center">
                  <h2>Connect With Socail</h2>
                  <ul>
                      <li> <a href="#" target="_blank"><img src="{{url('public/frontend/images/fa1.png')}}"></a></li>
                      <li> <a href="#" target="_blank"><img src="{{url('public/frontend/images/fa2.png')}}"></a></li>
                      <li> <a href="#" target="_blank"><img src="{{url('public/frontend/images/fa3.png')}}"></a></li>
                      <li> <a href="#" target="_blank"><img src="{{url('public/frontend/images/fa4.png')}}"></a></li>
                  </ul>
              </div>
          </div>
      </div>
      <div class="col-md-8  col-sm-12">
          <div class="contact-from">
            @include('layouts.message')
            <form id="contactForm" novalidate="true" method="post" action="{{route('user.contact.us.mail')}}">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="elementor-field-required">
                            <label for="name" class="field-label">Name<span>*</span></label>
                            <input type="text" class="form-field form-control" id="name" name="name" placeholder="Name"  data-error="Please enter your name" value="{{old('name', @Auth::user()->profile_name)}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="elementor-field-required">
                            <label for="name" class="field-label">Email<span>*</span></label>
                            <input type="text" placeholder="Email" id="email" class="form-field form-control" name="email"  data-error="Please enter your email" value="{{old('email',@Auth::user()->email)}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="elementor-field-required">
                            <label for="name" class="field-label">Phone number</label>
                            <input type="text" placeholder="Mobile No." id="tel" class="form-field form-control" name="mobile"  data-error="Please enter your mobile no." value="{{old('mobile',@Auth::user()->phone)}}">
                        </div>
                    </div>
                    {{-- <div class="col-sm-6">
                        <div class="elementor-field-required">
                            <label for="name" class="field-label">Subject<span>*</span></label>
                            <input type="text" class="form-field form-control" placeholder="Enter Here">
                        </div>
                    </div> --}}
                    <div class="col-sm-12">
                        <div class="elementor-field-required">
                            <label for="name" class="field-label">Message<span>*</span></label>
                            <textarea  class="form-control" id="message" placeholder="Your message" rows="5" data-error="Write your message" required="" name="message">{{old('message')}}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="pg_btn ne_pg_btns">Submit Now</button>
                    </div>
                </div>
            </form>
          </div>
      </div>
  </div>
</div>
</section>



{{-- <section id="contact" class="section">
    <div class="contact-form">
        <div class="container">
            <div class="row contact-form-area">
                <div class=" col-md-5 col-sm-12 order-md-7">
                    <div class="contact-right-area wow fadeIn">
                        <h2>Contact Address</h2>
                        <div class="contact-info">
                            <div class="single-contact">
                                <div class="contact-icon"> <i class="fa fa-map-marker"></i> </div>
                                <p>1601 Vine Street 4th Floor Los Angeles, CA 90028</p>
                            </div>
                            <div class="single-contact">
                                <div class="contact-icon"> <i class="fa fa-envelope"></i> </div>
                                <p><a href="mailto:info@reacherr.com"> info@reacherr.com  </a></p>
                            </div>
                            <div class="single-contact">
                                <div class="contact-icon"> <i class="fa fa-phone"></i> </div>
                                <p><a href="#">800-541-RCHR (7247) </a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-md-7 col-sm-12">
                    <div class="contact-block">
                        <h2>Contact Form</h2>
                        @include('layouts.message')
                        <form id="contactForm" novalidate="true" method="post" action="{{route('user.contact.us.mail')}}">
                            @csrf
                            <div class="row">
                                <div class="col-sm-12 ">
                                    <div class="form-group has-error">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name"  data-error="Please enter your name" value="{{old('name')}}">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group has-error">
                                        <input type="text" placeholder="Email" id="email" class="form-control" name="email"  data-error="Please enter your email" value="{{old('email')}}">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group has-error">
                                        <input type="text" placeholder="Mobile No." id="tel" class="form-control" name="mobile"  data-error="Please enter your mobile no." value="{{old('mobile')}}">

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group has-error">
                                        <textarea class="form-control" id="message" placeholder="Your Message" rows="5" data-error="Write your message" required="" name="message">{{old('message')}}</textarea>

                                    </div>
                                    <div class="submit-button">
                                        <button class="btn btn-sub new_button" id="submit" type="submit">Send</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
@endsection
@push('js')
<script type="text/javascript">
$(document).ready(function(){
	 jQuery.validator.addMethod("validate_email", function(value, element) {
            if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Enter valid Email");
        $("#contactForm").validate({
            rules: {
                name:{
                   required:true,
                },
                email:{
                  required:true,
                  validate_email:true,
                },
                mobile:{
                //   required:true,
                  number:true,
                  maxlength: 10,
               	  minlength:10,
                },
                subject:{
                  required:true,
                },
                message:{
                  required:true,
                },
            },
            messages: {
                name:{
                    required: 'Please enter your name ',
                },
                email:{
                    required: 'Please enter your email ',
                },
                mobile:{
                    required: 'Please enter your mobile no ',
                    number:'Please enter your mobile no properly',
                    maxlength:'mobile number must be number only and exactly 10 digits.',
                	minlength:'mobile number must be number only and exactly 10 digits.',
                },
                subject:{
                	required:'Please enter subject',
                },
                message:{
                	required:'Please enter your message',
                },
            },
            focusInvalid: false,
        });
    });

</script>
@endpush
