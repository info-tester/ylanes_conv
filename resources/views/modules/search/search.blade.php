@extends('layouts.app')
@section('title','Search')
@section('content')
<div class="about_us_ban banner_nn01">
    <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>
<section class="cat_subcat_nr">
    <div class="container">
        <h2>Choose Conversation Category</h2>
        <div class="cat_subcat_slider">
            <div class="owl-carousel owl-theme">
                @if(!empty(CustomHelper::getCategory()->isNotEmpty()))
                @foreach(CustomHelper::getCategory() as $category)
                <div class="item">
                    <a href="javascript:void(0);" id="catery{{ @$category->id }}" class="open_hid_dtop">
                        @if(!empty($category->picture))
                        <img src="{{url('storage/app/public/category_pics/'.@$category->picture)}}" alt="{{ $category->name }}" style="width: 24px; height: 22px;">
                        @else
                        <img src="{{url('public/frontend/images/Relationship.png')}}" alt="{{ $category->name }}">
                        @endif
                        {{ @$category->name }}
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    </a>
                    @if(!empty(CustomHelper::getSubCategory(@$category->id)->isNotEmpty()))
                    <div class="subb_catt01" id="lists{{ @$category->id }}" style="    position: relative;">
                        <ul>
                            @foreach(CustomHelper::getSubCategory(@$category->id) as $subCategory)
                            <li><a href="{{ route('search.page',@$subCategory->slug) }}">{{ @$subCategory->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!--cat_subcat-->
<section class="sortby_areaa">
    <div class="container">
        @if(!empty(@$sub_category))
        @if(!empty(@$sub_category->immediateParent))
        <div class="duble_texts">
            <h3>{{ @$sub_category->immediateParent->name }}</h3>
            <img src="{{url('public/frontend/images/ang.png')}}">
            {{-- <h3>{{ substr(@$sub_category->name,0,20) }} @if(substr(@$sub_category->name,20))...@endif</h3> --}}
            <h3>{{ @$sub_category->name }}</h3>
        </div>
        @endif
        @else
        @if(!empty(@$category_search))
        <div class="duble_texts">
            <h3>{{ @$category_search->name }}</h3>
        </div>
        @endif
        @endif
        <div class="right">
            <span>Sort by</span>
            <select class="form-select" onchange="srarchResult();" id="order_by">
                <option value="all">All</option>
                <option value="today">Today</option>
                <option value="towmorrow">Towmorrow</option>
                <option value="next">Next 7 days</option>
            </select>
            {{-- <a href="{{ route('create.conversation') }}" class="create"><img src="{{url('public/frontend/images/create.png')}}">Create</a> --}}
            <a href="{{ route('create.conversation') }}" class="create">Create room</a>
            <a href="{{ route('search.page') }}" class="create create2" style="">All Rooms</a>
        </div>
    </div>
</section>
<!--open_for_all-->
<div class="open_for_all_secc for_bg01 padb-70">
    <div class="container">
        <div class="col-12 text-center" id="mainloader" style="display: none;">
                <img src="{{ url('public/frontend/images/loader.gif') }}" style="width: 80px; padding-top: 40px;">
            </div>
        <div class="open_for_all {{-- new_topics_alls --}}" id="search-main-div" style="display: none;">
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
    $(document).ready(function() {
        srarchResult();
        var owl = $('.cat_subcat_slider .owl-carousel');
        owl.owlCarousel({
            margin:8,
            loop:false,
            nav: true,
            items:5,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                500:{
                    items:2,
                },
                640:{
                    items:2,
                },
                767:{
                    items:3,
                },
                1000:{
                    items:4,
                },
                1199:{
                    items:5,
                }
            }
        })
    })

    $(document).ready(function(){
        $('.open_hid_dtop').click(function(){
            $('.subb_catt01').hide();
            $(this).next().show();
        });
    });
</script>
<script>
    $(document).ready(function() {
        var owl = $('.for_slider_id01 .owl-carousel');
        owl.owlCarousel({
            margin: 16,
            nav: false,
            loop:true,
            dot:true,
            items:4,
            autoplay: true,
            responsiveClass:true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                767: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1199: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        })
    })
    function srarchResult(perPage){
        $('#search-main-div').hide();
        $('#mainloader').show();
        var category_search = {{ !empty(@$category_search) ? @$category_search->id : 'null' }};
        var sub_category = {{ !empty(@$sub_category) ? @$sub_category->id : 'null' }};
        var order_by = $('#order_by').val();
        $.ajax({
            url:"{{ route('search.results') }}",
            type:"GET",
            data: {
                'keywords':"{{ request()->keywords }}",
                'category':category_search,
                'sub_category':sub_category,
                'order_by':order_by,
                'per_page':perPage,
            },
            success: function(responce){
                $('#search-main-div').html(responce);
                $('#search-main-div').show();
                $('#mainloader').hide();
            },
            error: function(xhr){
                console.log(xhr);
            }
        });
    }
</script>

@endpush
