@extends('layouts.app')
@section('title')
Public Profile
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@section('content')
<section class="host_public_sec">
	<div class="container">
		<div class="host_public_inr">
			<div class="host_public_left sticky_new">
				<div class="host_public_left_inr">
					<div class="host_public_pick">
						@auth
						@php
						if(@auth::user()->id == @$host_data->id){
							$flag_is = 1;
						}
						else{
							$flag_is = 0;
						}
						@endphp
						@else
						@php
						$flag_is = 0;
						@endphp
						@endauth
						@if($flag_is == 0)
						<span class="heart_cc heart_cc{{ @$host_data->id }}"><a href="javascript:void(0)" @auth onclick="userToConnect({{ @$host_data->id }})" @else class="opensignin" @endauth><img src="{{ url('public/frontend/images/my_connects.png') }}" style="width: 18px;"></a></span>
						@endif
						<div class="host_public_pick_top">
							<em>
								@if(!empty(@$host_data->image))
								<img src="{{ url('storage/app/public/customer/profile_pics/'.@$host_data->image) }}" alt="">
								@else
								<img src="{{url('public/frontend/images/default_pic.png')}}" alt="">
								@endif
							</em>
							<h4>
								@if(!empty(@$host_data->profile_name))
								{{ substr(@$host_data->profile_name,0,25) }}
								@else
								{{ substr(@$host_data->first_name,0,25) }}
								@endif
							</h4>
							{{-- <ul class="star_cc">
								<li>
									<b><i class="fa fa-star"></i></b>
									<b><i class="fa fa-star"></i></b>
									<b><i class="fa fa-star"></i></b>
									<b><i class="fa fa-star"></i></b>
									<b><i class="fa fa-star"></i></b>
								</li>
								<li>4.6  <small>(102)</small></li>
							</ul> --}}
							<p><img src="{{url('public/frontend/images/followers.png')}}" alt="coins"> {{ CustomHelper::number_format_short(@$host_data->tot_hearts) }}
								&nbsp;&nbsp;
								<img src="{{url('public/frontend/images/upcoming_con1.png')}}" alt="icon">
								{{ CustomHelper::number_format_short(@$host_data->tot_conversation_completed,0) }}
							</p>
							@if(@$host_data->gender)
							<h5>Gender - <strong>
								@if(@$host_data->gender)
								@if(@$host_data->gender=='M')
								Male
								@elseif(@$host_data->gender=='F')
								Female
								@elseif(@$host_data->gender=='N')
								Non Binary
								@elseif(@$host_data->gender=='O')
								Others
								@endif
								@endif
							</strong>  </h5>
							@endif
							@if(!empty(@$host_data->year_of_birth))
							<h5>Age - <strong>
								@if(!empty(@$host_data->year_of_birth))
								@php
								$age = date('Y') - @$host_data->year_of_birth;
								@endphp
								@if($age < 25)
								Below 25
								@elseif($age >= 25 && $age <= 30)
								25-30
								@elseif($age >= 31 && $age <= 35)
								30-35
								@elseif($age >= 36 && $age <= 40)
								35-40
								@elseif($age >= 41 && $age <= 45)
								40-45
								@elseif($age >= 46 && $age <= 50)
								45-50
								@elseif($age >= 51 && $age <= 55)
								50-55
								@elseif($age >= 56 && $age <= 60)
								55-60
								@elseif($age >= 61)
								over 60
								@endif
								@endif
							</strong>  </h5>
							@endif
						</div>
						<div class="host_public_pick_bot">
							<ul class="host_item_list">
								<li>Country:
									@if(!empty(@$host_data->userCountryDetails)){{ @$host_data->userCountryDetails->name }} @endif
								</li>
							</ul>
							@if(!empty(@$host_data->media_link))
							<ul class="host_pub_sos" style="padding-left: 42px;">
								<li><a href="{{ !empty(@$host_data->media_link) ? @$host_data->media_link : '#' }}" target="_blank">Click to</a></li>
								<li><p> see social profile</p></li>
							</ul>
							@endif
							{{-- <div class="pro-social">
								<p><i class="fa fa-share"></i> Share :</p>
								<ul>
									<li>
										<a href="#"><img src="{{url('public/frontend/images/sos_icon1.png')}}"></a>
									</li>
									<li>
										<a href="#"><img src="{{url('public/frontend/images/twitter2.png')}}"></a>
									</li>
									<li>
										<a href="#"><img src="{{url('public/frontend/images/sos_icon5.png')}}"></a>
									</li>
								</ul>
							</div> --}}
						</div>
					</div>
				</div>
			</div>
			<div class="host_public_right">
				<div class="host_public_right_inr">
					<div class="host_public_right_pabel about_host_public">
						@if(!empty(@$host_data->profile_headline))
						<span class="tag_line">{{ @$host_data->profile_headline }}</span>
						@endif
						@if(!empty(@$host_data->about_me))
						<h4>Your life story in brief</h4>
						<p>{{ substr(@$host_data->about_me,0,300) }}</p>
						@if(substr(@$host_data->about_me,300))
						<p class="moretext3" style="display: none;">{{ substr(@$host_data->about_me,300) }}</p>
						<a href="javascript:void(0);" class="red_link" id="moreless-button3">Read more +</a>
						@endif
						@endif
						{{-- @if(@$host_data->user_to_category->isNotEmpty())
						<h4>Interest</h4>
						<ul class="tag_li host_public_category">
							@if(@$host_data->user_to_category->isNotEmpty())
							@foreach(@$host_data->user_to_category as $category)
							<li>{{ @$category->category->name }}</li>
							@endforeach
							@endif
						</ul>
						@endif --}}
						@if(!empty(@$host_data->describe_you))
						<h4>3 Words your friends use to describe you</h4>
						<p>{{ @$host_data->describe_you }}</p>
						@endif
						@if(!empty(@$host_data->holidays))
						<h4>Your idea of a perfect holiday</h4>
						<ul class="tag_li host_public_category">
							@if(!empty(@$host_data->holidays))
							@foreach(json_decode(@$host_data->holidays) as $hk => $holid)
							<li>{{ @$holid }}</li>
							@endforeach
							@endif
							@if(!empty(@$host_data->holidays_other))
							<li>{{ @$host_data->holidays_other }}</li>
							@endif
						</ul>
						@endif
						@if(!empty(@$host_data->problem_solve))
						<h4>One problem in the world you wish you could solve</h4>
						<p>{{ @$host_data->problem_solve }}</p>
						@endif
						@if(!empty(@$host_data->qualities_you))
						<h4>Qualities you admire the most in others</h4>
						<p>{{ @$host_data->qualities_you }}</p>
						@endif
                        @if(@$host_data->user_to_category_moderator->isNotEmpty() || !empty(@$host_data->memories))
						<h2 style="margin-top: 12px;">Moderator questions</h2>
                        @endif
						@if(@$host_data->user_to_category_moderator->isNotEmpty())
						<h4>A sub-category of focus</h4>
						<ul class="tag_li host_public_category">
							@if(@$host_data->user_to_category_moderator->isNotEmpty())
							@foreach(@$host_data->user_to_category_moderator as $category)
							<li>{{ @$category->category->name }}</li>
							@endforeach
							@endif
						</ul>
						@endif
						@if(!empty(@$host_data->memories))
						<h4>Talk about your personal experience, interest, relevance to at least one of your chosen sub-categories</h4>
						<p>{{ @$host_data->memories }}</p>
						@endif
					</div>
					@if(!empty(@$host_data->user_to_certificates->isNotEmpty()))
					<div class="host_public_right_pabel">
						<div class="pab_pro_tab">
							<ul class="nav nav-pills" id="pills-tab" role="tablist">
								<li role="presentation">
									<a class="active" id="tab1" data-toggle="pill" href="#tab11" role="tab" aria-controls="tab11" aria-selected="true">Academics</a>
								</li>
							</ul>
							<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="tab11" role="tabpanel" aria-labelledby="tab1">
									<div class="pab_pro_body">
										<div class="academics_li">
											@if(!empty(@$host_data->user_to_certificates))
											@foreach(@$host_data->user_to_certificates as $edu)
											<li>
												<em><img src="{{url('public/frontend/images/edu.png')}}" alt=""></em>
												{{ @$edu->institute_name }}, {{ @$edu->certification_name }}, {{ @$edu->year }}
											</li>
											@endforeach
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						{{-- <div class="host_public_right_pabel guest_review_sec">
							<h4>Guest Review</h4>
							<em class="guest_reti"><small><i class="fa fa-star"></i>4.6</small>124 Ratings</em>
							<div class="guest_review_itam">
								<ul class="guest_review_star">
									<li><img src="{{url('public/frontend/images/gus_rev.png')}}" alt=""> By : John Decosmo</li>
									<li>
										<span>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</span>
									</li>
								</ul>
								<div class="guest_review_taital">
									<span>Excellent Host</span>
									<strong>07th Nov, 2021, 10:00 am</strong>
								</div>
								<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially passagesversions of Lorem Ipsum.</p>
							</div>
							<div class="guest_review_itam">
								<ul class="guest_review_star">
									<li><img src="{{url('public/frontend/images/gus_rev.png')}}" alt=""> By : John Decosmo</li>
									<li>
										<span>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</span>
									</li>
								</ul>
								<div class="guest_review_taital">
									<span>Excellent Host</span>
									<strong>04th Nov, 2021, 10:00 am</strong>
								</div>
								<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially passagesversions of Lorem Ipsum.</p>
							</div>
							<div class="guest_review_itam">
								<ul class="guest_review_star">
									<li><img src="{{url('public/frontend/images/gus_rev.png')}}" alt=""> By : John Decosmo</li>
									<li>
										<span>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</span>
									</li>
								</ul>
								<div class="guest_review_taital">
									<span>Excellent Host</span>
									<strong>10th Nov, 2021, 10:00 am</strong>
								</div>
								<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially passagesversions of Lorem Ipsum.</p>
							</div>
							<div class="guest_review_itam">
								<ul class="guest_review_star">
									<li><img src="{{url('public/frontend/images/gus_rev.png')}}" alt=""> By : John Decosmo</li>
									<li>
										<span>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</span>
									</li>
								</ul>
								<div class="guest_review_taital">
									<span>Excellent Host</span>
									<strong>30th Nov, 2021, 10:00 am</strong>
								</div>
								<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially passagesversions of Lorem Ipsum.</p>
							</div>
							<div class="moretext4" style="display: none;">
								<div class="guest_review_itam">
									<ul class="guest_review_star">
										<li><img src="{{url('public/frontend/images/gus_rev.png')}}" alt=""> By : John Decosmo</li>
										<li>
											<span>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</span>
										</li>
									</ul>
									<div class="guest_review_taital">
										<span>Excellent Host</span>
										<strong>10th Nov, 2021, 10:00 am</strong>
									</div>
									<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially passagesversions of Lorem Ipsum.</p>
								</div>
								<div class="guest_review_itam">
									<ul class="guest_review_star">
										<li><img src="{{url('public/frontend/images/gus_rev.png')}}" alt=""> By : John Decosmo</li>
										<li>
											<span>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</span>
										</li>
									</ul>
									<div class="guest_review_taital">
										<span>Excellent Host</span>
										<strong>30th Nov, 2021, 10:00 am</strong>
									</div>
									<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially passagesversions of Lorem Ipsum.</p>
								</div>
							</div>
							<div class="more_reviw">
								<a href="#ur" class="pg_btn" id="moreless-button4">More Reviews</a>
							</div>
						</div> --}}
					</div>
					@endif
				</div>
			</div>
		</div>
	</section>
	@endsection
	@push('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script>
		$(document).ready(function() {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"autohide": false
			};
			$(".moreless-buttonratitra").click(function() {
				var dataIds = $(this).data('id');
				var elem = $(".moreless-buttonratitra"+dataIds).text();
				if (elem == "Read More +") {
					$(".moreless-buttonratitra"+dataIds).text("Read Less -");
					$(".moretext-ratitra"+dataIds).slideDown();
				} else {
					$(".moreless-buttonratitra"+dataIds).text("Read More +");
					$(".moretext-ratitra"+dataIds).slideUp();
				}
			});
			$(".moreless-buttonratiawd").click(function() {
				var dataIds = $(this).data('id');
				var elem = $(".moreless-buttonratiawd"+dataIds).text();
				if (elem == "Read More +") {
					$(".moreless-buttonratiawd"+dataIds).text("Read Less -");
					$(".moretext-ratiawd"+dataIds).slideDown();
				} else {
					$(".moreless-buttonratiawd"+dataIds).text("Read More +");
					$(".moretext-ratiawd"+dataIds).slideUp();
				}
			});
			$("#moreless-button3").click(function() {
				var elem = $("#moreless-button3").text();
				if (elem == "Read More +") {
					$("#moreless-button3").text("Read Less -");
					$(".moretext3").show();
				} else {
					$("#moreless-button3").text("Read More +");
					$(".moretext3").hide();
				}
			});
		});
		function userToConnect(hostIds){
			if(hostIds != '')
			{
				$.ajax({
					url:"{{ route('request.send.users') }}",
					type:"POST",
					data: {"_token":'{{ csrf_token() }}','to_user_id':hostIds},
					success:function(responce){
						if (responce == 1) {
							toastr.success('Request sent successfully.');
						}
						else if(responce == 0){
							toastr.success('Request already sent for this user.');
						}
					},
					error:function(xhr){
						console.log(xhr);
					}
				})
			}
		}
	</script>

	@endpush
