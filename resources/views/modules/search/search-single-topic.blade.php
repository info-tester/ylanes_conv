@extends('layouts.app')
@section('title','Search Single Topic')
@section('content')
<div class="about_us_ban banner_nn01">
    <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>
<section class="cat_subcat_nr">
    <div class="container">
        <h2>Choose Conversation Category</h2>
        <div class="cat_subcat_slider">
            <div class="owl-carousel owl-theme">
                @if(!empty(CustomHelper::getCategory()->isNotEmpty()))
                @foreach(CustomHelper::getCategory() as $category)
                <div class="item">
                    <a href="javascript:void(0);" id="catery{{ @$category->id }}" class="open_hid_dtop">
                        @if(!empty($category->picture))
                        <img src="{{url('storage/app/public/category_pics/'.@$category->picture)}}" alt="{{ $category->name }}" style="width: 24px; height: 22px;">
                        @else
                        <img src="{{url('public/frontend/images/Relationship.png')}}" alt="{{ $category->name }}">
                        @endif
                        {{ @$category->name }}
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    </a>
                    @if(!empty(CustomHelper::getSubCategory(@$category->id)->isNotEmpty()))
                    <div class="subb_catt01" id="lists{{ @$category->id }}" style="    position: relative;">
                        <ul>
                            @foreach(CustomHelper::getSubCategory(@$category->id) as $subCategory)
                            <li><a href="{{ route('search.page',@$subCategory->slug) }}">{{ @$subCategory->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!--cat_subcat-->
<section class="sortby_areaa new_sorts_area">
    <div class="container">
        <div class="right">
            <span>Sort by</span>
            <select class="form-select" id="order_by" onchange="searchSignleTopic($(this).val());">
                <option value="all" @if(request()->order_by == 'all') selected="" @endif>All</option>
                <option value="today" @if(request()->order_by == 'today') selected="" @endif>Today</option>
                <option value="towmorrow" @if(request()->order_by == 'towmorrow') selected="" @endif>Towmorrow</option>
                <option value="next" @if(request()->order_by == 'next') selected="" @endif>Next 7 days</option>
            </select>
            {{-- <a href="{{ route('create.conversation') }}" class="create"><img src="{{url('public/frontend/images/create.png')}}">Create</a> --}}
            <a href="{{ route('create.conversation') }}" class="create">Create room</a>
            <a href="{{ route('search.single.topic',@$topic->id) }}" class="create create2" style="">Reset</a>
        </div>
    </div>
</section>
<div class="open_for_all_secc pink_backs padb-70 pt-0">
    <div class="container">
        {{-- <div class="open_Heading">
            <div class="pg_hed">
                @if(!empty(@$conversations->isNotEmpty()))
                @if(!empty(@$conversations[0]))
                @if(@$conversations[0]->type == 'OP')
                <h1>Open for all</h1>
                @elseif(@$conversations[0]->type == 'ON')
                <h1>1 - TO - 1 Conversation</h1>
                @elseif(@$conversations[0]->type == 'CO')
                <h1>CONDITIONAL ENTRY</h1>
                @endif
                @endif
                @endif
                <p><span>{{ @$topic->topic_line_1 }} </span>- how o you feel? What all is going through your mind? How are you dealing  with yoursel, your partner and the relationship?</p>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-12">
                <div class="con_de_box" style="background:#ffffff;">
                    <div class="deatils_heads">
                        <h2>
                            <img src="{{url('public/frontend/images/cnd.png')}}"> {{ @$topic->topic_line_1 }}
                        </h2>
                    </div>
                    <p>
                        {{ @$topic->topic_line_2 }}
                    </p>
                </div>
            </div>
        </div>
        <div class="topic-main-div">
            <div class="row">
                @if(!empty(@$conversations->isNotEmpty()))
                @foreach(@$conversations as $conversation)
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="main_Body">
                        <div class="time_sec">
                            <ul>
                                <li class="right_Border">
                                    <img src="{{url('public/frontend/images/watch.png')}}" alt="icon">
                                    {{ date('dS M, Y', strtotime(@$conversation->date)) }}
                                </li>
                                <li class="left_gap">{{ date('h:i A', strtotime(@$conversation->time)) }}</li>
                            </ul>
                            @if(@$conversation->is_full == 'N')
                            @auth
                            @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                            <a href="javascript:void(0);">
                                <img src="{{url('public/frontend/images/double_user.png')}}" alt="icon">REGISTERED
                            </a>
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                <img src="{{url('public/frontend/images/double_user.png')}}" alt="icon">REGISTER
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                <img src="{{url('public/frontend/images/double_user.png')}}" alt="icon">REGISTER
                            </a>
                            @endauth
                            @endif
                        </div>
                        <div class="all_users_lists">
                            @if(!empty(@$conversation->conversationstousers->isNotEmpty()))
                            @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                            @if($keys < 3)
                            @if(!empty(@$conversationstouser->users))
                            <div class="user_List @if($keys == 0 || $keys == 2) shadow_inner @endif">
                                <div class="user_content">
                                    <div class="user">
                                        @if($conversationstouser->is_anonymously == 'N')
                                        @if(!empty(@$conversationstouser->users->image))
                                        <img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}" alt="user">
                                        @else
                                        <img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
                                        @endif
                                        @else
                                        <div class="topic_users" style="padding: 0px; margin: 0px;">
                                            <span class="topp_user_03" style="margin-left: 0px;     border: 1px solid #d7d7d7">
                                                {{ substr(@$conversationstouser->display_name,0,2 )}}
                                            </span>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="users_info_sec">
                                        <div class="users">
                                            <div class="users_info">
                                                @if($conversationstouser->is_anonymously == 'N')
                                                @if(!empty(@$conversationstouser->users->profile_name))
                                                <h2>{{ @$conversationstouser->users->profile_name }}</h2>
                                                @else
                                                <h2>{{ @$conversationstouser->users->first_name }}</h2>
                                                @endif
                                                @else
                                                <h2>{{ @$conversationstouser->display_name }}</h2>
                                                @endif
                                                <div class="heart">
                                                    <img src="{{url('public/frontend/images/heart.png')}}" alt="icon">
                                                    <span>{{ CustomHelper::number_format_short(@$conversationstouser->users->tot_hearts) }} -  ({{ CustomHelper::number_format_short(@$conversationstouser->users->tot_conversation_completed) }})</span>
                                                </div>
                                            </div>
                                            <p class="users_pas"><img src="{{url('public/frontend/images/gryI.png')}}" alt="icon">
                                                {{ substr(@$conversationstouser->conversation_text,0,30) }} @if(substr(@$conversationstouser->conversation_text,30))... @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endif
                            @endforeach
                            @if(@$conversation->conversationstousers->count() > 3)
                            <div class="extra_bodys">
                                <div class="userblogs" id="usersrow{{ @$conversation->id }}" style="display: none;">
                                    @foreach(@$conversation->conversationstousers as $keys1 => $conversationstouser)
                                    @if($keys1 >= 3)
                                    <div class="user_List @if($keys == 3 || $keys == 5) shadow_inner @endif">
                                        <div class="user_content">
                                            <div class="user">
                                                @if($conversationstouser->is_anonymously == 'N')
                                                @if(!empty(@$conversationstouser->users->image))
                                                <img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}" alt="user">
                                                @else
                                                <img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
                                                @endif
                                                @else
                                                <div class="topic_users" style="padding: 0px; margin: 0px;">
                                                    <span class="topp_user_03" style="margin-left: 0px;     border: 1px solid #d7d7d7">
                                                        {{ substr(@$conversationstouser->display_name,0,2 )}}
                                                    </span>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="users_info_sec">
                                                <div class="users">
                                                    <div class="users_info">
                                                        @if($conversationstouser->is_anonymously == 'N')
                                                        @if(!empty(@$conversationstouser->users->profile_name))
                                                        <h2>{{ @$conversationstouser->users->profile_name }}</h2>
                                                        @else
                                                        <h2>{{ @$conversationstouser->users->first_name }}</h2>
                                                        @endif
                                                        @else
                                                        <h2>{{ @$conversationstouser->display_name }}</h2>
                                                        @endif
                                                        <div class="heart">
                                                            <img src="{{url('public/frontend/images/heart.png')}}" alt="icon">
                                                            <span>{{ CustomHelper::number_format_short(@$conversationstouser->users->tot_hearts) }} -  ({{ CustomHelper::number_format_short(@$conversationstouser->users->tot_conversation_completed) }})</span>
                                                        </div>
                                                    </div>
                                                    <p class="users_pas"><img src="{{url('public/frontend/images/gryI.png')}}" alt="icon">
                                                        {{ substr(@$conversationstouser->conversation_text,0,30) }} @if(substr(@$conversationstouser->conversation_text,30))... @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                                <div id="users{{ @$conversation->id }}" class="arrow_uders">
                                    <img src="{{url('public/frontend/images/select-icon.png')}}">
                                </div>
                            </div>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-md-12 text-center pb-4">
                    {{-- <img src="{{ url('public/no-result.png') }}" style="width: 250px; padding-top: 50px; padding-bottom: 25px;">
                    <h3>No results found</h3>
                    <h5>Try different keywords or remove search filters</h5> --}}
                    <h3 class="search_h3">No upcoming room, please create a room and start having a conversation.</h3>
                    <a href="{{ route('create.conversation') }}" class="create_new_upcomming">Create room</a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!--open_for_all-->
@endsection
@push('js')
<script>
    $(document).ready(function() {
        // srarchResult();
        var owl = $('.cat_subcat_slider .owl-carousel');
        owl.owlCarousel({
            margin:8,
            loop:false,
            nav: true,
            items:5,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                500:{
                    items:2,
                },
                640:{
                    items:2,
                },
                767:{
                    items:3,
                },
                1000:{
                    items:4,
                },
                1199:{
                    items:5,
                }
            }
        })
    });
    function searchSignleTopic(value){
        window.location = "{{ route('search.single.topic',@$topic->id) }}?order_by="+value;
    }
</script>

@endpush
