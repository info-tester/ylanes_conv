@extends('layouts.app')
@section('title','Conversation Landing Page')
@section('content')
<div class="about_us_ban banner_nn01">
    <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>
<section class="cat_subcat_nr">
    <div class="container">
        <h2>Choose Conversation Category</h2>
        <div class="cat_subcat_slider">
            <div class="owl-carousel owl-theme">
                @if(!empty(CustomHelper::getCategory()->isNotEmpty()))
                @foreach(CustomHelper::getCategory() as $category)
                <div class="item">
                    <a href="javascript:void(0);" id="catery{{ @$category->id }}" class="open_hid_dtop">
                        @if(!empty($category->picture))
                        <img src="{{url('storage/app/public/category_pics/'.@$category->picture)}}" alt="{{ $category->name }}" style="width: 24px; height: 22px;">
                        @else
                        <img src="{{url('public/frontend/images/Relationship.png')}}" alt="{{ $category->name }}">
                        @endif
                        {{ @$category->name }}
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    </a>
                    @if(!empty(CustomHelper::getSubCategory(@$category->id)->isNotEmpty()))
                    <div class="subb_catt01" id="lists{{ @$category->id }}" style="    position: relative;">
                        <ul>
                            @foreach(CustomHelper::getSubCategory(@$category->id) as $subCategory)
                            <li><a href="{{ route('search.page',@$subCategory->slug) }}">{{ @$subCategory->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!--cat_subcat-->
<section class="sortby_areaa">
    <div class="container">
        <div class="duble_texts">
            <h3>Upcoming Conversations</h3>
        </div>
        <div class="right">
            <span>Sort by</span>
            <select class="form-select" onchange="srarchDetailsResult();" id="order_by">
                <option value="all" @if(@request()->order_by == 'all') selected="" @endif>All</option>
                <option value="today" @if(@request()->order_by == 'today') selected="" @endif>Today</option>
                <option value="towmorrow" @if(@request()->order_by == 'towmorrow') selected="" @endif>Towmorrow</option>
                <option value="next" @if(@request()->order_by == 'next') selected="" @endif>Next 7 days</option>
            </select>
            {{-- <a href="{{ route('create.conversation') }}" class="create"><img src="{{url('public/frontend/images/create.png')}}">Create</a> --}}
            <a href="{{ route('create.conversation') }}" class="create">Create room</a>
        </div>
    </div>
</section>
<!--open_for_all-->
<div class="open_for_all_secc for_bg01">
    <div class="container">
        <div class="pg_hed">
            <h1>Standard</h1>
            <p>Standard rooms are open for all - get filled on a first come first serve basis. </p>
            <a href="{{ route('see.all.topic','OP') }}">See all topic <img src="{{url('public/frontend/images/rm01.PNG')}}" alt=""></a>
        </div>
        <div class="open_for_all for_slider_id01">
            <div class="owl-carousel owl-theme">
                @if(@$conversations_open->isNotEmpty())
                @foreach(@$conversations_open as $conversation)
                <div class="item">
                    <div class="topic_info">
                        <h2><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="font-size: 19px; font-weight: 500;">
                            {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif + --}}
                            {{ @$conversation->topic->topic_line_1 }}
                        </a>
                        </h2>
                        <p class="con_paras"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,137) }} </a></p>
                        <div class="topic_users">
                            @if(@$conversation->conversationstousers->isNotEmpty())
                            @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                            @if($keys <= 2)
                            @if(!empty(@$conversationstouser->users))
                            @if($conversationstouser->is_anonymously == 'N')
                            @if(!empty(@$conversationstouser->users->image))
                            <span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}">@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</span>
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->users->profile_name,0,2 )}}
                                @if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
                            </span>
                            @endif
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->display_name,0,2 )}}
                                @if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
                            </span>
                            @endif
                            @endif
                            @endif
                            @endforeach
                            @endif
                            @if(@$conversation->is_full == 'N')
                            @auth
                            @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                            @if((strtotime(date('Y-m-d H:i:s').' + 5 minutes') > strtotime($conversation->date.' '.$conversation->time )))
                            <a href="javascript:void(0);"  data-conversationId="{{@$conversation->id}}" class="join_call_link">
                                Join Call
                            </a>
                            @else
                            <a href="javascript:void(0);">
                                REGISTERED
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                REGISTER
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                REGISTER
                            </a>
                            @endauth
                            @endif
                            <div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<div class="open_for_all_secc">
    <div class="container for_realativ for_bg02">
        <div class="pg_hed pg_hed002">
            <h1 style="text-align: center !important;">One-On-One Conversation</h1>
            <p>One-on-one rooms enable intimate conversations with another guest.</p>
            <a href="{{ route('see.all.topic','ON') }}">See all topic <img src="{{url('public/frontend/images/rm01.PNG')}}" alt=""></a>
        </div>
        <div class="open_for_all for_slider_id01">
            <div class="owl-carousel owl-theme">
                @if(@$conversations_one->isNotEmpty())
                @foreach(@$conversations_one as $conversation)
                <div class="item">
                    <div class="topic_info adjj_001">
                        <h2><a href="{{ route('conversation.details.page',@$conversation->id) }}"  style="font-size: 19px; font-weight: 500;">
                            {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif + --}}
                            {{ @$conversation->topic->topic_line_1 }}
                        </a></h2>
                        <p class="con_paras"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,137) }} </a></p>
                        <div class="topic_users new_all_tops">
                            @if(@$conversation->conversationstousers->isNotEmpty())
                            @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                            @if($keys <= 2)
                            @if(!empty(@$conversationstouser->users))
                            @if($conversationstouser->is_anonymously == 'N')
                            @if(!empty(@$conversationstouser->users->image))
                            <span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}">@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</span>
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->users->profile_name,0,2 )}}
                                @if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
                            </span>
                            @endif
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->display_name,0,2 )}}
                                @if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
                            </span>
                            @endif
                            @endif
                            @endif
                            @endforeach
                            @endif
                        </div>
                        <div class="all_conver forr_sado">
                            <p>{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></p>
                            @if(@$conversation->is_full == 'N')
                            @auth
                            @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                            @if((strtotime(date('Y-m-d H:i:s').' + 5 minutes') > strtotime($conversation->date.' '.$conversation->time )))
                            <a href="javascript:void(0);"  data-conversationId="{{@$conversation->id}}" class="join_call_link">
                                Join Call
                            </a>
                            @else
                            <a href="javascript:void(0);">
                                REGISTERED
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');" class="fr_obtn">
                                REGISTER
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');" class="fr_obtn">
                                REGISTER
                            </a>
                            @endauth
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<div class="open_for_all_secc @if(@$conversations_cond->isNotEmpty()) for_bg01 @endif">
    <div class="container">
        <div class="pg_hed">
            <h1>Premium</h1>
            <p>Premium rooms have Premium entry - a host can choose who to allow into the room.</p>
            <a href="{{ route('see.all.topic','CO') }}">See all topic <img src="{{url('public/frontend/images/rm01.PNG')}}" alt=""></a>
        </div>
        <div class="open_for_all for_slider_id01">
            <div class="owl-carousel owl-theme">
                @if(@$conversations_cond->isNotEmpty())
                @foreach(@$conversations_cond as $conversation)
                <div class="item">
                    <div class="topic_info">
                        <h2><a href="{{ route('conversation.details.page',@$conversation->id) }}"  style="font-size: 19px; font-weight: 500;">
                            {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif + --}}
                            {{ @$conversation->topic->topic_line_1 }}
                        </a></h2>
                        <p class="con_paras"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,137) }} </a></p>
                        <div class="topic_users">
                            @if(@$conversation->conversationstousers->isNotEmpty())
                            @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                            @if($keys <= 2)
                            @if(!empty(@$conversationstouser->users))
                            @if($conversationstouser->is_anonymously == 'N')
                            @if(!empty(@$conversationstouser->users->image))
                            <span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}">@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</span>
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->users->profile_name,0,2 )}}
                                @if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
                            </span>
                            @endif
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->display_name,0,2 )}}
                                @if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
                            </span>
                            @endif
                            @endif
                            @endif
                            @endforeach
                            @endif
                            @if(@$conversation->is_full == 'N')
                            @auth
                            @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                            @if((strtotime(date('Y-m-d H:i:s').' + 5 minutes') > strtotime($conversation->date.' '.$conversation->time )))
                            <a href="javascript:void(0);"  data-conversationId="{{@$conversation->id}}" class="join_call_link">
                                Join Call
                            </a>
                            @else
                            <a href="javascript:void(0);">
                                REGISTERED
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                REGISTER
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                REGISTER
                            </a>
                            @endauth
                            @endif
                            <div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal popup_list sign_popup" id="videoCallJoinConfirmation">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body rule">
                    <div class="singup_right_inr">
                        <h1>Enter Room </h1>
                        <form id="videoCallJoinConfirmationform">
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <h4>Rules Of Engagement</h4>
                                    </div>
                                    <div class="col-sm-12">
                                        <ul style="list-style: decimal; padding: 1px 0px 0px 20px;">
                                            <li class="popupbulates"> Speak with authenticity (Try to speak in  “I” statements)</li>
                                            <li class="popupbulates">Listen with intent (Try to understand; avoid judgements)</li>
                                            <li class="popupbulates">Engage with respect </li>
                                            <li class="popupbulates">Maintain confidentiality</li>
                                            <li class="popupbulates">Avoid advising (Share relevant  experiences, if any)</li>
                                        </ul>
                                        <div class="category-ul">
                                            <input type="checkbox" name="accept_terms" id="agree-video" value="Y" class="required">
                                            <label for="agree-video" >I agree</label>
                                        </div>
                                        <label id="accept_terms-error" class="error" for="accept_terms" style="display: none;">This field is required.</label>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <button class="pg_btn" >Enter</button>
                                    {{-- <a class="pg_btn" href="javascript:void(0);" >Enter</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(document).ready(function() {
        var owl = $('.cat_subcat_slider .owl-carousel');
        owl.owlCarousel({
            margin:8,
            loop:false,
            nav: true,
            items:5,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                500:{
                    items:2,
                },
                640:{
                    items:2,
                },
                767:{
                    items:3,
                },
                1000:{
                    items:4,
                },
                1199:{
                    items:5,
                }
            }
        })
    })
</script>
<script>
    $(document).ready(function() {
        var owl = $('.for_slider_id01 .owl-carousel');
        owl.owlCarousel({
            margin: 5,
            nav: false,
            loop:false,
            dot:true,
            items:4,
            autoplay: false,
            responsiveClass:true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                767: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1199: {
                    items: 3
                },
                1200: {
                    items:3
                }
            }
        })
    })
    function srarchDetailsResult(){
        var order_by = $('#order_by').val();
        url = "{{ route('landing.con.page') }}?order_by="+order_by;
        window.location.replace(url);
    }
</script>
<script>
    $('body').on('click', '.join_call_link', function() {
        console.log($(this).data('conversationid'))
        var conversationId= $(this).data('conversationid');
        $('#videoCallJoinConfirmationform').attr('action', '{{route('join.video.call')}}/'+conversationId)
        $('#videoCallJoinConfirmation').modal('show');
    })
    $('#videoCallJoinConfirmationform').validate({
        ignore: [],
        rules: {
            accept_terms:{
                required:true,
            },
        },
        messages: {
            accept_terms:{
                required:'Please agree all rule',
            },
        },
    });
</script>
@endpush
