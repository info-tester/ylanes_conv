<div class="row">
    @if(@$conversations->isNotEmpty() && @$topicData )
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 open_for_all mt-3">
        @foreach(@$topicData as $topic)
        <div class="row">
            <div class="col-md-12">
                <div class="con_de_box" style="background:#ffffff;">
                    <div class="deatils_heads">
                        <h2>
                            <img src="{{url('public/frontend/images/cnd.png')}}"> {{ @$topic->topic_line_1 }}
                        </h2>
                    </div>
                    <p class="topic_p">
                        {{ @$topic->topic_line_2 }}
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 open_for_all mt-3">
        <div class="row">
            @foreach(@$conversations as $conversation)
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 open_for_all mt-3">
                <div class="topic_info">
                    <h2><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #1b1f25; font-size: 19px; font-weight: 500;">
                        {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif +</a> --}}
                        {{ @$conversation->topic->topic_line_1 }} </a>
                    </h2>
                    <p class="con_paras"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,150) }} </a></p>
                    <div class="topic_users">
                        @php
                        $i=0;
                        @endphp
                        @if(@$conversation->conversationstousers->isNotEmpty())
                        @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                        @if($keys < 2)
                        @if(!empty(@$conversationstouser->users))
                        @if($conversationstouser->is_anonymously == 'N')
                        @if(!empty(@$conversationstouser->users->image))
                        <span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}"></span>
                        @else
                        <span class="topp_user_0{{ $keys+1 }}"><img src="{{url('public/frontend/images/default_pic.png')}}"></span>
                        @endif
                        @else
                        <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                            {{ substr(@$conversationstouser->display_name,0,2 )}}
                        </span>
                        @endif
                        @endif
                        @else
                        @php
                        $i++;
                        @endphp
                        @endif
                        @endforeach
                        @if($i > 0)
                        <span class="topp_user_03">+{{ $i }}</span>
                        @endif
                        @endif
                        @if(@$conversation->is_full == 'N')
                        @auth
                        @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                        <a href="javascript:void(0);">
                            REGISTERED
                        </a>
                        @else
                        <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                            REGISTER
                        </a>
                        @endif
                        @else
                        <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                            REGISTER
                        </a>
                        @endauth
                        @endif
                        <div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
                    </div>
                    {{-- @if(!empty(@$conversation->conversationstouserDef))
                    @if(!empty(@$conversation->conversationstouserDef->users))
                    <div class="users_infoo1">
                        <div class="users_nmm">
                            @if(@$conversation->conversationstouserDef->is_anonymously == 'N')
                            @if(!empty(@$conversation->conversationstouserDef2->users->profile_name))
                            {{ substr(@$conversation->conversationstouserDef2->users->profile_name,0,2) }}
                            @else
                            {{ substr(@$conversation->conversationstouserDef2->users->first_name,0,1) }}{{ substr(@$conversation->conversationstouserDef2->users->last_name,0,1) }}
                            @endif
                            @else
                            {{ substr(@$conversation->conversationstouserDef->display_name,0,2 )}}
                            @endif
                        </div>
                        <div class="user_innfoo">
                            <span><i class="fa fa-heart" aria-hidden="true"></i> 0 - (0)</span>
                            <p><img src="{{url('public/frontend/images/i.png')}}"> {{ substr(@$conversation->conversationstouserDef->conversation_text,0,20) }} @if(substr(@$conversation->conversationstouserDef->conversation_text,20))... @endif</p>
                        </div>
                    </div>
                    @endif
                    @else
                    @if(!empty(@$conversation->conversationstouserDef2->users))
                    <div class="users_infoo1">
                        <div class="users_nmm">
                            @if(@$conversation->conversationstouserDef2->is_anonymously == 'N')
                            @if(!empty(@$conversation->conversationstouserDef2->users->profile_name))
                            {{ substr(@$conversation->conversationstouserDef2->users->profile_name,0,2) }}
                            @else
                            {{ substr(@$conversation->conversationstouserDef2->users->first_name,0,1) }}{{ substr(@$conversation->conversationstouserDef2->users->last_name,0,1) }}
                            @endif
                            @else
                            {{ substr(@$conversation->conversationstouserDef2->display_name,0,2 )}}
                            @endif
                        </div>
                        <div class="user_innfoo">
                            <span><i class="fa fa-heart" aria-hidden="true"></i> 0 - (0)</span>
                            <p><img src="{{url('public/frontend/images/i.png')}}">{{ substr(@$conversation->conversationstouserDef2->conversation_text,0,20) }} @if(substr(@$conversation->conversationstouserDef2->conversation_text,20))... @endif</p>
                        </div>
                    </div>
                    @endif
                    @endif
                    <div class="all_conver">
                        <p><img src="{{url('public/frontend/images/fire.png')}}"> {{ @$conversation->conversation->count() }} Conversations</p>
                        <a href="{{ route('search.single.topic',@$conversation->topic_id) }}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div> --}}
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @elseif(@$conversations->isNotEmpty())
        @foreach(@$conversations as $conversation)
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 open_for_all mt-3">
            <div class="topic_info">
                <h2><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #1b1f25; font-size: 19px; font-weight: 500;">
                    {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif + --}}
                    {{ @$conversation->topic->topic_line_1,0,20 }}
                </a></h2>
                <p class="con_paras"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,150) }} </a></p>
                <div class="topic_users">
                    @php
                    $i=0;
                    @endphp
                    @if(@$conversation->conversationstousers->isNotEmpty())
                    @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                    @if($keys < 2)
                    @if(!empty(@$conversationstouser->users))
                    @if($conversationstouser->is_anonymously == 'N')
                    @if(!empty(@$conversationstouser->users->image))
                    <span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}"></span>
                    @else
                    <span class="topp_user_0{{ $keys+1 }}"><img src="{{url('public/frontend/images/default_pic.png')}}"></span>
                    @endif
                    @else
                    <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                        {{ substr(@$conversationstouser->display_name,0,2 )}}
                    </span>
                    @endif
                    @endif
                    @else
                    @php
                    $i++;
                    @endphp
                    @endif
                    @endforeach
                    @if($i > 0)
                    <span class="topp_user_03">+{{ $i }}</span>
                    @endif
                    @endif
                    @if(@$conversation->is_full == 'N')
                    @auth
                    @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                    <a href="javascript:void(0);">
                        REGISTERED
                    </a>
                    @else
                    <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                        REGISTER
                    </a>
                    @endif
                    @else
                    <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                        REGISTER
                    </a>
                    @endauth
                    @endif
                    <div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
                </div>

            </div>
        </div>
        @endforeach
    @else
    <div class="col-md-12 text-center pb-4">
        {{-- <img src="{{ url('public/no-result.png') }}" style="width: 250px; padding-top: 50px; padding-bottom: 25px;"> --}}
        {{-- <h3>No results found</h3>
        <h5>Try different keywords or remove search filters</h5> --}}
        <h3 class="search_h3">No upcoming room, please create a room and start having a conversation.</h3>
        <a href="{{ route('create.conversation') }}" class="create_new_upcomming">Create room</a>
        @if(count(@$topicData)>0 )
        <h3 class="search_h3_2">You can create conversation on the following topics</h3>
        @endif
    </div>
    @foreach(@$topicData as $topic)
    <div class="row">
        <div class="col-md-12">
            <div class="con_de_box" style="background:#ffffff;">
                <div class="deatils_heads">
                    <h2>
                        <img src="{{url('public/frontend/images/cnd.png')}}"> {{ @$topic->topic_line_1 }}
                    </h2>
                </div>
                <p class="topic_p">
                    {{ @$topic->topic_line_2 }}
                </p>
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div>
@if(@$conversations->isNotEmpty())
@if(@$conversations->total() != @$conversations->count())
<div class="row">
    <div class="col-sm-12">
        <div class="loader_btns">
            <a href="javascript:void(0);" onclick="srarchResult('{{ @$conversations->perPage() + 9 }}');"><img src="{{url('public/frontend/images/loaders.png')}}"> Load More</a>
        </div>
    </div>
</div>
@endif
@endif
