@extends('layouts.app')
@section('title','See All Topic')
@section('content')
<div class="about_us_ban banner_nn01">
    <img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>
<section class="cat_subcat_nr">
    <div class="container">
        <h2>Choose Conversation Category</h2>
        <div class="cat_subcat_slider">
            <div class="owl-carousel owl-theme">
                @if(!empty(CustomHelper::getCategory()->isNotEmpty()))
                @foreach(CustomHelper::getCategory() as $category)
                <div class="item">
                    <a href="javascript:void(0);" id="catery{{ @$category->id }}" class="open_hid_dtop">
                        @if(!empty($category->picture))
                        <img src="{{url('storage/app/public/category_pics/'.@$category->picture)}}" alt="{{ $category->name }}" style="width: 24px; height: 22px;">
                        @else
                        <img src="{{url('public/frontend/images/Relationship.png')}}" alt="{{ $category->name }}">
                        @endif
                        {{ @$category->name }}
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    </a>
                    @if(!empty(CustomHelper::getSubCategory(@$category->id)->isNotEmpty()))
                    <div class="subb_catt01" id="lists{{ @$category->id }}" style="    position: relative;">
                        <ul>
                            @foreach(CustomHelper::getSubCategory(@$category->id) as $subCategory)
                            <li><a href="{{ route('search.page',@$subCategory->slug) }}">{{ @$subCategory->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!--cat_subcat-->
<section class="sortby_areaa">
    <div class="container">
        @if(!empty(@$con_type))
        @if(@$con_type == 'OP')
        <div class="duble_texts">
            <h3>STANDARD</h3>
        </div>
        @elseif(@$con_type == 'ON')
        <div class="duble_texts">
            <h3>One-On-One CONVERSATION</h3>
        </div>
        @elseif(@$con_type == 'CO')
        <div class="duble_texts">
            <h3>PREMIUM</h3>
        </div>
        @endif
        @endif
        <div class="right">
            <span>Sort by</span>
            <select class="form-select" onchange="srarchDetailsResult();" id="order_by">
                <option value="all" @if(@request()->order_by == 'all') selected="" @endif>All</option>
                <option value="today" @if(@request()->order_by == 'today') selected="" @endif>Today</option>
                <option value="towmorrow" @if(@request()->order_by == 'towmorrow') selected="" @endif>Towmorrow</option>
                <option value="next" @if(@request()->order_by == 'next') selected="" @endif>Next 7 days</option>
            </select>
            {{-- <a href="{{ route('create.conversation') }}" class="create"><img src="{{url('public/frontend/images/create.png')}}">Create</a> --}}
            <a href="{{ route('create.conversation') }}" class="create">Create room</a>
            {{-- <a href="{{ route('search.page') }}" class="create create2" style="">Reset</a> --}}
        </div>
    </div>
</section>
<!--open_for_all-->
<div class="open_for_all_secc for_bg01 padb-70">
    <div class="container">
        <div class="open_for_all">
            <div class="row">
                @if(@$conversations->isNotEmpty())
                @foreach(@$conversations as $conversation)
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 open_for_all mt-3">
                    <div class="topic_info">
                        <h2><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #1b1f25; font-size: 19px; font-weight: 500;">
                            {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif + --}}
                            {{ @$conversation->topic->topic_line_1 }}
                        </a>
                        </h2>
                        <p class="con_paras"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,150) }} </a></p>
                        <div class="topic_users">
                            @php
                            $i=0;
                            @endphp
                            @if(@$conversation->conversationstousers->isNotEmpty())
                            @foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
                            @if($keys < 2)
                            @if(!empty(@$conversationstouser->users))
                            @if($conversationstouser->is_anonymously == 'N')
                            @if(!empty(@$conversationstouser->users->image))
                            <span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}"></span>
                            @else
                            <span class="topp_user_0{{ $keys+1 }}"><img src="{{url('public/frontend/images/default_pic.png')}}"></span>
                            @endif
                            @else
                            <span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
                                {{ substr(@$conversationstouser->display_name,0,2 )}}
                            </span>
                            @endif
                            @endif
                            @else
                            @php
                            $i++;
                            @endphp
                            @endif
                            @endforeach
                            @if($i > 0)
                            <span class="topp_user_03">+{{ $i }}</span>
                            @endif
                            @endif
                            @if(@$conversation->is_full == 'N')
                            @auth
                            @if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
                            <a href="javascript:void(0);">
                                REGISTERED
                            </a>
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                REGISTER
                            </a>
                            @endif
                            @else
                            <a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
                                REGISTER
                            </a>
                            @endauth
                            @endif
                            <div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-md-12 text-center  pb-4">
                    {{-- <img src="{{ url('public/no-result.png') }}" style="width: 250px; padding-top: 50px; padding-bottom: 25px;"> --}}
                    {{-- <h3>No results found</h3>
                    <h5>Try different keywords or remove search filters</h5> --}}
                    <h3 class="search_h3">No upcoming room, please create a room and start having a conversation.</h3>
                     <a href="{{ route('create.conversation') }}" class="create_new_upcomming">Create Room</a>
                </div>
                @endif
            </div>
            @if(@$conversations->isNotEmpty())
            @if(@$conversations->total() != @$conversations->count())
            <div class="row" id="loadMore">
                <div class="col-sm-12">
                    <div class="loader_btns">
                        <a href="{{ route('see.all.topic',@$con_type) }}?per_page={{ @$conversations->perPage() + 9 }}&order_by={{ @request()->order_by }}#loadMore"><img src="{{url('public/frontend/images/loaders.png')}}"> Load More</a>
                    </div>
                </div>
            </div>
            @endif
            @endif
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
    $(document).ready(function() {
        var owl = $('.cat_subcat_slider .owl-carousel');
        owl.owlCarousel({
            margin:8,
            loop:false,
            nav: true,
            items:5,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                500:{
                    items:2,
                },
                640:{
                    items:2,
                },
                767:{
                    items:3,
                },
                1000:{
                    items:4,
                },
                1199:{
                    items:5,
                }
            }
        })
    })

    $(document).ready(function(){
        $('.open_hid_dtop').click(function(){
            $('.subb_catt01').hide();
            $(this).next().show();
        });
    });
    function srarchDetailsResult(){
        var order_by = $('#order_by').val();
        var per_page = '{{ @request()->per_page }}';
        url = "{{ route('see.all.topic',@$con_type) }}?per_page="+per_page+"&order_by="+order_by;
        window.location.replace(url);
    }

</script>
<script>
    $(document).ready(function() {
        var owl = $('.for_slider_id01 .owl-carousel');
        owl.owlCarousel({
            margin: 16,
            nav: false,
            loop:true,
            dot:true,
            items:4,
            autoplay: true,
            responsiveClass:true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                767: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1199: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        })
    })
    // function srarchResult(perPage){
    //     $('#search-main-div').hide();
    //     $('#mainloader').show();
    //     var category_search = {{ !empty(@$category_search) ? @$category_search->id : 'null' }};
    //     var sub_category = {{ !empty(@$sub_category) ? @$sub_category->id : 'null' }};
    //     var order_by = $('#order_by').val();
    //     $.ajax({
    //         url:"{{ route('search.results') }}",
    //         type:"GET",
    //         data: {
    //             'keywords':"{{ request()->keywords }}",
    //             'category':category_search,
    //             'sub_category':sub_category,
    //             'order_by':order_by,
    //             'per_page':perPage,
    //         },
    //         success: function(responce){
    //             $('#search-main-div').html(responce);
    //             $('#search-main-div').show();
    //             $('#mainloader').hide();
    //         },
    //         error: function(xhr){
    //             console.log(xhr);
    //         }
    //     });
    // }
</script>

@endpush
