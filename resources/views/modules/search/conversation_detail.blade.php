@extends('layouts.app')
@section('title','Conversation Details Page')

@push('css')
<style>
    .card-wrapper {
        border: 1px dashed #adadad;
        padding: 10px 5px;
        border-radius: 4px;
        box-shadow: 1px 3px 8px 0px #00000063;
        /* display: none; */
    }

    .sk-chase {
        width: 70px;
        height: 70px;
        position: relative;
        animation: sk-chase 2.5s infinite linear both;
    }

    .sk-chase-dot {
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        top: 0;
        animation: sk-chase-dot 2.0s infinite ease-in-out both;
    }

    .sk-chase-dot:before {
        content: '';
        display: block;
        width: 25%;
        height: 25%;
        background-color: #0245c1;
        border-radius: 100%;
        animation: sk-chase-dot-before 2.0s infinite ease-in-out both;
    }

    .sk-chase-dot:nth-child(1) {
        animation-delay: -1.1s;
    }

    .sk-chase-dot:nth-child(2) {
        animation-delay: -1.0s;
    }

    .sk-chase-dot:nth-child(3) {
        animation-delay: -0.9s;
    }

    .sk-chase-dot:nth-child(4) {
        animation-delay: -0.8s;
    }

    .sk-chase-dot:nth-child(5) {
        animation-delay: -0.7s;
    }

    .sk-chase-dot:nth-child(6) {
        animation-delay: -0.6s;
    }

    .sk-chase-dot:nth-child(1):before {
        animation-delay: -1.1s;
    }

    .sk-chase-dot:nth-child(2):before {
        animation-delay: -1.0s;
    }

    .sk-chase-dot:nth-child(3):before {
        animation-delay: -0.9s;
    }

    .sk-chase-dot:nth-child(4):before {
        animation-delay: -0.8s;
    }

    .sk-chase-dot:nth-child(5):before {
        animation-delay: -0.7s;
    }

    .sk-chase-dot:nth-child(6):before {
        animation-delay: -0.6s;
    }

    @keyframes sk-chase {
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes sk-chase-dot {

        80%,
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes sk-chase-dot-before {
        50% {
            transform: scale(0.4);
        }

        100%,
        0% {
            transform: scale(1.0);
        }
    }

    .container-sk-chase {
        height: 100vh;
        width: 100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        background: #00000075;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 30;
    }
    .text-danger{
        margin: 8px 5px 14px;
        float: left;
        color: red !important;
        font: 400 16px/20px 'Roboto', sans-serif;
    }
    .img_vip{
        width: 40px;
        height: 40px;
        object-fit: cover;
        border-radius: 100%;
    }
    .main-center-div{
        padding: 30px !important;
    }
</style>

@endpush
@section('content')
<div class="about_us_ban banner_nn01">
	<img src="{{url('public/frontend/images/top_banner.png')}}" alt="">
</div>
<div class="conve_detailss">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="con_de_box">
					<div class="deatils_heads">
						<p style="width: 100%; padding-left: 35px; font-weight: 500;">{{ @$converstion_details->category->name }} > {{ @$converstion_details->sub_category->name }}</p>
						<h2>
							<img src="{{url('public/frontend/images/cnd.png')}}"> {{ @$converstion_details->topic->topic_line_1 }}
							@if(@$converstion_details->is_full == 'N')
							@auth
							@if(in_array(@auth::user()->id, @$converstion_details->conversationstousers->pluck('user_id')->toArray()))
							<a href="javascript:void(0);" style="float: left; margin-left: 15px;background-color: #ffca00;font-size: 14px;font-family: 'Roboto', sans-serif;font-weight: 400;color: #2c2b37;padding: 2px 12px;border-radius: 4px;height: 29px;margin-top: 4px;">
								REGISTERED
							</a>
							@else
							<a href="javascript:void(0);" onclick="joinconversation('{{ @$converstion_details->id }}');" style="float: left;     margin-left: 15px;background-color: #ffca00;font-size: 14px;font-family: 'Roboto', sans-serif;font-weight: 400;color: #2c2b37;padding: 2px 12px;border-radius: 4px;height: 29px;margin-top: 4px;">
								REGISTER
							</a>
							@endif
							@else
							<a href="javascript:void(0);" onclick="joinconversation('{{ @$converstion_details->id }}');" style="float: left; margin-left: 15px;background-color: #ffca00;font-size: 14px;font-family: 'Roboto', sans-serif;font-weight: 400;color: #2c2b37;padding: 2px 12px;border-radius: 4px;height: 29px;margin-top: 4px;">
								REGISTER
							</a>
							@endauth
							@endif
                            {{-- @auth
                            <a href="javascript:void(0);"
                            class="create_request" data-conversion="{{@$converstion_details->id}}"
                            style="float: left; margin-left: 15px;background-color: #ffca00;font-size: 14px;font-family: 'Roboto', sans-serif;font-weight: 400;color: #2c2b37;padding: 2px 12px;border-radius: 4px;height: 29px;margin-top: 4px;">
								Request For Join
							</a>
                            @endauth --}}
						</h2>
						<span><img src="{{url('public/frontend/images/cal1s.png')}}"> {{ date('d/m/Y', strtotime(@$converstion_details->date)) }}  | {{ date('h:i A', strtotime(@$converstion_details->time)) }}</span>
					</div>
					<p>
						{{ @$converstion_details->topic->topic_line_2 }}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="conve_participents">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="parti_headings">
					<h3 style="width: 100%">
						Participants
						<button class="shareLink openshare" type="button" style="background: none;border: 1px solid #000;border-radius: 50%;padding: 8px; float: right; cursor: pointer;">
							<img src="{{url('public/frontend/images/share.png')}}" alt="share" style="width: 20px;">
						</button>
					</h3>
					{{-- <p>{{ @$converstion_details->no_of_participants }}@if(@$converstion_details->type == 'ON')/2 @else/5 @endif</p> --}}
				</div>
				@if($converstion_join_users->isNotEmpty())
				@foreach($converstion_join_users as $keys => $users)
				<div class="partcipents_infos">
					<div class="part_info_top">
						<span>
							@if(@$users->is_anonymously == 'N')
							@if(!empty(@$users->users->image))
							<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank">
								<img src="{{ url('storage/app/public/customer/profile_pics/'.@$users->users->image) }}">
							</a>
							@else
							<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank">
								<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
							</a>
							@endif
							@else
							<div class="topic_users" style="padding: 0px; margin: 0px;">
								<span class="topp_user_03" style="margin-left: 0px;  font-size: 13px; border: 1px solid #d7d7d7; padding: 3px 4px 0 0 !important;">
									{{ substr(@$users->display_name,0,2 )}}
								</span>
							</div>
							@endif
							<em>
								<img src="{{url('public/frontend/images/ticks.png')}}">
							</em>
						</span>
						<div class="info_tops_cate">
							<div class="ad_fb">
								@if(@$users->is_anonymously == 'N')
								@if(!empty(@$users->users->profile_name))
								<h4>
									<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank" style="color:#000;">{{ substr(@$users->users->profile_name,0,7) }}</a>
									@if(@$users->is_creator == 'Y')
									<em style="font-size: 12px;"><b>(Host)</b></em>
									@else
									@if(@$keys == 0)
									<em style="font-size: 12px;"><b>(Host)</b></em>
									@endif
									@endif
								</h4>
								@else
								<h4>
									<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank" style="color:#000;">{{ substr(@$users->users->first_name,0,7) }}</a>
									@if(@$users->is_creator == 'Y')
									<em style="font-size: 12px;"><b>(Host)</b></em>
									@else
									@if(@$keys == 0)
									<em style="font-size: 12px;"><b>(Host)</b></em>
									@endif
									@endif
								</h4>
								@endif
								@else
								<h4>
									{{ @$users->display_name}}
									@if(@$users->is_creator == 'Y')
									<em style="font-size: 12px;"><b>(Host)</b></em>
									@else
									@if(@$keys == 0)
									<em style="font-size: 12px;"><b>(Host)</b></em>
									@endif
									@endif
								</h4>
								@endif
								<p><img src="{{url('public/frontend/images/hea.png')}}"> {{@$users->users->tot_hearts??0}}</p>
							</div>
							<div class="age_infos">
								<p>
									@if(@$users->users->gender)
									@if($users->users->gender=='M')
									Male
									@elseif($users->users->gender=='F')
									Female
									@elseif($users->users->gender=='N')
									Non Binary
									@elseif($users->users->gender=='O')
									Others
									@endif
									@endif
								,</p>
								<p>
									@if(!empty(@$users->users->year_of_birth))
									Age:
									@php
									$age = date('Y') - @$users->users->year_of_birth;
									@endphp
									@if($age < 25)
									Below 25
									@elseif($age >= 25 && $age <= 30)
									25-30
									@elseif($age >= 31 && $age <= 35)
									30-35
									@elseif($age >= 36 && $age <= 40)
									35-40
									@elseif($age >= 41 && $age <= 45)
									40-45
									@elseif($age >= 46 && $age <= 50)
									45-50
									@elseif($age >= 51 && $age <= 55)
									50-55
									@elseif($age >= 56 && $age <= 60)
									55-60
									@elseif($age >= 61)
									over 60
									@endif
									@endif
								</p>
							</div>
						</div>
					</div>
					<div class="part_info_btms">
						<p>{{ @$users->conversation_text }}</p>
					</div>
				</div>
				@endforeach
				@else
				<h2 class="text-dark text-center">No participants here <a href="javascript:void(0);" onclick="joinconversation('{{ @$converstion_details->id }}');" style="color: #ffca00;">click here</a> to join.</h2>
				@endif
			</div>
		</div>
	</div>
</div>
@if(@$conversations_open->isNotEmpty())
<div class="open_for_all_secc">
	<div class="container for_realativ for_bg02">
		<div class="pg_hed pg_hed002">
			<h1>Other conversation with same topic </h1>
			{{-- <a href="#">See all topic <img src="{{ url('public/frontend/images/rm01.PNG')}}" alt=""></a> --}}
		</div>
		<div class="open_for_all for_slider_id01">
			<div class="owl-carousel owl-theme">
				@if(@$conversations_open->isNotEmpty())
				@foreach(@$conversations_open as $conversation)
				<div class="item">
					<div class="topic_info">
						<h2><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="font-size: 19px; font-weight: 500;">
                            {{-- {{ substr(@$conversation->topic->topic_line_1,0,20) }} @if(substr(@$conversation->topic->topic_line_1,20)) ... @endif +</a> --}}
                            {{ @$conversation->topic->topic_line_1 }}</a>
                        </h2>
						<p class="con_paras">{{ substr(@$conversation->topic->topic_line_2,0,150) }} </p>
						<div class="topic_users">
							@if(@$conversation->conversationstousers->isNotEmpty())
							@foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
							@if($keys <= 2)
							@if(!empty(@$conversationstouser->users))
							@if($conversationstouser->is_anonymously == 'N')
							@if(!empty(@$conversationstouser->users->image))
							<span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}">@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</span>
							@else
							<span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
								{{ substr(@$conversationstouser->users->profile_name,0,2 )}}
								@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
							</span>
							@endif
							@else
							<span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
								{{ substr(@$conversationstouser->display_name,0,2 )}}
								@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
							</span>
							@endif
							@endif
							@endif
							@endforeach
							@endif
							@if(@$conversation->is_full == 'N')
							@auth
							@if(in_array(@auth::user()->id, @$conversation->conversationstousers->pluck('user_id')->toArray()))
							<a href="javascript:void(0);">
								REGISTERED
							</a>
							@else
							<a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
								REGISTER
							</a>
							@endif
							@else
							<a href="javascript:void(0);" onclick="joinconversation('{{ @$conversation->id }}');">
								REGISTER
							</a>
							@endauth
							@endif
							<div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
						</div>
					</div>
				</div>
				@endforeach
				@endif
			</div>
		</div>
	</div>
</div>
@endif
<div class="modal fade what_modal" id="myModalshare">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="share_slidars">
					<div class="share_icon_slid">
						<div class="owl-carousel">
							<div class="item">
								<div class="st-custom-button modalshare" data-network="facebook" data-url="{{ url()->current() }}">
									<em><i class="icofont-facebook"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="whatsapp" data-url="{{ url()->current() }}">
									<em><i class="icofont-whatsapp"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="twitter" data-url="{{ url()->current() }}">
									<em><i class="icofont-twitter"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="linkedin" data-url="{{ url()->current() }}">
									<em><i class="icofont-linkedin"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="snapchat" data-url="{{ url()->current() }}">
									<em><i class="icofont-snapchat"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="reddit" data-url="{{ url()->current() }}">
									<em><i class="icofont-reddit"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="pinterest" data-url="{{ url()->current() }}">
									<em><i class="icofont-pinterest"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="email" data-url="{{ url()->current() }}">
									<em><i class="icofont-envelope"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="telegram" data-url="{{ url()->current() }}">
									<em><i class="icofont-paper-plane"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="evernote" data-url="{{ url()->current() }}">
									<em><i class="evernote"><img src="{{url('public/frontend/images/evernote-brands.png')}}"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="googlebookmarks" data-url="{{ url()->current() }}">
									<em><i class="sos_gool_io_c"><img src="{{url('public/frontend/images/social_google.png')}}" alt=""></i></em>
								</div>
							</div>
							<div class="item">
								<div class="modalshare copyLink" onclick="copyToClipboard()">
									<em><i class="icofont-link"></i></em>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- <div class="container-sk-chase loading">
    <div class="sk-chase ">
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
    </div>
</div> --}}
@endsection
@push('js')
@include('layouts.extrescript')
@include('layouts.toaster')
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=#{property?._id}&product=custom-share-buttons" defer></script>
<script>
	function copyToClipboard(text) {
		var inputc = $('#myInput');
		inputc.value = window.location.href;
		inputc.focus();
		inputc.select();
		document.execCommand('copy');
		alert("URL Copied.");
	}
	$('.openshare').click(function(){
		$('#myModalshare').modal('show');
	});
	$(document).ready(function() {
		var owl = $('.share_icon_slid .owl-carousel');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			autoplay: false,
			loop: true,
			responsive: {
				0: {
					items: 5
				},
				350: {
					items: 6
				},
				415: {
					items: 7
				},
				480: {
					items: 5
				},
				520: {
					items: 6
				},
				768: {
					items: 5
				},
				991: {
					items: 6
				},
				1000: {
					items: 6
				}
			}
		})
	})
	$(document).ready(function() {
		var owl = $('.cat_subcat_slider .owl-carousel');
		owl.owlCarousel({
			margin:8,
			loop:true,
			nav: true,
			items:5,
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
				},
				500:{
					items:2,
				},
				640:{
					items:2,
				},
				767:{
					items:3,
				},
				1000:{
					items:4,
				},
				1199:{
					items:5,
				}
			}
		})
	})

</script>
<!--ALL topic-->
<script>
	$(document).ready(function() {
		var owl = $('.for_slider_id01 .owl-carousel');
		owl.owlCarousel({
			margin: 5,
			nav: false,
			loop:true,
			dot:true,
			items:4,
			autoplay: false,
			responsiveClass:true,
			responsive: {
				0: {
					items: 1
				},
				576: {
					items: 2
				},
				767: {
					items: 2
				},
				991: {
					items: 3
				},
				1199: {
					items: 3
				},
				1200: {
					items:3
				}
			}
		})
	})
</script>
<script>
    const loading = (loading) => {
        if (loading) {
            $('.loading').css('display', 'flex');
        } else {
            $('.loading').css('display', 'none');
        }
    }
    $(document).ready(function(){
        loading(false);
        $('.create_request').click(function() {
            var conversionId = $(this).data('conversion');
            var formData = new FormData();
            formData.append("jsonrpc", "2.0");
            formData.append('_token', '{{csrf_token()}}');
            formData.append('c', 'a');
            formData.append('conversionId', conversionId);
            $.ajax({
                url: "{{route('create.request')}}",
                type: 'post',
                data: formData,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                mimeType:"multipart/form-data",
                success: function(response) {
                    console.log(response);
                    loading(true);
                },
                error: function(error) {
                    console.log("error", error);
                }
            });
        })
    })
</script>
@endpush
