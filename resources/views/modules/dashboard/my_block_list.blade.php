@extends('layouts.app')
@section('title','My Block List')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right didlex">
					<h2>My Block List</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div id="home" class="tab-pane fade show in active">
							<div class="my_connect_sec">
								<div class="row">
									@if(@$block_users->isNotEmpty())
									@foreach(@$block_users as $users)
									@if($users->from_user_id == @auth::user()->id)
									@php
									@$users->user = $users->touser;
									@endphp
									@elseif($users->to_user_id == @auth::user()->id)
									@php
									@$users->user = $users->fromuser;
									@endphp
									@endif
									<div class="col-md-6 col-lg-6 col-xl-4 col-sm-6">
										<div class="connects_lists">
											<div class="connect_up new_ups">
												<span>
													@if(!empty(@$users->user->image))
													<img src="{{ url('storage/app/public/customer/profile_pics/'.@$users->user->image) }}" alt="user">
													@else
													<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
													@endif
												</span>
												<div class="con_info">
													<div class="head_use">
														@if(!empty(@$users->user->profile_name))
														<h2>{{ substr(@$users->user->profile_name,0,10) }}</h2>
														@else
														<h2>{{ substr(@$users->user->first_name,0,10) }} {{ substr(@$users->user->last_name,0,5) }}</h2>
														@endif
													</div>
													<ul>
														<li>
															<img src="{{url('public/frontend/images/user_wishlist.png')}}" alt="icon">0 -  (0)
														</li>
													</ul>
													<ul>
														<li>
															<img class="newuser" src="{{url('public/frontend/images/connectnewuser.png')}}" alt="icon">0
														</li>
													</ul>
												</div>
												<div class="block_sec_i">
													<a href="{{ route('my.connect.status.change',[@$users->id,'A']) }}" onclick="return confirm('Do you want to unblock this request?');"  class="block_ic">
														<img src="{{url('public/frontend/images/block.png')}}">
													</a>
												</div>

											</div>
										</div>
									</div>
									@endforeach
									@else
									<div class="col-md-12 text-center">
										<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
										<h3>No request found</h3>
									</div>
									@endif
								</div>
								<div class="row">
									<div class="col-12">
										<div class="expert_pagination">
											@if(@$block_users->total() > 15)
											<div class="pagination_bx">
												{{ @$connect_request_users->links("pagination::bootstrap-4") }}
											</div>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection