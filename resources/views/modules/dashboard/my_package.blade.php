@extends('layouts.app')
@section('title','Purchase History')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>Purchase History <a href="{{ route('purchase.package') }}" class="brw_a" style="cursor: pointer; margin-top: 0px;     position: absolute; right: 15px; height: 32px; font-size: 14px;line-height: 32px;">Purchase Ycoins</a></h2>
				</div>
				<div class="dash-right-inr">
					<div class="dash_top_search arrow_box ycoin_top">
						<form>
							<div class="das_inputss coins_rm002">
								<label>From date</label>
								<div class="dash-d">
									<input type="text" placeholder="Select date" id="datepicker" name="from_date1" value="{{ @request()->from_date1 }}">
									<span class="over_llp_rm1"><img src="{{ url('public/frontend/images/date.png')}}" alt=""></span>
								</div>
							</div>
							<div class="das_inputss coins_rm002">
								<label>To date</label>
								<div class="dash-d">
									<input type="text" placeholder="Select date" id="datepicker2" name="to_date1" value="{{ @request()->to_date1 }}">
									<span class="over_llp_rm1"><img src="{{ url('public/frontend/images/date.png')}}" alt=""></span>
								</div>
							</div>
							<div class="coins_rm003">
								<button class="btns_find"><img src="{{ url('public/frontend/images/fins.png')}}">Find</button>
							</div>
						</form>
					</div>
					<div class="w-100"></div>
					<div class="dashboard_bodys">
						@include('layouts.message')
						@if(@$purchase_packages->isNotEmpty())
						<div class="ReferCon">
							<div class="row">
								<div class="table_01 table">
									<div class="row amnt-tble">
										<div class="cel_area amunt cess">Transaction ID</div>
										<div class="cel_area amunt cess">Date</div>
										<div class="cel_area amunt cess">Paid Coin</div>
										<div class="cel_area amunt cess">Amount (INR) </div>
										<div class="cel_area amunt cess">Free Coin</div>
										<div class="cel_area amunt cess">Total Coin</div>
									</div>

									<!--table row-1-->
									@if(@$purchase_packages->isNotEmpty())
									@foreach(@$purchase_packages as $package)
									<div class="row small_screen2 for_bg_color_01">
										<div class="cel_area amunt-detail cess ">
											<span class="hide_big">Transaction ID</span>
											<span class="sm_size">{{ @$package->getPayment->order_id??'N/A' }}</span>
										</div>
										<div class="cel_area amunt-detail cess ">
											<span class="hide_big">Date</span>
											<span class="sm_size">{{ @$package->created_at->format('d/m/Y') }}</span>
										</div>
										<div class="cel_area amunt-detail cess ">
											<span class="hide_big">Paid Coin</span>
											<span class="sm_size">{{ @$package->conv_paid }}</span>
										</div>
										<div class="cel_area amunt-detail cess">
											<span class="hide_big">Amount (INR)</span>
											<span class="sm_size text-right">₹{{ @$package->amount }}</span>
										</div>
										<div class="cel_area amunt-detail cess">
											<span class="hide_big">Free Coin</span>
											<span class="sm_size">{{ @$package->conv_free }}</span>
										</div>
										<div class="cel_area amunt-detail cess">
											<span class="hide_big">Total Coin</span>
											<span class="sm_size">{{ @$package->conv_paid + @$package->conv_free  }}</span>
										</div>
									</div>
									@endforeach
									@endif
								</div>
							</div>
							@if(@$purchase_packages->total() > 10)
							<div class="pagination_bx text-left" style="border:none;">
								{{ @$purchase_packages->appends(['from_date1' => @request()->from_date1,'to_date1' => @request()->to_date1])->links("pagination::bootstrap-4") }}
							</div>
							@endif
						</div>
						@else
						<div class="col-md-12 text-center">
							<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
							<h3>No data found</h3>
						</div>
						@endif
					</div>
				</div>
			</div>



		</div>
	</div>
</div>
</div>

@endsection
@push('js')
<script>
	$( function() {
		$("#datepicker").datepicker({
			numberOfMonths: 1,
			onSelect: function (selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#datepicker2").datepicker("option", "minDate", dt);
			},
			maxDate: new Date("{{ date('m/d/Y') }}")
		});
		$("#datepicker2").datepicker({
			numberOfMonths: 1,
			onSelect: function (selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#datepicker").datepicker("option", "maxDate", dt);
			},
		});
	} );
</script>
@endpush
