@extends('layouts.app')
@section('title','Purchase Ycoin')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				{{-- <div class="cus-dashboard-right">
					<h2>Purchase Ycoin</h2>
				</div> --}}
                <div class="cus-dashboard-right">
					<h2>Purchase Ycoins <a href="{{ route('user.package') }}" class="brw_a" style="cursor: pointer; margin-top: 0px; position: absolute; right: 15px; height: 32px; font-size: 14px;line-height: 32px;">Purchase History</a></h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="ReferCon">
							<div class="row">
								<div class="table_01 table">
									<div class="row amnt-tble">
										<div class="cel_area amunt cess">Paid Coin</div>
										<div class="cel_area amunt cess">Price (INR) </div>
										<div class="cel_area amunt cess">Free Coin</div>
										<div class="cel_area amunt cess">Total Coin</div>
										<div class="cel_area amunt cess text-right">Action</div>
									</div>

									<!--table row-1-->
									@if(@$packages->isNotEmpty())
									@foreach(@$packages as $package)
									<div class="row small_screen2 for_bg_color_01">
										<div class="cel_area amunt-detail cess ">
											<span class="hide_big">Paid Coin</span>
											<span class="sm_size">{{ @$package->coin_paid }}</span>
										</div>
										<div class="cel_area amunt-detail cess">
											<span class="hide_big">Price (INR)</span>
											<span class="sm_size text-right">₹{{ @$package->price }}</span>
										</div>
										<div class="cel_area amunt-detail cess">
											<span class="hide_big">Free Coin</span>
											<span class="sm_size">{{ @$package->coin_free }}</span>
										</div>
										<div class="cel_area amunt-detail cess">
											<span class="hide_big">Total Coin</span>
											<span class="sm_size">{{ @$package->coin_total }}</span>
										</div>
										<div class="cel_area amunt-detail cess text-right">
											<span class="hide_big">Action</span>
											<span class="sm_size"><a href="{{ route('purchase.package.confirmation',@$package->id) }}" class="brw_a" style="cursor: pointer; margin-top: 0px;  height: 25px; font-size: 14px;line-height: 28px;">Purchase Ycoin</a></span>
										</div>
									</div>
									@endforeach
									@endif
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>



		</div>
	</div>
</div>
</div>

@endsection
@push('js')

@endpush
