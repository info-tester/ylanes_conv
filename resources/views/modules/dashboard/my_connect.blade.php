@extends('layouts.app')
@section('title','My Connent')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right didlex">
					<h2>My Connects</h2>
					<a href="{{ route('my.block.list') }}" class="block_lists">My Block list</a>
				</div>
				<div class="dash-right-inr">
					<div class="dash_conne_tabs_section arrow_box">
						<ul class="nav nav-tabs dash_tabs_in">
							<li class="@if(request()->type == 'A' || @Session::get('type') == 'A') active @else @if(empty(request()->type) && empty(@Session::get('type'))) active @endif @endif"><a data-toggle="tab" href="#home" @if(request()->type == 'A' || @Session::get('type') == 'A') class="active show" @else @if(empty(request()->type) && empty(@Session::get('type'))) class="active show" @endif @endif>All Connects</a></li>
							<li class="@if(request()->type == 'N' || @Session::get('type') == 'B') active @endif"><a data-toggle="tab" href="#menu1" @if(request()->type == 'N' || @Session::get('type') == 'B') class="active show" @endif>Request Received </a></li>
							<li class="@if(request()->type == 'S' || @$type == 'S') active @endif"><a data-toggle="tab" href="#menu2" @if(request()->type == 'S' || @Session::get('type') == 'S') class="active show" @endif>Request Sent</a></li>
						</ul>
						<div class="serac_drop">
							<select onchange="searchRequestByGroup($(this).val());">
								<option value="">Any group</option>
								@if(@$group_list->isNotEmpty())
								@foreach(@$group_list as $group)
								<option value="{{ @$group->id }}" @if(request()->group_id == @$group->id) selected="" @endif>{{ @$group->group_name }}</option>
								@endforeach
								@endif
							</select>
						</div>
						<div class="to_users">
							<img src="{{ url('public/frontend/images/double_user-big.png')}}">
							<div>
								<p>My Connects</p>
								<h3>{{ CustomHelper::number_format_short(@Auth::user()->tot_connects,0) }}</h3>
							</div>
						</div>
					</div>
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="tab-content">
							<div id="home" class="tab-pane fade @if(request()->type == 'A') show in active @else @if(empty(request()->type) && empty(@Session::get('type'))) show in active @endif @endif">
								<div class="my_connect_sec">
									<div class="row">
										@if(@$connect_request_users->isNotEmpty())
										@foreach(@$connect_request_users as $connect)
										@if($connect->from_user_id == @auth::user()->id)
										@php
										@$connect->user = $connect->touser;
										@$groupId = $connect->from_user_group_id;
										@$groupDetails = @$connect->fromGroup;
										@endphp
										@elseif($connect->to_user_id == @auth::user()->id)
										@php
										@$connect->user = $connect->fromuser;
										@$groupId = $connect->to_user_group_id;
										@$groupDetails = @$connect->toGroup;
										@endphp
										@endif
										<div class="col-md-6 col-lg-6 col-xl-4 col-sm-6">
											<div class="connects_lists">
												<div class="connect_up">
													<span>
														<a href="{{ route('public.profile',@$connect->user->id) }}" target="_blank">
															@if(!empty(@$connect->user->image))
															<img src="{{ url('storage/app/public/customer/profile_pics/'.@$connect->user->image) }}" alt="user">
															@else
															<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
															@endif
														</a>
													</span>
													<div class="con_info">
														<div class="head_use">
															<a href="{{ route('public.profile',@$connect->user->id) }}" target="_blank">
																@if(!empty(@$connect->user->profile_name))
																<h2>{{ substr(@$connect->user->profile_name,0,10) }}</h2>
																@else
																<h2>{{ substr(@$connect->user->first_name,0,10) }} {{ substr(@$connect->user->last_name,0,5) }}</h2>
																@endif
															</a>
															<a href="{{ route('my.connect.status.change',[@$connect->id,'BB']) }}" onclick="return confirm('Do you want remove this user from your connect list?');" class="crs_img"><img src="{{url('public/frontend/images/cross.png')}}"></a>
														</div>
														<ul>
															<li>
																<img src="{{url('public/frontend/images/user_wishlist.png')}}" alt="icon">0 -  (0)
															</li>
															<li>
																<img class="newuser" src="{{url('public/frontend/images/connectnewuser.png')}}" alt="icon">0
															</li>
														</ul>
														<p><img src="{{url('public/frontend/images/Colleagues.png')}}" alt="icon">
															@if(empty(@$groupId))
															<i class="fas fa-plus" onclick="createGroupId({{ @$connect->id }});"></i>
															@else
															@if(!empty(@$groupDetails))
															{{ substr(@$groupDetails->group_name,0,15) }}@if(substr(@$groupDetails->group_name,15))...@endif
															@else
															<i class="fas fa-plus" onclick="createGroupId({{ @$connect->id }});"></i>
															@endif
															@endif
														</p>
													</div>
												</div>
												<div class="connect_btm">
													<p>{{ substr(@$connect->topic->topic_line_1,0,40) }}</p>
													{{-- <a href="#" class="send_ic">
														<img src="{{ url('public/frontend/images/send.png')}}">
													</a> --}}
												</div>
											</div>
										</div>
										@endforeach
										@else
										<div class="col-md-12 text-center">
											<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
											<h3>No request found</h3>
										</div>
										@endif
									</div>
									<div class="row">
										<div class="col-12">
											<div class="expert_pagination">
												@if(@$connect_request_users->total() > 15)
												<div class="pagination_bx">
													{{ @$connect_request_users->appends(['type' => 'A'])->links("pagination::bootstrap-4") }}
												</div>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>






							<div id="menu1" class="tab-pane fade @if(request()->type == 'N' || @Session::get('type') == 'B') show in active @endif">
								<div class="my_connect_sec">
									<div class="row">
										@if(@$new_request_users->isNotEmpty())
										@foreach(@$new_request_users as $connect)
										@if($connect->from_user_id == @auth::user()->id)
										@php
										@$connect->user = $connect->touser;
										@$groupId = $connect->from_user_group_id;
										@$groupDetails = @$connect->fromGroup;
										@endphp
										@elseif($connect->to_user_id == @auth::user()->id)
										@php
										@$connect->user = $connect->fromuser;
										@$groupId = $connect->to_user_group_id;
										@$groupDetails = @$connect->toGroup;
										@endphp
										@endif
										<div class="col-md-6 col-lg-6 col-xl-4 col-sm-6">
											<div class="connects_lists">
												<div class="connect_up">
													<span>
														<a href="{{ route('public.profile',@$connect->user->id) }}" target="_blank">
															@if(!empty(@$connect->user->image))
															<img src="{{ url('storage/app/public/customer/profile_pics/'.@$connect->user->image) }}" alt="user">
															@else
															<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
															@endif
														</a>
													</span>
													<div class="con_info">
														<div class="head_use">
															<a href="{{ route('public.profile',@$connect->user->id) }}" target="_blank">
																@if(!empty(@$connect->user->profile_name))
																<h2>{{ substr(@$connect->user->profile_name,0,10) }}</h2>
																@else
																<h2>{{ substr(@$connect->user->first_name,0,10) }} {{ substr(@$connect->user->last_name,0,5) }}</h2>
																@endif
															</a>
														</div>
														<ul>
															<li>
																<img src="{{url('public/frontend/images/user_wishlist.png')}}" alt="icon">0 -  (0)
															</li>
															<li>
																<img class="newuser" src="{{url('public/frontend/images/connectnewuser.png')}}" alt="icon">0
															</li>
														</ul>
														<p><img src="{{url('public/frontend/images/Colleagues.png')}}" alt="icon">
															@if(empty(@$groupId))
															{{-- <i class="fas fa-plus" onclick="createGroupId({{ @$connect->id }});"></i> --}}
															@else
															@if(!empty(@$groupDetails))
															{{ substr(@$groupDetails->group_name,0,15) }}@if(substr(@$groupDetails->group_name,15))...@endif
															@else
															<i class="fas fa-plus" onclick="createGroupId({{ @$connect->id }});"></i>
															@endif
															@endif
														</p>
													</div>
												</div>
												<div class="connect_btm">
													<p>{{ substr(@$connect->topic->topic_line_1,0,40) }}</p>
													<div class="actionn_option">
														<a href="{{ route('my.connect.status.change',[@$connect->id,'N']) }}" onclick="return confirm('Do you want to accept this request?');" style="position: absolute; width: 26px; height: 26px; top: -30px; right: 45px;"><i class="fa fa-check" aria-hidden="true"></i></a>
														<a href="{{ route('my.connect.status.change',[@$connect->id,'B']) }}" onclick="return confirm('Do you want to reject this request?');" style="position: absolute; width: 26px; height: 26px; top: -30px; right: 12px;" class="crs_img"><img src="{{url('public/frontend/images/cross.png')}}"></a>
													</div>
												</div>
											</div>
										</div>
										@endforeach
										@else
										<div class="col-md-12 text-center">
											<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
											<h3>No request found</h3>
										</div>
										@endif
									</div>
									<div class="row">
										<div class="col-12">
											<div class="expert_pagination">
												@if(@$new_request_users->total() > 15)
												<div class="pagination_bx">
													{{ @$new_request_users->appends(['type' => 'N'])->links("pagination::bootstrap-4") }}
												</div>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="menu2" class="tab-pane fade @if(request()->type == 'S' || @Session::get('type') == 'S') show in active @endif">
								<div class="my_connect_sec">
									<div class="row">
										@if(@$send_request_users->isNotEmpty())
										@foreach(@$send_request_users as $connect)
										@if($connect->from_user_id == @auth::user()->id)
										@php
										@$connect->user = $connect->touser;
										@$groupId = $connect->from_user_group_id;
										@$groupDetails = @$connect->fromGroup;
										@endphp
										@elseif($connect->to_user_id == @auth::user()->id)
										@php
										@$connect->user = $connect->fromuser;
										@$groupId = $connect->to_user_group_id;
										@$groupDetails = @$connect->toGroup;
										@endphp
										@endif
										<div class="col-md-6 col-lg-6 col-xl-4 col-sm-6">
											<div class="connects_lists">
												<div class="connect_up">
													<span>
														<a href="{{ route('public.profile',@$connect->user->id) }}" target="_blank">
															@if(!empty(@$connect->user->image))
															<img src="{{ url('storage/app/public/customer/profile_pics/'.@$connect->user->image) }}" alt="user">
															@else
															<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
															@endif
														</a>
													</span>
													<div class="con_info">
														<div class="head_use">
															<a href="{{ route('public.profile',@$connect->user->id) }}" target="_blank">
																@if(!empty(@$connect->user->profile_name))
																<h2>{{ substr(@$connect->user->profile_name,0,10) }}</h2>
																@else
																<h2>{{ substr(@$connect->user->first_name,0,10) }} {{ substr(@$connect->user->last_name,0,5) }}</h2>
																@endif
															</a>
														</div>
														<ul>
															<li>
																<img src="{{url('public/frontend/images/user_wishlist.png')}}" alt="icon">0 -  (0)
															</li>
															<li>
																<img class="newuser" src="{{url('public/frontend/images/connectnewuser.png')}}" alt="icon">0
															</li>
														</ul>
														<p><img src="{{url('public/frontend/images/Colleagues.png')}}" alt="icon">
															@if(empty(@$groupId))
															{{-- <i class="fas fa-plus" onclick="createGroupId({{ @$connect->id }});"></i> --}}
															@else
															@if(!empty(@$groupDetails))
															{{ substr(@$groupDetails->group_name,0,15) }}@if(substr(@$groupDetails->group_name,15))...@endif
															@else
															<i class="fas fa-plus" onclick="createGroupId({{ @$connect->id }});"></i>
															@endif
															@endif
														</p>
													</div>
												</div>
												<div class="connect_btm">
													<p>{{ substr(@$connect->topic->topic_line_1,0,40) }}</p>
													<div class="actionn_option">
														{{-- <a href="{{ route('my.connect.status.change',[@$connect->id,'N']) }}" onclick="return confirm('Do you want to accept this request?');" style="position: absolute; width: 26px; height: 26px; top: -11px; right: 12px;"><i class="fa fa-check" aria-hidden="true"></i></a> --}}
														<a href="{{ route('my.connect.status.change',[@$connect->id,'S']) }}" onclick="return confirm('Do you want to withdraw the connect request?');" class="crs_img" style="position: absolute; width: 26px; height: 26px; top: -30px; right: 12px;"><img src="{{url('public/frontend/images/cross.png')}}"></a>
													</div>
												</div>
											</div>
										</div>
										@endforeach
										@else
										<div class="col-md-12 text-center">
											<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
											<h3>No request found</h3>
										</div>
										@endif
									</div>
									<div class="row">
										<div class="col-12">
											<div class="expert_pagination">
												@if(@$send_request_users->total() > 15)
												<div class="pagination_bx">
													{{ @$send_request_users->appends(['type' => 'S'])->links("pagination::bootstrap-4") }}
												</div>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<div class="modal popup_list sign_popup" id="create_group_popup">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
			</div>
			<div class="modal-body">
				<div class="sign_popup_body">
					<div class="singup_right_inr">
						<h1>Create New Group</h1>
						<form action="{{ route('create.group') }}" method="post" autocomplete="off" id="CreateGroupForm">
							@csrf
							<input type="hidden" name="connect_id">
							<div class="singup_form">
								<div class="row">
									<div class="col-sm-12">
										<div class="input_bx">
											<p>Create New Group</p>
											<input required="" name="group_name" placeholder="Create New Group" maxlength="299" id="group_name">
										</div>
										<label id="group_name-error" class="error" for="group_name" style="display: none;"></label>
									</div>
									@if(@$group_list->isNotEmpty())
									<div class="col-sm-12">
										<h1 style="border-bottom: 1px dotted #e9dddd; text-align: center;margin-top: 10px;">Or</h1>
										<div class="input_bx">
											<p>Select a Group</p>
											<select name="group_id" id="group_id">
												<option value="">Select Group</option>
												@if(@$group_list->isNotEmpty())
												@foreach(@$group_list as $group)
												<option value="{{ @$group->group_name }}">{{ @$group->group_name }}</option>
												@endforeach
												@endif

											</select>
										</div>
									</div>
									@endif
								</div>

								<div class="sing_btn">
									<button class="pg_btn" type="submit">Add to group</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
	function createGroupId(connectIds) {
		if(connectIds != ''){
			$('input[name="connect_id"]').val(connectIds);
			$('#create_group_popup').modal('show');
		}
		return false;
	}
	$(document).ready(function(){
		$('#CreateGroupForm').validate();
		$('select[name="group_id"]').change(function(){
			var ids = $(this).val();
			if(ids != ''){
				$('input[name="group_name"]').attr('required',false);
				$('input[name="group_name"]').val('');
			}
			else{
				$('input[name="group_name"]').attr('required',true);
			}
		});
	});
	function searchRequestByGroup(groupId){
		location.replace('{{ route('my.connect') }}?group_id='+groupId);
	}
</script>
@endpush
