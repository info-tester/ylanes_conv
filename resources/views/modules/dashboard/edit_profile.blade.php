@extends('layouts.app')
@section('title','Edit Profile')
@push('css')
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<style type="text/css">
	.chosen-container{
		width: 100% !important;
	}
	.chosen-container-multi .chosen-choices li.search-field input[type=text]{
		height: 43px !important;
	}
	.chosen-container-multi .chosen-choices li.search-choice {
		margin: 10px 5px 3px 0 !important;
	}
	.modal-header {
		min-height: 0px !important;
	}
	.interestlipart{
		width: 29%;
	}
	@media only screen and (max-width: 991px) {
		.interestlipart{
			width: 45% !important;
		}
		.settingBtn{
			right: 172px !important;
		}
	}
</style>
@endpush
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right didlex">
					<h2 style="width: 100%;">
						Edit Profile
						<a href="{{ route('user.settings') }}" class="brw_a settingBtn" style="cursor: pointer; margin-top: 0px; position: absolute; right: 200px; height: 32px; font-size: 14px;line-height: 32px;"><i class="fa fa-cog"></i> Setting</a>
						<a href="{{ route('public.profile',@Auth::user()->id) }}" target="_blank" class="brw_a" style="cursor: pointer; margin-top: 0px; /*position: absolute;*/ float: right; right: 15px; height: 32px; font-size: 14px;line-height: 32px;"><i class="fa fa-eye"></i> View Profile</a>
					</h2>

				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="profile_sec">
							<form id="editProfile" action="{{ route('store.user.details') }}" method="post" enctype="multipart/form-data">
								@csrf
								<div class="gender_sec">
									<div class="row">
										<div class="col-sm-6 col-md-4 col-lg-4">
											<label for="country">Country <span class="text-danger">*</span></label>
											<select name="country" required="" id="country" disabled="">
												<option value="">Select Country</option>
												@if(@$countries->isNotEmpty())
												@foreach(@$countries as $country)
												<option value="{{ @$country->id }}" @if(@Auth::user()->country == @$country->id) selected="" @endif>{{ @$country->name }} (+{{  @$country->phonecode}})</option>
												@endforeach
												@endif
											</select>
										</div>
										<div class="col-sm-6 col-md-4 col-lg-4">
											<label for="profile_name">Display name <span class="text-danger">*</span></label>
											<input type="text" placeholder="Enter Display name" name="profile_name" id="profile_name" value="{{ @Auth::user()->profile_name }}" required="" style="margin-bottom: 0px;" maxlength="15">
										</div>
										<div class="col-sm-6 col-md-4 col-lg-4">
											<label>Year of birth <span class="text-danger">*</span></label>
											<select name="year_of_birth" required="">
												<option value="">Select Year of birth</option>
												@php
												$currently_selected = date('Y');
												$earliest_year = date('Y') - 75;
												$latest_year = date('Y') - 15;
												foreach ( range( $latest_year, $earliest_year ) as $i ) {
													@endphp
													<option value="{{ @$i }}" @if(@auth::user()->year_of_birth == @$i) selected="" @endif>{{ @$i }}</option>
													@php
												}
												@endphp

											</select>
										</div>
										<div class="col-sm-12 col-md-12 col-lg-12 mt-4">
											<label>Gender <span class="text-danger">*</span></label>
											<div class="row">
												<div class="col-sm-12 col-md-2 col-lg-2 custom_Checkbox">
													<div class="radio-toolbar">
														<input type="radio" id="maLe" name="gender" value="M" @if(@Auth::user()->gender == 'M') checked="" @endif required="">
														<label for="maLe">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Male</label>
													</div>
												</div>
												<div class="col-sm-12 col-md-2 col-lg-2 custom_Checkbox">
													<div class="radio-toolbar">
														<input type="radio" id="feMale" name="gender" value="F" @if(@Auth::user()->gender == 'F') checked="" @endif required="">
														<label for="feMale">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Female</label>
													</div>
												</div>
												<div class="col-sm-12 col-md-3 col-lg-3 custom_Checkbox">
													<div class="radio-toolbar">
														<input type="radio" id="non_binary" name="gender" value="N" @if(@Auth::user()->gender == 'N') checked="" @endif required="">
														<label for="non_binary">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Non Binary</label>
													</div>
												</div>
												<div class="col-sm-12 col-md-2 col-lg-2 custom_Checkbox">
													<div class="radio-toolbar">
														<input type="radio" id="others" name="gender" value="O" @if(@Auth::user()->gender == 'O') checked="" @endif required="">
														<label for="others">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Others</label>
													</div>
												</div>
												<div class="col-sm-12 col-md-4 col-lg-4 custom_Checkbox mt-2">
													<div class="radio-toolbar">
														<input type="radio" id="do_not" name="gender" value="D" @if(@Auth::user()->gender == 'D') checked="" @endif required="">
														<label for="do_not" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Do not wish to disclose</label>
													</div>
												</div>
											</div>
										</div>
										{{-- <div class="col-sm-6 col-md-4 col-lg-4">
											<label>Marital Status</label>
											<select name="marital_status" required="">
												<option value="U" @if(@Auth::user()->marital_status == 'U') selected="" @endif>Unmarried</option>
												<option value="M" @if(@Auth::user()->marital_status == 'M') selected="" @endif>Married</option>
											</select>
										</div> --}}
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h4 class="das_tital">Profile Picture <span class="text-optional">(optional)</span></h4>
										<div class="uplodimg">
											<div class="uplodimgfil">
												<b>Upload profile picture</b>
												<input type="file" name="image" id="file" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" accept="image/*" />
												<label for="file">Upload profile picture <img src="{{ url('public/frontend/images/clickhe.png')}}" alt=""></label>
											</div>
											<div class="uplodimg_pick">
												@if(!empty(@Auth::user()->image))
												<img src="{{ url('storage/app/public/customer/profile_pics/'.@Auth::user()->image) }}" alt="" id="blah" class="afterImageUpload">
												@else
												<img src="{{url('public/frontend/images/default_pic.png')}}" alt="" id="blah" class="afterImageUpload">
												@endif
											</div>
										</div>
									</div>
								</div>
								<div class="mt-3">
									<label class="bold">Interest (Please pick at least any 3) <span class="text-danger">*</span></label>
									<ul class="interest" style="display: inline-block;">
										@if(@$sub_category->isNotEmpty())
										@foreach(@$sub_category as $cate)
										<li style="display: inline-block;" class="interestlipart">
											<label class="InterestBox">{{ @$cate->name }}
												<input type="checkbox" name="interest[]" value="{{ @$cate->id }}" @if(@Auth::user()->user_to_category->isNotEmpty()) @foreach(@Auth::user()->user_to_category as $user_to_categories) @if($user_to_categories->category_id == @$cate->id) checked="" @endif @endforeach @endif>
												<span class="checkmark"></span>
											</label>
										</li>
										@endforeach
										@endif
									</ul>
                                    <label id="interest[]-error" class="error" for="interest[]"></label>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-12">
										<label for="describe_you">3 words your friends use to describe you <span class="text-optional">(optional)</span></label>
										<input type="text" placeholder="3 words your friends use to describe you" id="describe_you" name="describe_you" value="{{ @Auth::user()->describe_you }}" maxlength="100">
                                        <span class="cont-r8">100 characters</span>
									</div>
								</div>
								<div>
									<label class="bold">Your idea of a perfect holiday <span class="text-optional">(optional)</span></label>
									<ul class="interest" style="display: inline-block; margin-bottom: 50px;">
										@if(@$holidays->isNotEmpty())
										@foreach(@$holidays as $holiday)
										<li style="display: inline-block;" class="interestlipart">
											<label class="InterestBox">{{ @$holiday->name }}
												<input type="checkbox" name="holidays[]" value="{{ @$holiday->name }}" @if(!empty(@Auth::user()->holidays)) @if(in_array(@$holiday->name,json_decode(@Auth::user()->holidays))) checked="" @endif @endif>
												<span class="checkmark"></span>
											</label>
										</li>
										@endforeach
										@endif
										{{-- <br> --}}
										<li style="display: inline-block; width: 55%; position: absolute; padding-left: 4px;">
											<label class="InterestBox" style="width: 20%;">Others
												<input type="checkbox" name="other_check_holidays" id="holidays_other" @if(!empty(@Auth::user()->holidays_other)) checked="" @endif>
												<span class="checkmark"></span>
											</label>
											<input type="text" placeholder="Others Holidays" name="holidays_other" value="{{ @Auth::user()->holidays_other }}" maxlength="30" style="@if(empty(@Auth::user()->holidays_other)) display: none; @else display: block; @endif width: 40%; top: -40px; left: 90px; position: relative;" id="holidays_others_area">
											<label id="holidays_others_area-error" class="error" for="holidays_others_area" style=" position: relative;top: -50px;left: 0px; display: none;"></label>
										</li>
									</ul>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-12">
										<label for="problem_solve">One problem in the world you wish you could solve <span class="text-optional">(optional)</span></label>
										<input type="text" placeholder="One problem in the world you wish you could solve" name="problem_solve" id="problem_solve" value="{{ @Auth::user()->problem_solve }}" maxlength="100">
                                        <span class="cont-r8">100 characters</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-12">
										<label for="qualities_you">Qualities you admire the most in others <span class="text-optional">(optional)</span></label>
										<input type="text" placeholder="Qualities you admire the most in others" name="qualities_you" id="qualities_you" value="{{ @Auth::user()->qualities_you }}" maxlength="100">
                                        <span class="cont-r8">100 characters</span>
									</div>
								</div>
								{{-- <div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">
										<label>Profession</label>
										<select class="profession" name="profession" style="text-transform: capitalize;">
											<option value="">Select you profession</option>
											@if(@$professions->isNotEmpty())
											@foreach(@$professions as $profession)
											<option value="{{ @$profession->id }}" @if(@$profession->id == @Auth::user()->profession) selected="" @endif>{{ @$profession->name }}</option>
											@endforeach
											@endif
										</select>
									</div>
								</div> --}}
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-12">
										<label for="about_me">Your life story in brief <span class="text-optional">(optional)</span></label>
										<textarea placeholder="Your life story in brief..." name="about_me" maxlength="2000" id="about_me">{{ @Auth::user()->about_me }}</textarea>
                                        <span class="cont-r8">2000 characters</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h4 class="das_tital">Moderator questions (answer at least one of 2 & 3)</h4>
									</div>
									<div class="col-sm-12 col-md-6 col-lg-6">

										<label class="mt-3">Choose a sub-category of focus (Max 3) <span class="text-optional">(optional)</span></label>
										<select class="chosen-select" name="moderator_sub_category[]" multiple="" data-mdb-filter="true" style="text-transform: capitalize;">
											@if(@$sub_category->isNotEmpty())
											@foreach(@$sub_category as $categor)
											<option value="{{ @$categor->id }}" @if(@Auth::user()->user_to_category_moderator->isNotEmpty()) @foreach(@Auth::user()->user_to_category_moderator as $moderator)
												@if(@$moderator->category_id == @$categor->id) selected="" @endif @endforeach @endif>{{ @$categor->name }}</option>
												@endforeach
												@endif
											</select>
										</div>
										<div class="col-sm-12 col-md-12 col-lg-12">
											<label for="profile_headline" class="mt-3">Profile headline <span class="text-optional">(optional)</span></label>
											<input type="text" placeholder="Profile headline" name="profile_headline" id="profile_headline" value="{{ @Auth::user()->profile_headline }}" maxlength="100">
                                            <span class="cont-r8">100 characters</span>
										</div>
										<div class="col-sm-12 col-md-12 col-lg-12">
											<label for="media_link" class="">Social Media Link <span class="text-optional">(optional)</span></label>
											<input type="text" placeholder="Social Media Link" name="media_link" id="media_link" value="{{ @Auth::user()->media_link }}">
										</div>
									</div>
									<div class="row mt-3">
										<div class="col-sm-12">
											<label>Talk about your personal experience, interest, relevance to at least one of your chosen sub-categories <span class="text-optional">(optional)</span></label>
											<textarea type="text" placeholder="Enter here" name="memories" maxlength="2000">{{ @Auth::user()->memories }}</textarea>
                                            <span class="cont-r8">2000 characters</span>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<label>Talk about your relevant degrees / certificates which have given you a first hand experience of being a good listener (Eg: Certificates in Counselling, Coaching, Healing etc.) 	Institue  <span class="text-optional">(optional)</span></label>
										</div>
										<div class="col-md-4 mt-2">
											<label>Name of institute</label>
											<input type="text" placeholder="Name of institute" id="institute_name" style="margin-bottom: 10px;">
											<label id="institute_name-error" class="error" for="institute_name" style="display: none;"></label>
										</div>
										<div class="col-md-4 mt-2">
											<label>Name of the certification</label>
											<input type="text" placeholder="Name of the certification" id="certification_name" style="margin-bottom: 10px;">
											<label id="certification_name-error" class="error" for="certification_name" style="display: none;"></label>
										</div>
										<div class="col-md-4 mt-2">
											@php
											$years = range(date('Y'), 1990);
											@endphp
											<label>Year of completion</label>
											<select id="year_of_completion" style="margin-bottom: 10px;">
												<option value="">Select year</option>
												@foreach($years as $year)
												<option value="{{ @$year }}">{{ @$year }}</option>
												@endforeach
											</select>
											<label id="year_of_completion-error" class="error" for="year_of_completion" style="display: none;"></label>
										</div>
										<div class="col-md-4">
											<a class="brw_a" style="cursor: pointer; margin-top: 10px;" id="addBtnInstitueData">Add</a>
										</div>
										<div class="col-md-12 mt-3 mb-3">
											<ul id="getUserCertification">

											</ul>
										</div>
									</div>
									<div class="doubleborder"></div>
									<div class="doubleborder"></div>
									<button type="submit">Save All Changes</button>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Crop Image</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form {{-- action="{{ route('store.user.details') }}" method="POST"  --}}enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="row">
							<div class="col-12">
								<div class="croppie-div" style="width: 100%;"></div>
								<input type="hidden" name="profile_picture" id="profile_picture">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<img src="{{ url('public/frontend/images/loader.gif') }}" style=" display: none; width: 45px;" id="uploadLoader">
						<button type="button" class="pg_btn" id="crop-img">Save changes</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endsection
	@push('js')
	<script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>
	<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
	<script type="text/javascript">
		$('#addBtnInstitueData').click(function(){
			var institute_name = $('#institute_name').val();
			var certification_name = $('#certification_name').val();
			var year_of_completion = $('#year_of_completion').val();
			if(certification_name == '' && year_of_completion == '' && institute_name == ''){
				$('#institute_name-error').html('This Field Is Required.');
				$('#institute_name-error').show();
				$('#certification_name-error').html('This Field Is Required.');
				$('#certification_name-error').show();
				$('#year_of_completion-error').html('This Field Is Required.');
				$('#year_of_completion-error').show();
			}
			else if(certification_name == ''){
				$('#certification_name-error').html('This Field Is Required.');
				$('#certification_name-error').show();
			}
			else if(year_of_completion == ''){
				$('#year_of_completion-error').html('This Field Is Required.');
				$('#year_of_completion-error').show();
			}
			else if(institute_name == ''){
				$('#institute_name-error').html('This Field Is Required.');
				$('#institute_name-error').show();
			}
			else{
				$('#certification_name-error').html('');
				$('#certification_name-error').hide();
				$('#year_of_completion-error').html('');
				$('#year_of_completion-error').hide();
				$('#institute_name-error').html('');
				$('#institute_name-error').hide();
				$.ajax({
					url:"{{ route('store.user.certificates') }}",
					type:"POST",
					data:{
						'_token':"{{ csrf_token() }}",
						'institute_name':institute_name,
						'certification_name':certification_name,
						'year':year_of_completion
					},
					success: function(responce){
						console.log(responce);
						if(responce == 0){
							$('#certification_name-error').html('This certification already exists.');
							$('#certification_name-error').show();
						}
						else if(responce == 1){
							$('#institute_name').val('');
							$('#certification_name').val('');
							$('#year_of_completion').prop('selectedIndex',0);
							getUserCertification();
						}
					},
					error: function(xhr){
						console.log(xhr);
					}
				});
			}

		});
		getUserCertification();
		function getUserCertification(){
			$.ajax({
				url:"{{ route('get.user.certificates') }}",
				type:"GET",
				success: function(responce){
					$('#getUserCertification').html(responce);
				},
				error: function(xhr){
					console.log(xhr);
				}
			});
		}
		function deleteCertification(ids){
			if (confirm("Are you sure to delete this certificates?")) {
				$.ajax({
					url:"{{ route('delete.user.certificates') }}"+ids,
					type:"GET",
					success: function(responce){
						if(responce == 1){
							getUserCertification();
						}
					},
					error: function(xhr){
						console.log(xhr);
					}
				});
			}
			else{
				return false;
			}
		}
		$('#holidays_other').click(function(){
			if($(this).prop('checked') == true){
				$('input[name="holidays[]"]').prop('checked', false);
				$('#holidays_others_area').show();
			}
			else{
				$('#holidays_others_area').hide();
				$('input[name="holidays_other"]').val('');
			}
		})
		$('input[name="holidays[]"]').click(function(){
			$('input[name="holidays[]"]').not(this).prop('checked', false);
			$('#holidays_other').prop('checked', false);
			$('#holidays_others_area').hide();
			$('input[name="holidays_other"]').val('');
		})
		jQuery.validator.addMethod("isUrlValid", function(value, element) {
			var urles = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/;
			return this.optional(element) || (urles.test(value));
		}, "Please provide valid website link!");
		$("#editProfile").validate({
			rules : {
            // first_name : {
            //    required : true,
            //    minlength : 2
            // },
            // last_name : {
            //    required : true,
            //    minlength : 2
            // },
            // email : {
            //    required : true,
            //    email : true,
            //    emailonly:true
            // },
            // phone : {
            //    required : true,
            //    digits : true,
            //    minlength:10,
            //    maxlength:10
            // }
            media_link : {
				isUrlValid:true
			},
            'interest[]':{
                    required:true,
                    minlength: 3
                },
        },
        messages : {
            // first_name :{
            //    required : "Please enter your first name",
            //    minlength : "Your first name must consist of at least 2 characters"
            // },
            // last_name :{
            //    required : "Please enter your last name",
            //    minlength : "Your last name must consist of at least 2 characters"
            // },
            // email: {
            //    required: 'Please enter a valid email address.',
            // },
            // phone:{
            //    required: "Please enter your phone no",
            //    digits: "Please enter a valid phone no",
            // }
            'interest[]':{
                    required:'Please pick at least any 3',
                    minlength: 'Please pick at least any 3'
                },

        },
        ignore: [],
    });
		var customIcon = document.createElement('img');
		customIcon.src = '{{ url('public/frontend/images/close.png') }}';
		var customIconMulti = new SelectPure(".multi-select-custom", {
			options: [
			@foreach(@$languages as $lang)
			{
				label: "{{ @$lang->name }}",
				value: "{{ @$lang->id }}",
			},
			@endforeach
			],
			@if(!empty(@auth::user()->language))
			value: [
			@foreach(json_decode(@auth::user()->language) as $user_language)
			"{{ $user_language }}",
			@endforeach
			],
			@endif
			multiple: true,
			inlineIcon: customIcon,
			onChange: value => {
				$('input[name="language"]').val('['+value+']');
			},
			classNames: {
				select: "select-pure__select",
				dropdownShown: "select-pure__select--opened",
				multiselect: "select-pure__select--multiple",
				label: "select-pure__label",
				placeholder: "select-pure__placeholder",
				dropdown: "select-pure__options",
				option: "select-pure__option",
				autocompleteInput: "select-pure__autocomplete",
				selectedLabel: "select-pure__selected-label",
				selectedOption: "select-pure__option--selected",
				placeholderHidden: "select-pure__placeholder--hidden",
				optionHidden: "select-pure__option--hidden",
			}
		});
		var resetCustomMulti = function() {
			customIconMulti.reset();
		};
	</script>
	<script>
		function dataURLtoFile(dataurl, filename) {
			var arr = dataurl.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);
			while(n--){
				u8arr[n] = bstr.charCodeAt(n);
			}
			return new File([u8arr], filename, {type:mime});
		}
		var uploadCrop;
		$(document).ready(function(){
			if($('.type').val()=='C'){
				$(".s_h_hids").slideDown(0);
			} else{
				$(".s_h_hids").slideUp(0);
			}
			$(".ccllk02").click(function(){
				$(".s_h_hids").slideDown();
			});
			$(".ccllk01").click(function(){
				$(".s_h_hids").slideUp();
				$('.cmpy').val('');
			});
			$('#croppie-modal').on('hidden.bs.modal', function() {
				uploadCrop.croppie('destroy');
			});
			$('#crop-img').click(function() {
				uploadCrop.croppie('result', {
					type: 'base64',
					format: 'png'
				}).then(function(base64Str) {
					$('#uploadLoader').show();
					let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
					console.log(file.mozFullPath);
					$('#profile_picture').val(base64Str);
					var reader = new FileReader();
					reader.onload = function(e){
						$('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
					};
					reader.readAsDataURL(file);
					$('.uploadImg').show();
					$.ajax({
						url:"{{ route('store.user.details') }}",
						type:"POST",
						data:{'_token':"{{ csrf_token() }}",'profile_picture':base64Str},
						success:function(responce){
							if(responce){
								$('.afterImageUpload').attr("src", "{{ url('storage/app/public/customer/profile_pics')}}/"+responce);
								$('#croppie-modal').modal('hide');
								$('#uploadLoader').hide();
							}
						}
					})
				});
			});
		});
		$("#file").change(function () {
			$('.uploadImg').html('');
			let files = this.files;
			console.log(files);
			let img  = new Image();
			if (files.length > 0) {
				let exts = ['image/jpeg', 'image/png', 'image/gif'];
				let valid = true;
				$.each(files, function(i, f) {
					if (exts.indexOf(f.type) <= -1) {
						valid = false;
						return false;
					}
				});
				if (! valid) {
					alert('Please choose valid image files (jpeg, png, gif) only.');
					$("#file").val('');
					return false;
				}
				$("#croppie-modal").modal("show");
				uploadCrop = $('.croppie-div').croppie({
					viewport: { width: 300, height: 300, type: 'square' },
					boundary: { width: $(".croppie-div").width(), height: 500 }
				});
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.upload-demo').addClass('ready');
					uploadCrop.croppie('bind', {
						url: e.target.result
					}).then(function(){
						console.log('jQuery bind complete');
					});
				}
				reader.readAsDataURL(this.files[0]);
			}
		});
		$(".chosen-select").chosen({
			no_results_text: "Oops, nothing found!",
			max_selected_options: 3
		})
	</script>
	@endpush
