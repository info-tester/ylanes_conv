@extends('layouts.app')
@section('title','My wallet')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>My wallet</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dash_top_search arrow_box ycoin_top">
						<form>
							<div class="das_inputss coins_rm001">
								<label>Type</label>
								<select name="type">
									<option value="">All</option>
									<option value="IN" @if(@request()->type == 'IN') selected="" @endif>IN</option>
									<option value="OUT" @if(@request()->type == 'OUT') selected="" @endif>OUT</option>
								</select>
							</div>
							<div class="das_inputss coins_rm002">
								<label>From date</label>
								<div class="dash-d">
									<input type="text" placeholder="Select date" id="datepicker" name="from_date1" value="{{ @request()->from_date1 }}">
									<span class="over_llp_rm1"><img src="{{ url('public/frontend/images/date.png')}}" alt=""></span>
								</div>
							</div>
							<div class="das_inputss coins_rm002">
								<label>To date</label>
								<div class="dash-d">
									<input type="text" placeholder="Select date" id="datepicker2" name="to_date1" value="{{ @request()->to_date1 }}">
									<span class="over_llp_rm1"><img src="{{ url('public/frontend/images/date.png')}}" alt=""></span>
								</div>
							</div>   
							<div class="coins_rm003">
								<button class="btns_find"><img src="{{ url('public/frontend/images/fins.png')}}">Find</button>
							</div>
						</form>
					</div>
					<div class="w-100"></div>
					<div class="dashboard_bodys">
						@include('layouts.message')
						@if(@$ycoin_history->isNotEmpty())
						<div class="ReferCon">
							<div class="row">
								<div class="table_01 table">
									<div class="row amnt-tble">
										<div class="cel_area amunt cess">Date</div>
										<div class="cel_area amunt cess">YCoins</div>
										<div class="cel_area amunt cess">Type </div>
										<div class="cel_area amunt cess">Description</div>
										<div class="cel_area amunt cess">Balance</div>
									</div>

									<!--table row-1-->
									@if(@$ycoin_history->isNotEmpty())
									@foreach(@$ycoin_history as $coins)
									<div class="row small_screen2 for_bg_color_01">
										<div class="cel_area amunt-detail cess "> 
											<span class="hide_big">Date</span> 
											<span class="sm_size">{{ @$coins->created_at->format('d/m/Y') }}</span>
										</div>
										<div class="cel_area amunt-detail cess "> 
											<span class="hide_big">YCoins</span> 
											<span class="sm_size">{{ @$coins->coin_free + @$coins->coin_paid }}</span>
										</div>
										<div class="cel_area amunt-detail cess"> 
											<span class="hide_big">Type</span> 
											<span class="sm_size text-right">{{ @$coins->type }}</span>
										</div>
										<div class="cel_area amunt-detail cess"> 
											<span class="hide_big">Description</span> 
											<span class="sm_size">
												@if(@$coins->category == 'C')
												Conversation
												@elseif(@$coins->category == 'S')
												Sign UP
												@elseif(@$coins->category == 'P')
												Ycoin Purchase
												@elseif(@$coins->category == 'R')
												Referral Earning
												@elseif(@$coins->category == 'A')
												Ycoin Bonus
												@endif
											</span>
										</div>
										<div class="cel_area amunt-detail cess"> 
											<span class="hide_big">Balance</span> 
											<span class="sm_size">{{ @$coins->balance_coin_free + @$coins->balance_coin_paid  }}</span>
										</div>
									</div>
									@endforeach
									@endif
								</div>
							</div>
							@if(@$ycoin_history->total() > 10)
							<div class="pagination_bx text-left" style="border:none;">
								{{ @$ycoin_history->appends(['type' => @request()->type,'from_date1' => @request()->from_date1,'to_date1' => @request()->to_date1])->links("pagination::bootstrap-4") }}
							</div>
							@endif
						</div>
						@else
						<div class="col-md-12 text-center">
							<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
							<h3>No data found</h3>
						</div>
						@endif
					</div>
				</div>
			</div>



		</div>
	</div>
</div>
</div>

@endsection
@push('js')
<script>
	$( function() {
		$("#datepicker").datepicker({
			numberOfMonths: 1,
			onSelect: function (selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#datepicker2").datepicker("option", "minDate", dt);
			},
			maxDate: new Date("{{ date('m/d/Y') }}")
		});
		$("#datepicker2").datepicker({
			numberOfMonths: 1,
			onSelect: function (selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#datepicker").datepicker("option", "maxDate", dt);
			},
		});
	} );
</script>
@endpush