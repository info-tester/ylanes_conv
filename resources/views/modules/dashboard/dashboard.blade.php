@extends('layouts.app')
@section('title','Dashboard')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>DASHBOARD</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						<div class="dashbord_headings">
							<h2><img src="{{url('public/frontend/images/infos.png')}}"> Hi, {{ @Auth::user()->profile_name }}</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut laibore dolore magna aliqua.labor consectetur adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua text labor labore et dolore magna aliqua.</p>
						</div>
						<div class="dash_thumbs">
							<div class="dash_thumbs_info">
								<a href="{{ route('upcoming.conversation') }}">
									<span><img src="{{url('public/frontend/images/thm1.png')}}"></span>
									<p>Hearts</p>
									<h3>{{ CustomHelper::number_format_short(@Auth::user()->tot_hearts) }}</h3>
								</a>
							</div>
							<div class="dash_thumbs_info">
								<a href="{{ route('past.conversation') }}">
									<span><img src="{{url('public/frontend/images/thm2.png')}}"></span>
									<p>Conversations</p>
									<h3>{{ CustomHelper::number_format_short(@Auth::user()->tot_conversation_completed) }}</h3>
								</a>
							</div>
							<div class="dash_thumbs_info">
								<a href="{{ route('my.connect') }}">
									<span><img src="{{url('public/frontend/images/thm3.png')}}"></span>
									<p>Connects</p>
									<h3>{{ CustomHelper::number_format_short(@Auth::user()->tot_connects) }}</h3>
								</a>
							</div>
							<div class="dash_thumbs_info">
								<a href="{{ route('referral.earn') }}">
									<span><img src="{{url('public/frontend/images/thm4.png')}}"></span>
									<p>Referral Earning</p>
									<h3>@if(@Auth::user()->userRef->isNotEmpty()){{ CustomHelper::number_format_short(@Auth::user()->userRef->count() * 100) }} @else 0 @endif Ycoins</h3>
								</a>
							</div>
							<div class="dash_thumbs_info">
								<a href="{{ route('ycoin.usage') }}">
									<span><img src="{{url('public/frontend/images/thm4.png')}}"></span>
									<p>Balance</p>
									<h3>{{ CustomHelper::number_format_short(@Auth::user()->conversation_balance_total) }} Ycoins</h3>
								</a>
							</div>
						</div>
						<div class="conver_sections">
							<h1>Conversations of my connects</h1>
							<div class="open_for_all for_slider_id01">
								<div class="owl-carousel owl-theme">
									@if(@$allConversation->isNotEmpty())
									@foreach(@$allConversation as $conversation)
									<div class="item">
										<div class="topic_info">
											<h2><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="font-size: 19px; font-weight: 500;">{{ substr(@$conversation->topic->topic_line_1,0,15) }} @if(substr(@$conversation->topic->topic_line_1,15)) ... @endif +</a></h2>
											<p class="con_paras" style="font-size: 13px;"><a href="{{ route('conversation.details.page',@$conversation->id) }}" style="color: #757676;">{{ substr(@$conversation->topic->topic_line_2,0,137) }} @if(substr(@$conversation->topic->topic_line_2,137)) ... @endif</a></p>
											<div class="topic_users">
												@if(@$conversation->conversationstousers->isNotEmpty())
												@foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
												@if($keys <= 2)
												@if(!empty(@$conversationstouser->users))
												@if($conversationstouser->is_anonymously == 'N')
												@if(!empty(@$conversationstouser->users->image))
												<span class="topp_user_0{{ $keys+1 }}"><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}">@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</span>
												@else
												<span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
													{{ substr(@$conversationstouser->users->profile_name,0,2 )}}
													@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
												</span>
												@endif
												@else
												<span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
													{{ substr(@$conversationstouser->display_name,0,2 )}}
													@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
												</span>
												@endif
												@endif
												@endif
												@endforeach
												@endif
												<div class="topp_date">{{ date('dS M', strtotime(@$conversation->date)) }} <strong>{{ date('h:i A', strtotime(@$conversation->time)) }}</strong></div>
											</div>
										</div>
									</div>
									@endforeach
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script>
	$(document).ready(function() {
		var owl = $('.cat_subcat_slider .owl-carousel');
		owl.owlCarousel({
			margin:8,
			loop:false,
			autoplay: false,
			nav: true,
			items:5,
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
				},
				500:{
					items:2,
				},
				640:{
					items:2,
				},
				767:{
					items:3,
				},
				1000:{
					items:4,
				},
				1199:{
					items:5,
				}
			}
		})
	})

	$(document).ready(function(){
		$('.open_hid_dtop').click(function(){
			$('.subb_catt01').hide();
			$(this).next().show();
		});
	});
</script>

<script>
	 $(document).ready(function() {
        var owl = $('.for_slider_id01 .owl-carousel');
        owl.owlCarousel({
            margin: 5,
            nav: false,
            loop:false,
            dot:true,
            items:4,
            autoplay: false,
            responsiveClass:true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                767: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1199: {
                    items: 3
                },
                1200: {
                    items:3
                }
            }
        })
    })
</script>

@endpush