@extends('layouts.app')
@section('title','Edit Profile')
@push('css')
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<style type="text/css">
	.chosen-container{
		width: 100% !important;
	}
	.chosen-container-multi .chosen-choices li.search-field input[type=text]{
		height: 43px !important;
	}
	.chosen-container-multi .chosen-choices li.search-choice {
		margin: 10px 5px 3px 0 !important;
	}
	.modal-header {
		min-height: 0px !important;
	}
	.interestlipart{
		width: 29%;
	}
	@media only screen and (max-width: 991px) {
		.interestlipart{
			width: 45% !important;
		}
	}
</style>
@endpush
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right didlex">
					<h2>
						Edit Profile
						<a href="{{ route('public.profile',@Auth::user()->id) }}" target="_blank" class="brw_a" style="cursor: pointer; margin-top: 0px;     position: absolute; right: 15px; height: 32px; font-size: 14px;line-height: 32px;"><i class="fa fa-eye"></i> View Profile</a>
					</h2>

				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="profile_sec">
							<form id="editProfile" action="{{ route('store.user.details') }}" method="post" enctype="multipart/form-data">
								@csrf

								<div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">
										<label>Email <a href="javascript:void(0);" class="pull-right"><small style="color: #5c004d;" data-toggle="modal" data-target="#modalupdateemailpopup"><img src="{{ url('public/frontend/images/host_dash2.png') }}" alt="" width="20"> Change</small></a> <small style="float: right; text-transform: lowercase;">{{ substr(@auth::user()->temp_email,0,30) }}@if(!empty(substr(@auth::user()->temp_email,30)))...@endif</small></label>
										<input type="text" placeholder="Enter here.." value="{{ @auth::user()->email }}" name="email" disabled="">
									</div>
									<div class="col-sm-12 col-md-6 col-lg-6">

										<label>Phone No <a href="javascript:void(0);" class="pull-right"><small style="color: #5c004d;" data-toggle="modal" data-target="#modalupdateloginmobile"><img src="{{ url('public/frontend/images/host_dash2.png') }}" alt="" width="20"> Change</small></a> <small style="float: right;">{{ @auth::user()->temp_phone }}</small></label>
										<input type="text" placeholder="Enter here.." value="+{{ @auth::user()->userCountryDetails->phonecode }}" name="phone" disabled="" style="width: 25%;">
										<input type="text" placeholder="Enter here.." value="{{ @auth::user()->phone }}" name="phone" disabled="" style="width: 73%;">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal popup_list sign_popup" id="modalupdateemailpopup">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
			</div>
			<div class="modal-body">
				<div class="sign_popup_body">
					<div class="singup_right_inr">
						<h1>Update Email</h1>
						<p>Please enter your email to continue</p>
						<form id="updateEmailEditProfile" action="{{ route('user.update.check.email.post',@Auth::user()->id) }}" method="post">
							@csrf
							<div class="singup_form">
								<div class="row">
									<div class="col-sm-12">
										<div class="input_bx">
											<input type="email" placeholder="Email address" name="temp_email" required="" id="registerupdateEmail">
											<span class="input_icon"><img src="{{url('public/frontend/images/input_icon2.png')}}" alt=""></span>
											<label id="chck_email_update_rd" class=" error chck_email_update_rd" style="display: none;"></label>
										</div>
									</div>
								</div>

								<div class="sing_btn">
									<a  href="javascript:void(0);" id="updateemailafterLogineditprofile" class="a_popup pg_btn">Continue</a>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal popup_list sign_popup" id="modalupdateloginmobile">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
			</div>
			<div class="modal-body">
				<div class="sign_popup_body">
					<div class="singup_right_inr">
						<h1>Update Mobile</h1>
						<p>Please enter your mobile no to continue</p>
						<form>
							<div class="singup_form">
								<div class="row">

									<div class="col-sm-12">
										<div class="input_bx">
											<select style="width: 23%" required="" name="country_code" id="country_code">
                                                <option value="">Country Code</option>
                                                @foreach(CustomHelper::getCountrycode() as $country_code)
                                                <option value="{{ @$country_code->id }}">{{ @$country_code->name }} (+{{ @$country_code->phonecode }})</option>
                                                @endforeach
                                            </select>
											<input type="text" name="temp_phone" placeholder="Mobile Number" onkeypress="validate();" maxlength="10" style="width: 75%">
											<span class="input_icon"><img src="{{url('public/frontend/images/input_icon3.png')}}" alt=""></span>
											<label id="error_temp_phone" class="error" style="display: none;"></label>
										</div>
									</div>
								</div>

								<div class="sing_btn">
									<a  href="javascript:void(0);" id="sin_mod" class="a_popup pg_btn openUpdateotpbyPhone">Next</a>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal popup_list sign_popup" id="modalupdateloginotp">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
			</div>
			<div class="modal-body">
				<div class="sign_popup_body">
					<div class="singup_right_inr">
						<h1>Verify Your Mobile Number</h1>
						<p class="vd_hidden_update"></p>
						<form id="userPhoneUpdateEditprofileForm" action="{{ route('user.varify.otp.update.mobile') }}" method="post">
							@csrf
							<div class="singup_form">
								<div class="row">
									<div class="col-sm-12">
										<div class="input_bx">
											<input type="hidden" name="country_code_popup">
											<input type="password" name="phone_update_otp" placeholder="Please Enter OTP">
											<span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
											<label id="error_phone_otp_update" class="error" style="display: none;"></label>
										</div>
									</div>
								</div>

								<div class="sing_btn">
									<button class="a_popup pg_btn" type="submit" style="color: #fff;">Submit</button>
								</div>

								<div class="already_have" id="otpresendupdatePhone">
									<span>Didn’t receive OTP yet? <a  href="javascript:void(0);" class="a_popup reSendOtpUpdatePhoneGuest" >Resend OTP</a> </span>
								</div>
								<div class="already_have" id="otpcountdownupdatePhone" style="display: none;"></div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function () {
		jQuery.validator.addMethod("emailonly", function(value, element) {
			return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value.toLowerCase()) || /.+@(gmail|yahoo|hotmail)\.com$/.test(value.toLowerCase());
		}, "Please provide valid email-id!");
		$('#registerupdateEmail').blur(function(event) {
			if($('#registerupdateEmail').val() != ''){
				$("#chck_email_update_rd").html('');
				var $this = $(this);
				var email = $(this).val();
				var reqData = {
					'_token': '{{ @csrf_token() }}',
					'params': {
						'email': email,
						'user_id': "{{ @auth::user()->id }}"
					}
				};
				if(email != '') {
					$.ajax({
						url: '{{ route('user.update.check.email') }}',
						type: 'POST',
						dataType: 'json',
						data: reqData,
					})
					.done(function(response) {
						if(!!response.error) {
							$("#chck_email_update_rd").html(response.error.merchant);
							$("#chck_email_update_rd").show();
							$('#registerupdateEmail').val('');
							$($(this).next().html(''));
						} else {
                                // $("#chck_eml_grn").html(response.result.message);
                                $("#chck_email_update_rd").html('');
                                $("#chck_email_update_rd").hide();
                            }
                        })
					.fail(function(error) {
						console.log(error);
					});
				}

			} else {
				$("#chck_email_update_rd").html('');
				$("#chck_email_update_rd").hide();
			}
		});
		$('#updateemailafterLogineditprofile').click(function (){
			$('#updateEmailEditProfile').submit();
		});
		$('#updateEmailEditProfile').validate({
			rules : {
				temp_email : {
					required : true,
					email : true,
					emailonly:true
				}
			},
			messages : {

				temp_email: {
					required: 'Please provide valid email-id!',
				}
			}
		});
		$('a.openUpdateotpbyPhone').click(function(){

			var tmp_phone = $('input[name="temp_phone"]').val(),
			country_code = $('select[name="country_code"]').val(),

			tmpPhone = parseInt($.trim($('input[name="temp_phone"]').val()));
				$('input[name="country_code_popup"]').val(country_code);
                // alert(country_code);
                if(country_code == '' || country_code == undefined){
                	$('label#error_temp_phone').text('please choose a country code.');
                    $('label#error_temp_phone').show();
                }
                else if(tmp_phone == '' || tmpPhone.toString().length != 10 || tmpPhone.toString().length != 10)

                {

                    // alert("inside if");

                    $('label#error_temp_phone').text('please enter a valid phone no.');

                    $('label#error_temp_phone').show();

                }
                else{

                    // alert("inside else");

                    $('label#error_temp_phone').hide();

                }



                if(tmpPhone.toString().length == 10 && country_code != '' && country_code != undefined){



                	$.ajax({

                		url: "{{ url('user/update/phone/verify') }}/"+{{ @Auth::user()->id }},

                		type:"POST",

                		data: {

                			'_token': "{{ csrf_token() }}",

                			'phone': tmp_phone,
                            'country_code':country_code,

                		},

                		success: function(responce){

                			console.log(responce);

                			if(responce)

                			{
                				if(responce == 0)
                				{
                					$('label#error_temp_phone').text('Mobile no already exist. Please enter a valid mobile no.');
                					$('label#error_temp_phone').show();
                				}
                				else{

                					$('.vd_hidden_update').html('Enter the 6 digit code sent to you at '+responce.temp_phone);
                					$('input#phoneVcodeUser').val(responce.phone_vcode);
                					$('input#phoneVcodeUserId').val(responce.id);

                					$('.vd_hidden_update').show();
                					$('#modalsignin').modal('hide');
                					$('#modalsignin2').modal('hide');
                					$('#modalloginmobile').modal('hide');
                					$('#modalloginotp').modal('hide');
                					$('#modalupdateloginmobile').modal('hide');
                					$('#modalupdateloginotp').modal('show');
                				}

                			}

                			else{

                			}

                		},

                		error: function(xhr){

                			console.log(xhr);

                		}

                	});

                }

            });
		$('a.reSendOtpUpdatePhoneGuest').click(function(){
			var tmp_phone = $('input[name="temp_phone"]').val();
            var country_code = $('select[name="country_code"]').val();
			$.ajax({

				url: "{{ url('user/update/phone/verify') }}/"+{{ @Auth::user()->id }},

				type:"POST",

				data: {

					'_token': "{{ csrf_token() }}",

					'phone': tmp_phone,
                    'country_code':country_code,

				},

				success: function(responce){

					console.log(responce);

					if(responce)

					{
						if(responce == 0)
						{
							$('label#error_temp_phone').text('Mobile no already exist. Please enter a valid mobile no.');
							$('label#error_temp_phone').show();
						}
						else{

							$('.vd_hidden_update').html('Enter the 6 digit code sent to you at '+responce.temp_phone);
							$('input#phoneVcodeUser').val(responce.phone_vcode);
							$('input#phoneVcodeUserId').val(responce.id);

							$('.vd_hidden_update').show();
							var timeleft = 90;
							var downloadTimer = setInterval(function(){
								if(timeleft <= 0){
									clearInterval(downloadTimer);
									$('#otpresendupdatePhone').show();
									$('#otpcountdownupdatePhone').hide();
								} else {
									$('#otpresendupdatePhone').hide();
									$('#otpcountdownupdatePhone').html('<span>Didn’t receive OTP yet? <a  href="javascript:void(0);" class="a_popup">Send OTP again in '+ timeleft +' Sec</a> </span>');
									$('#otpcountdownupdatePhone').show();
								}
								timeleft -= 1;
							}, 1000);
						}

					}

					else{

					}

				},

				error: function(xhr){

					console.log(xhr);

				}

			});
		});
		$('#userPhoneUpdateEditprofileForm').submit(function(){
			var phone_otp = $('input[name="phone_update_otp"]').val();
			var PhoneVcode = $('input#phoneVcodeUser').val();
			if(phone_otp == '' || phone_otp != PhoneVcode)
			{
				$('label#error_phone_otp_update').text('please enter a valid OTP.');
				$('label#error_phone_otp_update').show();
				return false;
			}
			else{
				$('#userPhoneUpdateEditprofileForm').submit();
			}
		})
	})
</script>
@endpush
