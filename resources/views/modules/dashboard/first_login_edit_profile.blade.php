@extends('layouts.app')
@section('title','Please pick your interest areas')
@push('css')
<style type="text/css">
	.interestlipart{
		width: 20%;
	}
	@media only screen and (max-width: 991px) {
		.interestlipart{
			width: 45% !important;
		}
	}
</style>
@endpush
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right didlex">
					<h2>Please pick your interest areas</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="profile_sec">
							<form id="editinterestAreaform" action="{{ route('store.user.details') }}" method="post" enctype="multipart/form-data">
								@csrf
								<div class="mt-3">
									{{-- <label class="bold">Please pick your interest areas</label> --}}
									<ul class="interest" style="display: inline-block;">
										@if(@$sub_category->isNotEmpty())
										@foreach(@$sub_category as $cate)
										<li style="display: inline-block; " class="interestlipart">
											<label class="InterestBox">{{ @$cate->name }}
												<input type="checkbox" name="interest[]" value="{{ @$cate->id }}" @if(@Auth::user()->user_to_category->isNotEmpty()) @foreach(@Auth::user()->user_to_category as $user_to_categories) @if($user_to_categories->category_id == @$cate->id) checked="" @endif @endforeach @endif required="">
												<span class="checkmark"></span>
											</label>
										</li>
										@endforeach
										@endif
									</ul>
									<label id="interest-error" class="error" style="display: none;"></label>
								</div>
								<div class="doubleborder"></div>
								<div class="doubleborder"></div>
								<button type="submit" id="saveChanges">Save All Changes</button>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function () {
		$('#saveChanges').click(function(){
			var atLeastOneIsChecked = $('input[name="interest[]"]:checked').length > 2;
			if (atLeastOneIsChecked != false) {
				$('#interest-error').hide();
				$('#interest-error').html('');
				$("#editinterestAreaform").submit();
			}
			else{
				$('#interest-error').html('please pick at least any 3');
				$('#interest-error').show();
			}
			return false;
		});
	});
</script>

@endpush
