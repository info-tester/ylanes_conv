@extends('layouts.app')
@section('title','Refered Users')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>Refered users</h2> 
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						<div class="ReferCon">
							<div class="row">
								@if(@$users->isNotEmpty())
								@foreach(@$users as $user)
								<div class="col-sm-12 col-md-12 col-lg-12">
									<div class="re_User">
										<ul>
											<li>
												<div class="row_userst">

													<div class="userpl0">
														<p>{{ substr(@$user->user->first_name,0,1) }}{{ substr(@$user->user->last_name,0,1) }}</p>    
													</div>
												</div>
											</li>
											<li>
												<span class="td"><img src="{{url('public/frontend/images/referd-user-watch.png')}}" alt="watch">{{ @$user->created_at->format('dS M, Y') }}</span>
											</li>
											<li>
												<span class="joind"><img src="{{url('public/frontend/images/joind.png')}}" alt="joind">Joined</span>
											</li>
											<li>
												<span class="coins"><img src="{{url('public/frontend/images/refer-coin.png')}}" alt="coin">Coins : 100 </span>
											</li>
										</ul>
									</div>
								</div>
								@endforeach
								@else
								<div class="col-md-12 col-md-12 col-lg-12 text-center">
									<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
									<h3>No users found</h3>
								</div>
								@endif
							</div>
						</div>
						@if(@$users->total() > 15)
						<div class="pagination_bx text-left">
							{{ @$users->links("pagination::bootstrap-4") }}
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection