@extends('layouts.app')
@section('title','Refer And Earn')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>REFER AND EARN</h2>
				</div>
				<div class="">
					<div class="earnCon">

						<div class="row">
							<div class="col-sm-12 col-md-8 col-lg-8">
								<h4>Refer Friends And Earn <span>100</span> Coins</h4>
								<p>Invite your friends to Ylanes. If they sign up, you will get 100 coins</p>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-4">
								<a href="{{ route('referral.user.list') }}"><img src="{{url('public/frontend/images/double_user.png')}}" alt="icon">My Reffered Users</a>
							</div>
						</div>


					</div>
					<div class="process_sec">

						<div class="toppad">
							<div class="refer_Header">
								<div class="row">
									<div class="col-sm-4 col-md-4 col-lg-4">
										<div class="process inviteShape">
											<div class="ceRound">
												<img src="{{url('public/frontend/images/invitation.png')}}" alt="invitation">
											</div>
											<h6>Send Invitation</h6>
											<p>Send your referral link to your friend and tell them to connect Ylanes</p>
										</div>
									</div>
									<div class="col-sm-4 col-md-4 col-lg-4">
										<div class="process regiShape">
											<div class="ceRound">
												<img src="{{url('public/frontend/images/Registration.png')}}" alt="Registration">
											</div>
											<h6>Registration</h6>
											<p>Send your referral link to your friend and tell them to connect Ylanes</p>
										</div>
									</div>
									<div class="col-sm-4 col-md-4 col-lg-4 centerdiv">
										<div class="process">
											<div class="ceRound">
												<img src="{{url('public/frontend/images/Earn-Coins.png')}}" alt="Earn-Coins">
											</div>
											<h6>Earn Coins</h6>
											<p>Send your referral link to your friend and tell them to connect Ylanes</p>
										</div>
									</div>
								</div>
							</div>
							<div class="share_Link">
								<h6>Share the link</h6>
								<p>You can also share your referral link by copying and sending it or sharing it on your social media.</p>
								<ul>
									<li>
										<form>
											<input type="text" value="{{ route('referral.code.link',@auth::user()->ref_code) }}" placeholder="enter your address" id="myInput">
											<button onclick="copyToClipboard()" type="button"><img src="{{url('public/frontend/images/copy-link.png')}}" alt="img">Copy link</button>
										</form>
									</li>
									<li>
										<div class="shaRe">
											<button class="shareLink openshare" type="button">
												<img src="{{url('public/frontend/images/share.png')}}" alt="share">
											</button>
										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="modal fade what_modal" id="myModalshare">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="share_slidars">
					<div class="share_icon_slid">
						<div class="owl-carousel">
							<div class="item">
								<div class="st-custom-button modalshare" data-network="facebook" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-facebook"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="whatsapp" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-whatsapp"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="twitter" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-twitter"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="linkedin" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-linkedin"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="snapchat" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-snapchat"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="reddit" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-reddit"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="pinterest" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-pinterest"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="email" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-envelope"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="telegram" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="icofont-paper-plane"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="evernote" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="evernote"><img src="{{url('public/frontend/images/evernote-brands.png')}}"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="googlebookmarks" data-url="{{ route('referral.code.link',@auth::user()->ref_code) }}">
									<em><i class="sos_gool_io_c"><img src="{{url('public/frontend/images/social_google.png')}}" alt=""></i></em>
								</div>
							</div>
							<div class="item">
								<div class="modalshare copyLink" onclick="copyToClipboard()">
									<em><i class="icofont-link"></i></em>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=#{property?._id}&product=custom-share-buttons" defer></script>
<script>
	function copyToClipboard(text) {
		var inputc = $('#myInput');
		inputc.value = window.location.href;
		inputc.focus();
		inputc.select();
		document.execCommand('copy');
		alert("URL Copied.");
	}
	$('.openshare').click(function(){
		$('#myModalshare').modal('show');
	});
	$(document).ready(function() {
		var owl = $('.share_icon_slid .owl-carousel');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			autoplay: false,
			loop: true,
			responsive: {
				0: {
					items: 5
				},
				350: {
					items: 6
				},
				415: {
					items: 7
				},
				480: {
					items: 5
				},
				520: {
					items: 6
				},
				768: {
					items: 5
				},
				991: {
					items: 6
				},
				1000: {
					items: 6
				}
			}
		})
	})
</script>
@endpush