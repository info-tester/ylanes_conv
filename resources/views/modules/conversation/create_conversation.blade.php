@extends('layouts.app')
@section('title','Create Room')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right didlex">
					<h2>Create Room</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="profile_sec">
							@if(@auth::user()->conversation_balance_total < 200)
							<h2 class="text-center">Please purchase the package to <a href="{{ route('purchase.package') }}" style="color: #ffc000;"> click here</a> then create a conversation.</h2>
							@else
							<form method="post" id="conversationForm" action="{{ route('create.conversation.store') }}" autocomplete="off">
								@csrf
								<div class="gender_sec">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Category</label>
											<select  id="category" required="" name="category">
												<option value="">Select category</option>
												@if(@$category->isNotEmpty())
												@foreach(@$category as $cat)
												<option value="{{ @$cat->id }}">{{ @$cat->name }}</option>
												@endforeach
												@endif
											</select>
										</div>
										<div class="col-sm-6 col-md-4 col-lg-4">
											<label>Sub category</label>
											<select id="subcategory" required="" name="sub_category">
												<option value="">Select sub category</option>
											</select>
										</div>
										<div class="col-sm-6 col-md-4 col-lg-4">
											<label>Topic</label>
											<select id="topic" required="" name="topic">
												<option value="">Select topic</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 col-md-6 col-lg-4">
										<label>Date</label>
										<div class="dash-d">
											<input type="text" placeholder="Select date" id="datepicker" required="" name="date">
											<span class="over_llp2"><img src="{{url('public/frontend/images/date.png')}}" alt=""></span>
										</div>
									</div>
									<div class="col-sm-6 col-md-6 col-lg-4">
										<label>Time</label>
										<div class="dash-d">
											<select id="time" name="time" required="">
												<option value="">Select time </option>
												@php
												$start = strtotime('12:00 AM');
												$end   = strtotime('11:59 PM');
												for($i = $start;$i<=$end;$i+=600){
													@endphp
													<option value="{{ date('h:i A', $i) }}">{{ date('h:i A', $i) }}</option>
													@php
												}
												@endphp
											</select>
											{{-- <span class="over_llp2"><img src="{{url('public/frontend/images/clc.png')}}" alt=""></span> --}}
										</div>
									</div>
								</div>
								<div class="gender_sec">
									<div class="row">
										<div class="col-sm-12 col-md-12 col-lg-12 custTypebox">
											<label>Type</label>
											<div class="check_ins">
												<div class=" custom_Checkbox">
													<div class="radio-toolbar">
														<input class="typeRadio" type="radio" id="one-on-one " name="type" value="ON" required="">
														<label for="one-on-one ">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														one-on-one </label>
													</div>
												</div>
												<div class="custom_Checkbox">
													<div class="radio-toolbar">
														<input class="typeRadio" type="radio" id="Open" name="type" value="OP" required="">
														<label for="Open">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
                                                            Standard</label>
													</div>
												</div>
												<div class=" custom_Checkbox">
													<div class="radio-toolbar">
														<input class="typeRadio" type="radio" id="Conditional" name="type" value="CO" required="">
														<label for="Conditional">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Premium</label>
													</div>
												</div>
											</div>
											<label id="type-error" class="error" for="type" style="display: none;"></label>
										</div>
									</div>
								</div>
								<div class="gender_sec">
									<div class="row">
										<div class="col-sm-5 col-md-5 col-lg-5 custTypebox">
											<label>Do you want to join anonymously?</label>
											<div class="check_ins">
												<div class="custom_Checkbox">
													<div class="radio-toolbar">
														<input class="typeRadio" type="radio" id="yes" name="is_anonymously" value="Y" required="" onchange="displaynameHideShowJoiningtime($(this).val());">
														<label for="yes">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														Yes</label>
													</div>
												</div>
												<div class=" custom_Checkbox">
													<div class="radio-toolbar">
														<input class="typeRadio" type="radio" id="no" name="is_anonymously" value="N" required="" onchange="displaynameHideShowJoiningtime($(this).val());">
														<label for="no">
															<div class="outer_checked">
																<div class="inner_Checked"></div>
															</div>
														No</label>
													</div>
												</div>
											</div>
											<label id="is_anonymously-error" class="error" for="is_anonymously" style="display: none;"></label>
										</div>
										<div class="col-sm-6 col-md-6 col-lg-6 " id="displaynameareaPopupbeforejoining" style="display: none;">
											<label>Display name</label>
                                           	<input type="text" name="display_name" required="" placeholder="Display name" autocomplete="new-display_name" maxlength="15">
                                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-12">
										<label>Your take</label>
										<textarea placeholder="Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc." minlength="50" maxlength="300" required="" name="conversation_text"></textarea>
									</div>
								</div>
								<div class="doubleborder"></div>
								<div class="doubleborder"></div>
								<button class="create" type="submit">Save Room</button>
							</form>
							@endif
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@push('js')
<script>
	$(function() {
		$("#datepicker").datepicker({
			minDate: new Date(),
		});
	});
	$("input[name=time]").clockpicker({
		placement: 'bottom',
		align: 'left',
		autoclose: true,
		default: 'now',
		donetext: "Select",
		init: function() {
			console.log("colorpicker initiated");
		},
		beforeShow: function() {
			console.log("before show");
		},
		afterShow: function() {
			console.log("after show");
		},
		beforeHide: function() {
			console.log("before hide");
		},
		afterHide: function() {
			console.log("after hide");
		},
		beforeHourSelect: function() {
			console.log("before hour selected");
		},
		afterHourSelect: function() {
			console.log("after hour selected");
		},
		beforeDone: function() {
			console.log("before done");
		},
		afterDone: function() {
			console.log("after done");
		}
	});
	$(document).ready( function(){
		$('#category').change(function(){
			catId = $(this).val();
			$.ajax({
				url:"{{ route('get.sub.category') }}/"+catId,
				type:"GET",
				success: function(responce){
					$('#subcategory').html(responce.result.get_sub_categories);
				},
				error: function(xhr){
					console.log(xhr);
				}
			});
		});
		$('#subcategory').change(function(){
			subcategoryId = $(this).val();
			$.ajax({
				url:"{{ route('get.sub.category.topic') }}/"+subcategoryId,
				type:"GET",
				success: function(responce){
					$('#topic').html(responce.result.get_topic);
				},
				error: function(xhr){
					console.log(xhr);
				}
			});
		});
        jQuery.validator.addMethod("greaterThan",
            function(value, element, params) {
                return new Date() < new Date(params);
            },'Please enter time that greater than current time.');
		$('#conversationForm').validate({
            rules: {
                time: {
                    greaterThan: function(){
                        let dateTime= $('#datepicker').val() +' '+$('#time').val();
                        return  new Date(dateTime);
                    }
                }
            },
        });
	})
</script>
@endpush
