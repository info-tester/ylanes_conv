@extends('layouts.app')
@section('title','Past Rooms')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>Past Rooms</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dash_top_search arrow_box">
						<form>
							<div class="cone_divs">
								<div class=" all_in_con">
									<div class="das_inputss slect1s">
										<label>Type</label>
										<select name="type">
											<option value="">All</option>
											<option value="CO" @if(request()->type == 'CO') selected="" @endif>Premium</option>
											<option value="OP" @if(request()->type == 'OP') selected="" @endif>Standard</option>
											<option value="ON" @if(request()->type == 'ON') selected="" @endif>one-on-one </option>
										</select>
									</div>
									<div class="das_inputss slect1s">
										<label>Topic</label>
										<select name="topic">
											<option value="">Select Topic</option>
											@if(@$topics->isNotEmpty())
											@foreach(@$topics as $topic)
											<option value="{{ @$topic->id }}" @if(@request()->topic == @$topic->id) selected="" @endif>{{ @$topic->topic_line_1 }}</option>
											@endforeach
											@endif
										</select>
									</div>
									<div class="das_inputss slect2s">
										<label>From date</label>
										<div class="dash-d">
											<input type="text" placeholder="Select " id="datepicker" name="from_date1" value="{{ @request()->from_date1 }}">
											<span class="over_llp_rm1"><img src="{{ url('public/frontend/images/da.png')}}" alt=""></span>
										</div>
									</div>
									<div class="das_inputss slect2s">
										<label>To date</label>
										<div class="dash-d">
											<input type="text" placeholder="Select " id="datepicker1" name="to_date1" value="{{ @request()->to_date1 }}">
											<span class="over_llp_rm1"><img src="{{ url('public/frontend/images/da.png')}}" alt=""></span>
										</div>
									</div>
								</div>
								<div class="coins_rm003">
									<button class="btns_find"><img src="{{ url('public/frontend/images/fins.png')}}">Find</button>
								</div>
							</div>

						</form>
					</div>

					<div class="all_ists_dashs past_cons">
						<div class="row">
							@if(@$allConversation->isNotEmpty())
							@foreach(@$allConversation as $conversation)
							<div class="col-md-6">
								<div class="outer_area convers_area">
									<div class="lists_tops pd-9">
										<h2>{{ substr(@$conversation->topic->topic_line_1,0,25) }}</h2>
										<p>{{ substr(@$conversation->topic->topic_line_2,0,57) }} </p>
										{{-- <a href="#" class="cross_btns"><img src="{{ url('public/frontend/images/cross_btns.png') }}"> </a> --}}
										<div class="dateTime">
											<p class="dt dtl"><img src="{{ url('public/frontend/images/watch.png')}}" alt="watch"><span>{{ date('dS M, Y', strtotime(@$conversation->date)) }}</span><span>{{ date('h:i A', strtotime(@$conversation->time)) }}</span></p>

											<a href="{{ route('past.conversation.details',@$conversation->id) }}" class="fed_gives">View Feedback</a>
										</div>
									</div>
									<div class="topic_users convs_topics">
										@if(@$conversation->conversationstousers->isNotEmpty())
										@foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
										@if($keys <= 2)
										@if(!empty(@$conversationstouser->users))
										@if($conversationstouser->is_anonymously == 'N')
										@if(!empty(@$conversationstouser->users->image))
										<span class="topp_user_0{{ $keys+1 }}"><a href="{{ route('public.profile',@$conversationstouser->users->id) }}" ><img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}">@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</a></span>
										@else
										<span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
											<a href="{{ route('public.profile',@$conversationstouser->users->id) }}" style="color: #39393d;" >{{ substr(@$conversationstouser->users->profile_name,0,2 )}}
											@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif</a>
										</span>
										@endif
										@else
										<span class="topp_user_0{{ $keys+1 }}" style="padding: 4px 0 0 0 !important;">
											{{ substr(@$conversationstouser->display_name,0,2 )}}
											@if($keys==0)<img src="{{url('public/frontend/images/ticks.png')}}" class="img_tis">@endif
										</span>
										@endif
										@endif
										@endif
										@endforeach
										@endif

										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
									{{-- <div class="list_listses">

										@if(!empty(@$conversation->conversationstousers->isNotEmpty()))
										@foreach(@$conversation->conversationstousers as $keys => $conversationstouser)
										<div class="lists_mids pd-10">
											<em>
												@if(@$conversationstouser->is_anonymously == 'N')
												@if(!empty(@$conversationstouser->users->image))
												<img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversationstouser->users->image) }}" alt="user">
												@else
												<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
												@endif
												@else
												<div class="topic_users" style="padding: 0px; margin: 0px;">
													<span class="topp_user_03" style="margin-left: 0px; border: 1px solid #d7d7d7; font-style: normal">
														{{ substr(@$conversationstouser->display_name,0,2 )}}
													</span>
												</div>
												@endif
											</em>
											<div class="midls_list_info new_info_pas">
												<div class="div_fles">
													@if(@$conversationstouser->is_anonymously == 'N')
													@if(!empty(@$conversationstouser->users->profile_name))
													<h4>{{ substr(@$conversationstouser->users->profile_name,0,7) }}</h4>
													@else
													<h4>{{ substr(@$conversationstouser->users->first_name,0,7) }}</h4>
													@endif
													@else
													<h4>{{ @$conversationstouser->display_name }}</h4>
													@endif
													<span><img src="{{ url('public/frontend/images/user_wishlist.png')}}" alt="icon">0 </span>
												</div>
												<p>
													@if(@$conversationstouser->users->gender == 'M')
													Male
													@elseif(@$conversationstouser->users->gender == 'F')
													Female
													@elseif(@$conversationstouser->users->gender == 'N')
													Non Binary
													@elseif(@$conversationstouser->users->gender == 'O')
													Others
													@endif
												</p>
											</div>
										</div>
										@endforeach
										@endif
										<div class="lists_mids vi_fed">
											<a href="{{ route('past.conversation.details',@$conversation->id) }}">View Feedback <img src="{{ url('public/frontend/images/rra.png')}}"></a>
										</div>
									</div>--}}
								</div>
							</div>
							@endforeach
							@else
							<div class="col-md-12 text-center">
								<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
								<h3>No data found</h3>
							</div>
							@endif
						</div>
						@if(@$allConversation->total() > 4)
						<div class="pagination_bx text-left">
							{{ @$allConversation->appends([
								'type' => @request()->type,
								'topic' => @request()->topic,
								'from_date1' => @request()->from_date1,
								'to_date1' => @request()->to_date1,
								])->links("pagination::bootstrap-4") }}
							</div>
							@endif
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection
	@push('js')
	<script>
		$(function() {
			$("#datepicker").datepicker({
				maxDate: new Date(),
			});
			$("#datepicker1").datepicker({
				minDate: new Date(),
			});
		});
	</script>
	@endpush
