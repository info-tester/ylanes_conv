@extends('layouts.app')
@section('title','Past Rooms')
@push('css')
<style type="text/css">
	<style>
	.tooltips {
		position: relative;
		display: inline-block;
		border-bottom: 1px dotted black;
	}

	.tooltips .tooltiptext {
		visibility: hidden;
		width: 120px;
		background-color: #5a5a5b;
		color: #fff;
		text-align: center;
		border-radius: 6px;
		padding: 5px;

		/* Position the tooltip */
		position: absolute;
		z-index: 1;
	}

	.tooltips:hover .tooltiptext {
		visibility: visible;
	}
	.post_feedback_areaa .afterReview i:hover {
		color: lightgray !important;
	}
</style>
</style>
@endpush
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>Past Rooms</h2>
				</div>
				<div class="dash-right-inr">
					@include('layouts.message')
					<div class="all_ists_dashs coinn_mainn">
						<div class="conv_dtls_main pastt_convvn">
							<div class="conv_dtls_left">
								<h1><img src="{{url('public/frontend/images/fire.png')}}" alt=""> {{ @$conversation->topic->topic_line_1 }}
									{{-- @if(@$conversation->type == 'OP')
									<b>(STANDARD)</b>
									@elseif(@$conversation->type == 'ON')
									<b>(ONE-ON-ONE)</b>
									@elseif(@$conversation->type == 'CO')
									<b>(PREMIUM)</b>
									@endif --}}
                                    @if(@$converstion_join_current_users->status=='C' && @$converstion_join_current_users->is_evicted=='N' && @$converstion_join_current_users->is_leave_parament=='N')
                                    <a href="javascript:;" class="brw_a click_video_call" style="cursor: pointer;margin-top: 0px;height: 25px; font-size: 13px;line-height: 27px;float: right;font-weight: 500;">Join Call</a>
                                    @endif
								</h1>
								<p>{{ @$conversation->topic->topic_line_2 }}</p>
								<span><img src="{{url('public/frontend/images/iconn12.PNG')}}" alt=""> {{ @$conversation->category->name }}</span>
								<span><img src="{{url('public/frontend/images/iconn11.PNG')}}" alt="" class="mmyuu"> {{ @$conversation->sub_category->name }}</span>

								<div class="pastt_postedby">
									<h3>
										<img src="{{url('public/frontend/images/iconn10.PNG')}}" alt=""> Posted by
										@if(!empty(@$conversation->users))
										@if(!empty(@$conversation->users->profile_name))
										{{ @$conversation->users->profile_name }}
										@else
										{{ @$conversation->users->first_name }}
										@endif
										@else
										@if(!empty(@$conversation->conversationstouserDef2))
										@if(!empty(@$conversation->conversationstouserDef2->users->profile_name))
										{{ @$conversation->conversationstouserDef2->users->profile_name }}
										@else
										{{ @$conversation->conversationstouserDef2->users->first_name }}
										@endif
										@endif
										@endif
									</h3>
									<h3><img src="{{url('public/frontend/images/upcoming_con.png')}}" alt="" class="mmyuu002"> {{ date('dS M, Y', strtotime(@$conversation->date)) }}  |  {{ date('h:i A', strtotime(@$conversation->time)) }}</h3>
                                    @if(@$conversation->type == 'OP')
									<h3 class="mmyuu002"> Type -  (STANDARD)</h3>
									@elseif(@$conversation->type == 'ON')
									<h3 class="mmyuu002"> Type - (ONE-ON-ONE)</h3>
									@elseif(@$conversation->type == 'CO')
									<h3 class="mmyuu002"> Type - (PREMIUM)</h3>
									@endif
								</div>
							</div>
							<div class="conv_dtls_right">
								<p>
									<img src="{{url('public/frontend/images/iconn16.PNG')}}" alt="">
									@if(@$conversation->conversationsReviews->count() == @$conversation_to_user->count())
									@if(@$my_reviews->isNotEmpty())
									{{ @$my_reviews->sum('hearts') }}
									@else
									--
									@endif
									@else
									--
									@endif
								</p>
								<p>
									<img src="{{url('public/frontend/images/iconn14.PNG')}}" alt="">
									{{-- @if(@$converstion_join_current_users->tot_report) --}}
									{{$converstion_join_current_users->tot_report}}
									{{-- @endif --}}
								</p>
								{{-- <p>
									<img src="{{url('public/frontend/images/iconn15.PNG')}}" alt="">
									@if(@$conversation->conversationsReviews->count() == @$conversation_to_user->count())
									@if(@$my_reviews->isNotEmpty())
									Evict
									@else
									--
									@endif
									@else
									--
									@endif
								</p>
								<p><img src="{{url('public/frontend/images/iconn14.PNG')}}" alt="">
									@if(@$conversation->conversationsReviews->count() == @$conversation_to_user->count())
									@if(@$my_reviews->isNotEmpty())
									1
									@else
									--
									@endif
									@else
									--
									@endif
								</p> --}}
							</div>
						</div>

						<!--======================-->


						<div class="pastt_convv_innfo">
							@if(@$conversation_to_user->isNotEmpty())
                            <form id="reviewFrom" action="{{ route('post.conversation.review') }}" method="post">
                                @csrf
                                <input type="hidden" name="conversation_id" value="{{ @$conversation->id }}">
							@foreach(@$conversation_to_user as $conv_users)

							<div class="post_feedback_areaa feedbc-n02">
								<div class="usr_info3">
									<span>
										{{-- <img src="{{url('public/frontend/images/cn6.png')}}" alt=""> --}}
										@if(@$conv_users->is_anonymously == 'N')
										@if(!empty(@$conv_users->users->image))
										<img src="{{ url('storage/app/public/customer/profile_pics/'.@$conv_users->users->image) }}" alt="user">
										@else
										<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
										@endif
										@else
										<a href="{{ route('public.profile',@$conv_users->users->id) }}" target="_blank">
											<div class="topic_users" style="padding: 0px; margin: 0px;">
													<p class="topp_user_03" style="margin: 0px;font-style: normal; padding :9px !important; font-weight: 600;">
														{{ substr(@$conv_users->display_name,0,2 )}}
													</p>
											</div>
										</a>
										@endif
									</span>
									<h5>
										@if(@$conv_users->is_anonymously == 'N')
										@if(!empty(@$conv_users->users->profile_name))
										{{ substr(@$conv_users->users->profile_name,0,8) }}
										@else
										{{ substr(@$conv_users->users->first_name,0,8) }}
										@endif
										@else
										<a href="{{ route('public.profile',@$conv_users->users->id) }}" target="_blank">
											@if(!empty(@$conv_users->users->profile_name))
											{{ substr(@$conv_users->users->profile_name,0,8) }}
											@else
											{{ substr(@$conv_users->users->first_name,0,8) }}
											@endif
										</a>
										@endif
									</h5>
									<p><img src="{{url('public/frontend/images/heart.png')}}" alt=""> <strong>{{ CustomHelper::number_format_short(@$conv_users->users->tot_hearts) }}</strong>
										<a href="javascript:void(0);" class="tooltips"><img src="{{url('public/frontend/images/i.png')}}">
											<em class="tooltiptext">{{ @$conv_users->conversation_text }}</em>
										</a>
									</p>
								</div>

								@if(in_array(@$conv_users->user_id,@$user_reviews))
								<div class="rating afterReview p-5 shadow js-wc-star-rating" data-id="33" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
									<div style="display: flex; flex-wrap: nowrap;">
										@if(@$conversation->conversationsReviews->isNotEmpty())
										@foreach(@$conversation->conversationsReviews as $reviews)
										@if(@$reviews->to_user_id == @$conv_users->user_id)
										@if(@$reviews->hearts == 1)
										<i title="Very Bad" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Poorly" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Medium" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Good" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Excellent!" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										@elseif(@$reviews->hearts == 2)
										<i title="Very Bad" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Poorly" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Medium" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Good" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Excellent!" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										@elseif(@$reviews->hearts == 3)
										<i title="Very Bad" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Poorly" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Medium" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Good" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Excellent!" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										@elseif(@$reviews->hearts == 4)
										<i title="Very Bad" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Poorly" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Medium" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Good" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Excellent!" class="far fa-heart" style="color: lightgray; margin: 2px; font-size: 4em; cursor: auto;"></i>
										@elseif(@$reviews->hearts == 5)
										<i title="Very Bad" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Poorly" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Medium" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Good" class="fas fa-heart" style="color: #ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										<i title="Excellent!" class="fas fa-heart" style="color:#ffc107; margin: 2px; font-size: 4em; cursor: auto;"></i>
										@endif
										@endif
										@endforeach
										@endif
									</div>
								</div>
								@if(@$conversation->conversationsReviews->isNotEmpty())
								@foreach(@$conversation->conversationsReviews as $reviews)
								@if(@$reviews->to_user_id == @$conv_users->user_id)
								{{-- <form> --}}
									<span>
										{{ @$reviews->reviews_text }}
									</span>
									{{-- <input type="text" name="review" placeholder="Write review" disabled="" value="{{ @$reviews->reviews_text }}"> --}}
								{{-- </form> --}}
								@endif
								@endforeach
								@endif
								@else
								<div class="rating ratings{{ @$conv_users->id }}" data-id="{{ @$conv_users->id }}"></div>
								{{-- <form class="reviewFormAction{{ @$conv_users->id }}" action="{{ route('post.conversation.review') }}" method="post">
									@csrf --}}
									<input type="text" name="rew_rating{{ @$conv_users->user_id }}" style="position: absolute; visibility: hidden;" value="" class="rew_rating{{ @$conv_users->id }}" id="rew_rating{{ @$conv_users->id }}" required>
									<input type="text" name="review{{ @$conv_users->user_id }}" placeholder="Write review" maxlength="100">
									{{-- <input type="hidden" name="conversation_id" value="{{ @$conversation->id }}"> --}}
									{{-- <input type="hidden" name="conversation_user_id" value="{{ @$conv_users->id }}"> --}}
									<input type="hidden" name="to_user_id[]" value="{{ @$conv_users->user_id }}">
                                    <label id="rew_rating{{ @$conv_users->user_id }}-error" class="error" for="rew_rating{{ @$conv_users->id }}"></label>
									{{-- <input type="submit" value="" data-id="{{ @$conv_users->id }}" class="actionFormButton"> --}}
								{{-- </form> --}}
								@endif
                                @if(!in_array($conv_users->user_id,$report))
								<a href="javascript:;" class="report" data-userid="{{@$conv_users->user_id}}" data-roomid="{{@$conv_users->conversation_id}}"><img src="{{url('public/frontend/images/iconn14.PNG')}}" alt=""></a>
                                @endif
							</div>
							@endforeach
                            <div class="rate_conversation">
                                <img src="{{url('public/frontend/images/iconn03.PNG')}}" alt="">
                                @if(!empty(@$conversation->conversationReviews))
                                <p>You rated this conversation : </p>
                                @if(@$conversation->conversationReviews->rating == 'happy')
                                <a href="javascript:void(0);" class="rate_conversation_click_active"><img src="{{url('public/frontend/images/iconn04.PNG')}}" alt=""></a>
                                @elseif(@$conversation->conversationReviews->rating == 'ok')
                                <a href="javascript:void(0);" class="rate_conversation_click_active"><img src="{{url('public/frontend/images/iconn05.PNG')}}" alt=""></a>
                                @elseif(@$conversation->conversationReviews->rating == 'sad')
                                <a href="javascript:void(0);" class="rate_conversation_click_active"><img src="{{url('public/frontend/images/iconn06.PNG')}}" alt=""></a>
                                @endif
                                @else
                                <p>Rate the conversation : </p>
                                <a href="javascript:;" data-value="happy" class="rate_conversation_click"><img src="{{url('public/frontend/images/iconn04.PNG')}}" alt=""></a>
                                <a href="javascript:;" data-value="ok" class="rate_conversation_click"><img src="{{url('public/frontend/images/iconn05.PNG')}}" alt=""></a>
                                <a href="javascript:;" data-value="sad" class="rate_conversation_click"><img src="{{url('public/frontend/images/iconn06.PNG')}}" alt=""></a>
                                <input type="hidden" name="conversation_rate" id="conversation_rate" required>
                                <label id="conversation_rate-error" class="error" for="conversation_rate"></label>
                                @endif
                            </div>
                            {{-- <input type="submit" value="Post Review" class="create"> --}}
                            @if(!empty(@$conversation->conversationReviews))
                            @else
                            <button class="post-review-btn" type="submit">Post Review</button>
                            @endif
                            </form>
							@endif
						</div>



						{{-- <div class="rate_conversation">
							<img src="{{url('public/frontend/images/iconn03.PNG')}}" alt="">
							@if(!empty(@$conversation->conversationReviews))
							<p>You rated this conversation : </p>
							@if(@$conversation->conversationReviews->rating == 'happy')
							<a href="javascript:void(0);"><img src="{{url('public/frontend/images/iconn04.PNG')}}" alt=""></a>
							@elseif(@$conversation->conversationReviews->rating == 'ok')
							<a href="javascript:void(0);"><img src="{{url('public/frontend/images/iconn05.PNG')}}" alt=""></a>
							@elseif(@$conversation->conversationReviews->rating == 'sad')
							<a href="javascript:void(0);"><img src="{{url('public/frontend/images/iconn06.PNG')}}" alt=""></a>
							@endif
							@else
							<p>Rate the conversation : </p>
							<a href="{{ route('post.review.conversation',[@$conversation->id,'happy']) }}" onclick="return confirm('Are you sure to rate this conversation?');"><img src="{{url('public/frontend/images/iconn04.PNG')}}" alt=""></a>
							<a href="{{ route('post.review.conversation',[@$conversation->id,'ok']) }}" onclick="return confirm('Are you sure to rate this conversation?');"><img src="{{url('public/frontend/images/iconn05.PNG')}}" alt=""></a>
							<a href="{{ route('post.review.conversation',[@$conversation->id,'sad']) }}" onclick="return confirm('Are you sure to rate this conversation?');"><img src="{{url('public/frontend/images/iconn06.PNG')}}" alt=""></a>
							@endif
						</div> --}}
						@if(@$conversation->conversationsReviews->count() == @$conversation_to_user->count())
						@if(@$my_reviews->isNotEmpty())
						<div class="past_review_minn">
							<h2>Feedback received by you</h2>
							@foreach(@$my_reviews as $myreview)
							@if(!empty($myreview->reviews_text))
							<div class="past_revieww">
								<p>{{ @$myreview->reviews_text }}</p>
								<span><img src="{{url('public/frontend/images/iconn02.PNG')}}" alt=""> {{ @$myreview->created_at->format('dS M, Y') }}</span>
							</div>
							@endif
							@endforeach
						</div>
						@endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@if(@$converstion_join_current_users->status=='C' && @$converstion_join_current_users->is_evicted=='N')
<div class="modal popup_list sign_popup" id="videoCallJoinConfirmation">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body rule">
                    <div class="singup_right_inr">
                        <h1>Enter Room</h1>
                        <form id="videoCallJoinConfirmationform" action="{{route('join.video.call',['conversationId'=>$conversation->id])}}">
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <h4>Rules Of Engagement</h4>
                                    </div>
                                    <div class="col-sm-12">
                                        <ul style="list-style: decimal; padding: 1px 0px 0px 20px;">
                                            <li class="popupbulates"> Speak with authenticity (Try to speak in  “I” statements)</li>
                                            <li class="popupbulates">Listen with intent (Try to understand; avoid judgements)</li>
                                            <li class="popupbulates">Engage with respect </li>
                                            <li class="popupbulates">Maintain confidentiality</li>
                                            <li class="popupbulates">Avoid advising (Share relevant  experiences, if any)</li>
                                        </ul>
                                        <div class="category-ul">
                                            <input type="checkbox" name="accept_terms" id="agree-video" value="Y" class="required">
                                            <label for="agree-video" >I agree</label>
                                        </div>
                                        <label id="accept_terms-error" class="error" for="accept_terms" style="display: none;">This field is required.</label>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <button class="pg_btn" >Enter</button>
                                    {{-- <a class="pg_btn" href="javascript:void(0);" >Enter</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@push('js')
<script src="{{ url('public/frontend/js/jquery.star-rating.js') }}"></script>
<script>
	$('.rating').starRating(
	{
		starSize: 1.5,
		showInfo: false
	});
	$('.rating').click(function () {
		var dataid = $(this).data('id');
		var rating = $('.ratings'+dataid+' input[name="rating"]:checked').val();
		$('.rew_rating'+dataid).val(rating);
	})

	$('.actionFormButton').click(function(){
		var dataids = $(this).data('id');
		$('.reviewFormAction'+dataids).validate({
			rule:{
				rew_rating:{
					required:true,
				},
			},
			messages : {
				rew_rating :{
					required : "Please select your rating.",
				},
			}
		});
		$('.reviewFormAction'+dataids).submit();
	})
    $('.click_video_call').click(function(){

    $('#videoCallJoinConfirmation').modal('show');
    })
    $('#videoCallJoinConfirmationform').validate({
        ignore: [],
        rules: {
            accept_terms:{
                required:true,
            },
        },
        messages: {
            accept_terms:{
                required:'Please agree all rule',
            },
        },
    });
    $('#reviewFrom').validate({
        ignore: [],
        rule:{
            conversation_rate:{
                required:true,
            },
        },
        messages : {
            conversation_rate :{
                required : "Please select rate the conversation.",
            },
        }
    });
    $('.rate_conversation_click').click(function(){
        $('.rate_conversation_click').removeClass('rate_conversation_click_active')
        $('#conversation_rate').val($(this).data('value'))
        $(this).addClass('rate_conversation_click_active');
    })

    $('body').on('click', '.report', function() {
        var roomId=$(this).data('roomid');
        var userId=$(this).data('userid');
        var currentContent =$(this);

        swal({
            // title: "New video call request",
            text: "Do you want to report this user?",
            icon: "info",
            // buttons: ['Cancle', 'OK'],
            dangerMode: true,
            buttons: true,
            buttons: {
                catch: {
                    text: "NO",
                    value: "No",
                    className:"swal-button--danger"
                },
                default: {
                    text: "YES",
                    value: "Yes",
                    className:"swal-button--cancel"
                },
            },
        })
        .then((willDelete) => {
            switch (willDelete) {
                case "Yes":
                    if (willDelete) {
                        var reqData = {
                            'jsonrpc' : '2.0',
                            '_token' : '{{csrf_token()}}',
                            'params' : {
                                'roomId':roomId,
                                'userId':userId,
                            }
                        };
                        $.ajax({
                            url: "{{ route('report.user') }}",
                            method: 'post',
                            dataType: 'json',
                            data: reqData,
                            success: function(response){

                                currentContent.hide();

                                // console.log(response);
                                // if(response.result.call_complete==1){
                                //     activeRoom.disconnect();

                                // }
                            }, error: function(error) {
                                console.log(error)
                            }
                        });
                    } else {
                        return false;
                    }
                break;
            }
        });
    });
</script>
@endpush
