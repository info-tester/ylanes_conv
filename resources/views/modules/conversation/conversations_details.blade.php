@extends('layouts.app')
@section('title','Conversation Details')
@push('css')
<style type="text/css">
	.if_rejected{
		background:#ffabab;
	}
    .popupbulates::marker{
        font-size: 15px
    }
</style>
@endpush
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>
						@if(@$converstion_details->type == 'CO')
						Premium
						@elseif(@$converstion_details->type == 'OP')
						Standard
						@else
						one-on-one
						@endif
					</h2>
				</div>
				<div class="dash-right-inr">
					@include('layouts.message')
					<div class="all_ists_dashs coinn_mainn">
						<div class="conv_dtls_main">
							<div class="conv_dtls_left">
								<h1><img src="{{ url('public/frontend/images/fire.png')}}" alt=""> {{ @$converstion_details->topic->topic_line_1 }}
                                    @if(@$converstion_join_current_users->status=='C' && @$converstion_join_current_users->is_evicted=='N' && @$converstion_join_current_users->is_leave_parament=='N' && (strtotime(date('Y-m-d H:i:s').' + 5 minutes') > strtotime($converstion_details->date.' '.$converstion_details->time )))
                                    <a href="javascript:;" class="brw_a click_video_call" style="cursor: pointer;margin-top: 0px;height: 25px; font-size: 13px;line-height: 27px;float: right;font-weight: 500;">Join Call</a>
                                    @endif
                                </h1>
								<p>{{ @$converstion_details->topic->topic_line_2 }}</p>
							</div>
							<div class="conv_dtls_right">
								<span><img src="{{ url('public/frontend/images/upcoming_con.png')}}" alt=""> {{ date('dS M, Y', strtotime(@$converstion_details->date)) }} <strong>{{ date('h:i A', strtotime(@$converstion_details->time)) }}</strong></span>
							</div>
						</div>

						<!--======================-->
						@if(@$converstion_details->type == 'CO' || @$converstion_details->type == 'ON')
						<h2 class="no_reqq">Requests : {{ @$converstion_details->no_of_participants }}</h2>
						@else
						<h2 class="no_reqq share-link-11">Registered users : {{ @$converstion_details->no_of_participants }}
                            <button class="shareLink openshare converstion-details-share-link" data-id="{{ @$converstion_details->id }}" type="button" >
                                <img src="{{url('public/frontend/images/share.png')}}" alt="share" style="width: 20px;">
                            </button>
                        </h2>
						@endif
						<div class="acept_rejectt">
							@if($converstion_join_users->isNotEmpty())
							@foreach($converstion_join_users as $keys => $users)
							@if($keys <= 15)
							<div class="acept_rejectt_boxx @if(@$users->status == 'C')if_approver @elseif(@$users->status == 'R') if_rejected @else @if(@$loop->iteration % 2 == 0) shadow_none @endif  @endif">
								<div class="user_pic">
									@if(@$users->is_anonymously == 'N')
									@if(!empty(@$users->users->image))
									<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank">
										<img src="{{ url('storage/app/public/customer/profile_pics/'.@$users->users->image) }}" alt="user">
									</a>
									@else
									<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank">
										<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
									</a>
									@endif
									@else
									<div class="topic_users" style="padding: 0px; margin: 0px;">
										<span class="topp_user_03" style="margin-left: 0px;  font-size: 13px; border: 1px solid #d7d7d7; padding: 3px 4px 0 0 !important;">
											{{ substr(@$users->display_name,0,2 )}}
										</span>
									</div>
									@endif
								</div>
								<div class="users_infooo">
									@if(@$users->is_anonymously == 'N')
									@if(!empty(@$users->users->profile_name))
									<h2>
										<a href="{{ route('public.profile',@$users->user_id) }}" style="color:#ffcb00;" target="_blank">{{ substr(@$users->users->profile_name,0,7) }}</a>
										@if(@$users->is_creator == 'Y')
										<em><b>(Host)</b></em>
										@else
										@if(@$keys == 0)
										<em><b>(Host)</b></em>
										@endif
										@endif
									</h2>
									@else
									<h2>
										<a href="{{ route('public.profile',@$users->user_id) }}" style="color:#ffcb00;" target="_blank">{{ substr(@$users->users->first_name,0,7) }}</a>
										@if(@$users->is_creator == 'Y')
										<em><b>(Host)</b></em>
										@else
										@if(@$keys == 0)
										<em><b>(Host)</b></em>
										@endif
										@endif
									</h2>
									@endif
									@else
									<h2>
										{{ @$users->display_name}}
										@if(@$users->is_creator == 'Y')
										<em><b>(Host)</b></em>
										@else
										@if(@$keys == 0)
										<em><b>(Host)</b></em>
										@endif
										@endif
									</h2>
									@endif
									<p><img src="{{ url('public/frontend/images/gryI.png')}}" alt="icon"> {{ @$users->conversation_text }}</p>
								</div>
								<div class="userr_heart">
									<img src="{{ url('public/frontend/images/heart.png')}}" alt="icon">
									<span>{{ CustomHelper::number_format_short(@$users->users->tot_hearts) }} - ({{ CustomHelper::number_format_short(@$users->users->tot_conversation_completed) }})</span>
								</div>
								<div class="actionn_option">
									@if(@$converstion_details->type == 'CO' || @$converstion_details->type == 'ON')
									@if(@$users->status == 'C')
									<p><img src="{{ url('public/frontend/images/tickk.png')}}" alt="icon"> Approved</p>
									@elseif(@$users->status == 'R')
									<p><img src="{{ url('public/frontend/images/tickk.png')}}" alt="icon"> Rejected</p>
									@else
									@if(@$is_creator == 'yes')
									<a href="{{ route('conversation.user.approved.reject',[@$users->id,@$converstion_details->id,'A']) }}" onclick="return confirm('Do you want to approve this request?');"><i class="fa fa-check" aria-hidden="true"></i></a>
									<a href="{{ route('conversation.user.approved.reject',[@$users->id,@$converstion_details->id,'R']) }}" onclick="return confirm('Do you want to reject this request?');"><i class="fa fa-times" aria-hidden="true"></i></a>
									@else
									Request Pending
									@endif
									@endif
									@else
									<p><img src="{{ url('public/frontend/images/tickk.png')}}" alt="icon"> Joined</p>
									@endif
								</div>
							</div>
							@endif
							@endforeach
							@endif
							<!--open all-->
							@if($converstion_join_users->count() > 15)
							<div class="click_show_all" id="usersrow3" style="display: none;">
								@if($converstion_join_users->isNotEmpty())
								@foreach($converstion_join_users as $keys => $users)
								@if($keys > 15)
								<div class="acept_rejectt_boxx @if(@$users->status == 'C')if_approver @elseif(@$users->status == 'R') if_rejected @else @if(@$loop->iteration % 2 == 0) shadow_none @endif @endif">
									<div class="user_pic">
										@if(@$users->is_anonymously == 'N')
										@if(!empty(@$users->users->image))
										<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank">
											<img src="{{ url('storage/app/public/customer/profile_pics/'.@$users->users->image) }}" alt="user">
										</a>
										@else
										<a href="{{ route('public.profile',@$users->user_id) }}" target="_blank">
											<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
										</a>
										@endif
										@else
										<div class="topic_users" style="padding: 0px; margin: 0px;">
											<span class="topp_user_03" style="margin-left: 0px;  font-size: 13px; border: 1px solid #d7d7d7; padding: 3px 4px 0 0 !important;">
												{{ substr(@$users->display_name,0,2 )}}
											</span>
										</div>
										@endif
									</div>
									<div class="users_infooo">
										@if(@$users->is_anonymously == 'N')
										@if(!empty(@$users->users->profile_name))
										<h2><a href="{{ route('public.profile',@$users->user_id) }}" style="color:#ffcb00;" target="_blank">{{ substr(@$users->users->profile_name,0,7) }}</a></h2>
										@else
										<h2>{{ substr(@$users->users->first_name,0,7) }}</h2>
										@endif
										@else
										<h2>{{ @$users->display_name }}</h2>
										@endif
										<p><img src="{{ url('public/frontend/images/gryI.png')}}" alt="icon"> {{ @$users->conversation_text }}</p>
									</div>
									<div class="userr_heart">
										<img src="{{ url('public/frontend/images/heart.png')}}" alt="icon">
										<span>{{ CustomHelper::number_format_short(@$users->users->tot_hearts) }} - ({{ CustomHelper::number_format_short(@$users->users->tot_conversation_completed) }})</span>
									</div>
									<div class="actionn_option">
										@if(@$converstion_details->type == 'CO')
										@if(@$users->status == 'C')
										<p><img src="{{ url('public/frontend/images/tickk.png')}}" alt="icon"> Approved</p>
										@elseif(@$users->status == 'R')
										<p><img src="{{ url('public/frontend/images/tickk.png')}}" alt="icon"> Rejected</p>
										@else
										@if(@$is_creator == 'yes')
										<a href="{{ route('conversation.user.approved.reject',[@$users->id,@$converstion_details->id,'A']) }}" onclick="return confirm('Do you want to approve this request?');"><i class="fa fa-check" aria-hidden="true"></i></a>
										<a href="{{ route('conversation.user.approved.reject',[@$users->id,@$converstion_details->id,'R']) }}" onclick="return confirm('Do you want to reject this request?');"><i class="fa fa-times" aria-hidden="true"></i></a>
										@else
										Request Pending
										@endif
										@endif
										@else
										<p><img src="{{ url('public/frontend/images/tickk.png')}}" alt="icon"> Joined</p>
										@endif
									</div>
								</div>
								@endif
								@endforeach
								@endif
							</div>

							<div id="users3" class="showw_moree">
								<span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
							</div>
							@endif
							<!--open all-->

						</div>
						<!--======================-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@if(@$converstion_join_current_users->status=='C' && @$converstion_join_current_users->is_evicted=='N' && @$converstion_join_current_users->is_leave_parament=='N')
<div class="modal popup_list sign_popup" id="videoCallJoinConfirmation">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body rule">
                    <div class="singup_right_inr">
                        <h1>Enter Room </h1>
                        <form id="videoCallJoinConfirmationform" action="{{route('join.video.call',['conversationId'=>$converstion_details->id])}}">
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <h4>Rules Of Engagement</h4>
                                    </div>
                                    <div class="col-sm-12">
                                        <ul style="list-style: decimal; padding: 1px 0px 0px 20px;">
                                            <li class="popupbulates"> Speak with authenticity (Try to speak in  “I” statements)</li>
                                            <li class="popupbulates">Listen with intent (Try to understand; avoid judgements)</li>
                                            <li class="popupbulates">Engage with respect </li>
                                            <li class="popupbulates">Maintain confidentiality</li>
                                            <li class="popupbulates">Avoid advising (Share relevant  experiences, if any)</li>
                                        </ul>
                                        <div class="category-ul">
                                            <input type="checkbox" name="accept_terms" id="agree-video" value="Y" class="required">
                                            <label for="agree-video" >I agree</label>
                                        </div>
                                        <label id="accept_terms-error" class="error" for="accept_terms" style="display: none;">This field is required.</label>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <button class="pg_btn" >Enter</button>
                                    {{-- <a class="pg_btn" href="javascript:void(0);" >Enter</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="modal fade what_modal" id="myModalshare">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="share_slidars">
					<div class="share_icon_slid">
						<div class="owl-carousel">
							<div class="item">
								<div class="st-custom-button modalshare" data-network="facebook" data-url="#">
									<em><i class="icofont-facebook"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="whatsapp" data-url="#">
									<em><i class="icofont-whatsapp"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="twitter" data-url="#">
									<em><i class="icofont-twitter"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="linkedin" data-url="#">
									<em><i class="icofont-linkedin"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="snapchat" data-url="#">
									<em><i class="icofont-snapchat"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="reddit" data-url="#">
									<em><i class="icofont-reddit"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="pinterest" data-url="#">
									<em><i class="icofont-pinterest"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="email" data-url="#">
									<em><i class="icofont-envelope"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="telegram" data-url="#">
									<em><i class="icofont-paper-plane"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="evernote" data-url="#">
									<em><i class="evernote"><img src="{{url('public/frontend/images/evernote-brands.png')}}"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="googlebookmarks" data-url="#">
									<em><i class="sos_gool_io_c"><img src="{{url('public/frontend/images/social_google.png')}}" alt=""></i></em>
								</div>
							</div>
							<div class="item">
								<div class="modalshare copyLink" onclick="copyToClipboard()">
									<em><i class="icofont-link"></i></em>
									<input type="text" placeholder="enter your address" id="myInput" style="opacity: 0;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=#{property?._id}&product=custom-share-buttons" defer></script>
<script>
    $('.openshare').click(function(){
		$('#myModalshare').modal('show');
        var dataIds = $(this).data('id');
		$('.modalshare').attr('data-url',"{{ route('conversation.details.page') }}"+dataIds);
		$('input#myInput').attr('value','{{ route('conversation.details.page') }}'+dataIds);
	});
    $('.click_video_call').click(function(){

        $('#videoCallJoinConfirmation').modal('show');
    })
    $('#videoCallJoinConfirmationform').validate({
        ignore: [],
        rules: {
            accept_terms:{
                required:true,
            },
        },
        messages: {
            accept_terms:{
                required:'Please agree all rule',
            },
        },
    });
    $(document).ready(function() {
		var owl = $('.share_icon_slid .owl-carousel');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			autoplay: false,
			loop: true,
			responsive: {
				0: {
					items: 5
				},
				350: {
					items: 6
				},
				415: {
					items: 7
				},
				480: {
					items: 5
				},
				520: {
					items: 6
				},
				768: {
					items: 5
				},
				991: {
					items: 6
				},
				1000: {
					items: 6
				}
			}
		})
	})
</script>
@endpush

