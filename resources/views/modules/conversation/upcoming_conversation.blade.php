@extends('layouts.app')
@section('title','My Rooms')
@section('content')
<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>My Rooms </h2>
				</div>
				<div class="dash-right-inr">
					<div class="dash_top_search arrow_box">
						<form>
							<div class="row">
								<div class="col-md-4 col-sm-6">
									<div class="das_inputss">
										<label>Conversations Type</label>
										<select name="type">
											<option value="">All</option>
											<option value="CO" @if(request()->type == 'CO') selected="" @endif>Premium</option>
											<option value="OP" @if(request()->type == 'OP') selected="" @endif>Standard</option>
											<option value="ON" @if(request()->type == 'ON') selected="" @endif>one-on-one </option>
										</select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6 p-0">
									<button class="btns_find"><img src="{{ url('public/frontend/images/fins.png')}}">Find</button>
								</div>
							</div>
						</form>
					</div>

					<div class="all_ists_dashs">
						@include('layouts.message')
						<div class="row">
							@if(@$allConversation->isNotEmpty())
							@foreach(@$allConversation as $conversation)
							<div class="col-md-6">
								<div class="outer_area">
									<div class="lists_tops pd-12">
										<h2>{{ substr(@$conversation->topic->topic_line_1,0,30) }}</h2>
										<p>{{ substr(@$conversation->topic->topic_line_2,0,57) }} </p>
										<a href="javascript:void(0);" data-id="{{ @$conversation->id }}" class="cross_btns openshare" style="right: 40px;"><img src="{{ url('public/frontend/images/referral.png') }}" style="filter: brightness(0.5);"> </a>
										<a href="{{ route('cancel.conversation.by.user',@$conversation->id) }}" class="cross_btns" onclick="return confirm('Do you want to cancel this conversation?');"><img src="{{ url('public/frontend/images/cross_btns.png') }}"> </a>
									</div>
									@if(!empty(@$conversation->users))
									<div class="lists_mids pd-10">
										<em>
											@if(@$conversation->conversationstouserDef->is_anonymously == 'N')
											@if(!empty(@$conversation->users->image))
											<img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversation->users->image) }}" alt="user">
											@else
											<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
											@endif

											@else
											<div class="topic_users" style="padding: 0px; margin: 0px;">
												<span class="topp_user_03" style="margin-left: 0px; border: 1px solid #d7d7d7; font-style: normal">
													{{ substr(@$conversation->conversationstouserDef->display_name,0,2 )}}
												</span>
											</div>
											@endif
										</em>
										<div class="midls_list_info">
											@if(@$conversation->conversationstouserDef->is_anonymously == 'N')
											@if(!empty(@$conversation->users->profile_name))
											<h4>{{ substr(@$conversation->users->profile_name,0,20) }}</h4>
											@else
											<h4>{{ substr(@$conversation->users->first_name,0,20) }}</h4>
											@endif
											@else
											<h4>{{ @$conversation->conversationstouserDef->display_name}}</h4>
											@endif
											<div class="div_fles">
												{{-- <p><img src="{{ url('public/frontend/images/gryI.png')}}" alt="icon"> {{ substr(@$conversation->conversationstouserDef->conversation_text,0,30) }} @if(substr(@$conversation->conversationstouserDef->conversation_text,30))... @endif</p> --}}
												<p><img src="{{ url('public/frontend/images/gryI.png')}}" alt="icon"> {{substr(@$conversation->conversationstouserDef->conversation_text,0,40) }}</p>
												<span><img src="{{ url('public/frontend/images/user_wishlist.png')}}" alt="icon">{{ CustomHelper::number_format_short(@$conversation->conversationstouserDef->users->tot_hearts) }} -  ({{ CustomHelper::number_format_short(@$conversation->conversationstouserDef->users->tot_conversation_completed) }})</span>
											</div>
										</div>
									</div>
									@else
									@if(!empty(@$conversation->conversationstouserDef2))
									<div class="lists_mids pd-10">
										<em>
											@if(@$conversation->conversationstouserDef2->is_anonymously == 'N')
											@if(!empty(@$conversation->conversationstouserDef2->users->image))
											<img src="{{ url('storage/app/public/customer/profile_pics/'.@$conversation->conversationstouserDef2->users->image) }}" alt="user">
											@else
											<img src="{{url('public/frontend/images/default_pic.png')}}" alt="user">
											@endif
											@else
											<div class="topic_users" style="padding: 0px; margin: 0px;">
												<span class="topp_user_03" style="margin-left: 0px; border: 1px solid #d7d7d7; font-style: normal">
													{{ substr(@$conversation->conversationstouserDef2->display_name,0,2 )}}
												</span>
											</div>
											@endif
										</em>
										<div class="midls_list_info">
											@if(@$conversation->conversationstouserDef2->is_anonymously == 'N')
											@if(!empty(@$conversation->conversationstouserDef2->users->profile_name))
											<h4>{{ substr(@$conversation->conversationstouserDef2->users->profile_name,0,20) }}</h4>
											@else
											<h4>{{ substr(@$conversation->conversationstouserDef2->users->first_name,0,20) }}</h4>
											@endif
											@else
											<h4>{{ @$conversation->conversationstouserDef2->display_name}}</h4>
											@endif
											<div class="div_fles">
												{{-- <p><img src="{{ url('public/frontend/images/gryI.png')}}" alt="icon"> {{ substr(@$conversation->conversationstouserDef2->conversation_text,0,30) }} @if(substr(@$conversation->conversationstouserDef2->conversation_text,30))... @endif</p> --}}
												<p><img src="{{ url('public/frontend/images/gryI.png')}}" alt="icon"> {{substr(@$conversation->conversationstouserDef2->conversation_text,0,40)}}</p>
												<span><img src="{{ url('public/frontend/images/user_wishlist.png')}}" alt="icon">{{ CustomHelper::number_format_short(@$conversation->conversationstouserDef2->users->tot_hearts) }} -  ({{ CustomHelper::number_format_short(@$conversation->conversationstouserDef2->users->tot_conversation_completed) }})</span>
											</div>
										</div>
									</div>
									@endif
									@endif

									<div class="pd-10">
										<div class="dateTime">
											<p class="dt"><img src="{{ url('public/frontend/images/watch.png')}}" alt="watch"><span>{{ date('dS M, Y', strtotime(@$conversation->date)) }}</span><span>{{ date('h:i A', strtotime(@$conversation->time)) }}</span></p>
											@if(!empty(@$conversation->users))
											<p class="userP"><img class="person" src="{{ url('public/frontend/images/persons.png')}}" alt="persons">Host:
												@if(@$conversation->creator_user_id == @auth()->user()->id)
												@if(!empty(@$conversation->users->profile_name))
												{{ substr(@$conversation->users->profile_name,0,10) }}
												@else
												{{ substr(@$conversation->users->first_name,0,10) }}
												@endif
												@else
												@if(!empty(@$conversation->users))
												@if(!empty(@$conversation->users->profile_name))
												{{ substr(@$conversation->users->profile_name,0,10) }}
												@else
												{{ substr(@$conversation->users->first_name,0,10) }}
												@endif
												@endif
												@endif
											</p>
											@elseif(@$conversation->created_by == 'A')
											@if(!empty(@$conversation->conversationstouserDef2))
											<p class="userP"><img class="person" src="{{ url('public/frontend/images/persons.png')}}" alt="persons">Host:
												@if(@$conversation->conversationstouserDef2->is_anonymously == 'N')
												{{ substr(@$conversation->conversationstouserDef2->users->profile_name,0,15) }}
												@else
												{{ substr(@$conversation->conversationstouserDef2->display_name,0,15) }}
												@endif
											</p>
											@endif
											@endif
										</div>
									</div>

									<div class="moreInfo_sec">
										<ul>
											@if(@$conversation->type == 'CO')
											<li class="request">
												<p>Requests<span>{{ @$conversation->conversationsconditionalCountUser->count() }}</span></p>
											</li>
											@endif
											<li class="conditional">
												@if(@$conversation->type == 'CO')
												<p>Premium</p>
												@elseif(@$conversation->type == 'OP')
												<p>Standard</p>
												@else
												<p>one-on-one </p>
												@endif
											</li>
										</ul>
                                        @if(@$conversation->conversationstousers[0]->status=='C' && @@$conversation->conversationstousers[0]->is_evicted=='N' && @@$conversation->conversationstousers[0]->is_leave_parament=='N' && (strtotime(date('Y-m-d H:i:s').' + 5 minutes') > strtotime($conversation->date.' '.$conversation->time )))
										<ul>
											<li class="info">
												<a href="javascript:;" data-conversationId="{{$conversation->id}}" class="join_call_link">Join Call</a>
											</li>
										</ul>
                                        @endif
										<ul>
											<li class="info">
												<a href="{{ route('view.conversation.details',@$conversation->id) }}">More Info<img src="{{ url('public/frontend/images/moreinfo-arrow.png')}}" alt="arrow"></a>
											</li>
										</ul>
									</div>



								</div>
							</div>
							@endforeach
							@else
							<div class="col-md-12 text-center">
								<img src="{{ url('public/frontend/no-data-found.png') }}" style="width: 200px; ">
								<h3>No upcoming room, please create a room and start having a conversation.</h3>
                                <a href="{{ route('create.conversation') }}" class="create_new_upcomming">Create room</a>
							</div>
							@endif
						</div>
						@if(@$allConversation->total() > 4)
						<div class="pagination_bx text-left">
							{{ @$allConversation->appends(['type' => @request()->type])->links("pagination::bootstrap-4") }}
						</div>
						@endif
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade what_modal" id="myModalshare">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="share_slidars">
					<div class="share_icon_slid">
						<div class="owl-carousel">
							<div class="item">
								<div class="st-custom-button modalshare" data-network="facebook" data-url="#">
									<em><i class="icofont-facebook"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="whatsapp" data-url="#">
									<em><i class="icofont-whatsapp"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="twitter" data-url="#">
									<em><i class="icofont-twitter"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="linkedin" data-url="#">
									<em><i class="icofont-linkedin"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="snapchat" data-url="#">
									<em><i class="icofont-snapchat"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="reddit" data-url="#">
									<em><i class="icofont-reddit"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="pinterest" data-url="#">
									<em><i class="icofont-pinterest"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="email" data-url="#">
									<em><i class="icofont-envelope"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="telegram" data-url="#">
									<em><i class="icofont-paper-plane"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="evernote" data-url="#">
									<em><i class="evernote"><img src="{{url('public/frontend/images/evernote-brands.png')}}"></i></em>
								</div>
							</div>
							<div class="item">
								<div class="st-custom-button modalshare" data-network="googlebookmarks" data-url="#">
									<em><i class="sos_gool_io_c"><img src="{{url('public/frontend/images/social_google.png')}}" alt=""></i></em>
								</div>
							</div>
							<div class="item">
								<div class="modalshare copyLink" onclick="copyToClipboard()">
									<em><i class="icofont-link"></i></em>
									<input type="text" placeholder="enter your address" id="myInput" style="opacity: 0;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal popup_list sign_popup" id="videoCallJoinConfirmation">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body rule">
                    <div class="singup_right_inr">
                        <h1>Enter Room </h1>
                        <form id="videoCallJoinConfirmationform">
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <h4>Rules Of Engagement</h4>
                                    </div>
                                    <div class="col-sm-12">
                                        <ul style="list-style: decimal; padding: 1px 0px 0px 20px;">
                                            <li class="popupbulates"> Speak with authenticity (Try to speak in  “I” statements)</li>
                                            <li class="popupbulates">Listen with intent (Try to understand; avoid judgements)</li>
                                            <li class="popupbulates">Engage with respect </li>
                                            <li class="popupbulates">Maintain confidentiality</li>
                                            <li class="popupbulates">Avoid advising (Share relevant  experiences, if any)</li>
                                        </ul>
                                        <div class="category-ul">
                                            <input type="checkbox" name="accept_terms" id="agree-video" value="Y" class="required">
                                            <label for="agree-video" >I agree</label>
                                        </div>
                                        <label id="accept_terms-error" class="error" for="accept_terms" style="display: none;">This field is required.</label>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <button class="pg_btn" >Enter</button>
                                    {{-- <a class="pg_btn" href="javascript:void(0);" >Enter</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=#{property?._id}&product=custom-share-buttons" defer></script>
<script>
	$('.openshare').click(function(){
		$('#myModalshare').modal('show');
		var dataIds = $(this).data('id');
		$('.modalshare').attr('data-url',"{{ route('conversation.details.page') }}"+dataIds);
		$('input#myInput').attr('value','{{ route('conversation.details.page') }}'+dataIds);
	});
	function copyToClipboard(text) {
		var inputc = $('#myInput');
		inputc.value = window.location.href;
		inputc.focus();
		inputc.select();
		document.execCommand('copy');
		alert("URL Copied.");
	}
	$(function() {
		$("#datepicker").datepicker({
			minDate: new Date(),
		});
	});
	$("input[name=time]").clockpicker({
		placement: 'bottom',
		align: 'left',
		autoclose: true,
		default: 'now',
		donetext: "Select",
		init: function() {
			console.log("colorpicker initiated");
		},
		beforeShow: function() {
			console.log("before show");
		},
		afterShow: function() {
			console.log("after show");
		},
		beforeHide: function() {
			console.log("before hide");
		},
		afterHide: function() {
			console.log("after hide");
		},
		beforeHourSelect: function() {
			console.log("before hour selected");
		},
		afterHourSelect: function() {
			console.log("after hour selected");
		},
		beforeDone: function() {
			console.log("before done");
		},
		afterDone: function() {
			console.log("after done");
		}
	});
	$(document).ready( function(){
		$('#category').change(function(){
			catId = $(this).val();
			$.ajax({
				url:"{{ route('get.sub.category') }}/"+catId,
				type:"GET",
				success: function(responce){
					$('#subcategory').html(responce.result.get_sub_categories);
				},
				error: function(xhr){
					console.log(xhr);
				}
			});
		});
		$('#subcategory').change(function(){
			subcategoryId = $(this).val();
			$.ajax({
				url:"{{ route('get.sub.category.topic') }}/"+subcategoryId,
				type:"GET",
				success: function(responce){
					$('#topic').html(responce.result.get_topic);
				},
				error: function(xhr){
					console.log(xhr);
				}
			});
		});
		$('#conversationForm').validate();
	})
	$(document).ready(function() {
		var owl = $('.share_icon_slid .owl-carousel');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			autoplay: false,
			loop: true,
			responsive: {
				0: {
					items: 5
				},
				350: {
					items: 6
				},
				415: {
					items: 7
				},
				480: {
					items: 5
				},
				520: {
					items: 6
				},
				768: {
					items: 5
				},
				991: {
					items: 6
				},
				1000: {
					items: 6
				}
			}
		})
	})
</script>
<script>
    $('body').on('click', '.join_call_link', function() {
        console.log($(this).data('conversationid'))
        var conversationId= $(this).data('conversationid');
        $('#videoCallJoinConfirmationform').attr('action', '{{route('join.video.call')}}/'+conversationId)
        $('#videoCallJoinConfirmation').modal('show');
    })
    $('#videoCallJoinConfirmationform').validate({
        ignore: [],
        rules: {
            accept_terms:{
                required:true,
            },
        },
        messages: {
            accept_terms:{
                required:'Please agree all rule',
            },
        },
    });
</script>
@endpush
