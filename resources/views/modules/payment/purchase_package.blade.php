@extends('layouts.app')



@section('title', 'Pay Now')
@push('css')
    <style>
        .ReferCon .row{
            justify-content: center;
        }
        .payment {
            border-radius: 6px;
            box-shadow: 0px 0px 16px 0px rgb(174 174 174 / 50%);

        }
        .payment_text{
            padding: 20px 10px 10px 10px;
            font-size: 20px;
            line-height: 26px;

        }
        .brw_a{
            text-align: center;
            width: 100%;
        }
        .payment_text label{
            line-height: 38px;
        }
        .payment_text h3 {
            text-align: center;
            line-height: 55px;
        }
    </style>
@endpush
@section('content')

<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>Purchase Package</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="ReferCon">
							<div class="row">
                                <div class="col-md-6 col-sm-12 payment">
                                    <div class="form-group payment_text">
                                        <h3>Package Information</h3>
                                        <label>Package Paid Coin:  </label> {{ @$package->coin_paid }}
                                        <br/>
                                        <label>Package Free Coin:  </label> {{ @$package->coin_free }}
                                        <br/>
                                        <label>Package Total Coin:  </label> {{ @$package->coin_total }}
                                        <br/>
                                        <label>Package Price (INR) :  </label> {{@$package->price}}
                                        <br/>
                                        <span class="sm_size"><a href="{{ route('purchase.package.user',@$package->id) }}" class="brw_a">Pay Now</a></span>
                                    </div>
                                </div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('js')


@endpush
