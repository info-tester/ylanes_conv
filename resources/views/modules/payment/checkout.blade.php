@extends('layouts.app')



@section('title', 'Pay Now')
@push('css')
    <style>
        .ReferCon .row{
            justify-content: center;
        }
        .payment {
            border-radius: 6px;
            box-shadow: 0px 0px 16px 0px rgb(174 174 174 / 50%);

        }
        .payment_text{
            padding: 20px 10px 10px 10px;
            font-size: 20px;
            line-height: 26px;

        }
        .brw_a{
            text-align: center;
            width: 100%;
        }
        .payment_text label{
            line-height: 38px;
        }
        .payment_text h3 {
            text-align: center;
            line-height: 55px;
        }
    </style>
@endpush
@section('content')

<div class="dashboard_inner">
	<div class="container">
		<div class="row">
			@include('layouts.user_sideber')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
					<h2>Purchase Package</h2>
				</div>
				<div class="dash-right-inr">
					<div class="dashboard_bodys">
						@include('layouts.message')
						<div class="ReferCon">
							<div class="row">
                                <div class="col-md-6 col-sm-12 payment">
                                    <div class="form-group payment_text">
                                        <h3>Package Information</h3>
                                        <label>Package Paid Coin:  </label> {{ @$package->coin_paid }}
                                        <br/>
                                        <label>Package Free Coin:  </label> {{ @$package->coin_free }}
                                        <br/>
                                        <label>Package Total Coin:  </label> {{ @$package->coin_total }}
                                        <br/>
                                        <label>Package Price (INR) :  </label> {{@$package->price}}
                                        <br/>
                                        {{-- <span class="sm_size"><a href="{{ route('purchase.package.user',@$package->id) }}" class="brw_a">Pay Now</a></span> --}}
                                    </div>
                                </div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<div class="pe_top"></div>

<div class="make_payment_sec">

    <div class="container">

        <div class="col-sm-3">
		<h1>Payment Processing</h1>

			<button type="submit" class="pg_btn" id="rzp-button1" style="visibility:hidden;">Pay Now</button>

		 </div>

    </div>

</div>

@endsection



@push('js')


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var options = {
    "key": "{{env('RAZORPAY_KEY')}}", // Enter the Key ID generated from the Dashboard
    "amount": "{{$amount*100}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "YLanes",
    "description": "Deal purchase for deal ID {{$token_id}}",
    "image": "{{asset('public/frontend/images/logo.png')}}",
    "order_id": "{{$order_id}}", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "callback_url": "{{route('save.payment.confirmation',['id'=>$token_id])}}",
    "prefill": {
        "name": "{{$user_details->profile_name}}",
        "email": "{{Auth::user()->email}}",
        "contact": "{{Auth::user()->phone}}"
    },
    "notes": {
        "pay_id": "{{$pay_id}}"
    },
    "theme": {
        "color": "#3399cc"
    },
	"modal": {
        "escape": false,
		"confirm_close":true,
		"ondismiss":function(){
            window.location.href="{{route('purchase.package')}}";
		}
    }
};
var rzp1 = new Razorpay(options);
rzp1.on('payment.failed', function (response){
        // alert(response.error.code);
        // alert(response.error.description);
        // alert(response.error.source);
        // alert(response.error.step);
        // alert(response.error.reason);
        // alert(response.error.metadata.order_id);
        // alert(response.error.metadata.payment_id);
});
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
document.getElementById("rzp-button1").click();
</script>

@endpush
