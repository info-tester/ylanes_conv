<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->


    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

    <!-- CSS Reset : BEGIN -->
    <style>

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
            background: #f1f1f1;
        }
        .click_btn:hover {
            background: #ffe88b !important;
            color: #000 !important;
        }
    </style>


</head>


<body>
    <style type="text/css">
        .linkk:hover {
            background: #01b02e !important;
        }
    </style>
    <div style="max-width:640px; margin:0 auto;">
        <div
            style="background: #fff; border:1px solid transparent; border-bottom: none;height: 100px; margin: -9px 0px -13px 0px;">
            <div
                style="float: none; text-align: center; margin-top: 20px; background:url('{{ URL::to('#') }}') repeat center center">
                <img src="{{ URL::to('public/frontend/images/logo.png') }}" width="135" alt="">
            </div>
        </div>
        <div style="max-width:620px; border:1px solid transparent; margin:0 0; padding:15px; ">

            <div style="display:block; overflow:hidden; width:100%;">
                Dear Admin,
                <p>
                  <b>You have received an enquirey from {{@$data['name']}} </b>
                </p>
                <p>
                    <h1><b><u>Here are the details :</u></b> </h1>
                    <br>
                    <p><b>Name :</b> {{@$data['name']}} </p>
                    <p><b>Email :</b> {{@$data['user_email']}} </p>
                    @if(@$data['number'])<p><b>Phone :</b> {{@$data['number']}} </p>@endif
                    <p><b>Message:</b> {{@$data['message']}} </p>
                </p>

            </div>

            <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">
                Thank you,<br>
                Team YLanes
            </p>

        </div>
    </div>
</body>
</html>
