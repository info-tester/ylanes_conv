<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->


    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

    <!-- CSS Reset : BEGIN -->
    <style>

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
            background: #f1f1f1;
        }
        .click_btn:hover {
            background: #ffe88b !important;
            color: #000 !important;
        }
    </style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; font-family:'Roboto'; ">
	<center style="width: 100%; background-color: #f1f1f1;">
       <div style="max-width: 550px; background: #fff;">
          <table style="padding: 20px; padding-top: 50px; width: 100%; ">
            <thead>
                <tr>
                    <td style="text-align: center;">
                        <img src="{{ URL::to('public/frontend/images/logo.png') }}" style="width: 140px;">            
                    </td>
                </tr>
            </thead>
            <tbody style="border:1px solid #f1f1f1; display: block; padding: 20px 10px;">
                <tr>                    
                    <td>
                        <h2 style="margin: 0; color: #4e4e4e; font-size: 25px; font-weight: 500; margin-bottom: 25px;">Welcome to YLanes!</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 16px; line-height: 24px; color: #5f5f5f; margin-top: 0; margin-bottom: 10px;">Thank you for registering on YLanes. We are delighted to welcome you to our site</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 16px; line-height: 24px; color: #5f5f5f;  margin-top: 0; margin-bottom: 10px;">Before we get started, please click the button below to verify your email address.</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <a href="{{ url('user/update/email/'.@$user->email_vcode.'/'.md5($user->id)) }}" class="click_btn" style="margin: 30px 0 50px 0; display: inline-block; background: #ffca00; color: #fff; text-decoration: none; padding: 8px 20px; border-radius: 25px; ">Click here</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 16px; line-height: 24px; color: #5f5f5f; margin-bottom: 0; margin-top: 25px;">Thank you,</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 16px; line-height: 24px; color: #5f5f5f; margin: 0;">Team YLanes</p>
                    </td>
                </tr>
            </tbody>

        </table>

    </div>

</center>
</body>
</html>