@extends('admin.layouts.app')
@section('title')
Bulk Category Upload
@endsection
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Bulk Category Upload</h4>

                            <a href="{{ route('admin.manage.category') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Bulk Category Upload</h4>
                            <p class="m-t-0 m-b-30">Instructions: <br><br>
                                - You can upload category and sub category in bulk using a csv file. <br>
                                - Please <a href="{{ url('public/backend/Sample_Csv.CSV') }}">click here</a> to download the sample csv file. <br>
                                - The file structure must match with the sample file.   <br>
                                - If you want to add subcategory under a category that is already added make sure you type the category name to match.
                            </p>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.add.category.post') }}">
                                @csrf
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls">
                                    <input type="file" class="custom-file-input required" id="uploaded_file" name="uploaded_file" pattern="^.+\.(csv|CSV)$">
                                    <label class="custom-file-label extrlft" for="uploaded_file">Upload File</label>
                                </div>
                                <div class="col-md-12">
                                    <span class="attachNames"></span>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                            @if(!empty(session()->get('datas')))
                            <div class="col-md-7">
                                <h5>Status of Upload is as follows:</h5>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="row">Category</th>
                                            <th scope="row">Sub Category</th>
                                            <th scope="row">Upload Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(session()->get('datas') as $data)
                                        <tr>
                                            <td>
                                                {{ @$data['category_name'] }}
                                            </td>
                                            <td>
                                                @if(!empty(@$data['sub_category_name']))
                                                {{ @$data['sub_category_name'] }}
                                                @else
                                                --
                                                @endif
                                            </td>
                                            <td>
                                                @if(@$data['is_upload'] == 1)
                                                Success
                                                @else
                                                Failed
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        $('select[name="parent_id"]').change(function(){
            var data = $(this).val();
            if(data != ''){
                $('#descArea').hide();
            }
            else{
                $('#descArea').show();
            }
        });
        
        
        $("#editCustomerForm").validate({
            rules:{
                uploaded_file: {
                    required: true,
                    extension: "csv,CSV"
                },

            },
            messages: { 
                uploaded_file: {
                    required : 'Please choose a csv file!',
                    extension: 'Please upload a csv file!',
                    pattern: 'Please upload a csv file!'
                }
            },
        });
        $('#uploaded_file').change(function(){
            var thisval = $(this).val();
            if(thisval == ''){
                $('input[name="name"]').addClass('required',true);
            }
            else{
                $('input[name="name"]').removeClass('required',false);
            }
            var filenames = '';
            for (var i = 0; i < this.files.length; i++) {
                filenames += '<li><b>' + this.files[i].name + '</b></li>';
            }
            $(".attachNames").html('<ul style="list-style: none;padding-left: 0px;">' + filenames + '</ul>');
        });
    });
</script>
@endpush