@extends('admin.layouts.app')
@section('title')
Add Category
@endsection
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Add Category</h4>

                            <a href="{{ route('admin.manage.category') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Add Category</h4>
                            <p class="m-t-0 m-b-30">If you want to add a category then do NOT select anything from category dropdown. Just write the category name in the Category Title textbox</p>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.add.category.post') }}">
                                @csrf
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="exampleInputEmail2">Select Category</label>
                                    <select class="form-control" name="parent_id" id="exampleInputEmail2">
                                        <option value="">Select Category</option>
                                        @if(@$categories->count() > 0)
                                        @foreach(@$categories as $category)
                                        <option value="{{ @$category->id }}">{{ @$category->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>                                    
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="exampleInputEmail1" id="CatLabel">Category Title</label>
                                    <input type="text" class="form-control required" id="exampleInputEmail1" name="name" placeholder="Enter Category Title" value="{{ old('name') }}">
                                </div>
                                
                                
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls mt-3" id="categoryImageArea">
                                    <input type="file" class="custom-file-input" id="profile_pic" name="categoryPicture">
                                    <label class="custom-file-label extrlft" for="profile_pic" style="margin: 14px 5px 0px 5px;">@lang('admin_lang.upload_picture')</label>
                                    <label class="mt-4"> Recommended size:(158px*158px)</label>                                               
                                    <span class="errorPic" style="color: red;"></span>

                                    
                                    <div class="profile" style="display: none;">
                                        <img src="" id="profilePictures" style="height: 100px;width: 100px;">
                                    </div>
                                </div>
                                {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" id="descArea">
                                    <label for="exampleInputEmail100">Description</label>
                                    <textarea type="text" class="form-control" id="exampleInputEmail100" name="desc" placeholder="Enter description" value="{{ old('description') }}"></textarea>
                                </div> --}}
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        $('select[name="parent_id"]').change(function(){
            var data = $(this).val();
            if(data != ''){
                $('#descArea').hide();
                $('#CatLabel').text('Sub Category Title');
                $('#exampleInputEmail1').attr('placeholder','Enter Sub Category Title');
                $('#categoryImageArea').hide();
            }
            else{
                $('#descArea').show();
                $('#CatLabel').text('Category Title');
                $('#exampleInputEmail1').attr('placeholder','Enter Category Title');
                $('#categoryImageArea').show();
            }
        });
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val());
            if(filename != ""){
                filename_arr = filename.split("."),
                ext = filename_arr[1];
                ext = ext.toLowerCase();
                if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                    $(".profile").show();
                    $(".errorpic").text("");
                    readURL(this);
                }else{
                    $(".errorpic").html('@lang('validation.img_upload')');
                    $('#profilePictures').attr('src', "");
                }
            } 
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(".profiles").show();
                    $('#profilePictures').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        
        $("#editCustomerForm").validate({
            rules:{
                'name':{
                    required:true
                },


            },
            messages: { 
                name: { 
                    required: 'Category title field is required.'
                },
            },
        });

    });
</script>
@endpush