@extends('admin.layouts.app')
@section('title')
Manage Category
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Manage Category</h4>

                            <a href="{{ route('admin.add.category') }}" class="pull-right btn btn-dark"><i class="fa fa-plus"></i> Add Category</a>
                            <a href="{{ route('admin.add.bulk.category') }}" class="pull-right btn btn-dark mr-3"><i class="fa fa-plus"></i> Bulk Category Upload</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form method="get" action="{{ route('admin.manage.category') }}">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto col-md-3">
                                                <label class="sr-only">Select Category</label>
                                                <select class="form-control mb-2" name="category_id">
                                                    <option value="">Select Category</option>  
                                                    @if(@$parent_cat->isNotEmpty())
                                                    @foreach(@$parent_cat as $cat)
                                                    <option value="{{ @$cat->id }}" @if(request()->category_id == @$cat->id) selected="" @endif>{{ @$cat->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-3">
                                                <label class="sr-only" for="inlineFormInput">Keyword</label>
                                                <input type="text" class="form-control mb-2" id="inlineFormInput" name="keyword" placeholder="Keyword" value="{{ @request()->keyword }}">
                                            </div>
                                            {{-- <div class="col-auto col-md-3">
                                                <label class="sr-only">Show in header</label>
                                                <select class="form-control mb-2" name="show_header">
                                                    <option value="">Select show in header</option>  
                                                    <option value="Y" @if(@request()->show_header == 'Y') selected="" @endif>Yes</option>  
                                                    <option value="N" @if(@request()->show_header == 'N') selected="" @endif>No</option>
                                                </select>
                                            </div> --}}
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                <a href="{{ route('admin.manage.category') }}" class="btn btn-info mb-2">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Category Title</th>
                                        <th>Sub Category Title</th>
                                        {{-- <th>Show Header</th> --}}
                                        <th>Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                    <tr>
                                        <td>
                                            @if(@$category->picture)
                                            <img src="{{ url('storage/app/public/category_pics/'.@$category->picture) }}" style="width: 50px;">
                                            @endif
                                        </td>
                                        @if($category->parent_id == 0)
                                        <td>{{$category->name}}</td>
                                        <td>N/A</td>
                                        @else
                                        <td>{{$category->immediateParent->name}}</td>
                                        <td>{{$category->name}}</td>
                                        @endif
                                        {{-- <td>
                                            @if($category->parent_id == 0)
                                            @if($category->show_header == 'Y')
                                            Yes
                                            @else
                                            No
                                            @endif
                                            @else
                                            --
                                            @endif
                                        </td> --}}
                                        <td>
                                            @if($category->status == 'A')
                                            Active
                                            @else
                                            Inactive
                                            @endif
                                        </td>
                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$category->id }}" data-id="{{ @$category->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$category->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        <li><a href="{{ route('admin.edit.category',$category->id) }}" class="text-dark" title="Edit"> Edit</a></li>
                                                        {{-- @if($category->parent_id == 0)
                                                        @if($category->show_header == 'Y')
                                                        <li><a href="{{ route('admin.category.header.status.change',@$category->slug) }}" class="text-dark" title="Hide from header" onclick="return confirm('Do you want to hide category from the header?')"> Hide from header</a></li>
                                                        @else
                                                        <li><a href="{{ route('admin.category.header.status.change',@$category->slug) }}" class="text-dark" title="Show in header" onclick="return confirm('Do you want to show category in the header?')"> Show in header</a></li>
                                                        @endif
                                                        @endif --}}
                                                        @if($category->status == 'A')
                                                        <li><a href="{{ route('admin.change.status.category',$category->slug) }}" class="text-dark" title="Inactive" onclick="return confirm('Do you want to change status for this category ?')"> Inactive</a></li>
                                                        @else
                                                        <li><a href="{{ route('admin.change.status.category',$category->slug) }}" class="text-dark" title="Active" onclick="return confirm('Do you want to change status for this category ?')">Active</a></li>
                                                        @endif
                                                        <li><a href="{{ route('admin.delete.category',$category->id) }}" class="text-dark"  onclick="return confirm(' you want to delete this category?');" title="Delete">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
@push('js')
<script>
    $(".action-dots").click(function() {
        var data_id = $(this).data('id'); 
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
</script>

@endpush