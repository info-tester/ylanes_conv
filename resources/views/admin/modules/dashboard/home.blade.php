@extends('admin.layouts.app')
@section('title')
Dashboard
@endsection
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Dashboard</h4>

                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->


                <div class="row">

                    <div class="col-xl-3 col-sm-6">
                        <a href="{{ route('admin.manage.users') }}">
                            <div class="card-box widget-box-two widget-two-custom">
                                <i class="mdi mdi-account-multiple widget-two-icon"></i>
                                <div class="wigdet-two-content">
                                    <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics">Total Users</p>
                                    <h2 class="font-600"><span><i class="mdi mdi-arrow-up"></i></span> <span data-plugin="counterup">{{ @$users_count }}</span></h2>
                                    <p class="m-0">{{ date('dS M, Y') }}</p>
                                </div>
                            </div>
                        </a>
                    </div><!-- end col -->

                    <div class="col-xl-3 col-sm-6">
                        <a href="{{ route('admin.manage.conversation') }}">
                            <div class="card-box widget-box-two widget-two-custom">
                                <i class="mdi mdi-crown widget-two-icon"></i>
                                <div class="wigdet-two-content">
                                    <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics">Upcoming Conversations</p>
                                    <h2 class="font-600"><span><i class="mdi mdi-arrow-up"></i></span> <span data-plugin="counterup">{{ @$up_conversations }}</span></h2>
                                    <p class="m-0">{{ date('dS M, Y') }}</p>
                                </div>
                            </div>
                        </a>
                    </div><!-- end col -->

                    <div class="col-xl-3 col-sm-6">
                        <a href="{{ route('admin.manage.conversation') }}">
                            <div class="card-box widget-box-two widget-two-custom">
                                <i class="mdi mdi-crown widget-two-icon"></i>
                                <div class="wigdet-two-content">
                                    <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics">Past Conversations</p>
                                    <h2 class="font-600"><span><i class="mdi mdi-arrow-up"></i></span> <span data-plugin="counterup">{{ @$past_conversations }}</span></h2>
                                    <p class="m-0">{{ date('dS M, Y') }}</p>
                                </div>
                            </div>
                        </a>
                    </div><!-- end col -->

                    <div class="col-xl-3 col-sm-6">
                        <a href="{{ route('admin.manage.topic') }}">
                            <div class="card-box widget-box-two widget-two-custom">
                                <i class="mdi mdi-auto-fix widget-two-icon"></i>
                                <div class="wigdet-two-content">
                                    <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics">Topics</p>
                                    <h2 class="font-600"><span><i class="mdi mdi-arrow-up"></i></span> <span data-plugin="counterup">{{ @$topics }}</span></h2>
                                    <p class="m-0">{{ date('dS M, Y') }}</p>
                                </div>
                            </div>
                        </a>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
