@extends('admin.layouts.app')
@section('title')
User Details
@endsection
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">User Details</h4>
                            <a href="{{ route('admin.manage.users') }}" class="pull-right btn btn-dark mr-3"><i class="fa fa-arrow-left"></i> Back</a>
                            <a href="{{ route('admin.user.view.conversation',@$user->id) }}" class="pull-right btn btn-dark mr-3"><i class="fa fa-eye"></i> View Conversation</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="profile-user-box" style="margin-top: 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span class="pull-left m-r-15">
                                        @if(@$user->image)
                                        @php
                                        $image_path = 'storage/app/public/customer/profile_pics/'.@$user->image;
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <img src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$user->image) }}" alt="" class="thumb-lg rounded-circle">
                                        @else
                                        @endif
                                        @else
                                        <img src="{{ url('public/frontend/images/default_pic.png'
                                        )}}" alt="" class="thumb-lg rounded-circle">
                                        @endif
                                    </span>
                                    <div class="media-body">
                                        <h4 class="m-t-5 m-b-5 font-18 ellipsis">{{ @$user->profile_name }}</h4>
                                        {{-- <h5 class="m-t-5 m-b-5 ellipsis">{{ @$user->profile_name }}</h5> --}}
                                        <p class="font-13"> {{ @$user->userCountryDetails->name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ meta -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card-box">
                            <h4 class="header-title mt-0 m-b-20">Personal Information</h4>
                            <div class="panel-body">
                                <div class="text-left">
                                    <p class="text-muted font-13"><strong>User Id :</strong> <span class="m-l-15">{{ @$user->unique_id }}</span></p>
                                    {{-- <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">{{ @$user->first_name }} {{ @$user->last_name }}</span></p> --}}
                                    <p class="text-muted font-13"><strong>Display Name :</strong> <span class="m-l-15">{{ @$user->profile_name  }}</span></p>
                                    <p class="text-muted font-13"><strong>Year Of Birth :</strong> <span class="m-l-15">{{ @$user->year_of_birth  }}</span></p>

                                    <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">{{ @$user->phone }}</span></p>

                                    <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{ @$user->email }}</span></p>
                                    <p class="text-muted font-13"><strong>Gender :</strong> <span class="m-l-15">
                                        @if(@$user->gender)
                                        @if($user->gender=='M')
                                        Male
                                        @elseif($user->gender=='F')
                                        Female
                                        @elseif($user->gender=='N')
                                        Non Binary
                                        @elseif($user->gender=='O')
                                        Others
                                        @endif
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Interest :</strong> <span class="m-l-15">
                                        @if(@$user->user_to_category->isNotEmpty())
                                        @foreach(@$user->user_to_category as $k => $regCat)
                                        @if($k != (@$user->user_to_category->count() - 1))
                                        {{ @$regCat->category->name }},
                                        @else
                                        {{ @$regCat->category->name }}
                                        @endif
                                        @endforeach
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>3 Words Your Friends Use To Describe You :</strong> <span class="m-l-15">
                                        {{ @$user->describe_you }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Your Idea Of A Perfect Holiday :</strong> <span class="m-l-15">
                                        @if(!empty(@$user->holidays))
                                        @foreach(json_decode(@$user->holidays) as $hk => $holid)
                                        @if($hk != (count(json_decode(@$user->holidays)) -1 ))
                                        {{ @$holid }},
                                        @else
                                        {{ @$holid }}
                                        @endif
                                        @endforeach
                                        @endif
                                        @if(!empty(@$user->holidays_other))
                                        @if(!empty(@$user->holidays)),@endif {{ @$user->holidays_other }}
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>One Problem In The World You Wish You Could Solve :</strong> <span class="m-l-15">
                                        {{ @$user->problem_solve }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Qualities You Admire The Most In Others :</strong> <span class="m-l-15">
                                        {{ @$user->qualities_you }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Your Life Story In 10 Sentences :</strong> <span class="m-l-15">
                                        {{ @$user->about_me }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>A Sub-Category Of Focus :</strong> <span class="m-l-15">
                                        @if(@$user->user_to_category_moderator->isNotEmpty())
                                        @foreach(@$user->user_to_category_moderator as $kd => $modCat)
                                        @if($kd != (@$user->user_to_category_moderator->count() - 1))
                                        {{ @$modCat->category->name }},
                                        @else
                                        {{ @$modCat->category->name }}
                                        @endif
                                        @endforeach
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Talk About Your Personal Experience, Interest, Relevance To At Least One Of Your Chosen Sub-Categories :</strong> <span class="m-l-15">{{ @$user->memories }}</span></p>
                                    <p class="text-muted font-13"><strong>Signup From :</strong> <span class="m-l-15">
                                        @if($user->signup_from=='S')
                                        Website
                                        @elseif($user->signup_from=='F')
                                        Facebook
                                        @elseif($user->signup_from=='G')
                                        Google
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Status :</strong> <span class="m-l-15">
                                        @if($user->status=='A')
                                        Active
                                        @elseif($user->status=='I')
                                        Inactive
                                        @elseif($user->status=='U')
                                        Unverified
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Hearts :</strong> <span class="m-l-15">
                                        {{ number_format(@$user->tot_hearts,0) }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Conversation Completed :</strong> <span class="m-l-15">
                                        {{ number_format(@$user->tot_conversation_completed,0) }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Person Talked :</strong> <span class="m-l-15">
                                        {{ number_format(@$user->tot_person_talked,0) }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Connects:</strong> <span class="m-l-15">
                                        {{ number_format(@$user->tot_connects,0) }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Conversation Balance:</strong> <span class="m-l-15">
                                        {{ number_format(@$user->conversation_balance_total,0) }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Conversation Balance Free:</strong> <span class="m-l-15">
                                        {{ number_format(@$user->conversation_balance_total,0) }}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Total Conversation Balance Paid:</strong> <span class="m-l-15">
                                        {{ number_format(@$user->conversation_balance_paid,0) }}
                                    </span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="card-box">
                            <h4 class="header-title mt-0 m-b-20">Degrees / Certificates</h4>
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name Of The Certification</th>
                                            <th>Year Of Completion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(@$user->user_to_certificates->isNotEmpty())
                                        @foreach(@$user->user_to_certificates as $certificates)
                                        <tr>
                                            <td>{{ @$certificates->certification_name }}</td>
                                            <td>{{ @$certificates->year }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @include('admin.layouts.footer')
                </div>
            </div>
            @endsection
