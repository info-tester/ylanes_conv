@extends('admin.layouts.app')
@section('title')
User Conversations
@endsection

@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush




@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">View Conversations</h4>
                            <a href="{{ route('admin.manage.users') }}" class="pull-right btn btn-dark mr-3"><i class="fa fa-arrow-left"></i> Back</a>
                            <a href="{{ route('admin.user.view',@$user->id) }}" class="pull-right btn btn-dark mr-3"><i class="fa fa-eye"></i> View User Details</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="profile-user-box" style="margin-top: 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span class="pull-left m-r-15">
                                        @if(@$user->image)
                                        @php
                                        $image_path = 'storage/app/public/customer/profile_pics/'.@$user->image;
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <img src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$user->image) }}" alt="" class="thumb-lg rounded-circle">
                                        @else
                                        @endif
                                        @else
                                        <img src="{{ url('public/frontend/images/default_pic.png'
                                        )}}" alt="" class="thumb-lg rounded-circle">
                                        @endif
                                    </span>
                                    <div class="media-body">
                                        <h4 class="m-t-5 m-b-5 font-18 ellipsis">{{ @$user->profile_name }}</h4>
                                        {{-- <h5 class="m-t-5 m-b-5 ellipsis">{{ @$user->profile_name }}</h5> --}}
                                        <p class="font-13"> {{ @$user->userCountryDetails->name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ meta -->
                    </div>
                </div>


                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Topic</th>
                                        <th>Date/Time</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$conversations->isNotEmpty())
                                    @foreach($conversations as $conversation)
                                    <tr>
                                        <td>{{ @$conversation->getConversation->category->name }}<br><br>
                                            {{ @$conversation->getConversation->sub_category->name }}
                                        </td>
                                        <td>{{ @$conversation->getConversation->topic->topic_line_1 }}</td>
                                        <td>{{ date('d/m/Y', strtotime(@$conversation->getConversation->date)) }}<br>
                                            {{ date('h:i A', strtotime(@$conversation->getConversation->time)) }}
                                        </td>
                                        <td>
                                            @if(@$conversation->getConversation->type == 'CO')
                                            Premium
                                            @elseif(@$conversation->getConversation->type == 'OP')
                                            Standard
                                            @else
                                            one-on-one
                                            @endif
                                        </td>
                                        <td>
                                            @if(@$conversation->getConversation->status == 'A')
                                            Active
                                            @elseif(@$conversation->getConversation->status == 'I')
                                            Inactive
                                            @elseif(@$conversation->getConversation->status == 'CO')
                                            Completed
                                            @else
                                            Canceled
                                            @endif
                                        </td>
                                        <td>
                                            @if(@$conversation->getConversation->created_by == 'A')
                                                Admin
                                            @else
                                                @if($user->id==$conversation->getConversation->creator_user_id)
                                                You
                                                @else
                                                    @if(!empty(@$conversation->getConversation->users))
                                                        {{ @$conversation->getConversation->users->profile_name }}<br>
                                                        {{ @$conversation->getConversation->users->email }}
                                                    @else
                                                        --
                                                    @endif
                                                @endif

                                            @endif
                                        </td>
                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$conversation->id }}" data-id="{{ @$conversation->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$conversation->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        <li><a href="{{ route('admin.conversation.view',@$conversation->conversation_id) }}" class="text-dark" title="View"> View</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">


                    @include('admin.layouts.footer')
                </div>
            </div>
        </div>
    </div>
</div>
@push('js')

<script>
    $(".action-dots").click(function() {
        var data_id = $(this).data('id');
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
</script>
@endpush
@endsection
