@extends('admin.layouts.app')
@section('title')
Manage User
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 100px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Manage User</h4>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form method="get" action="{{ route('admin.manage.users') }}">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="inlineFormInput">Keyword</label>
                                                <input type="text" class="form-control mb-2" id="inlineFormInput" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}">
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="inlineFormInputGroup">Status</label>
                                                <select class="form-control mb-2" id="inlineFormInputGroup" name="status">
                                                    <option value="">Select Status</option>
                                                    <option value="A" @if(@$key['status'] == 'A') selected="" @endif>Active</option>
                                                    <option value="I" @if(@$key['status'] == 'I') selected="" @endif>Inactive</option>
                                                    <option value="U" @if(@$key['status'] == 'U') selected="" @endif>Unverified</option>
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="inlineFormInputGroup874">Signup From</label>
                                                <select class="form-control mb-2" id="inlineFormInputGroup874" name="signup_from">
                                                    <option value="">Select Signup From</option>
                                                    <option value="S" @if(@$key['signup_from'] == 'S') selected="" @endif>Website</option>
                                                    <option value="G" @if(@$key['signup_from'] == 'G') selected="" @endif>Google</option>
                                                    {{-- <option value="F" @if(@$key['signup_from'] == 'F') selected="" @endif>Facebook</option> --}}
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker1">From Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker1" name="from_date1" placeholder="From Date" autocomplete="new-date" value="{{ @$key['from_date1'] }}">
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker">To Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker" name="to_date1" placeholder="To Date" autocomplete="new-date" value="{{ @$key['to_date1'] }}">
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                <a href="{{ route('admin.manage.users') }}" class="btn btn-info mb-2">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <div class="container-fluid">
                                    <div class="tot_count">
                                        Total No Users : - {{count(@$allUser)}}
                                    </div>
                                </div>
                                <thead>
                                    <tr>
                                        {{-- <th>Name</th> --}}
                                        <th>User ID</th>
                                        <th>Display Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Register Date</th>
                                        <th>Signup From</th>
                                        <th>Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($allUser as $user)
                                    <tr>
                                        {{-- <td>{{ @$user->first_name }} {{ @$user->last_name }}</td> --}}
                                        <td>{{ @$user->unique_id }}</td>
                                        <td>{{ @$user->profile_name }}</td>
                                        <td>{{ @$user->email }}</td>
                                        <td>{{ @$user->phone }}</td>
                                        <td>{{@$user->created_at->format('d/m/Y')}}</td>
                                        <td>
                                            @if($user->signup_from=='S')
                                            Website
                                            @elseif($user->signup_from=='F')
                                            Facebook
                                            @elseif($user->signup_from=='G')
                                            Google
                                            @elseif($user->signup_from=='L')
                                            Linkedin
                                            @endif

                                        </td>

                                        <td>
                                            @if($user->status=='A')
                                            Active
                                            @elseif($user->status=='I')
                                            Inactive
                                            @elseif($user->status=='U')
                                            Unverified
                                            @endif
                                        </td>

                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$user->id }}" data-id="{{ @$user->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$user->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        <li>
                                                            <a href="{{route('admin.user.view',@$user->id)}}" title="View Details" class="text-dark">View</a>
                                                        </li>
                                                        @if($user->status=='A')
                                                        <li>
                                                            <a href="{{route('admin.user.status',@$user->id)}}" onclick="return confirm('Do you want to change status for this user?')" title="Inactive" class="text-dark">Inactive</a>
                                                        </li>

                                                        @elseif($user->status=='I')
                                                        <li>
                                                            <a href="{{route('admin.user.status',@$user->id)}}" class="text-dark" onclick="return confirm('Do you want to change status for this user?')" title="Active">
                                                                Active
                                                            </a>
                                                        </li>
                                                        @endif
                                                        @if($user->status!='U')
                                                        <li>
                                                            <a href="javascript:void(0);" class="text-dark" title="Send Ycoin" onclick="openCoinModal('{{ @$user->id }}');">
                                                                Send Ycoin
                                                            </a>
                                                        </li>
                                                        @endif
                                                        <li>
                                                            <a href="{{ route('admin.user.delete',@$user->id) }}" onclick="return confirm('Do you want to delete this user?');" class="text-dark">Delete</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route('admin.user.view.conversation',@$user->id)}}" title="View Details" class="text-dark">View Conversation</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
<div class="modal fade" id="sendYcoinModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send Ycoin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="popupYcoinForm" action="{{ route('admin.send.ycoin') }}" method="post">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="popup_user_id">
                    <div class="col-12">
                        <label for="subcategory">Ycoin <span class="text-danger">*</span></label>
                        <input type="text" name="popup_y_coin" required="" min="1" class="form-control" placeholder="Enter Ycoin" onkeypress="validate();">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="SendCoinButton">Send Ycoin</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    function openCoinModal(userids){
        $('input[name="popup_user_id"]').val(userids);
        $('#sendYcoinModel').modal('show');
    }
    $(".action-dots").click(function() {
        var data_id = $(this).data('id');
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
        $('#popupYcoinForm').validate({
            rules: {
                popup_user_id: {
                    required: true,
                    digits: true
                }
            }
        });
        $('#SendCoinButton').click(function(){
            $('#popupYcoinForm').submit();
        })
    });
</script>
@endpush
