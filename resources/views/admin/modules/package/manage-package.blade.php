@extends('admin.layouts.app')
@section('title')
Manage Package
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Manage Package</h4>
                            <a href="{{ route('admin.add.package') }}" class="pull-right btn btn-dark"><i class="fa fa-plus"></i> Add Package</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>Paid Coin</th>
                                        <th>Free Coin</th>
                                        <th>Total Coin</th>
                                        <th class="text-right">Price (INR)</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$packages->isNotEmpty())
                                    @foreach($packages as $package)
                                    <tr>
                                        <td>{{ @$package->coin_paid }}</td>
                                        <td>{{ @$package->coin_free }}</td>
                                        <td>{{ @$package->coin_total }}</td>
                                        <td class="text-right">₹{{ @$package->price }}</td>
                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$package->id }}" data-id="{{ @$package->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$package->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        <li><a href="{{ route('admin.edit.package',@$package->id) }}" class="text-dark" title="Edit"> Edit</a></li>
                                                        <li><a href="{{ route('admin.delete.package',@$package->id) }}" class="text-dark"  onclick="return confirm(' you want to delete this package?');" title="Delete">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
@push('js')
<script>
    $(".action-dots").click(function() {
        var data_id = $(this).data('id'); 
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
</script>
@endpush