@extends('admin.layouts.app')
@section('title')
Edit Package
@endsection
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Edit Package</h4>

                            <a href="{{ route('admin.manage.package') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Edit Package</h4>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.update.package',@$package_data->id) }}">
                                @csrf
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="price" id="CatLabelprice">Price</label>
                                    <input type="text" class="form-control required" id="price" name="price" placeholder="Enter Price" value="{{ @$package_data->price }}" onkeypress='validate(event)'>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="conv_paid" id="CatLabelconv_paid">Paid Coin</label>
                                    <input type="text" class="form-control required" id="conv_paid" name="conv_paid" placeholder="Enter Paid Conversations" value="{{ @$package_data->coin_paid }}" onkeypress='validate(event)' min="1">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="conv_free" id="CatLabelconv_free">Free Coin</label>
                                    <input type="text" class="form-control required" id="conv_free" name="conv_free" placeholder="Enter Free Conversations" value="{{ @$package_data->coin_free }}" onkeypress='validate(event)'>
                                </div>
                                
                               
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        $("#editCustomerForm").validate();
    });
</script>
<script type="text/javascript">
    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endpush