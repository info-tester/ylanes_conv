@extends('admin.layouts.app')
@section('title')
Add Schedule
@endsection
@push('css')
<style type="text/css">
    .interestlipart{
        width: 48%;
        float: left;
    }
    @media (max-width: 767px) {
        .interestlipart{
            width: 100% !important;
        }
    }
</style>
@endpush
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Add Schedule</h4>

                            <a href="{{ route('admin.manage.schedule') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Add Topic</h4>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.add.schedule.post') }}">
                                @csrf
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                    <label for="category">Daily frequency in hour</label>
                                    <select class="form-control" name="freq_hour" id="freq_hour" required="">
                                        <option value="">Select hour</option>
                                        <option value="1" @if(old('freq_hour') == '1') selected="" @endif>Every 1 hour</option>
                                        <option value="2" @if(old('freq_hour') == '2') selected="" @endif>Every 2 hour</option>
                                        <option value="3" @if(old('freq_hour') == '3') selected="" @endif>Every 3 hour</option>
                                        <option value="4" @if(old('freq_hour') == '4') selected="" @endif>Every 4 hour</option>
                                        <option value="6" @if(old('freq_hour') == '6') selected="" @endif>Every 6 hour</option>
                                        <option value="12" @if(old('freq_hour') == '12') selected="" @endif>Every 12 hour</option>
                                    </select>                                    
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                    <label for="repeat_after_days" id="CatLabel1">Repeat After Days</label>
                                    <select class="form-control" name="repeat_after_days" id="repeat_after_days" required="">
                                        <option value="0">No repetition</option>
                                        @for ($i = 1; $i <= 30; $i++)
                                        <option value="{{ @$i }}" @if(old('repeat_after_days') == @$i) selected="" @endif>{{ @$i }} Days</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="datepicker88">Start Date</label>
                                    <div class="dash-d">
                                        <input class="form-control" type="text" placeholder="Select date" id="datepicker88" required="" name="start_date" autocomplete="news-start_date" value="{{ old('start_date') }}">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="time">Start Time</label>
                                    <div class="dash-d">
                                        <select class="form-control" id="time" name="time" required="">
                                            <option value="">Select Time </option>
                                            @php
                                            $start = strtotime('12:00 AM');
                                            $end   = strtotime('11:59 PM');
                                            for($i = $start;$i<=$end;$i+=600){
                                                @endphp
                                                <option value="{{ date('h:i A', $i) }}" @if(old('time') == date('h:i A', $i)) selected="" @endif>{{ date('h:i A', $i) }}</option>
                                                @php
                                            }
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                                    <label for="category">Choose Topic</label>
                                    <ul class="interest" style="display: inline-block; width: 100%; padding: 0px;">
                                        @if(@$sub_categories->isNotEmpty())
                                        @foreach(@$sub_categories as $sub_cat)
                                        @if(@$sub_cat->allsubCategoriesTopics->isNotEmpty())
                                        <li style="display: inline-block;" class="interestlipart">
                                            <h6 style="color: #000;">{{ @$sub_cat->immediateParent->name }} > {{ @$sub_cat->name }}</h6>
                                            @foreach(@$sub_cat->topics as $topic)
                                            <label class="InterestBox" style="width: 100%;">
                                                <input type="checkbox" name="topic_id[]" id="topic_id" value="{{ @$topic->id }}" required="" style="width: 15px; height: 15px;">
                                                <span class="checkmark"></span>
                                                {{ @$topic->topic_line_1 }}
                                            </label>
                                            @endforeach
                                        </li>
                                        @endif
                                        @endforeach
                                        @endif
                                    </ul>  
                                    <label id="topic_id-error" class="error" style="display: none;" for="topic_id[]"></label>                              
                                </div>
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        
        
        
        $("#editCustomerForm").validate();
        
    });
    $(function() {
        $("#datepicker88").datepicker({
            minDate: new Date(),
        });
    });
</script>
@endpush