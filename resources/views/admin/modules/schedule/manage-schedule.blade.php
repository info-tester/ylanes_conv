@extends('admin.layouts.app')
@section('title')
Conversation Schedules
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Conversation Schedules</h4>
                            <a href="{{ route('admin.add.schedule') }}" class="pull-right btn btn-dark"><i class="fa fa-plus"></i> Add Schedule</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form method="get" action="#">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto col-md-3">
                                                <select class="form-control mb-2" name="topic_id" id="topic" >
                                                    <option value="">Select Topic</option>
                                                    @if(@$topics->isNotEmpty())
                                                    @foreach(@$topics as $topic)
                                                    <option value="{{ @$topic->id }}" @if(@request()->topic_id == @$topic->id) selected="" @endif>{{ @$topic->topic_line_1 }} <small><b>(Sub-category : {{ @$topic->subcategory->name }})</b></small></option>
                                                    @endforeach
                                                    @endif
                                                </select>                                    
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker1">From Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker1" name="from_date1" placeholder="From Date" autocomplete="new-date" value="{{ @request()->from_date1 }}">
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker">To Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker" name="to_date1" placeholder="To Date" autocomplete="new-date" value="{{ @request()->to_date1 }}">
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                <a href="{{ route('admin.manage.schedule') }}" class="btn btn-info mb-2">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>Topic</th>
                                        <th>Daily frequency in hour</th>
                                        <th>Repeat After Days</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$schedule_master->isNotEmpty())
                                    @foreach($schedule_master as $schedule)
                                    <tr>
                                        <td>
                                            @if(@$schedule->ConversationScheduleDetails->isNotEmpty())
                                            {{ @$schedule->ConversationScheduleDetails[0]->topics->topic_line_1 }}
                                            @endif
                                        </td>
                                        <td>{{ @$schedule->freq_hour }} hour</td>
                                        <td>
                                            @if(@$schedule->repeat_after_days == 0)
                                            No repetition
                                            @else
                                            {{ @$schedule->repeat_after_days }} days
                                            @endif
                                        </td>
                                        <td>{{ date('d/m/Y', strtotime(@$schedule->start_date)) }}</td>
                                        <td>
                                            {{ date('h:i:s A', strtotime(@$schedule->start_time)) }}
                                        </td>
                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$schedule->id }}" data-id="{{ @$schedule->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$schedule->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        @if(@$schedule->is_updated == 'N')
                                                        <li><a href="{{ route('admin.edit.schedule',@$schedule->id) }}" class="text-dark" title="Edit"> Edit</a></li>
                                                        @endif
                                                        <li><a href="{{ route('admin.delete.schedule',@$schedule->id) }}" class="text-dark"  onclick="return confirm(' you want to delete this schedule?');" title="Delete">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
@push('js')
<script>
    $(".action-dots").click(function() {
        var data_id = $(this).data('id'); 
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
</script>

@endpush