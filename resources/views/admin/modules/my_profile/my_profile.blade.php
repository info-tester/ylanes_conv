@extends('admin.layouts.app')
@section('title')
Edit Profile
@endsection
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Edit profile</h4>

                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Edit profile</li>
                            </ol>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 m-b-30 header-title">Add Category</h4>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.view.profile',@Auth::guard('admin')->user()->id) }}">
                                @csrf
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" class="form-control required" id="exampleInputEmail1" name="name" placeholder="Enter Name" value="{{ @Auth::guard('admin')->user()->name }}">
                                </div>
                                <input type="hidden" id="old_email" value="{{ @$customer->email }}"/>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="email">Email</label>
                                    <input id="email" name="email" type="email" placeholder='@lang('admin_lang.email')' class="form-control required" value="{{ @Auth::guard('admin')->user()->email }}">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="phone">Phone</label>
                                    <input id="phone" name="phone" type="text" class="form-control required" placeholder='Enter Phone' value="{{ @Auth::guard('admin')->user()->phone }}" onkeypress='validate(event)' maxlength="10">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls mt-3">
                                    <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic">
                                    <label class="custom-file-label extrlft" for="profile_pic" style="margin: 20px 5px 0px 5px;">@lang('admin_lang.upload_picture')</label>
                                    <label class="mt-4"> @lang('admin_lang.recommended_sze_100')</label>                                               
                                    <span class="errorPic" style="color: red;"></span>

                                    @if(Auth::guard('admin')->user()->profile_pic)
                                    @php
                                    $image_path = 'storage/app/public/sub_admin_pics/'.Auth::guard('admin')->user()->profile_pic; 
                                    @endphp
                                    @if(file_exists(@$image_path))
                                    <div class="profiles" style="display: block;">
                                        <img id="profilePictures" src="{{ URL::to('storage/app/public/sub_admin_pics/'.@Auth::guard('admin')->user()->profile_pic) }}" alt="" style="width: 100px;height: 100px;">
                                    </div>
                                    @endif
                                    @else
                                    <div class="profile" style="display: none;">
                                        <img src="" id="profilePictures" style="height: 100px;width: 100px;">
                                    </div>
                                    @endif
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="password_n" class="col-form-label">
                                        @lang('admin_lang.nw_password')
                                    </label>
                                    <input id="nw_password" name="nw_password" type="password" class="form-control " placeholder="@lang('admin_lang.nw_min_pass_len')" value="" ><br>
                                    <span class="error_nw_password">@lang('admin_lang.pass_min_len_8')</span>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="password_c" class="col-form-label">
                                        @lang('admin_lang.confirm_password')
                                    </label>
                                    <input id="confirm_password" name="confirm_password" type="password" class="form-control " placeholder="@lang('admin_lang.confirm_password')" value="" >
                                    <span class="error_confirm_password" style="color: red;"></span>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        // $(document).on("blur",'#password',function(e){
        //     $("#password").val("");
        // });
        
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val());
            if(filename != ""){
                filename_arr = filename.split("."),
                ext = filename_arr[1];
                ext = ext.toLowerCase();
                if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                    $(".profile").show();
                    $(".errorpic").text("");
                    readURL(this);
                }else{
                    $(".errorpic").html('@lang('validation.img_upload')');
                    $('#profilePictures').attr('src', "");
                }
            } 
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(".profiles").show();
                    $('#profilePictures').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        
        $(document).on("blur","#email",function(){
        // alert("dfksjdkf");
        var email = $.trim($("#email").val()),
        old_email = "{{ Auth::guard('admin')->user()->email }}";

        if(email != "")
        {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test(email)){
                var rurl = '{{ route('admin.check.duplicate.email.update') }}';
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data:{
                        email:email,
                        old_email:old_email
                    },
                    success:function(resp){
                            // alert(resp);
                            if(resp == 0){
                                $(".error_email").text('@lang('validation.email_id_already_exist')');
                                $("#email").val("");
                            }else{
                                $(".error_email").text("");
                            }
                        }
                    }); 
            }else{
                    // alert("if email format false");
                    $(".errorEmail").text('@lang('validation.please_provide_valid_email_id')');
                }
                
            }else{
                // alert("if blank");
                $(".errorEmail").text('@lang('validation.please_provide_valid_email_id')');
                
            }
        });

        $(document).on("blur","#phone",function(){
        // alert("jhdhgshdg");
        var phone = $.trim($("#phone").val()),
        old_phone = "{{ @Auth::guard('admin')->user()->phone }}";
        
        if(phone != "")
        {
            var rurl = '{{ route('admin.check.duplicate.phone.update') }}';
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                    phone:phone,
                    old_phone:$.trim(old_phone)
                },
                success:function(resp){
                    if(resp == 0){

                        $(".error_phone").text('@lang('admin_lang.phone_number_already_exist')');
                        $("#phone").val("");
                    }
                }
            });  
        }
    });
        jQuery.validator.addMethod("emailonly", function(value, element) {
            return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value.toLowerCase()) || /.+@(gmail|yahoo|hotmail)\.com$/.test(value.toLowerCase());
        }, "Please provide valid email-id!");
        $("#editCustomerForm").validate({
            rules:{
                'name':{
                    required:true
                },
                'email':{
                    required:true,
                    email:true,
                    emailonly: true
                },
                'phone':{
                    required:true,
                    minlength: 10,
                    maxlength: 10
            },
            'nw_password': {
                minlength: 8
            },            
            'confirm_password':{
                equalTo: '#nw_password'
            },
        },
        messages: { 
            name: { 
                required: 'The name field is required.'
            },
            email: { 
                required: 'The email field is required.',
                email: 'Please provide valid email-id'
            },
            phone:{
                required: 'The phone field is required.'
            },
            nw_password: {
             required: 'This field is required',
             minlength: "The password must be at least 8 characters."
         },
         confirm_password: {
            equalTo : '@lang('merchant_lang.confirm_pass')'
        }
    },
});

    });
</script>
<script type="text/javascript">
    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endpush