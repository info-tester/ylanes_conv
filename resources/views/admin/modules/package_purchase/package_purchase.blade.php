@extends('admin.layouts.app')
@section('title')
Package Purchase
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left"> Package Purchase</h4>
                            {{-- <a href="{{ route('admin.add.package') }}" class="pull-right btn btn-dark"><i class="fa fa-plus"></i> Add Package</a> --}}
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form method="post" action="{{ route('admin.manage.package.purchase') }}">
                                        @csrf
                                        <div class="form-row align-items-center">
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="inlineFormInput">Keyword</label>
                                                <input type="text" class="form-control mb-2" id="inlineFormInput" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}">
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker1">From Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker1" name="from_date1" placeholder="From Date" autocomplete="new-date" value="{{ @$key['from_date1'] }}">
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker">To Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker" name="to_date1" placeholder="To Date" autocomplete="new-date" value="{{ @$key['to_date1'] }}">
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                <a href="{{ route('admin.manage.package.purchase') }}" class="btn btn-info mb-2">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Display Name</th>
                                        <th>Transaction ID</th>
                                        <th>Date</th>
                                        <th>Price (INR)</th>
                                        <th>Paid Coin</th>
                                        <th>Free Coin</th>
                                        <th>Total Coin</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$purchase_packages->isNotEmpty())
                                    @foreach($purchase_packages as $package)
                                    <tr>
                                        <td>{{@$package->getUser->unique_id}}</td>
                                        <td>{{@$package->getUser->profile_name}}</td>
                                        <td>{{@$package->getPayment->order_id??'N/A'}}</td>
                                        <td>{{ @$package->created_at->format('d/m/Y') }}</td>
                                        <td>₹{{ @$package->amount }}</td>
                                        <td>{{ @$package->conv_paid }}</td>
                                        <td>{{ @$package->conv_free }}</td>
                                        <td>{{ @$package->conv_paid + @$package->conv_free  }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
@push('js')
<script>
    $(".action-dots").click(function() {
        var data_id = $(this).data('id');
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
</script>
@endpush
