@extends('admin.layouts.app')
@section('title')
Manage Topic
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Manage Topic</h4>
                            <a href="{{ route('admin.add.topic') }}" class="pull-right btn btn-dark"><i class="fa fa-plus"></i> Add Topic</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form method="get" action="{{ route('admin.manage.topic') }}">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto col-md-3">
                                                <select class="form-control mb-2" name="category_id" id="category" >
                                                    <option value="">Select Category</option>
                                                    @if(@$category->isNotEmpty())
                                                    @foreach(@$category as $category)
                                                    <option value="{{ @$category->id }}" @if(@request()->category_id == @$category->id) selected="" @endif>{{ @$category->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-3">
                                                <select class="form-control mb-2" name="sub_category_id" id="subcategory">
                                                    <option value="">Select Sub Category</option>
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-3">
                                                <label class="sr-only" for="inlineFormInput">Keyword</label>
                                                <input type="text" class="form-control mb-2" id="inlineFormInput" name="keyword" placeholder="Keyword" value="{{ @request()->keyword }}">
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                <a href="{{ route('admin.manage.topic') }}" class="btn btn-info mb-2">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Topic Line 1</th>
                                        <th>Topic Line 2</th>
                                        <th>Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($all_topics as $topic)
                                    <tr>
                                        <td>{{ @$topic->category->name }}<br><br>{{ @$topic->subcategory->name }}</td>
                                        <td>{{ @$topic->topic_line_1 }}</td>
                                        <td>{{ @$topic->topic_line_2 }}</td>
                                        <td>
                                            @if(@$topic->status == 'A')
                                            Active
                                            @else
                                            Inactive
                                            @endif
                                        </td>
                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$topic->id }}" data-id="{{ @$topic->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$topic->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        <li><a href="{{ route('admin.topic.edit',@$topic->id) }}" class="text-dark" title="Edit"> Edit</a></li>

                                                        @if($topic->status == 'A')
                                                        <li><a href="{{ route('admin.status.topic.change',@$topic->id) }}" class="text-dark" title="Inactive" onclick="return confirm('Do you want to change status for this topic?')"> Inactive</a></li>
                                                        @else
                                                        <li><a href="{{ route('admin.status.topic.change',@$topic->id) }}" class="text-dark" title="Active" onclick="return confirm('Do you want to change status for this topic?')">Active</a></li>
                                                        @endif
                                                        <li><a href="{{ route('admin.delete.topic',@$topic->id) }}" class="text-dark"  onclick="return confirm(' you want to delete this topic?');" title="Delete">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
@push('js')
<script>
    $(".action-dots").click(function() {
        var data_id = $(this).data('id');
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
    $(document).ready(function(){
        $('#category').change(function(){
            catId = $(this).val();
            $.ajax({
                url:"{{ route('admin.get.sub.category') }}/"+catId,
                type:"GET",
                success: function(responce){
                    $('#subcategory').html(responce.result.get_sub_categories);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
        $.ajax({
            url:"{{ route('admin.get.sub.category') }}/"+$('#category').val()+"/"+'{{@request()->sub_category_id}}',
            type:"GET",
            success: function(responce){
                $('#subcategory').html(responce.result.get_sub_categories);
            },
            error: function(xhr){
                console.log(xhr);
            }
        });
    })
</script>

@endpush
