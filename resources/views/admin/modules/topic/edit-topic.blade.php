@extends('admin.layouts.app')
@section('title')
Edit Topic
@endsection
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Edit Topic</h4>

                            <a href="{{ route('admin.manage.topic') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Edit Topic</h4>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.topic.post',@$topic_data->id) }}">
                                @csrf
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                                    <label for="exampleInputEmail2">Select Category</label>
                                    <select class="form-control" id="exampleInputEmail2" disabled="">
                                        <option>{{ @$topic_data->category->name }}</option>
                                    </select>                                    
                                </div> 
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                                    <label for="exampleInputEmail2">Select Sub Category</label>
                                    <select class="form-control" id="exampleInputEmail2" disabled="">
                                        <option>{{ @$topic_data->subcategory->name }}</option>
                                    </select>                                    
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="topic_line_1" id="CatLabel1">Topic Line 1</label>
                                    <input type="text" class="form-control required" id="topic_line_1" name="topic_line_1" placeholder="Enter Topic Line 1" value="{{ @$topic_data->topic_line_1 }}">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="topic_line_2" id="CatLabel2">Topic Line 2</label>
                                    <input type="text" class="form-control required" id="topic_line_2" name="topic_line_2" placeholder="Enter Topic Line 1" value="{{ @$topic_data->topic_line_2 }}">
                                </div>
                                
                               
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        
        
        
        $("#editCustomerForm").validate();

    });
</script>
@endpush