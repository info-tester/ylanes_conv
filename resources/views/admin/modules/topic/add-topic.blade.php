@extends('admin.layouts.app')
@section('title')
Add Topic
@endsection
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Add Topic</h4>

                            <a href="{{ route('admin.manage.topic') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Add Topic</h4>
                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.add.topic.post') }}">
                                @csrf
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                                    <label for="category">Select Category</label>
                                    <select class="form-control" name="category_id" id="category" required="">
                                        <option value="">Select Category</option>
                                        @if(@$category->isNotEmpty())
                                        @foreach(@$category as $category)
                                        <option value="{{ @$category->id }}">{{ @$category->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>                                    
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                                    <label for="subcategory">Select Sub Category</label>
                                    <select class="form-control" name="sub_category_id" id="subcategory" required="">
                                        <option value="">Select Sub Category</option>
                                    </select>                                    
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="topic_line_1" id="CatLabel1">Topic Line 1</label>
                                    <input type="text" class="form-control required" id="topic_line_1" name="topic_line_1" placeholder="Enter Topic Line 1" value="{{ old('topic_line_1') }}">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="topic_line_2" id="CatLabel2">Topic Line 2</label>
                                    <input type="text" class="form-control required" id="topic_line_2" name="topic_line_2" placeholder="Enter Topic Line 1" value="{{ old('topic_line_2') }}">
                                </div>
                                
                               
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addCustomer">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#editCustomerForm").submit();
        });
        
        
        
        $("#editCustomerForm").validate();
        $('#category').change(function(){
            catId = $(this).val();
            $.ajax({
                url:"{{ route('admin.get.sub.category') }}/"+catId,
                type:"GET",
                success: function(responce){
                    $('#subcategory').html(responce.result.get_sub_categories);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
        $('#subcategory').change(function(){
            subcategoryId = $(this).val();
            $.ajax({
                url:"{{ route('get.sub.category.topic') }}/"+subcategoryId,
                type:"GET",
                success: function(responce){
                    $('#topic').html(responce.result.get_topic);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
    });
</script>
@endpush