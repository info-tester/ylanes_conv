@extends('admin.layouts.app')
@section('title')
Edit Conversation
@endsection
@push('css')
<link href="{{url('public/frontend/css/jquery-clockpicker.min.css')}}" rel="stylesheet">
@endpush
@section('content')

<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Edit Conversation</h4>

                            <a href="{{ route('admin.manage.conversation') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Edit Conversation</h4>

                            @include('layouts.back_msg')
                            <form role="form" class="form-row" id="conversationForm" method="post" enctype="multipart/form-data" action="{{ route('admin.update.conversation',@$conversation->id) }}">
                                @csrf
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="category">Select Category</label>
                                    <select class="form-control" name="category" id="category" required="">
                                        <option value="">Select Category</option>
                                        @if(@$category->isNotEmpty())
                                        @foreach(@$category as $category)
                                        <option value="{{ @$category->id }}" @if(@$conversation->category_id == @$category->id) selected="" @endif>{{ @$category->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>                                    
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="subcategory">Select Sub Category</label>
                                    <select class="form-control" name="sub_category" id="subcategory" required="">
                                        <option value="{{ @$conversation->sub_category_id }}">{{ @$conversation->sub_category->name }}</option>
                                    </select>                                    
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="topic">Topic</label>
                                    <select class="form-control" id="topic" required="" name="topic">
                                        <option value="{{ @$conversation->topic_id }}">{{ @$conversation->topic->topic_line_1 }}</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 mt-2">
                                    <label for="datepicker88">Date</label>
                                    <div class="dash-d">
                                        <input class="form-control" type="text" placeholder="Select date" id="datepicker88" required="" name="date" value="{{date('m/d/Y', strtotime(@$conversation->date)) }}">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 mt-2">
                                    <label for="time">Time</label>
                                    <div class="dash-d">
                                        <select class="form-control" id="time" name="time" required="">
                                            <option value="">Select Time </option>
                                            @php
                                            $start = strtotime('12:00 AM');
                                            $end   = strtotime('11:59 PM');
                                            for($i = $start;$i<=$end;$i+=600){
                                                @endphp
                                                <option value="{{ date('h:i A', $i) }}" @if(@$conversation->time == date('H:i:s', $i)) selected="" @endif>{{ date('h:i A', $i) }}</option>
                                                @php
                                            }
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 mt-3">
                                    <button type="button" class="btn btn-purple waves-effect waves-light" id="addConversation">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end col -->


                </div>

            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
@push('js')
<script src="{{url('public/frontend/js/jquery-clockpicker.min.js')}}"></script>
<script type="text/javascript">
    $(function() {
        $("#datepicker88").datepicker({
            minDate: new Date(),
        });
    });
    $(document).ready(function(){
        $("#addConversation").click(function(){
            $("#conversationForm").submit();
        });
        $('#category').change(function(){
            catId = $(this).val();
            $.ajax({
                url:"{{ route('admin.get.sub.category') }}/"+catId,
                type:"GET",
                success: function(responce){
                    $('#subcategory').html(responce.result.get_sub_categories);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
        $('#subcategory').change(function(){
            subcategoryId = $(this).val();
            $.ajax({
                url:"{{ route('get.sub.category.topic') }}/"+subcategoryId,
                type:"GET",
                success: function(responce){
                    $('#topic').html(responce.result.get_topic);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
        $('#conversationForm').validate();
    });
    $("input[name=time]").clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        default: 'now',
        donetext: "Select",
        init: function() {
            console.log("colorpicker initiated");
        },
        beforeShow: function() {
            console.log("before show");
        },
        afterShow: function() {
            console.log("after show");
        },
        beforeHide: function() {
            console.log("before hide");
        },
        afterHide: function() {
            console.log("after hide");
        },
        beforeHourSelect: function() {
            console.log("before hour selected");
        },
        afterHourSelect: function() {
            console.log("after hour selected");
        },
        beforeDone: function() {
            console.log("before done");
        },
        afterDone: function() {
            console.log("after done");
        }
    });
</script>
@endpush