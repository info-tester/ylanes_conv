@extends('admin.layouts.app')
@section('title')
Details Conversation
@endsection
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Details Conversation</h4>
                            <a href="{{ route('admin.manage.conversation') }}" class="pull-right btn btn-dark"><i class="fa fa-arrow-left"></i> Back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                @if(!empty(@$conversation->users))
                <div class="row">
                    <div class="col-sm-12">
                        <div class="profile-user-box" style="margin-top: 0px;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="pull-left m-r-15">
                                        @if(@$conversation->users->image)
                                        @php
                                        $image_path = 'storage/app/public/customer/profile_pics/'.@$conversation->users->image;
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <img src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$conversation->users->image) }}" alt="" class="thumb-lg rounded-circle">
                                        @else
                                        @endif
                                        @else
                                        <img src="{{ url('public/frontend/images/default_pic.png')}}" alt="" class="thumb-lg rounded-circle">
                                        @endif
                                    </span>
                                    <div class="media-body">
                                        {{-- <h4 class="m-t-5 m-b-5 font-18 ellipsis">{{ @$conversation->users->first_name }} {{ @$conversation->users->last_name }}</h4> --}}
                                        <h4 class="m-t-5 m-b-5 font-18 ellipsis">{{ @$conversation->users->profile_name }}</h4>
                                        {{-- <h5 class="m-t-5 m-b-5 ellipsis">{{ @$conversation->users->profile_name }}</h5> --}}
                                        <p class="font-13"> {{ @$conversation->users->email }}</p>
                                        <p class="text-muted m-b-0"><small>{{ @$conversation->users->userCountryDetails->name }} </small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ meta -->
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 m-b-20">Details Conversation</h4>
                            <div class="panel-body">
                                <div class="text-left">
                                    <p class="text-muted font-13"><strong>Category :</strong> <span class="m-l-15">{{ @$conversation->category->name }}</span></p>
                                    <p class="text-muted font-13"><strong>Sub Category :</strong> <span class="m-l-15">{{ @$conversation->sub_category->name }}</span></p>
                                    <p class="text-muted font-13"><strong>Topic :</strong> <span class="m-l-15">{{ @$conversation->topic->topic_line_1 }}</span></p>
                                    <p class="text-muted font-13"><strong>Date :</strong> <span class="m-l-15">{{ date('d/m/Y', strtotime(@$conversation->date)) }}</span></p>
                                    <p class="text-muted font-13"><strong>Created By :</strong> <span class="m-l-15">
                                        @if(@$conversation->created_by == 'A')
                                        Admin
                                        @else
                                        User
                                        @endif
                                    </span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 m-b-20">Details Conversation</h4>
                            <div class="panel-body">
                                <div class="text-left">
                                    <p class="text-muted font-13"><strong>Time :</strong> <span class="m-l-15">{{ date('h:i A', strtotime(@$conversation->time)) }}</span></p>
                                    <p class="text-muted font-13"><strong>Type :</strong> <span class="m-l-15">
                                        @if(@$conversation->type == 'CO')
                                        Premium
                                        @elseif(@$conversation->type == 'OP')
                                        Standard
                                        @else
                                        one-on-one
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Status :</strong> <span class="m-l-15">
                                        @if(@$conversation->status == 'A')
                                        Active
                                        @elseif(@$conversation->status == 'I')
                                        Inactive
                                        @elseif(@$conversation->status == 'CO')
                                        Completed
                                        @else
                                        Canceled
                                        @endif
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Extended :</strong> <span class="m-l-15">
                                        {{@$conversation->no_of_extend}}
                                    </span></p>
                                    <p class="text-muted font-13"><strong>Paid Extended :</strong> <span class="m-l-15">
                                        {{@$conversation->no_of_extend? @$conversation->no_of_extend-1 : 0}}
                                    </span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="header-title mt-0 m-b-20">Join Users</h4>
                            <div class="panel-body">
                                @if(@$conversation->conversationstousers->isNotEmpty())
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Conversation Text</th>
                                            <th>Is Creator</th>
                                            <th>Hearts Received</th>
                                            <th>Reports Received</th>
                                            <th>Extended</th>
                                            <th>Paid Extended</th>
                                            <th>Call Time</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach(@$conversation->conversationstousers as $conversation)
                                        <tr>
                                            <th scope="row">
                                                {{ @$conversation->users->profile_name }}
                                            </th>
                                            <td>{{ @$conversation->users->email }}</td>
                                            <td>{{ @$conversation->conversation_text }}</td>
                                            <td>
                                                @if(@$conversation->is_creator == 'Y')
                                                Yes
                                                @else
                                                No
                                                @endif
                                            </td>
                                            <td>{{ @$conversation->hearts_received }}</td>
                                            <td>{{ @$conversation->reports_received }}</td>
                                            <td>{{ @$conversation->no_of_extend }}</td>
                                            <td>{{ @$conversation->no_of_extend?$conversation->no_of_extend-1:0 }}</td>
                                            <td>{{ CustomHelper::timeDifference(@$conversation->duration) }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection
