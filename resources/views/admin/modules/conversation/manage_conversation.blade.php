@extends('admin.layouts.app')
@section('title')
Manage Conversations
@endsection
@push('css')
<style type="text/css">
    .show-actions {
        display: none;
        position: absolute;
        z-index: 20;
        background: #fff;
        top: 26px;
        right: 0;
        width: 130px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px 1px #efefef;
        padding: 5px 10px 0px;
    }
    .actions-main {
        position: relative;
    }
    .actions-main ul{
        padding-left: 0px;
        margin-bottom: 0rem;
    }
    .actions-main ul li{
        padding-bottom: 5px;
    }
    .actions-main ul li a{
        display: block;
    }
</style>
@endpush
@section('content')
<div id="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">Manage Conversations</h4>
                            <a href="{{ route('admin.add.conversation') }}" class="pull-right btn btn-dark"><i class="fa fa-plus"></i> Add Conversations</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form method="get" action="{{ route('admin.manage.conversation') }}">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto col-md-2">
                                                <select class="form-control mb-2" name="category" id="category" >
                                                    <option value="">Select Category</option>
                                                    @if(@$category->isNotEmpty())
                                                    @foreach(@$category as $category)
                                                    <option value="{{ @$category->id }}" @if(@request()->category == @$category->id) selected="" @endif>{{ @$category->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <select class="form-control mb-2" name="sub_category" id="subcategory">
                                                    <option value="">Select Sub Category</option>
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <select class="form-control mb-2" id="topic" name="topic">
                                                    <option value="">Select Topic</option>
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only">Select Type</label>
                                                <select class="form-control mb-2" name="type">
                                                    <option value="">All</option>
                                                    <option value="ON" @if(@request()->type == 'ON') selected="" @endif>one-on-one </option>
                                                    <option value="OP" @if(@request()->type == 'OP') selected="" @endif>Standard</option>
                                                    <option value="CO" @if(@request()->type == 'CO') selected="" @endif>Premium</option>
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker10">From Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker10" name="from_date1" placeholder="From Date" autocomplete="new-date" value="{{ @request()->from_date1 }}">
                                            </div>
                                            <div class="col-auto col-md-2">
                                                <label class="sr-only" for="datepicker11">To Date</label>
                                                <input type="text" class="form-control mb-2" id="datepicker11" name="to_date1" placeholder="To Date" autocomplete="new-date" value="{{ @request()->to_date1 }}">
                                            </div>
                                            <div class="col-auto col-md-2 mt-2">
                                                <label class="sr-only">Created By</label>
                                                <select class="form-control mb-2" name="created_by">
                                                    <option value="">All</option>
                                                    <option value="U" @if(@request()->created_by == 'U') selected="" @endif>User</option>
                                                    <option value="A"  @if(@request()->created_by == 'A') selected="" @endif>Admin</option>
                                                </select>
                                            </div>
                                            <div class="col-auto col-md-2 mt-2">
                                                <label class="sr-only">Select Status</label>
                                                <select class="form-control mb-2" name="status">
                                                    <option value="">Select Status</option>
                                                    <option value="A" @if(@request()->status == 'A') selected="" @endif>Active</option>
                                                    <option value="I" @if(@request()->status == 'I') selected="" @endif>Inactive</option>
                                                    <option value="C" @if(@request()->status == 'C') selected="" @endif>Canceled</option>
                                                    <option value="CO" @if(@request()->status == 'CO') selected="" @endif>Completed</option>
                                                </select>
                                            </div>
                                            <div class="col-auto mt-2">
                                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                <a href="{{ route('admin.manage.conversation') }}" class="btn btn-info mb-2">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.back_msg')
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer">
                                <div class="container-fluid">
                                    <div class="tot_count">
                                        Total No Conversations : - {{count(@$conversations)}}
                                    </div>
                                </div>
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Category</th>
                                        <th>Topic</th>
                                        <th>Date/Time</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$conversations->isNotEmpty())
                                    @foreach($conversations as $conversation)
                                    <tr>
                                        <td>
                                            @if(!empty(@$conversation->users))
                                            {{ @$conversation->users->profile_name }}<br>
                                            {{ @$conversation->users->email }}
                                            @else
                                            --
                                            @endif
                                        </td>
                                        <td>{{ @$conversation->category->name }}<br><br>
                                            {{ @$conversation->sub_category->name }}
                                        </td>
                                        <td>{{ @$conversation->topic->topic_line_1 }}</td>
                                        <td>{{ date('d/m/Y', strtotime(@$conversation->date)) }}<br>
                                            {{ date('h:i A', strtotime(@$conversation->time)) }}
                                        </td>
                                        <td>
                                            @if(@$conversation->type == 'CO')
                                            Premium
                                            @elseif(@$conversation->type == 'OP')
                                            Standard
                                            @else
                                            one-on-one
                                            @endif
                                        </td>
                                        <td>
                                            @if(@$conversation->status == 'A')
                                            Active
                                            @elseif(@$conversation->status == 'I')
                                            Inactive
                                            @elseif(@$conversation->status == 'CO')
                                            Completed
                                            @else
                                            Canceled
                                            @endif
                                        </td>
                                        <td>
                                            @if(@$conversation->created_by == 'A')
                                            Admin
                                            @else
                                            User
                                            @endif
                                        </td>
                                        <td>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$conversation->id }}" data-id="{{ @$conversation->id }}" style="opacity: 0.4; display: block;"><img src="{{ url('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$conversation->id }}" style="display: none;">
                                                    <ul class="text-left" style="list-style: none;">
                                                        <li><a href="{{ route('admin.conversation.view',@$conversation->id) }}" class="text-dark" title="View"> View</a></li>
                                                        @if(@$conversation->conversationstousers->isEmpty())
                                                        @if(@$conversation->status != 'C')
                                                        <li><a href="{{ route('admin.edit.conversation',@$conversation->id) }}" class="text-dark" title="Edit"> Edit</a></li>
                                                        @endif
                                                        @endif

                                                        @if($conversation->status == 'A')
                                                        <li><a href="{{ route('admin.conversation.status.change',$conversation->id) }}" class="text-dark" title="Inactive" onclick="return confirm('Do you want to change status for this conversation?')"> Inactive</a></li>
                                                        @else
                                                        <li><a href="{{ route('admin.conversation.status.change',$conversation->id) }}" class="text-dark" title="Active" onclick="return confirm('Do you want to change status for this conversation?')">Active</a></li>
                                                        @endif
                                                        <li><a href="#" class="text-dark"  onclick="return confirm(' you want to delete this conversation?');" title="Delete">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layouts.footer')

    </div>


</div>
@endsection
@push('js')
<script>
    $( function() {
        $("#datepicker10").datepicker({
          numberOfMonths: 1,
          onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#datepicker11").datepicker("option", "minDate", dt);
        },
        // maxDate: new Date("{{ date('m/d/Y') }}")
    });
        $("#datepicker11").datepicker({
          numberOfMonths: 1,
          onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#datepicker10").datepicker("option", "maxDate", dt);
        },
    });
    } );
    $(".action-dots").click(function() {
        var data_id = $(this).data('id');
        $("#show-action"+data_id).slideToggle();
    });
    $(document).on('click', function() {
        var $target = $(event.target);
        if(!$target.closest('.show-actions').length && !$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
            $('.show-actions').slideUp();
        }
    });
    $(document).ready(function(){
        $('#category').change(function(){
            catId = $(this).val();
            $.ajax({
                url:"{{ route('admin.get.sub.category') }}/"+catId,
                type:"GET",
                success: function(responce){
                    $('#subcategory').html(responce.result.get_sub_categories);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
        $('#subcategory').change(function(){
            subcategoryId = $(this).val();
            $.ajax({
                url:"{{ route('get.sub.category.topic') }}/"+subcategoryId,
                type:"GET",
                success: function(responce){
                    $('#topic').html(responce.result.get_topic);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        });
        $.ajax({
            url:"{{ route('admin.get.sub.category') }}/"+$('#category').val(),
            type:"GET",
            success: function(responce){
                $('#subcategory').html(responce.result.get_sub_categories);
            },
            error: function(xhr){
                console.log(xhr);
            }
        });
    });
</script>

@endpush
