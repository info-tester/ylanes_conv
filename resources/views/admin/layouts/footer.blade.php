<footer class="footer text-right">
	{{ date('Y') }} © YLances. - <a href="{{route('welcome')}}" target="_blank">ylances.com</a>
</footer>
