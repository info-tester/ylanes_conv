<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left" style="background-color: #fff;">
        <a href="{{ route('admin.dashboard') }}" class="logo">
            <span>
                <img src="{{url('public/frontend/images/logo.png')}}" alt="" height="35">
            </span>
            <i>
                <img src="{{url('public/frontend/images/logo.png')}}" alt="" height="20">
            </i>
        </a>
    </div>

    <nav class="navbar-custom">

        <ul class="list-inline float-right mb-0">


            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user"data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                     @if(@Auth::guard('admin')->user()->profile_pic)
                     <img src="{{url('storage/app/public/sub_admin_pics/'.@Auth::guard('admin')->user()->profile_pic)}}" alt="user" class="rounded-circle">
                     @else
                    <img src="{{url('public/backend/images/users/avatar-1.jpg')}}" alt="user" class="rounded-circle">
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="text-overflow"><small>Welcome ! {{ @Auth::guard('admin')->user()->name }}</small> </h5>
                    </div>

                    <!-- item-->
                    <a href="{{ route('admin.view.profile') }}" class="dropdown-item notify-item">
                        <i class="mdi mdi-account-circle"></i> <span>Profile</span>
                    </a>
                    <!-- item-->
                    <a href="{{ url('/admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                        <i class="mdi mdi-power"></i> <span>Logout</span>
                        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </a>



                </div>
            </li>

        </ul>
        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="dripicons-menu"></i>
                </button>
            </li>
        </ul>

    </nav>

</div>
