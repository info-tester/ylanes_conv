<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Navigation</li>
                <li class="{{ Route::is('admin.dashboard') ? 'active' : '' }}">
                    <a href="{{ route('admin.dashboard') }}"><i class="fi-air-play"></i> <span> Dashboard </span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.category') }}" class="{{ Route::is('admin.manage.category','admin.add.category','admin.edit.category','admin.add.bulk.category') ? 'active' : '' }}"><i class="fa fa-list-alt"></i> <span> Manage Category</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.users') }}" class="{{ Route::is('admin.manage.users','admin.user.view','admin.user.view.conversation') ? 'active' : '' }}"><i class="fa fa fa-user-o"></i> <span> User Management</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.topic') }}" class="{{ Route::is('admin.manage.topic','admin.add.topic','admin.topic.edit') ? 'active' : '' }}"><i class="fa fa-list"></i> <span> Manage Topic</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.conversation') }}" class="{{ Route::is('admin.manage.conversation','admin.add.conversation','admin.conversation.view','admin.edit.conversation') ? 'active' : '' }}"><i class="fa fa-cogs"></i> <span> Manage Conversations</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.schedule') }}" class="{{ Route::is('admin.manage.schedule','admin.add.schedule','admin.edit.schedule') ? 'active' : '' }}"><i class="fa fa-calendar"></i> <span style="font-size:15px;"> Conversation Schedules</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.package') }}" class="{{ Route::is('admin.manage.package','admin.add.package','admin.edit.package') ? 'active' : '' }}"><i class="fa fa-gift"></i> <span style="font-size:15px;"> Manage Packages</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.manage.package.purchase') }}" class="{{ Route::is('admin.manage.package.purchasee') ? 'active' : '' }}"><i class="fa fa-gift"></i> <span style="font-size:15px;"> Package Purchase</span></a>
                </li>
            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
