<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>Admin :: @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="" name="description" />
    <meta content="YLances" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{url('public/frontend/images/fav.png')}}">

    <!-- App css -->
    <link href="{{url('public/backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('public/backend/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('public/backend/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('public/backend/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('public/backend/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{url('public/backend/js/modernizr.min.js')}}"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
    <style type="text/css">
        label.error{
            color: #ff0000 !important;
            font-size: 13px !important;
        }
        .fa {
            font-size: 17px;
        }
        .action-dots:hover {
            opacity: 1 !important;
        }
    </style> 
    @stack('css')
</head>


<body class="{{ Route::is('admin.login','admin.password.request','admin.password.reset') ? 'bg-accpunt-pages' : '' }}">


    @yield('content')


    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="{{url('public/backend/js/jquery.min.js')}}"></script>
    <script src="{{url('public/backend/js/popper.min.js')}}"></script><!-- Popper for Bootstrap -->
    <script src="{{url('public/backend/js/bootstrap.min.js')}}"></script>
    <script src="{{url('public/backend/js/metisMenu.min.js')}}"></script>
    <script src="{{url('public/backend/js/waves.js')}}"></script>
    <script src="{{url('public/backend/js/jquery.slimscroll.js')}}"></script>
    <script src="{{url('public/backend/plugins/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{url('public/backend/plugins/counterup/jquery.counterup.min.js')}}"></script>

    <!-- App js -->
    <script src="{{url('public/backend/js/jquery.core.js')}}"></script>
    <script src="{{url('public/backend/js/jquery.app.js')}}"></script>
    <script src="{{url('public/backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/backend/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
    <script type="text/javascript">
        function validate(evt) {
          var theEvent = evt || window.event;
         // Handle paste
         if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    $(document).ready(function() {
        $('#datatable').DataTable({
            "searching": false,
            "ordering": false
        });
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
        });
    });

    $( function() {
        $("#datepicker1").datepicker({
          numberOfMonths: 1,
          onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#datepicker").datepicker("option", "minDate", dt);
        },
        maxDate: new Date("{{ date('m/d/Y') }}")
    });
        $("#datepicker").datepicker({
          numberOfMonths: 1,
          onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#datepicker1").datepicker("option", "maxDate", dt);
        },
    });
    } );
</script>
@stack('js')
</body>
</html>