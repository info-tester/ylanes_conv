@if (session()->has('success'))
<div class="">
    <div class="alert alert-success vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="fa fa-times"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
</div>
@elseif (session()->has('error'))
<div class="">
    <div class="alert alert-danger vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="fa fa-times"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
</div>
@endif
@if ($errors->any())
<div class="">
    <div class="alert alert-danger alert-dismissible">
        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
@if(!empty(Session::get('url_redirect_after_purchase_balance')))
<div class="">
    <div class="alert alert-warning vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="fa fa-times"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-triangle"></i></span> 
        Please fillup the minimum mandatory field on your profile. then you can register for a conversation.
    </div>
</div>
@endif