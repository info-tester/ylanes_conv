{{-- <footer class="foot_sec foot_bdr">
    <div class="foot_top">
        <div class="container">
            <div class="foot_top_inr">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="foot_bx">
                            <div class="foot_logo">
                                <img src="{{url('public/frontend/images/logo1.png')}}" alt="">
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis nunc eu eros tincidunt eleifend at ut dui. Sed sit amet congue diam Nam facilisis maximus imperdiet posuere.<span class="moretext2" style="display: none;">Lorem ipsum dolor sit amet, consectetur text suspendisse pharetra erat consectetur text </span>
                            </p>
                            <a href="#url" class="pg_btn red_btn"> <span id="moreless-button2">Learn more</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="foot_bx">
                            <h4>Quick Links</h4>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Login</a></li>
                                <li><a href="#">Sign up</a></li>
                                <li><a href="#">How it works</a></li>
                                <li><a href="#">Contact us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="foot_bx">
                            <h4>Categories</h4>
                            <ul>
                                @if(!empty(CustomHelper::footergetCategory()->isNotEmpty()))
                                @foreach(CustomHelper::footergetCategory() as $category)
                                <li><a href="{{ route('search.page',@$category->slug) }}">{{ @$category->name }} </a></li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="foot_bx">
                            <h4>Contact Info</h4>
                            <ul>
                                <li>Address :<br>
                                This is s simply dummy text or address text here show here</li>
                                <li class="foot_cls">
                                    Phone : <a href="tel:919876543210">+91 9876543210  /  </a><br>
                                    <a href="tel:911234567890">+91 1234567890</a>
                                </li>
                                <li>Email : <a href="mailto:ylanes-info@gmail.com">ylanes-info@gmail.com </a></li>
                            </ul>
                        </div>
                        <div class="foot_bx_soc">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="foot_botom">
        <div class="container">
            <div class="foot_botom_inr">
                <li>Copyright © 2021 <a href="#">www.YLanes.com</a></li>
                <li>All rights reserved</li>
            </div>
        </div>
    </div>

</footer> --}}

<footer class="new_ft_secs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_foot_top">
                    <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="ft_ftrst">
                        <a href="#">
                        <img src="{{url('public/frontend/images/ft-logo.png')}}">
                        </a>
                        <div class="flw_us">
                            <p>Follow us on</p>
                            <ul>
                                <li><a href="#"> <img src="{{url('public/frontend/images/ff.png')}}"></a></li>
                                <li><a href="#"> <img src="{{url('public/frontend/images/ff2.png')}}"></a></li>
                                <li><a href="#"> <img src="{{url('public/frontend/images/ff3.png')}}"></a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-6">
                        <div class="st_ftrst">
                        <div class="new_ft_links">
                            <ul>
                                <li><a href="{{route('welcome')}}">Home</a></li>
                                <li><a href="{{route('user.about.us')}}">About Us</a></li>
                                <li><a href="{{route('user.how.it.works')}}">How It Works</a></li>
                                <li><a href="{{route('landing.con.page')}}">Conversations</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-6">
                        <div class="thr_ftrst">
                        <div class="new_ft_links">
                            <ul>
                                <li><a href="{{route('user.privacy.policy')}}">Privacy Policy</a></li>
                                <li><a href="{{route('user.privacy.terms')}}">Terms & conditions</a></li>
                                <li><a href="{{route('user.faq')}}">FAQs</a></li>
                                <li><a href="{{route('user.contact.us')}}">Contact Us</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="new_foot_btms">
                    <p>YLanes is a conversations-only platform designed to connect like-minded individuals for meaningful conversations. This platform is not designed to support guests with severe mental illnesses.YLanes has a zero tolerance policy towards bad behavior (bullying, harassment, nudity, offensive language, ridiculing etc.); offenders would be permanently debarred from the platform. To go through our Terms and Conditions in detail please click here </p>
                    <h6>All rights reserved</h6>
                    <div class="ft_shp">
                        <img src="{{url('public/frontend/images/sh8s.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
 </footer>
{{-- <div class="foot_arw fixed">
    <a href="javascript:" id="return-to-top"><img src="{{url('public/frontend/images/foot_ar_bot.png')}}" alt=""></a>
</div> --}}

<div class="modal popup_list sign_popup" id="modalsignin">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Login</h1>
                        <p>Please enter your login info to continue</p>
                        <form id="popupLoginForm" action="{{ route('login') }}" method="post" autocomplete="off">
                            @csrf
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                           <input type="email" name="email" required="" placeholder="Email address" autocomplete="new-email">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon2.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="password" name="password" required="" placeholder="Password" autocomplete="new-password">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tram_check_bx">
                                    <div class="category-ul">
                                        <input type="checkbox" checked="" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember">Remember Me</label>
                                        <a href="{{route('user.forget.password.set')}}" class="for_pas">Forgot Password?</a>
                                    </div>
                                </div>
                                <div class="sing_btn">
                                    <button type="submit" class="pg_btn" >Login</button>
                                </div>
                                <div class="sing_btn sing_btn_otp">
                                    <a href="javascript:void(0);" class="a_popup openmobile pg_btn" id="clocv">Login with Mobile</a>
                                </div>
                                <div class="or_div">
                                    <strong><span>Or Login with</span></strong>
                                </div>
                                <div class="sing_up_sos">
                                    <ul>
                                        {{-- <li><a href="{{ url('login/facebook') }}" class="sos_btn sos_fac"><img src="{{url('public/frontend/images/sos_icon1.png')}}" alt="">Facebook</a></li> --}}
                                        <li><a href="{{ url('login/google') }}" class="sos_btn sos_goo"><img src="{{url('public/frontend/images/sos_icon2.png')}}" alt="">Google</a></li>
                                    </ul>
                                </div>
                                <div class="already_have">
                                    <span>Already have an account? <a  href="javascript:void(0);"  id="clocv" class="a_popup opensignup" >Sign Up</a> </span>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal popup_list sign_popup" id="modalsignin2">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Guest Sign Up</h1>
                        <p>Please fill in the below fields to continue with us</p>
                        <form id="registerForm" action="{{ route('register') }}" method="post">
                            @csrf
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="text" placeholder="Display name" name="profile_name" required="" maxlength="15">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon1.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-6">
                                        <div class="input_bx">
                                            <input type="text" placeholder="First name" name="first_name" required="">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon1.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input_bx">
                                            <input type="text" placeholder="Last name" name="last_name" required="">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon1.png')}}" alt=""></span>
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="email" placeholder="Email address" name="email" required="" id="registerEmail">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon2.png')}}" alt=""></span>
                                            <label id="chck_email_rd" class=" error chck_email_rd" style="display: none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <select style="width: 23%" required="" name="country_code" id="country_code">
                                                <option value="">Country Code</option>
                                                @foreach(CustomHelper::getCountrycode() as $country_code)
                                                <option value="{{ @$country_code->id }}">{{ @$country_code->name }} (+{{ @$country_code->phonecode }})</option>
                                                @endforeach
                                            </select>
                                            <input type="text" placeholder="Mobile number" name="phone" maxlength="10" id="registerPnone" style="width: 75%">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon3.png')}}" alt=""></span>
                                            <label id="country_code-error" class="error" for="country_code" style="display: none;"></label>
                                            <label id="chck_phone_rd" class=" error chck_phone_rd" style="display: none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="password" placeholder="Password" name="password" required autocomplete="new-password" id="password">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="password" placeholder="Confirm password" name="password_confirmation" required="" autocomplete="new-password">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tram_check_bx">
                                    <div class="category-ul">
                                        <input type="checkbox" id="condition" name="condition" style="display: block; visibility: hidden; position: absolute;" required="" checked="">
                                        <label for="condition">I agree to YLanes <a href="{{route('user.privacy.terms')}}" target="_blank">Terms &amp; Condition</a> and
                                            <a href="{{route('user.privacy.policy')}}" target="_blank">Privacy Policy</a>
                                        </label>
                                    </div>
                                    <label id="condition-error" class="error" for="condition" style="display: none;"></label>
                                </div>
                                <div class="sing_btn">
                                    <button type="submit" class="pg_btn">Sign Up</button>
                                </div>
                                <div class="or_div">
                                    <strong><span>Or Sign Up with</span></strong>
                                </div>
                                <div class="sing_up_sos">
                                    <ul>
                                        {{-- <li><a href="{{ url('login/facebook') }}" class="sos_btn sos_fac"><img src="{{url('public/frontend/images/sos_icon1.png')}}" alt="">Facebook</a></li> --}}
                                        <li><a href="{{ url('login/google') }}" class="sos_btn sos_goo"><img src="{{url('public/frontend/images/sos_icon2.png')}}" alt="">Google</a></li>
                                    </ul>
                                </div>
                                <div class="already_have">
                                    <span>Already have an account? <a  href="javascript:void();" class="a_popup opensignin" id="sin_mod">Login</a> </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal popup_list sign_popup" id="modalloginmobile">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Login with Mobile</h1>
                        <p>Please enter your mobile no to continue</p>
                        <form>
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="text" name="otp_phone" placeholder="Mobile number" onkeypress="validate();" maxlength="10">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon3.png')}}" alt=""></span>
                                            <label id="error_tmp_phone" class="error" style="display: none;"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <a  href="javascript:void(0);" id="sin_mod" class="a_popup pg_btn {{-- openloginotp --}} openloginotpbyPhone">Next</a>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal popup_list sign_popup" id="modalloginotp">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Verify Your Mobile Number</h1>
                        <p class="vd_hidden"></p>
                        <form>
                            <div class="singup_form">
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <input type="password" name="phone_otp" placeholder="Please enter OTP">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
                                            <label id="error_phone_otp" class="error" style="display: none;"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <a class="pg_btn phoneVerifyOtpButton"  href="#">Submit</a>
                                </div>

                                <div class="already_have" id="otpresend">
                                    <span>Didn’t receive OTP yet? <a  href="#" class="a_popup reSendOtpGuest" >Resend OTP</a> </span>
                                </div>
                                <div class="already_have" id="otpcountdown" style="display: none;"></div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="phoneVcodeUser" value="{{ @$user->phone_vcode }}">
<input type="hidden" id="phoneVcodeUserId" value="{{ @$user->id }}">

<div class="modal popup_list sign_popup" id="joinFirstPopup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Please agree to:</h1>
                        <form>
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul style="list-style: disc; padding: 1px 0px 0px 20px;">
                                            <li class="popupbulates">Lorem ipsum dolor sit amet</li>
                                            <li class="popupbulates">Lorem Ipsum has been the industry's</li>
                                            <li class="popupbulates">But also the leap into electronic typesetting</li>
                                            <li class="popupbulates">PageMaker including versions of Lorem Ipsum</li>
                                        </ul>
                                        <div class="category-ul">
                                            <input type="checkbox" name="agree" id="agree" required="" value="Y"> <label for="agree" style="font-size: 14px; line-height: 20px;">I agree</label>
                                        </div>
                                        <label id="agree-error" class="error" style="display: none;"></label>
                                    </div>

                                </div>

                                <div class="sing_btn">
                                    <a class="pg_btn" href="javascript:void(0);" onclick="joiningContinue();">Continue</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal popup_list sign_popup" id="joinPopup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Register Conversation</h1>
                        <div class="alert" id="popupConversationalert" style="display: none;">

                        </div>
                        <form class="popupJoiningArea">
                            <input type="hidden" name="conversation_id">
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="{{-- input_bx --}}">
                                            <p>Do you want to join anonymously?</p>
                                            <input type="radio" name="join_type" id="jyes" required="" style="width: 15px; height: 15px;" value="Y" onchange="displaynameHideShowJoiningtime($(this).val());">
                                            <label for="jyes">Yes</label>
                                            <input type="radio" name="join_type" id="jno" required="" style="width: 15px; height: 15px;" value="N" onchange="displaynameHideShowJoiningtime($(this).val());"> <label for="jno">No</label>
                                        </div>
                                        <label id="join_type-error" class="error" style="display: none;"></label>
                                    </div>
                                    <div class="col-sm-12" style="display: none;" id="displaynameareaPopupbeforejoining">
                                        <div class="input_bx">
                                            <p>Display name</p>
                                            <input type="text" name="conversation_display_name_popup" id="displayName" required="" placeholder="Display name" maxlength="15">
                                        </div>
                                        <label id="displayName-error" class="error" style="display: none;"></label>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            {{-- <p>What you will offer to this conversation</p> --}}
                                            <p>Your take</p>
                                            <textarea required="" placeholder="Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc." maxlength="300" name="conversation_text_popup" id="conversation_text" style="height: 100px;"></textarea>
                                        </div>
                                        <label id="conversation_text-error" class="error" for="conversation_text" style="display: none;"></label>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <a class="pg_btn" href="javascript:void(0);" onclick="joiningconversation();">Register</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal popup_list sign_popup" id="joinPopup1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h2 style="text-align: center">To register in a room you must complete your profile</h2>
                        <div class="alert" id="popupConversationalert" style="display: none;">

                        </div>
                        <div class="sing_btn" style="    margin-top: 30px;">
                            <a class="pg_btn" href="{{ route('user.edit.profile') }}" >Go To Profile</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade pric_modasl" id="myprivacys" style="background:#0000007a;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Privacy Policy</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="pri_lists_para">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever  when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade pric_modasl" id="mytermscn" style="background:#0000007a;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Terms & Condition</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="pri_lists_para">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever  when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                </p>
            </div>
        </div>
    </div>
</div>
