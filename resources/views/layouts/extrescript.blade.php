@if(@auth()->user()->id!=null)
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
@include('layouts.toaster')
<style>
    .swal-text {
        text-align: center !important;
    }
</style>
<script>
Pusher.logToConsole = true;

    var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {
        cluster: '{{env('PUSHER_APP_CLUSTER')}}'
    });

    var socketId = null;
    pusher.connection.bind('connected', function() {
        socketId = pusher.connection.socket_id;
		var urlRefresh = "{{route('refresh.dashboard')}}";
		$.ajax({
			url:urlRefresh,
			dataType: 'json',
			data: {
				socketId: socketId
			},
			type: 'get',
			success: function(resp){
				console.log(resp);
			}
		});
    });
    channel = pusher.subscribe('Ylanes');

	console.log(socketId);
</script>
@if(Request::segment(1)=="video-call")
<script>
	channel.bind('receive-event', function(data) {
		newmsg = data.message;
		auth_id='{{auth()->user()->id}}';
		console.log("receive event triggered:");
		console.log(data);
		if(data.to_id==auth_id)
		{
            callRequest(data);
		}
	});
</script>
@endif
<script>
    function callRequest(data){
        swal({
            title: data.from +" wants to participate in the conversation ",
            text: "Your Take: \n"+data.conversation_text+ ".\n Do you want to allow him?",
            icon: "info",
            // buttons: ['Cancle', 'OK'],
		    dangerMode: true,
            // timer: 15000,
            buttons: true,
            buttons: {
                catch: {
                    text: "No",
                    value: "No",
                    className:"swal-button--danger"
                },
                default: {
                    text: "Yes",
                    value: "Yes",
                    className:"swal-button--cancel"
                },
            },
        })
        .then((willDelete) => {
            switch (willDelete) {
                case "Yes":
                console.log('Yes');
                callAccept(data)
                break;

                case "No":
                console.log('No');
                callReject (data)

                break;

                default:
                console.log('ignore');
                callReject (data)
            }

        });
    }
    function callAccept(data){
        var conversionId = data.conversation_id;
        var from_user = data.from_id;
        var to_user = data.to_id;
        var formData = new FormData();
        formData.append("jsonrpc", "2.0");
        formData.append('_token', '{{csrf_token()}}');
        formData.append('conversionId', conversionId);
        formData.append('from_user', from_user);
        formData.append('to_user', to_user);

        formData.append('status', 'A');
        $.ajax({
            url: "{{route('request.call.status')}}",
            type: 'post',
            data: formData,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            mimeType:"multipart/form-data",
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log("error", error);
            }
        });
    }
    function callReject(data){
        var conversionId = data.conversation_id;
        var from_user = data.from_id;
        var to_user = data.to_id;
        var formData = new FormData();
        formData.append("jsonrpc", "2.0");
        formData.append('_token', '{{csrf_token()}}');
        formData.append('conversionId', conversionId);
        formData.append('from_user', from_user);
        formData.append('to_user', to_user);

        formData.append('status', 'R');
        $.ajax({
            url: "{{route('request.call.status')}}",
            type: 'post',
            data: formData,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            mimeType:"multipart/form-data",
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log("error", error);
            }
        });
    }
    channel.bind('join-event-accept', function(data) {
		newmsg = data.message;
        var conversionId = data.conversation_id;
		auth_id='{{auth()->user()->id}}';
		if(data.to_id==auth_id)
		{
            console.log(data);
            toastr.success('Your Join Request Accepted');
            window.location.href = '{{ route("welcome") }}/video-call/'+conversionId;
		}
	});
    channel.bind('join-event-reject', function(data) {
		newmsg = data.message;
        var conversionId = data.conversation_id;
		auth_id='{{auth()->user()->id}}';
		if(data.to_id==auth_id)
		{
            console.log(data);
            toastr.error('Your Join Request Rejected');
		}
	});
//     $(document).ready(function(){
//     swal({
//     title: "soumo wants to participate in the conversation ",
//     text: "His Takes: . \n Do you want to allow him?",
//     icon: "info",
//     // buttons: ['Cancle', 'OK'],
//     dangerMode: true,
//     // timer: 15000,
//     buttons: true,
//     buttons: {
//         catch: {
//             text: "No",
//             value: "No",
//             className:"swal-button--danger"
//         },
//         default: {
//             text: "Yes",
//             value: "Yes",
//             className:"swal-button--cancel"
//         },
//     },
// })
// })

channel.bind('chat-event', function(data) {
    newmsg = data.message;
    var conversionId = data.conversation_id;
    auth_id='{{auth()->user()->id}}';
    console.log(auth_id)
    console.log(data.to_id);
    console.log(jQuery.inArray(auth_id, data.to_id));

    if(jQuery.inArray(auth_id, data.to_id) !== -1 && conversionId==$('#conversation_id').val()){
        console.log(data.to_id);
        console.log($('#openMessage').is(':visible'))
        if($('#openMessage').is(':visible')==true){
            $('.chat_notification_div').show();
        }
        let msg='';
        data.messageData.forEach(function(item, index){
            senderName=data.allUser[item.user_id] !=null? data.allUser[item.user_id] : item.get_sender.profile_name;
            if(item.file_name !== null){
                file = "{{url('storage/app/public/message_files/')}}";
                file = file+'/'+item.file_name;
                filename=item.file_name.substr(item.file_name.lastIndexOf("_")+1)
                attach_file=`${item.message?'<br>':''}<a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
            }else{
                attach_file=``;
            }
            if(item.user_id!="{{auth()->user()->id}}"){
                msg+=`
                <div class="chat_mass_itm">
                    <div class="media">
                        <div class="media-body">
                            <h5>${senderName}</h5>
                            <div class="chat_mass_bx">
                                <p>${item.message?item.message:''}${attach_file}</p>
                            </div>
                            <span><img src="{{URL::to('public/frontend/images/mt.png')}}"> ${getTimeDiff(item.created_at)}</span>
                        </div>
                    </div>
                </div>`;
            }
        });
        $('.messages_rigrt_body').append(msg);
        indexxx += $('.messa_rigrt_body_panel').height();
        $('.messages_rigrt_body .chat_mass_itm').each(function(i , value){
            height+=parseInt($(this).height());
        })
        $(".messages_rigrt_body").animate({ scrollTop: height}, 1000);




    }
    // if(data.to_id==auth_id)
    // {
    //     console.log(data);
    //     toastr.success('Your Join Request Accepted');
    //     toastr.success('');
    //     window.location.href = '{{ route("welcome") }}/video-call/'+conversionId;
    // }
});
</script>


@endif
