<header class="header_sec pad_i header_box_shw header_login">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light nav_top index_header">
            <a class="navbar-brand" href="{{ route('welcome') }}"><img src="{{url('public/frontend/images/logo.png')}}" alt="logo" /></a>


            <div class="search-section input-group">
                <form action="{{ route('search.page') }}">
                    <div class="d-flex align-items-center mrtop_rs">
                        <input type="text" class="" name="keywords" placeholder="Search here" value="{{ @request()->keywords }}" id="keyword_header_search">
                        <span class="input-group-btn"><button class="btn btn-default" type="submit"> <img src="{{url('public/frontend/images/search.png')}}"> </button></span>
                    </div>
                    <div id="keywordList" class="for_auto_complete" style="display: none;"></div>
                </form>
            </div>
            <div class="mob_srch_icon"><img src="{{url('public/frontend/images/search.png')}}"></div>
            @auth
            <div class="login_btn log-outsd">
                <ul>
                    <li><a href="{{route('landing.con.page')}}" class="con_btns"><img src="{{url('public/frontend/images/comment_1.png')}}" class="hvern"><img src="{{url('public/frontend/images/comment_1h.png')}}"  class="hverb">Upcoming Conversations</a></li>
                </ul>
            </div>
            <div class="rm_header_info">
                <a href="{{route('ycoin.usage')}}"><p><img src="{{url('public/frontend/images/coins.png')}}" alt="coins"><span class="off_rs"></span> {{-- {{ CustomHelper::number_format_short(@Auth::user()->conversation_balance_total) }} --}} {{ @Auth::user()->conversation_balance_total }}</p></a>
                <p><img src="{{url('public/frontend/images/followers.png')}}" alt="coins"><span class="off_rs"></span> {{ CustomHelper::number_format_short(@Auth::user()->tot_hearts) }}</p>
            </div>
            <div class="af_log_dv ml-auto">
                <em href="#url" id="profidrop" >
                    <b>
                        @if(!empty(@Auth::user()->image))
                        <img src="{{ url('storage/app/public/customer/profile_pics/'.@Auth::user()->image) }}" alt="" class="afterImageUpload">
                        @else
                        <img src="{{url('public/frontend/images/default_pic.png')}}" alt="" class="afterImageUpload">
                        @endif
                        <span class="off_rs">Hi, {{ substr(@Auth::user()->profile_name,0,5) }}@if( substr(@Auth::user()->profile_name,5))..@endif</span>
                    </b><span><img src="{{url('public/frontend/images/drop.png')}}" alt=""></span>
                </em>
                <div class="profidropdid" id="profidropdid">
                    <ul>
                        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('user.edit.profile') }}">My Profile</a></li>
                        {{-- <li><a href="{{ route('user.settings') }}">Setting</a></li> --}}
                        <li><a href="{{ route('my.connect') }}">My Connects</a></li>
                        <li><a href="{{ route('create.conversation') }}">Create Room</a></li>
                        <li><a href="{{ route('upcoming.conversation') }}">My Rooms </a></li>
                        <li><a href="{{ route('past.conversation') }}">Past Rooms</a></li>
                        <li><a href="{{ route('ycoin.usage') }}">My wallet</a></li>
                        <li><a href="{{ route('referral.earn') }}">Referral Earning</a></li>
                        {{--<li><a href="#">My Payments</a></li>
                         <li><a href="{{ route('purchase.package') }}">Purchase Package</a></li> --}}
                        <li><a href="{{ route('purchase.package') }}">Purchase Ycoin</a></li>
                        <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>

            @else
            <div class="login_btn">
                <ul>
                    <li><a href="{{route('landing.con.page')}}" class="con_btns"><img src="{{url('public/frontend/images/comment_1.png')}}" class="hvern"><img src="{{url('public/frontend/images/comment_1h.png')}}"  class="hverb">Upcoming Conversations</a></li>
                    <li><a href="javascript:void(0);" class="pg_btn bodr_btn opensignup">Sign up</a></li>
                    <li><a href="javascript:void(0);" class="pg_btn opensignin">Login</a></li>
                </ul>
            </div>
            @endauth
        </nav>
    </div>
</header>
<div class="mar_top"></div>
