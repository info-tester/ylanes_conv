<div class="col-lg-3 col-md-12 col-sm-12">
	<div class="cusdashb-left">
		<div class="mobile_menu2"> <i class="fa fa-bars" aria-hidden="true"></i>
			<span>Show Menu</span>
		</div>
		<div class="user_dash_lf_inr" id="mobile_menu_dv2">
			<div class="user_Outer">
				<div class="user_Detil">
					<div class="user">
						@if(!empty(@Auth::user()->image))
                        <img src="{{ url('storage/app/public/customer/profile_pics/'.@Auth::user()->image) }}" alt="" class="afterImageUpload">
                        @else
                        <img src="{{url('public/frontend/images/default_pic.png')}}" alt="" class="afterImageUpload">
                        @endif
					</div>
					<h4>{{ substr(@Auth::user()->profile_name,0,15) }}@if(substr(@Auth::user()->profile_name,15))...@endif</h4>
					<a href="#">{{ substr(@Auth::user()->email,0,25) }}@if(substr(@Auth::user()->email,25))...@endif</a>
				</div>
				<div class="listIng">
					<ul>
						<li>
							<p>{{ CustomHelper::number_format_short(@Auth::user()->tot_hearts) }} <span>Hearts</span></p>
						</li>
						<li>
							<p>{{ CustomHelper::number_format_short(@Auth::user()->tot_conversation_completed,0) }} <span>Conversations</span></p>
						</li>
						<li>
							<p>{{ CustomHelper::number_format_short(@Auth::user()->tot_connects,0) }} <span>Connects</span></p>
						</li>
					</ul>
				</div>
			</div>
			<div class="nav_Pannel">
				<ul>
					<li>
						<a href="{{ route('dashboard') }}" class="{{ Route::is('dashboard') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/dashboard.png')}}" alt="icon"> </span>Dashboard<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
		               <a href="{{ route('user.edit.profile') }}" class="{{ Route::is('user.edit.profile','user.settings') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/persons.png')}}" alt="icon"> </span>My Profile<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
		            </li>
		            {{-- <li>
		               <a href="{{ route('user.settings') }}" class="{{ Route::is('user.settings') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/persons.png')}}" alt="icon"> </span>Setting<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
		            </li> --}}
					<li>
						<a href="{{ route('my.connect') }}" class="{{ Route::is('my.connect','my.block.list') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/my_connects.png')}}" alt="icon"> </span>My Connects<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
						<a href="{{ route('create.conversation') }}" class="{{ Route::is('create.conversation') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/upcoming_con.png')}}" alt="icon"> </span>Create Room<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
						<a href="{{ route('upcoming.conversation') }}" class="{{ Route::is('upcoming.conversation','view.conversation.details') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/upcoming_con.png')}}" alt="icon"> </span>My Rooms <img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
						<a href="{{ route('past.conversation') }}" class="{{ Route::is('past.conversation','past.conversation.details') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/past-con.png')}}" alt="icon"> </span>Past Rooms<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
						<a href="{{ route('ycoin.usage') }}" class="{{ Route::is('ycoin.usage') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/ycoins-usage.png')}}" alt="icon"> </span>My wallet<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
						<a href="{{ route('referral.earn') }}" class="{{ Route::is('referral.earn','referral.user.list') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/referral.png')}}" alt="icon"> </span>Referral Earning<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					{{-- <li>
						<a href="#"><span><img src="{{url('public/frontend/images/payment.png')}}" alt="icon"> </span>My Payments<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li> --}}
					{{-- <li>
						<a href="{{ route('purchase.package') }}" class="{{ Route::is('purchase.package') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/payment.png')}}" alt="icon"> </span>Purchase Package<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li> --}}
					<li>
						<a href="{{ route('purchase.package') }}" class="{{ Route::is('user.package','purchase.package') ? 'active' : '' }}"><span><img src="{{url('public/frontend/images/payment.png')}}" alt="icon"> </span>Purchase Ycoin<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}"></a>
					</li>
					<li>
						<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							<span><img src="{{url('public/frontend/images/log-out.png')}}" alt="icon"> </span>Log Out<img class="dash_Arrow" src="{{url('public/frontend/images/dashboard-rightarrow.png')}}">
						</a>
						<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
