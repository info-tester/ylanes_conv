<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>YLanes | @yield('title')</title>
    <link rel="shortcut icon" href="{{url('public/frontend/images/fav.png')}}">
    <link href="{{url('public/frontend/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('public/frontend/css/cale1.css')}}">
    <link rel="stylesheet" href="{{url('public/frontend/css/jquery-ui1.css')}}">
    <link href="{{url('public/frontend/css/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <!--Old css-->
    <link href="{{url('public/frontend/css/style.css')}}" rel="stylesheet">
    <!--New css-->
    <link href="{{url('public/frontend/css/style_new.css')}}" rel="stylesheet">
    <link href="{{url('public/frontend/css/responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('public/frontend/css/select2.css')}}">
    <link href="{{url('public/frontend/css/font-awesome/all.min.css')}}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Averia+Serif+Libre:wght@300;400;700&family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="{{url('public/frontend/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{url('public/frontend/icofont/icofont.min.css')}}" rel="stylesheet">
    <!-- Select2 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        label.error{
            color: #ff0000 !important;
            /*font-size: 13px !important;*/
        }
        .for_auto_complete {
            position: relative;
            display: inline-block;
            width: 100%;
        }
        .for_auto_complete ul {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            top: -17px;
            left: 0;
            right: 0;
        }
        .for_auto_complete ul li {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }
        .for_auto_complete ul li a {
            color: #333;
            text-transform: none;
            padding-left: 1.5rem;
            font-weight: 500;
            font-size: 14px;
        }
        .for_auto_complete ul li img {
            width: 30px;
        }
        .popupbulates::marker{
            color: #000;
            font-size: 20px;
        }
    </style>
    @stack('css')
</head>
<body>

    @if(Request::segment(1)!='video-call')
    @include('layouts.header')
    @endif
    @yield('content')
    @if(Request::segment(1)!='video-call')
    @include('layouts.footer')
    @endif

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('public/frontend/js/jquerymin.js')}}"></script>
    <script src="{{url('public/frontend/js/bootstrap.js')}}"></script>
    <script src="{{url('public/frontend/js/owl.carousel.js')}}"></script>
    <script src="{{url('public/frontend/js/custom.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" integrity="sha512-vSyPWqWsSHFHLnMSwxfmicOgfp0JuENoLwzbR+Hf5diwdYTJraf/m+EKrMb4ulTYmb/Ra75YmckeTQ4sHzg2hg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="{{url('public/frontend/js/jquery-clockpicker.min.js')}}"></script>
    <script src="{{url('public/frontend/js/jquery-ui.js')}}"></script>
    @include('layouts.extrescript')
    <!-- Select2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#country_code").select2();
            $('#keyword_header_search').keyup(function(e){
                var keyword = $(this).val();
                if(keyword != '' && keyword.length >= 2)
                {
                    var reqData = {
                        _token: '{{ @csrf_token() }}',
                        params: {
                            keyword: keyword
                        }
                    }
                    $.ajax({
                        url:"{{ route('autocomplete.keyword') }}",
                        method: "POST",
                        data: reqData,
                        success:function(response){
                            if(response.error) {
                                console.log(response.error.message);
                            } else {
                                if(response.result.parent_categories.length > 0 || response.result.sub_categories.length > 0 || response.result.topic.length > 0) {
                                    var searchHtml = '<ul>';
                                    response.result.parent_categories.forEach(function(item, index) {
                                        if(item.picture){
                                            img = '<img style="margin-right:15px" src="{{ url('storage/app/public/category_pics')}}/'+item.picture+'">';
                                        }
                                        else{
                                            img = '';
                                        }
                                        searchHtml += '<li><a href="{{ url('search')}}/'+item.slug+'"> '+ img +' '+item.name+'</a></li>';
                                        if(item.sub_categories.length > 0) {
                                            item.sub_categories.forEach(function(item2, index) {
                                                searchHtml += '<li><a href="{{ url('search')}}/'+item2.slug+'">'+item2.name+' in <b>'+item.name+'</b></a></li>';
                                            });
                                        }
                                    });
                                    response.result.sub_categories.forEach(function(item, index) {
                                        searchHtml += '<li><a href="{{ url('search')}}/'+item.slug+'">'+item.name+' in <b>'+item.immediate_parent.name+'</b></a></li>';
                                    });

                                        // products
                                        response.result.topic.forEach(function(item, index) {
                                            searchHtml += '<li><a href="{{ url('search-single-topic')}}/'+item.id+'"><i style="margin-right:15px" class="fa fa-search"></i>'+item.topic_line_1+' in <b> '+item.subcategory.name+'</b></a></li>';
                                        });
                                        searchHtml += '</ul>';
                                        $('#keywordList').html(searchHtml);
                                        $('#keywordList').show();
                                    } else {
                                        $('#keywordList').hide();
                                    }
                                }
                            }
                        });
                } else {
                    $('#keywordList').hide();
                }
            });

            $(".opensignin").click(function(){
                $('#modalsignin2').modal('hide');
                $('#modalloginmobile').modal('hide');
                $('#modalloginotp').modal('hide');
                $('#modalsignin').modal('show');

            });
            $(".opensignup").click(function(){
                $('#modalsignin').modal('hide');
                $('#modalloginmobile').modal('hide');
                $('#modalloginotp').modal('hide');
                $('#modalsignin2').modal('show');
            });

            $(".openmobile").click(function(){
                $('#modalsignin').modal('hide');
                $('#modalsignin2').modal('hide');
                $('#modalloginotp').modal('hide');
                $('#modalloginmobile').modal('show');
            });
            $(".openloginotp").click(function(){
                $('#modalsignin').modal('hide');
                $('#modalsignin2').modal('hide');
                $('#modalloginmobile').modal('hide');
                $('#modalloginotp').modal('show');
            });

            jQuery.validator.addMethod("emailonly", function(value, element) {
                return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value.toLowerCase()) || /.+@(gmail|yahoo|hotmail)\.com$/.test(value.toLowerCase());
            }, "Please enter a valid email address.");
            $("#registerForm").validate({
                rules : {
                    first_name : {
                        required : true,
                        minlength : 2
                    },
                    last_name : {
                        required : true,
                        minlength : 2
                    },
                    email : {
                        required : true,
                        email : true,
                        emailonly:true
                    },
                    country_code : {
                        required : true,
                        digits:true
                    },
                    phone : {
                        required : true,
                        digits : true,
                        minlength:10,
                        maxlength:10
                    },
                    password : {
                        required : true,
                        minlength : 8
                    },
                    password_confirmation : {
                        required : true,
                        minlength : 8,
                        equalTo : "#password"
                    }
                },
                messages : {
                    first_name :{
                        required : "Please enter your first name",
                        minlength : "Your first name must consist of at least 2 characters"
                    },
                    last_name :{
                        required : "Please enter your last name",
                        minlength : "Your last name must consist of at least 2 characters"
                    },
                    email: {
                        required: 'Please enter a valid email address.',
                    },
                    phone:{
                        required: "Please enter your phone no",
                        digits: "Please enter a valid phone no",
                    },
                    country_code:{
                        required: "Please select your country code",
                        digits: "Please enter a valid country code",
                    },
                    password : {
                        required : "Please enter a password",
                        minlength : "Your password must be consist of at least 8 characters"
                    },
                    password_confirmation : {
                        required : "Please enter a password",
                        minlength : "Your password must be consist of at least 8 characters",
                        equalTo : "Please enter the same password as above"
                    }
                }
            });
            $('#registerEmail').blur(function(event) {
                if($('#registerEmail').val() != ''){
                    $("#chck_email_rd").html('');
                    var $this = $(this);
                    var email = $(this).val();
                    var reqData = {
                        '_token': '{{ @csrf_token() }}',
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_email_rd").html(response.error.merchant);
                                $("#chck_email_rd").show();
                                $('#registerEmail').val('');
                                $($(this).next().html(''));
                            } else {
                                // $("#chck_eml_grn").html(response.result.message);
                                $("#chck_email_rd").html('');
                                $("#chck_email_rd").hide();
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }

                } else {
                    $("#chck_email_rd").html('');
                    $("#chck_email_rd").hide();
                }
            });
            $('#registerPnone').blur(function(event) {

                if($('#registerPnone').val() != ''){
                    $("#chck_phone_rd").html('');
                    var $this = $(this);
                    var phone = $(this).val();
                    var reqData = {
                        '_token': '{{ @csrf_token() }}',
                        'params': {
                            'email': phone
                        }
                    };
                    if(phone != '') {
                        $.ajax({
                            url: '{{ route('user.check.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_phone_rd").html(response.error.merchant);
                                $("#chck_phone_rd").show();
                                $('#registerPnone').val('');
                                $($(this).next().html(''));
                            } else {
                                $("#chck_phone_rd").html('');
                                $("#chck_phone_rd").hide();
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }

                } else {
                    $("#chck_phone_rd").html('');
                    $("#chck_phone_rd").hide();
                }
            });
            $("#popupLoginForm").validate({
                rules:{
                    'email':{
                        required:true,
                        email:true,
                        emailonly: true,
                    }
                },
                messages:
                {
                    email: {
                        required: 'Please enter a valid email address.',
                        email:"Please enter a valid email address.",
                        emailonly: "Please enter a valid email address.",
                    }
                }
            });
            $('a.openloginotpbyPhone').click(function(){
                var tmp_phone = $('input[name="otp_phone"]').val(),
                tmpPhone = parseInt($.trim($('input[name="otp_phone"]').val()));
                if(tmp_phone == '' || tmpPhone.toString().length != 10 || tmpPhone.toString().length != 10)
                {
                    $('label#error_tmp_phone').text('please enter a valid phone no.');
                    $('label#error_tmp_phone').show();
                }
                else{
                    $('label#error_tmp_phone').hide();
                }
                if(tmpPhone.toString().length == 10){
                    $.ajax({
                        url: "{{ route('user.check.phone') }}",
                        type:"POST",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            'phone': tmp_phone,
                        },
                        success: function(responce){
                            console.log(responce);
                            if(responce)
                            {
                                if(responce == 0)
                                {
                                    $('label#error_tmp_phone').text('Mobile no not exist in our records.');
                                    $('label#error_tmp_phone').show();
                                }
                                else if(responce == 1){
                                    $('label#error_tmp_phone').text('Mobile no not verified.');
                                    $('label#error_tmp_phone').show();
                                }
                                else if(responce == 2){
                                    $('label#error_tmp_phone').text('Your account is inactive by admin!');
                                    $('label#error_tmp_phone').show();
                                }
                                else{

                                    $('.vd_hidden').html('Enter the 6 digit code sent to you at '+responce.phone);
                                    $('input#phoneVcodeUser').val(responce.phone_vcode);
                                    $('input#phoneVcodeUserId').val(responce.id);

                                    $('.vd_hidden').show();
                                    $('#modalsignin').modal('hide');
                                    $('#modalsignin2').modal('hide');
                                    $('#modalloginmobile').modal('hide');
                                    $('#modalloginotp').modal('show');
                                }

                            }
                            else{

                            }
                        },
                        error: function(xhr){
                            console.log(xhr);
                        }
                    });
                }
            });
            $('a.phoneVerifyOtpButton').click(function(){

                var phone_otp = $('input[name="phone_otp"]').val();

                var PhoneVcode = $('input#phoneVcodeUser').val();

                if(phone_otp == '' || phone_otp != PhoneVcode)

                {

                    $('label#error_phone_otp').text('please enter a valid OTP.');

                    $('label#error_phone_otp').show();

                }

                else{

                    $('label#error_phone_otp').hide();

                }

                if(phone_otp == PhoneVcode)

                {

                    $.ajax({

                        url: "{{ route('user.check.phone') }}",

                        type:"POST",

                        data: {

                            '_token': "{{ csrf_token() }}",

                            'user_id': $('#phoneVcodeUserId').val(),

                            'phone_otp': phone_otp,

                        },

                        success: function(responce){
                            console.log(responce);

                            if(responce != 0)

                            {
                                if(responce == 'p_page'){
                                    window.location.replace("{{ route('purchase.package') }}");
                                }
                                else if(responce == 'e_page'){
                                    window.location.replace("{{ route('dashboard') }}");
                                }
                                else{
                                    window.location.replace(responce);
                                }
                            }

                            else{

                                $('#verifyPhoneNo').hide();

                                $('#verifyPhoneOtp').hide();

                                $('input#UserPhone').val('');

                                $('#phoneVerifyed').hide();

                            }

                        },

                        error: function(xhr){

                            console.log(xhr);

                        }

                    });

                }

            });
            $('a.reSendOtpGuest').click(function(){
                var tmp_phone = $('input[name="otp_phone"]').val();
                $.ajax({

                    url: "{{ route('user.check.phone') }}",

                    type:"POST",

                    data: {

                        '_token': "{{ csrf_token() }}",

                        'phone': tmp_phone,

                    },

                    success: function(responce){

                        console.log(responce);

                        if(responce)

                        {
                            if(responce == 0)
                            {
                                $('label#error_tmp_phone').text('Mobile no not exist in our records.');
                                $('label#error_tmp_phone').show();
                            }
                            else if(responce == 1){
                                $('label#error_tmp_phone').text('Mobile no not verified.');
                                $('label#error_tmp_phone').show();
                            }
                            else{

                                $('.vd_hidden').html('Enter the 6 digit code sent to you at '+responce.phone);
                                $('input#phoneVcodeUser').val(responce.phone_vcode);
                                $('input#phoneVcodeUserId').val(responce.id);

                                $('.vd_hidden').show();
                                var timeleft = 90;
                                var downloadTimer = setInterval(function(){
                                  if(timeleft <= 0){
                                    clearInterval(downloadTimer);
                                    $('#otpresend').show();
                                    $('#otpcountdown').hide();
                                } else {
                                    $('#otpresend').hide();
                                    $('#otpcountdown').html('<span>Didn’t receive OTP yet? <a  href="#" class="a_popup">Send OTP again in '+ timeleft +' Sec</a> </span>');
                                    $('#otpcountdown').show();
                                }
                                timeleft -= 1;
                            }, 1000);
                            }

                        }

                        else{

                        }

                    },

                    error: function(xhr){

                        console.log(xhr);

                    }

                });
            });
        });
function validate(evt) {
    var theEvent = evt || window.event;

    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {

        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
function joinconversation(convids){
    $('input[name="conversation_id"]').val(convids);
    $('textarea[name="conversation_text_popup"]').val('');
    $('input[name=join_type]').attr('checked',false);
    $('#popupConversationalert').html('');
    $('#popupConversationalert').hide('');
    $('.popupJoiningArea').show();
    @auth
    @php
    if(empty(@auth::user()->country) || empty(@auth::user()->profile_name) || empty(@auth::user()->year_of_birth) || empty(@auth::user()->gender)){
        @endphp
        $.ajax({
            url:"{{ route('session.set.before.login') }}"+convids,
            type:"GET",
            success: function(responce){
                if(responce == 1){
                    // location.replace("{{ route('user.edit.profile') }}");

                    $('#joinPopup1').modal('show');
                }
            }
        });
        @php
    }
    else{
    @endphp
        $('#joinPopup').modal('show');
    @php
    }
    @endphp
    @else
    $.ajax({
        url:"{{ route('session.set.before.login') }}"+convids,
        type:"GET",
        success: function(responce){
            if(responce == 1){
                $('#modalsignin').modal('show');
            }
        }
    })
    @endauth
    // $('#joinFirstPopup').modal('show');
}
function joiningContinue(){
    var agree = $('input[name="agree"]:checked').val();
    if (agree != "Y") {
        $('#agree-error').html('This field is required.');
        $('#agree-error').show();
    }
    else{
        $('#agree-error').html('');
        $('#agree-error').hide();
        $('input[name=agree]').attr('checked',false);
        $('#joinFirstPopup').modal('hide');
        $('#joinPopup').modal('show');
    }
}
function displaynameHideShowJoiningtime(value){
    if(value != ''){
        if (value == 'Y') {
            $('#displaynameareaPopupbeforejoining').show();
        }
        else if(value == 'N'){
            $('#displaynameareaPopupbeforejoining').hide();
        }
    }
}
function joiningconversation(){
    var join_type = $('input[name="join_type"]:checked').val();
    var conversation_text = $('textarea[name="conversation_text_popup"]').val();
    var conversation_id = $('input[name="conversation_id"]').val();
    var display_name = $('input[name="conversation_display_name_popup"]').val();

    if(join_type == '' || join_type === undefined  && conversation_text == ''){
        $('#join_type-error').html('This field is required.');
        $('#conversation_text-error').html('This field is required.');
        $('#join_type-error').show();
        $('#conversation_text-error').show();
    }
    else if(join_type == '' || join_type === undefined){
        $('#join_type-error').html('This field is required.');
        $('#join_type-error').show();
        $('#conversation_text-error').html('');
        $('#conversation_text-error').hide();
    }
    else if(conversation_text == ''){
        $('#conversation_text-error').html('This field is required.');
        $('#conversation_text-error').show();
        $('#join_type-error').html('');
        $('#join_type-error').hide();
    }
    else if(conversation_text.length < 50){
        $('#conversation_text-error').html('Please enter at least 50 characters. Max 300 characters.');
        $('#conversation_text-error').show();
        $('#join_type-error').html('');
        $('#join_type-error').hide();
    }
    else{
        $('#join_type-error').html('');
        $('#conversation_text-error').html('');
        $('#join_type-error').hide();
        $('#conversation_text-error').hide();
        if(join_type == 'Y'){
            if(display_name == '' || display_name === undefined){
                $('#displayName-error').html('This field is required.');
                $('#displayName-error').show();
                return false;
            }
        }
        else{
            $('#displayName-error').html('');
            $('#displayName-error').hide();
        }
        $.ajax({
            type:"Post",
            url:"{{ route('joining.conversation') }}",
            data:{
                '_token':"{{ csrf_token() }}",
                'join_type': join_type,
                'display_name': display_name,
                'conversation_text': conversation_text,
                'conversation_id': conversation_id,
            },
            success: function(response){
                console.log(response);
                if (response == 0) {
                    $('#popupConversationalert').removeClass('alert-success');
                    $('#popupConversationalert').addClass('alert-danger');
                    $('#popupConversationalert').html('The conversation is currently not available.');
                    $('#popupConversationalert').show();
                }
                else if(response == 1){
                    $('#joinPopup').modal('hide');
                    $('#modalsignin').modal('show');
                }
                else if(response == 2){
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('The conversation has already registered all participants.');
                    $('#popupConversationalert').show();
                }
                else if(response == 3){
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('You already registered this conversation.');
                    $('#popupConversationalert').show();
                }
                else if(response == 4){
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('You already requested to register this conversation.');
                    $('#popupConversationalert').show();
                }
                else if(response == 5){
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('Your request was rejected by the creator this conversation.');
                    $('#popupConversationalert').show();
                }
                else if(response == 6){
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('You already canceled this conversation. you can not register this conversation.');
                    $('#popupConversationalert').show();
                }
                else if(response == 'yes'){
                    $('.popupJoiningArea').hide();
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('You have successfully registered in this conversation.');
                    $('#popupConversationalert').show();
                    @if(Route::is('search.page'))
                    srarchResult();
                    @elseif(Route::is('search.single.topic') || Route::is('landing.con.page'))
                    window.setTimeout(function(){location.reload()},1000)
                    @endif
                }
                else if(response == 'request-send'){
                    $('.popupJoiningArea').hide();
                    $('#popupConversationalert').removeClass('alert-danger');
                    $('#popupConversationalert').addClass('alert-success');
                    $('#popupConversationalert').html('You request send successfully to the users wait for accept request.');
                    $('#popupConversationalert').show();
                    @if(Route::is('search.page'))
                    srarchResult();
                    @elseif(Route::is('search.single.topic') || Route::is('landing.con.page'))
                    window.setTimeout(function(){location.reload()},1000)
                    @endif
                }
                else if(response == 7){
                    $('#popupConversationalert').removeClass('alert-success');
                    $('#popupConversationalert').addClass('alert-danger');
                    $('#popupConversationalert').html('Not sufficient balance to register this conversation.');
                    $('#popupConversationalert').show();
                }
                $('input[name="conversation_display_name_popup"]').val('');
                $('textarea[name="conversation_text_popup"]').val('');
                $('input[name=join_type]').attr('checked',false);
            },
            error: function(xhr){
                console.log(xhr);
            }
        });
    }
}
</script>
@stack('js')
</body>
</html>
