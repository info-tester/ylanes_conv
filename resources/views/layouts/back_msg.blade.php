@if (session()->has('success'))
<div class="alert alert-icon alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <i class="mdi mdi-check-all"></i>
    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
</div>
@elseif (session()->has('error'))
<div class="alert alert-icon alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert"  aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <i class="mdi mdi-block-helper"></i>
    <strong>{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
</div>
@endif
@if ($errors->any())
<div class="alert alert-icon alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert"  aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>

    @foreach ($errors->all() as $error)
    <i class="mdi mdi-block-helper"></i> {{ $error }}<br>
    @endforeach
</div>
@endif