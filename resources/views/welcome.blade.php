@extends('layouts.app')
@section('title','Home')
@section('content')
<section class="new_banners">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-7">
                <div class="new_banners_conts">
                    <img src="{{url('public/frontend/images/sha2.png')}}">
                    <h1>A Welcoming Space for <span> Meaningful</span> Conversations </h1>
                    <p>Wide range of topics, convenient timings, choice of room type, gifting and receiving hearts, amazing people from across the world, authentic connections and much more</p>
                    <div class="banner_anchers">
                        <a href="{{ route('landing.con.page') }}" class="get_bn_btns">
                            Get Started <img src="{{url('public/frontend/images/arbr.png')}}">
                        </a>
                        {{-- <a href="#" class="play_st_btns">
                            <img src="{{url('public/frontend/images/pla1.png')}}">
                        </a>
                        <a href="#" class="play_st_btns">
                            <img src="{{url('public/frontend/images/pla2.png')}}">
                        </a> --}}
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="new_banner_imgs">
                    <img src="{{url('public/frontend/images/new_baner.png')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="shps">
       <img src="{{url('public/frontend/images/sha1.png')}}">
    </div>
</section>
<section class="new_abouts">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_secs_headings">
                    <h1>About YLanes</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="abut_set">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-lg-5">
                    <div class="abt_mob_imgs">
                    <img src="{{url('public/frontend/images/abt_mobs.png')}}">
                    </div>
                </div>
                <div class="col-md-6 col-lg-7">
                    <div class="abuts_contents">
                        <p class="abts_cont">YLanes enables Meaningful Conversations with like-minded people</p>
                        <div class="cont_lb1">
                            <em>
                                <img src="{{url('public/frontend/images/tt.png')}}">
                            </em>
                            <div class="cnt_ab">
                                <h3>Share your stories & ideas</h3>
                                <p>on topics that matter to you (anonymously if you wish) </p>
                            </div>
                        </div>
                        <div class="cont_lb1">
                            <em>
                                <img src="{{url('public/frontend/images/tt.png')}}">
                            </em>
                            <div class="cnt_ab">
                                <h3>Connect and gain perspective </h3>
                                <p>from like-minded individuals from across the world on topics of interest </p>
                            </div>
                        </div>
                        <div class="cont_lb1">
                            <em>
                                <img src="{{url('public/frontend/images/tt.png')}}">
                            </em>
                            <div class="cnt_ab">
                                <h3>Hold a safe space for others</h3>
                                <p>to share their thoughts, on topics of your experience or expertise. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="spas_5">
            <img src="{{url('public/frontend/images/sha5.png')}}">
        </div>
    </div>
</section>
<section class="new_con_tops">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_secs_headings">
                    <h1>Top conversation topics</h1>
                    <div class="shst">
                        <img src="{{url('public/frontend/images/shp4.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="list_topsx">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="tops_geads">
                        <h1>You can choose from a list of <span>75+ conversation topics </span>
                            covering Relationships, Reflections and Exploring Self
                        </h1>
                    </div>
                    <div class="tops_geads_uls">
                        <ul>
                            {{-- @foreach(@$topics as $key=>$topic)
                            <li>
                                <a href="{{ route('search.page') }}?keywords={{ @$topic->topic_line_1 }}">
                                <em>
                                    @if($key==0)
                                    <img src="{{url('public/frontend/images/to1.png')}}">
                                    @elseif($key==1)
                                    <img src="{{url('public/frontend/images/to3.png')}}">
                                    @elseif($key==2)
                                    <img src="{{url('public/frontend/images/to4.png')}}">
                                    @elseif($key==3)
                                    <img src="{{url('public/frontend/images/to2.png')}}">
                                    @elseif($key==4)
                                    <img src="{{url('public/frontend/images/to5.png')}}">
                                    @elseif($key==5)
                                    <img src="{{url('public/frontend/images/to6.png')}}">
                                    @elseif($key==6)
                                    <img src="{{url('public/frontend/images/to7.png')}}">
                                    @elseif($key==7)
                                    <img src="{{url('public/frontend/images/to8.png')}}">
                                    @endif
                                </em>
                                    {{ @$topic->topic_line_1 }}
                                </a>
                            </li>
                            @endforeach --}}
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Dating stories">
                                <em>
                                    <img src="{{url('public/frontend/images/to1.png')}}">
                                </em>
                                Dating stories
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Purpose of life">
                                <em>
                                    <img src="{{url('public/frontend/images/purpose_of_life.png')}}">
                                </em>
                                Purpose of life
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Movies that moved me">
                                <em>
                                    <img src="{{url('public/frontend/images/movies_move.png')}}">
                                </em>
                                Movies that moved me
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Ready for divorce?">
                                <em>
                                    <img src="{{url('public/frontend/images/divorce.png')}}">
                                </em>
                                Ready for divorce?
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=My relationship with my boss">
                                <em>
                                    <img src="{{url('public/frontend/images/my_boss.png')}}">
                                </em>
                                My relationship with my boss
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Habits - Alcohol">
                                <em>
                                    <img src="{{url('public/frontend/images/habit-alchohol.png')}}">
                                </em>
                                Habits - Alcohol
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Parenting">
                                <em>
                                    <img src="{{url('public/frontend/images/to4.png')}}">
                                </em>
                                Parenting
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('search.page') }}?keywords=Imposter Syndrome">
                                <em>
                                    <img src="{{url('public/frontend/images/to8.png')}}">
                                </em>
                                Imposter Syndrome
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('search.page') }}">
                                    <em>
                                        <img src="{{url('public/frontend/images/to10.png')}}">
                                    </em>
                                    Explore all
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="new_how_works">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="new_secs_headings">
                        <h1>How it works</h1>
                        <div class="shstne">
                            <img src="{{url('public/frontend/images/sha5.png')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="new_hw_infos">
                <div class="row">
                    <div class="col-sm-6 col-lg-3" >
                        <div class="new_hw_boxes">
                            <em>
                                <img src="{{url('public/frontend/images/h1.png')}}">
                            </em>
                            <p>Signup and provide <br> details about yourself </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="new_hw_boxes">
                            <em>
                                <img src="{{url('public/frontend/images/h2.png')}}">
                            </em>
                            <p>Find a topic <br> you enjoy  </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="new_hw_boxes">
                            <em>
                                <img src="{{url('public/frontend/images/h3.png')}}">
                            </em>
                            <p>Register for a room <br> or create your own  </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="new_hw_boxes">
                            <em>
                                <img src="{{url('public/frontend/images/h5.png')}}">
                            </em>
                            <p>Join the room and <br> engage with like-minded people  </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="new_vid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="video_setups">
                        <div class="video_players">
                            <div class="lessions_imgs">
                                <span>
                                    <img src="{{url('public/frontend/images/new-videos.png')}}">
                                    {{-- <div class="vpop new_aud" data-type="vimeo" data-id="172052320" data-autoplay='true'>
                                        <div class="playIcon">
                                        <div class="circle_box">
                                            <div class="circle"></div>
                                        </div>
                                        <img src="{{url('public/frontend/images/pls.png')}}" alt="" rel="" class="pays">
                                        </div>
                                    </div> --}}
                                </span>
                                <!-- copy this stuff and down -->
                                <div id="video-popup-overlay"></div>
                                <div id="video-popup-container">
                                    <div id="video-popup-close" class="fade">&#10006;</div>
                                    <div id="video-popup-iframe-container">
                                        <iframe id="video-popup-iframe" src="" width="100%" height="100%" frameborder="0"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="new_exploe_form">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_secs_headings">
                    <h1>Explore rooms</h1>
                    <div class="shst">
                        <img src="{{url('public/frontend/images/shp4.png')}}">
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-10 col-md-8 col-lg-7 col-xl-6 mx-auto">
                <div class="ex_rms_paras">
                    <p>There are three types of rooms you can join or create* </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="expl_boxs">
                    <div class="ex_tops">
                        <em>
                            <img src="{{url('public/frontend/images/ic1h.png')}}" class="hovern">
                            <img src="{{url('public/frontend/images/ic1.png')}}" class="hoverb">
                        </em>
                        <h3>Standard</h3>
                        <p>Standard rooms are open for all - rooms get filled on a first come, first serve basis. </p>
                    </div>
                    <div class="ex_btms">
                        <a href="{{route('see.all.topic',['convType'=>'OP'])}}">Explore Room</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="expl_boxs">
                    <div class="ex_tops">
                        <em>
                            <img src="{{url('public/frontend/images/ic2h.png')}}" class="hovern">
                            <img src="{{url('public/frontend/images/ic2.png')}}" class="hoverb">
                        </em>
                        <h3>Premium </h3>
                        <p>Premium rooms have premium entry - a host can choose who to allow into the room. </p>
                    </div>
                    <div class="ex_btms">
                    <a href="{{route('see.all.topic',['convType'=>'CO'])}}">Explore Room</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="expl_boxs">
                    <div class="ex_tops">
                        <em>
                            <img src="{{url('public/frontend/images/ic4h.png')}}" class="hovern">
                            <img src="{{url('public/frontend/images/ic4.png')}}" class="hoverb">
                        </em>
                        <h3>One-on-One </h3>
                        <p>One-on-one rooms enable intimate conversations with another guest.  </p>
                    </div>
                    <div class="ex_btms">
                    <a href="{{route('see.all.topic',['convType'=>'ON'])}}">Explore Room</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="new_testim">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_secs_headings">
                    <h1>User experiences</h1>
                </div>
            </div>
            <div class="col-12">
                <div class="testis_news_one">
                    <ul>
                        <li>
                            <div class="user_names_test">
                                <span>
                                    <img src="{{url('public/frontend/images/aruna.jpg')}}">
                                </span>
                                <div class="hover_tesrs">
                                    <div>
                                        <h3>Aruna</h3>
                                        <p>The topics on YLanes are so well curated, I would like to participate in at least 30 of these conversations</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="sech_ths">
                            <div class="user_names_test">
                                <span>
                                    <img src="{{url('public/frontend/images/dhristi.jpg')}}">
                                </span>
                                <div class="hover_tesrs">
                                    <div>
                                        <h3>Drishti</h3>
                                        <p>It is great to meet amazing people sharing my interests; I feel understood, accepted and love exchanging perspectives</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="three_ys">
                            <div class="user_names_test">
                                <span>
                                    <img src="{{url('public/frontend/images/piyush.jpg')}}">
                                </span>
                                <div class="hover_tesrs">
                                    <div>
                                        <h3>Piyush</h3>
                                        <p> Talking to strangers initially felt scary, but in the small groups of 4-5 people sharing stories on topics we care, I felt more liberated than in many known circles</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="sech_ths">
                            <div class="user_names_test">
                                <span>
                                    <img src="{{url('public/frontend/images/vasu.jpg')}}">
                                </span>
                                <div class="hover_tesrs">
                                    <div>
                                        <h3>Vasu</h3>
                                        <p>I love this unique and well-crafted platform – Video calls, the safety features, “rules of engagement”, “your take on a topic”, the concept of “hearts”, “resources” etc.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="sech_ths">
                            <div class="user_names_test">
                                <span>
                                    <img src="{{url('public/frontend/images/tammana.jpg')}}">
                                </span>
                                <div class="hover_tesrs">
                                    <div>
                                        <h3>Tamanna</h3>
                                        <p> At YLanes, everyone is an equal participant; No advice, No counselling, No gyan, No spectators, only sharing stories... and I love it!</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="stdps">
                        <img src="{{url('public/frontend/images/sha5.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="new_plas">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="new_play_t">
                    <p>Also available on</p>
                    <ul>
                        {{-- <li><a href="#"> <img src="{{url('public/frontend/images/gp.png')}}"> </a></li>
                        <li><a href="#"> <img src="{{url('public/frontend/images/ap.png')}}"> </a></li> --}}
                        <li><a href="#"> <img src="{{url('public/frontend/images/ep.png')}}"> </a></li>
                    </ul>
                    <div class="foot_arw fixed ">
                        <a href="javascript:" id="return-to-top"><img src="{{url('public/frontend/images/upa.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </section>
@endsection

@push('js')
    <script>
        $(".vpop").on('click', function(e) {
            e.preventDefault();
            $("#video-popup-overlay,#video-popup-iframe-container,#video-popup-container,#video-popup-close").show();

            var srchref='',autoplay='',id=$(this).data('id');
            if($(this).data('type') == 'vimeo') var srchref="https://player.vimeo.com/video/";
            else if($(this).data('type') == 'youtube') var srchref="https://www.youtube.com/embed/";

            if($(this).data('autoplay') == true) autoplay = '?autoplay=1';

            $("#video-popup-iframe").attr('src', srchref+id+autoplay);

            $("#video-popup-iframe").on('load', function() {
                $("#video-popup-container").show();
            });
        });

        $("#video-popup-close, #video-popup-overlay").on('click', function(e) {
            $("#video-popup-iframe-container,#video-popup-container,#video-popup-close,#video-popup-overlay").hide();
            $("#video-popup-iframe").attr('src', '');
        });
</script>
@endpush
