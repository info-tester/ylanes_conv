@extends('layouts.app')
@section('title','Home')
@section('content')
<div class="bannersec">
    <img src="{{url('public/frontend/images/ban_jpg.jpg')}}" alt="">
    <div class="banner_bx">
        <div class="container">
            <div class="banner_tex">
                <!--<h5><b>Best</b> Hosts</h5>-->
                <strong>Group Conversation Like Never Before</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore lorem ipsum amet.</p>
                <a href="{{ route('landing.con.page') }}" class="brw_a">Browse Conversations <i class="fa fa-angle-right"></i></a>

            </div>
        </div>
    </div>
</div>

<section class="how_dose_work">
    <div class="container">
        <div class="pg_hed">
            <h1>How dose it works</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua enim</p>
        </div>
        <div class="how_dose_work_inr">
            <div class="row">
                <div class="col-sm-4">
                    <div class="how_dose_bx">
                        <span>1</span>
                        <em><img src="{{url('public/frontend/images/how_it1.png')}}" alt=""></em>
                        <h4><a href="#url">Join - FREE</a></h4>
                        <p>Lorem ipsum dolor amet consectetur<br> adipiscing elit. Cras maximus<br> varius purus sed.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="how_dose_bx">
                        <span>2</span>
                        <em><img src="{{url('public/frontend/images/how_it3.png')}}" alt=""></em>
                        <h4><a href="#url">Schedule Conversation</a></h4>
                        <p>Lorem ipsum dolor amet consectetur<br> adipiscing elit. Cras maximus<br> varius purus sed.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="how_dose_bx">
                        <span>3</span>
                        <em><img src="{{url('public/frontend/images/how_it2.png')}}" alt=""></em>
                        <h4><a href="#url">Discuss On Video</a></h4>
                        <p>Lorem ipsum dolor amet consectetur<br> adipiscing elit. Cras maximus<br> varius purus sed.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="why_you_choose">
    <div class="container">
        <div class="why_you_choose_inr">
            <div class="why_you_img">
                <img src="{{url('public/frontend/images/wht_you1.jpg')}}" alt="">
            </div>
            <div class="why_you_tx">
                <div class="why_you_tx_inr">
                    <h5>Why You Choose Us Among<br> Other Site?</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut laboare et dolore lorem ipsum dolor sit amet consectetur adipiscing elit sed dummy caption</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut laboare et dolore lorem ipsum dolor sit amet consectetur adipiscing elit sed dummy caption
                        <span class="moretext" style="display: none;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut laboare et dolore lorem ipsum dolor sit amet consectetur adipiscing elit sed dummy caption</span>
                    </p>
                    <span></span>
                    <a href="#url" class="pg_btn red_btn"> <span id="moreless-button">Learn more</span> <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="clients_say_sec">
    <div class="container">
        <div class="pg_hed">
            <h3>What Our Clients Say</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua enim</p>
        </div>
        <div class="clients_say_inr">
            <div class="clients_say_slid">
                <div class="owl-carousel">
                    <div class="item">
                        <div class="clients_say_bx">
                            <em class="coma_in"><img src="{{url('public/frontend/images/coma.png')}}" alt=""></em>
                            <p>Lorem ipsum dolor sit amet consectetur elit dolor dummy consectetur eiusmod do eiusmod tempor incididunt labore lorem ipsum eiusmod incididunt adipiscing sed do eiusmod do eiusmod ut labore Lorem dolor amet, consectetur elit, adipiscing</p>
                            <div class="media">
                                <span><img src="{{url('public/frontend/images/clients1.jpg')}}" alt=""></span>
                                <div class="media-body">
                                    <h4>Sumona Roy</h4>
                                    <b>Graphic Designer, (USA)</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="clients_say_bx">
                            <em class="coma_in"><img src="{{url('public/frontend/images/coma.png')}}" alt=""></em>
                            <p>Lorem ipsum dolor sit amet consectetur elit dolor dummy consectetur eiusmod do eiusmod tempor incididunt labore lorem ipsum eiusmod incididunt adipiscing sed do eiusmod do eiusmod ut labore Lorem dolor amet, consectetur elit, adipiscing</p>
                            <div class="media">
                                <span><img src="{{url('public/frontend/images/clients2.jpg')}}" alt=""></span>
                                <div class="media-body">
                                    <h4>Abhijeet Chatterjee</h4>
                                    <b>Graphic Designer, (India)</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(@$topics->isNotEmpty())
<section class="find_tutor_sec">
    <div class="container">
        <div class="pg_hed">
            <h4>Popular Topics</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua enim</p>
        </div>
        <div class="find_tutor_inr">
            <div class="row">
                @foreach(@$topics as $topic)
                <div class="col-md-2">
                    <div class="find_tutor_bx">
                        <a href="{{ route('search.page') }}?keywords={{ @$topic->topic_line_1 }}">{{ substr(@$topic->topic_line_1,0,20) }}@if(substr(@$topic->topic_line_1,20))...@endif<em><i class="fa fa-angle-right"></i></em></a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="find_tutor_foot">
                <p> Don’t see what you’re looking for?  <a href="{{ route('search.page') }}">See All <i class="fa fa-angle-right"></i></a>  </p>
            </div>
        </div>
    </div>
</section>
@endif


<section class="bcome_sec">
    <div class="container">
        <div class="bcome_inr">
            <div class="pg_hed">
                <h4>Create an account on Ylanes</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae pharetra erat<br> fusce quis ipsum dolor sit amet</p>
                <a href="#" class="get_str">
                    <em>Get started <img src="{{url('public/frontend/images/get_arw.png')}}" alt=""></em><br>
                    <span>Simply some dummy caption text here</span>
                </a>
            </div>
        </div>
    </div>
</section>
@endsection
