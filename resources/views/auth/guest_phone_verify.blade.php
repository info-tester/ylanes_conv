@extends('layouts.app')
@section('title')
Verify Phone
@endsection
@section('content')
<div class="singup_sec otp_pg">
	<div class="singup_left">
		<img src="{{url('public/frontend/images/sinup_img.jpg')}}" alt="">
		<span class="sig_ico"><img src="{{url('public/frontend/images/sig_ico.png')}}" alt=""></span>
	</div>
	<div class="singup_right">
		<div class="singup_right_inr">
			<h1>Let's get started</h1>
			<p>Thank you for signing up with Ylanes. We have sent a verification email to your email address. Please click on the verification link sent to your email address</p>
			<div class="verification_ph">
				<h4>Verify Your Mobile Number</h4>
				<p class="vd_hidden5">Enter the 6 digit code sent to you at +{{$user->userCountryDetails->phonecode}}{{ $user->phone }} <a href="javascript:;" id="editphone" style="white-space: nowrap;">Change phone number</a>
                    {{-- OTP is {{ $user->phone_vcode }} --}}
                </p>
			</div>
			@include('layouts.message')
			<form action="{{ route('guest.phone.verify.post',$user->id) }}" method="post" id="pageLoginForm">
				@csrf
				<div class="singup_form">
					<div class="row">
						<div class="col-sm-12">
							<div class="input_bx">
								<input type="password" name="phone_otp" placeholder="Please Enter OTP">
								<span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
								<label id="phone_otp-error" class="error" for="phone_otp" style="display: none;"></label>
							</div>
						</div>
					</div>

					<div class="sing_btn">
						<button type="submit" class="pg_btn">Verify OTP</button>
					</div>
					<div class="already_have" id="otpresend">
						<span>Didn’t receive OTP yet? <a  href="javascript:void(0);" class="a_popup reSendOtpGuest" >Resend OTP</a> </span>
					</div>
					<div class="already_have" id="otpcountdown" style="display: none;"></div>
				</div>
			</form>
		</div>
	</div>
    <div class="clearfix"></div>
</div>

<div class="modal popup_list sign_popup" id="changePhone">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/frontend/images/popcross.png')}}"></button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <div class="singup_right_inr">
                        <h1>Chnage Phone Number</h1>
                        {{-- <p>Please fill in the below fields to continue with us</p> --}}
                        <form id="changePhoneForm" action="{{ route('user.change.mobile') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{@$user->id}}" name="user_id">
                            <div class="singup_form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input_bx">
                                            <select style="width: 23%" required="" name="country_code" id="country_code">
                                                <option value="">Country Code</option>
                                                @foreach(CustomHelper::getCountrycode() as $country_code)
                                                <option value="{{ @$country_code->id }}" @if($country_code->id==$user->userCountryDetails->id) selected @endif>{{ @$country_code->name }} (+{{ @$country_code->phonecode }})</option>
                                                @endforeach
                                            </select>
                                            <input type="text" placeholder="Mobile number" name="phone" maxlength="10" id="registerPnone1" style="width: 75%" value="{{ $user->phone }}">
                                            <span class="input_icon"><img src="{{url('public/frontend/images/input_icon3.png')}}" alt=""></span>
                                            <label id="country_code-error" class="error" for="country_code" style="display: none;"></label>
                                            <label id="chck_phone_rd" class=" error chck_phone_rd" style="display: none;"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="sing_btn">
                                    <button type="submit" class="pg_btn">Change Phone</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function (){
		$("#pageLoginForm").validate({
			rules:{
				'phone_otp':{
					required:true,
				}
			},
			messages:
			{
				phone_otp: {
					required: 'Please provide valid OTP!',
				}
			},
			submitHandler: function (form) {
				var otp = $('input[name="phone_otp"]').val();
				var PhoneVcode = $('input#phoneVcodeUser').val();
				if(otp != PhoneVcode)
				{
					$('#phone_otp-error').text('You entered incorrect OTP. Please enter correct OTP. ');
					$('#phone_otp-error').show();
					return false;
				}
				else{
					form.submit();
				}
			}
		});
		$('a.reSendOtpGuest').click(function(){
			var tmp_phone = "{{ $user->phone }}";
			$.ajax({

				url: "{{ route('user.resend.otp') }}",

				type:"POST",

				data: {

					'_token': "{{ csrf_token() }}",

					'phone': tmp_phone,

				},

				success: function(responce){

					console.log(responce);

					if(responce)

					{
						$('.vd_hidden5').html('Enter the 6 digit code sent to you at '+responce.phone);
						$('input#phoneVcodeUser').val(responce.phone_vcode);
						$('input#phoneVcodeUserId').val(responce.id);

						$('.vd_hidden5').show();
						var timeleft = 90;
						var downloadTimer = setInterval(function(){
							if(timeleft <= 0){
								clearInterval(downloadTimer);
								$('#otpresend').show();
								$('#otpcountdown').hide();
							} else {
								$('#otpresend').hide();
								$('#otpcountdown').html('<span>Didn’t receive OTP yet? <a  href="javascript:void(0);" class="a_popup">Send OTP again in '+ timeleft +' Sec</a> </span>');
								$('#otpcountdown').show();
							}
							timeleft -= 1;
						}, 1000);
					}

					else{

					}

				},

				error: function(xhr){

					console.log(xhr);

				}

			});
		});
	})
</script>
<script>
    $('#editphone').click(function(){
        $('#changePhone').modal('show');
    })
    $('#registerPnone1').blur(function(event) {
        if($('#registerPnone1').val() != ''){
            $("#chck_phone_rd").html('');
            var $this = $(this);
            var phone = $(this).val();
            var reqData = {
                '_token': '{{ @csrf_token() }}',
                'params': {
                    'email': phone,
                    'user_id':'{{$user->id}}'
                }
            };
            if(phone != '') {
                $.ajax({
                    url: '{{ route('user.check.email') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: reqData,
                })
                .done(function(response) {
                    if(!!response.error) {
                        $("#chck_phone_rd").html(response.error.merchant);
                        $("#chck_phone_rd").show();
                        $('#registerPnone1').val('');
                        $($(this).next().html(''));
                    } else {
                        $("#chck_phone_rd").html('');
                        $("#chck_phone_rd").hide();
                    }
                })
                .fail(function(error) {
                    console.log(error);
                });
            }

        } else {
            $("#chck_phone_rd").html('');
            $("#chck_phone_rd").hide();
        }
    });
    $("#changePhoneForm").validate({
        rules : {
            country_code : {
                required : true,
                digits:true
            },
            phone : {
                required : true,
                digits : true,
                minlength:10,
                maxlength:10
            },
        },
        messages : {

            phone:{
                required: "Please enter your phone no",
                digits: "Please enter a valid phone no",
            },
            country_code:{
                required: "Please select your country code",
                digits: "Please enter a valid country code",
            },
        }
    });
</script>
@endpush
