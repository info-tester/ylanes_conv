@extends('layouts.app')
@section('title')
Signup
@endsection
@section('content')
<div class="singup_sec login_sec_pg">
    <div class="singup_left">
        <img src="{{url('public/frontend/images/sinup_img3.jpg')}}" alt="">
        <span class="sig_ico"><img src="{{url('public/frontend/images/sig_ico.png')}}" alt=""></span>
    </div>
    <div class="singup_right">
        <div class="singup_right_inr">
            <h1>Verify Details</h1>
            <p>Please enter your email and mobile</p>
            @include('layouts.message')
            <form action="{{ route('facebook.email.login', $id) }}" method="post" id="myForm">
                @csrf
                <div class="singup_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input_bx">
                                <input type="text" placeholder="Display name" name="profile_name" required="" maxlength="15" value="{{ @$users->profile_name }}">
                                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon1.png')}}" alt=""></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_bx">
                                <input type="email" placeholder="Email address" name="email" required="" autocomplete="new-email" value="{{ @$users->email }}" id="registerEmail2">
                                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon2.png')}}" alt=""></span>
                                <label id="chck_email_rd2" class=" error chck_email_rd2" style="display: none;"></label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_bx">
                                <select style="width: 23%" required="" name="country_code" id="country_code">
                                    <option value="">Country Code</option>
                                    @foreach(CustomHelper::getCountrycode() as $country_code)
                                    <option value="{{ @$country_code->id }}" @if(@$users->country == @$country_code->id) selected="" @endif>{{ @$country_code->name }} (+{{ @$country_code->phonecode }})</option>
                                    @endforeach
                                </select>
                                <input type="text" placeholder="Mobile Number" name="phone" maxlength="10" onkeypress="validate();" value="{{ @$users->phone }}" id="registerPnone2" style="width: 75%" required="">
                                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon3.png')}}" alt=""></span>
                                <label id="country_code-error" class="error" for="country_code" style="display: none;"></label>
                                <label id="chck_phone_rd2" class=" error chck_phone_rd" style="display: none;"></label>
                            </div>
                        </div>
                    </div>
                    <div class="sing_btn">
                        <button class="pg_btn">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection
@push('js')
<script type="text/javascript">
    $(document).ready(function() {
        jQuery.validator.addMethod("emailonly", function(value, element) {
            return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value.toLowerCase()) || /.+@(gmail|yahoo|hotmail)\.com$/.test(value.toLowerCase());
        }, "Please provide valid email-id!");
        $('#myForm').validate({
            rules: {
                email : {
                    required: true,
                    email:true,
                    emailonly:true
                },
                country_code : {
                    required: true,
                    digits:true
                },
                phone : {
                    required: true,
                    digits:true,
                    maxlength:10
                }
            },
            messages:{
                country_code:{
                    required: "Please select your country code",
                    digits: "Please enter a valid country code",
                }
            }
        });
        $('#registerEmail2').blur(function(event) {
            if($('#registerEmail2').val() != ''){
                $("#chck_email_rd2").html('');
                var $this = $(this);
                var email = $(this).val();
                var reqData = {
                    '_token': '{{ @csrf_token() }}',
                    'params': {
                        'email': email,
                        'user_id': "{{ @$users->id }}"
                    }
                };
                if(email != '') {
                    $.ajax({
                        url: '{{ route('user.check.email') }}',
                        type: 'POST',
                        dataType: 'json',
                        data: reqData,
                    })
                    .done(function(response) {
                        if(!!response.error) {
                            $("#chck_email_rd2").html(response.error.merchant);
                            $("#chck_email_rd2").show();
                            $('#registerEmail2').val('');
                            $($(this).next().html(''));
                        } else {
                                // $("#chck_eml_grn").html(response.result.message);
                                $("#chck_email_rd2").html('');
                                $("#chck_email_rd2").hide();
                            }
                        })
                    .fail(function(error) {
                        console.log(error);
                    });
                }

            } else {
                $("#chck_email_rd2").html('');
                $("#chck_email_rd2").hide();
            }
        });
        $('#registerPnone2').blur(function(event) {
            if($('#registerPnone2').val() != ''){
                $("#chck_phone_rd2").html('');
                var $this = $(this);
                var phone = $(this).val();
                var reqData = {
                    '_token': '{{ @csrf_token() }}',
                    'params': {
                        'email': phone,
                        'user_id': "{{ @$users->id }}"
                    }
                };
                if(phone != '') {
                    $.ajax({
                        url: '{{ route('user.check.email') }}',
                        type: 'POST',
                        dataType: 'json',
                        data: reqData,
                    })
                    .done(function(response) {
                        if(!!response.error) {
                            $("#chck_phone_rd2").html(response.error.merchant);
                            $("#chck_phone_rd2").show();
                            $('#registerPnone2').val('');
                            $($(this).next().html(''));
                        } else {
                            $("#chck_phone_rd2").html('');
                            $("#chck_phone_rd2").hide();
                        }
                    })
                    .fail(function(error) {
                        console.log(error);
                    });
                }

            } else {
                $("#chck_phone_rd2").html('');
                $("#chck_phone_rd2").hide();
            }
        });
    });
</script>
@endpush
