@extends('layouts.app')
@section('title')
Forgot Password
@endsection
@push('css')
<style type="text/css">
    .login_sec_pg .singup_left>img {
        height: 400px;
    }
</style>
@endpush
@section('content')
<div class="singup_sec login_sec_pg">
    <div class="singup_left">
        <img src="{{url('public/frontend/images/sinup_img3.jpg')}}" alt="">
        <span class="sig_ico"><img src="{{url('public/frontend/images/sig_ico.png')}}" alt=""></span>
    </div>
    <div class="singup_right">
        <div class="singup_right_inr">
            <h1>Forgot Password</h1>
            <p>Please enter your register email to continue</p>
            @include('layouts.message')
            <form action="{{ route('user.link.forget.password.set') }}" method="post" id="myForm">
                @csrf
                <div class="singup_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input_bx">
                                <input type="email" placeholder="Email address" name="email" value="{{ old('email') }}" autocomplete="new-email">
                                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon2.png')}}" alt=""></span>
                            </div>
                        </div>
                    </div>
                    <div class="sing_btn">
                        <button class="pg_btn">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection
@push('js')


<script type="text/javascript">
    $(document).ready(function() {
        jQuery.validator.addMethod("emailonly", function(value, element) {
            return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value.toLowerCase()) || /.+@(gmail|yahoo|hotmail)\.com$/.test(value.toLowerCase());
        }, "Please provide valid email-id!");
        $('#myForm').validate({
            rules:{
                'email':{
                    required:true,
                    email:true,
                    emailonly: true,
                }
            },
            messages:
            {
                email: {
                    required: 'Please provide valid email-id!',
                    email:"Please provide valid email-id!",
                    emailonly: "Please provide valid email-id!",
                }
            }

        });

    });
</script>
@endpush
