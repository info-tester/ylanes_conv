@extends('layouts.app')
@section('title')
Set New Password
@endsection
@push('css')
<style type="text/css">
  .login_sec_pg .singup_left>img {
    height: 520px;
  }
</style>
@endpush
@section('content')
<div class="singup_sec login_sec_pg">
  <div class="singup_left">
    <img src="{{url('public/frontend/images/sinup_img3.jpg')}}" alt="">
    <span class="sig_ico"><img src="{{url('public/frontend/images/sig_ico.png')}}" alt=""></span>
  </div>
  <div class="singup_right">
    <div class="singup_right_inr">
      <h1>Reset Password</h1>
      <p>Please enter your OTP and password to continue</p>
      @include('layouts.message')
      <form action="{{ route('user.update.forget.password.set',@$id) }}" method="post" id="myForm">
        @csrf
        <div class="singup_form">
          <div class="row">
            <div class="col-sm-12">
              <div class="input_bx">
                <input type="text" placeholder="OTP" name="otp" autocomplete="new-otp" required="" onkeypress="validate();">
                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon1.png')}}" alt=""></span>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="input_bx">
                <input type="password" placeholder="Password" name="new_password" id="new_password" autocomplete="new-password" required="">
                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="input_bx">
                <input type="password" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation" autocomplete="new-password-confirmation" required="">
                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
              </div>
            </div>
          </div>
          <div class="sing_btn">
            <button class="pg_btn">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
@endsection
@push('js')

<script type="text/javascript">
  $(document).ready(function() {
    $("#myForm").validate({
      rules: {
        otp:{
          digits:true,
        },
        new_password: {
          required: true,
          minlength: 8,
        },
        password_confirmation: {
          required: true,
          minlength: 8,
          equalTo: "#new_password",
        },
      },
    });
  });
</script>

@endpush
