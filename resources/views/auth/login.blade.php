@extends('layouts.app')
@section('title')
Login
@endsection
@push('css')
<style type="text/css">
    .otp_pg .singup_left>img {
        height: 500px;
    }
</style>
@endpush
@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="singup_sec otp_pg">
    <div class="singup_left">
        <img src="{{url('public/frontend/images/sinup_img3.jpg')}}" alt="">
        <span class="sig_ico"><img src="{{url('public/frontend/images/sig_ico.png')}}" alt=""></span>
    </div>
    <div class="singup_right">
        <div class="singup_right_inr">
            <h1>Let's get started</h1>
            <p>Thank you for signing up with Ylanes. We have sent a verification email to your email address. Please click on the verification link sent to your email address</p>
            @include('layouts.message')
            {{-- <form action="{{ route('login') }}" method="post" id="pageLoginForm"> --}}
                @csrf
                <div class="singup_form">
                    {{-- <div class="row">
                        <div class="col-sm-12">
                            <div class="input_bx">
                                <input type="email" placeholder="Email address" name="email" value="{{ old('email') }}" autocomplete="new-email">
                                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon2.png')}}" alt=""></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_bx">
                                <input type="password" placeholder="Password" name="password" autocomplete="new-password">
                                <span class="input_icon"><img src="{{url('public/frontend/images/input_icon4.png')}}" alt=""></span>
                            </div>
                        </div>
                    </div>
                    <div class="tram_check_bx">
                        <div class="category-ul">
                            <input type="checkbox" checked=""  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">Remember Me</label>
                            <a href="{{route('user.forget.password.set')}}" class="for_pas">Forgot Password?</a>
                        </div>
                    </div> --}}
                    <div class="sing_btn">
                        <button class="pg_btn opensignin">Login</button>
                    </div>
                    <div class="sing_btn sing_btn_otp">
                        <a href="javascript:void(0);" class="a_popup openmobile pg_btn" id="clocv">Login with Mobile</a>
                    </div>
                    {{-- <div class="or_div">
                        <strong><span>Or Login with</span></strong>
                    </div> --}}
                    {{-- <div class="sing_up_sos">
                        <ul>
                            <li><a href="{{ url('login/facebook') }}" class="sos_btn sos_fac"><img src="{{url('public/frontend/images/sos_icon1.png')}}" alt="">Facebook</a></li>
                            <li><a href="{{ url('login/google') }}" class="sos_btn sos_goo"><img src="{{url('public/frontend/images/sos_icon2.png')}}" alt="">Google</a></li>
                            <li><a href="{{ url('login/linkedin') }}" class="sos_btn sos_lin"><img src="{{url('public/frontend/images/sos_icon3.png')}}" alt="">Linkedin</a></li>
                        </ul>
                    </div> --}}
                    {{-- <div class="already_have">
                        <span>Already have an account? <a href="javascript:void(0);" class="opensignup">Sign Up</a>  </span>
                        <a href="{{ route('host.sign.up.step.1') }}" class="singup_link">Sign Up as a Host</a>
                    </div> --}}
                </div>
            {{-- </form> --}}
        </div>
    </div>
    <div class="clearfix"></div>
</div>

@endsection
@push('js')
<script type="text/javascript">
    $(document).ready(function (){
        $("#pageLoginForm").validate({
            rules:{
                'email':{
                    required:true,
                    email:true,
                    emailonly: true,
                }
            },
            messages:
            {
                email: {
                    required: 'Please provide valid email-id!',
                    email:"Please provide valid email-id!",
                    emailonly: "Please provide valid email-id!",
                }
            }
        });
    })
</script>
@endpush
