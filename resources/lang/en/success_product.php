<?php
//jayatri
//english language
return [
	'-800' => [
		'code' 		=> '-800',
		'message'	=> 'Error: ',
		'meaning'	=> 'The combination already exists.'
	],
	'-801' => [
		'code' 		=> '-801',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product status successfully changed.'
	],
	'-802' => [
		'code' 		=> '-802',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product deleted successfully.'
	],
	'-1000' => [
		'code' 		=> '-1000',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product updated successfully!'
	],
	'-4001' => [
		'code' 		=> '-4001',
		'message'	=> 'Success: ',
		'meaning'	=> 'Images added for the item'
	],
	'-4002' => [
		'code' 		=> '-4002',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product discount price updated successfully!'
	],
	'-3001' => [
		'code' 		=> '-4001',
		'message'	=> 'Error: ',
		'meaning'	=> 'Must select an image!'
	]
];