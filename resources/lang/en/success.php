<?php
//jayatri
//english language
return [
	'-800' => [
		'code' 		=> '-800',
		'message'	=> 'Success!',
		'meaning'	=> 'Category deleted successfully!'
	],
	'-1000' => [
		'code' 		=> '-1000',
		'message'	=> 'Success!',
		'meaning'	=> 'Category added successfully!'
	],
	'-4001' => [
		'code' 		=> '-4001',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Subcategory added successfully!'
	],
	'-4000' => [
		'code' 		=> '-4000',
		'message'	=> 'Success!',
		'meaning'	=> 'Product category deleted successfully!'
	],
	'-4004' => [
		'code' 		=> '-4004',
		'message'	=> 'Success!',
		'meaning'	=> 'Product subcategory deleted successfully!'
	],
	'-4002' => [
		'code' 		=> '-4002',
		'message'	=> 'Success!',
		'meaning'	=> 'Category updated successfully!'
	],
	'-4003' => [
		'code' 		=> '-4003',
		'message'	=> 'Success!',
		'meaning'	=> 'Status changed successfully!'
	],
	'-4006' => [
		'code' 		=> '-4006',
		'message'	=> 'Success!',
		'meaning'	=> 'Manufacturer added successfully!'
	],
	'-4007' => [
		'code' 		=> '-4007',
		'message'	=> 'Success!',
		'meaning'	=> 'Manufacturer details updated successfully!'
	],
	'-4008' => [
		'code' 		=> '-4008',
		'message'	=> 'Success!',
		'meaning'	=> 'Manufacturer details deleted successfully!'
	],
	'-4009' => [
		'code' 		=> '-4009',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option created successfully!'
	],
	'-4011' => [
		'code' 		=> '-4011',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option updated successfully!'
	],
	'-4010' => [
		'code' 		=> '-4010',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option values created successfully!'
	],
	'-4012' => [
		'code' 		=> '-4012',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option details deleted successfully!'
	],
	'-4014' => [
		'code' 		=> '-4014',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option values updated successfully!'
	],
	'-4018' => [
		'code' 		=> '-4018',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option values deleted successfully!'
	],
	'-4015' => [
		'code' 		=> '-4015',
		'message'	=> 'Success!',
		'meaning'	=> 'Sub-Admin created successfully!Details has been sent to mail!'
	],
	'-4016' => [
		'code' 		=> '-4016',
		'message'	=> 'Success!',
		'meaning'	=> 'Product category updated successfully!'
	],
	'-4017' => [
		'code' 		=> '-4017',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of product category updated successfully!'
	],
	'-4019' => [
		'code' 		=> '-4019',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of subadmin updated successfully!'
	],
	'-4020' => [
		'code' 		=> '-4020',
		'message'	=> 'Success!',
		'meaning'	=> 'Details of sub-admin updated successfully!'
	],
	'-4021' => [
		'code' 		=> '-4021',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of product option updated successfully!'
	],
	'-4022' => [
		'code' 		=> '-4022',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant created successfully! Merchant details sent to the email!-ar'
	],
	'-4023' => [
		'code' 		=> '-4023',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant details updated successfully!'
	],
	'-4024' => [
		'code' 		=> '-4024',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant status has been changed successfully!'
	],
	'-4025' => [
		'code' 		=> '-4025',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant approved successfully!'
	],
	'-4026' => [
		'code' 		=> '-4026',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant deleted successfully!'
	],
	'-4027' => [
		'code' 		=> '-4027',
		'message'	=> 'Success!',
		'meaning'	=> 'Message has been sent successfully!'
	],
	'-4028' => [
		'code' 		=> '-4028',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer deleted successfully!'
	],
	'-4029' => [
		'code' 		=> '-4029',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer created successfully!'
	],
	'-4030' => [
		'code' 		=> '-4030',
		'message'	=> 'Success!',
		'meaning'	=> 'Student updated successfully!'
	],
	'-4031' => [
		'code' 		=> '-4031',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of customer updated successfully!'
	],
	'-4032' => [
		'code' 		=> '-4032',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver created successfully! Login details has been sent to registered email-id!'
	],
	'-4033' => [
		'code' 		=> '-4033',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver details updated successfully!'
	],
	'-4034' => [
		'code' 		=> '-4034',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver details deleted successfully!'
	],
	'-4035' => [
		'code' 		=> '-4035',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of driver updated successfully!'
	],
	'-4036' => [
		'code' 		=> '-4036',
		'message'	=> 'Success!',
		'meaning'	=> 'Message has been sent successfully!'
	],
	'-4037' => [
		'code' 		=> '-4037',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver details updated successfully! Verification mail of your email-id has been sent!'
	],
	'-4038' => [
		'code' 		=> '-4038',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost is added successfully!'
	],
	'-4039' => [
		'code' 		=> '-4039',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost is updated successfully!'
	],
	'-4040' => [
		'code' 		=> '-4040',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost is deleted successfully!'
	],
	'-4041' => [
		'code' 		=> '-4041',
		'message'	=> 'Success!',
		'meaning'	=> 'Settings for shipping cost is updated successfully!'
	],
	'-4042' => [
		'code' 		=> '-4042',
		'message'	=> 'Success!',
		'meaning'	=> 'External order is created successfully!'
	],
	'-4043' => [
		'code' 		=> '-4043',
		'message'	=> 'Success!',
		'meaning'	=> 'Order has been cancelled!'
	],
	'-4044' => [
		'code' 		=> '-4044',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of order has been changed!'
	],
	'-4045' => [
		'code' 		=> '-4045',
		'message'	=> 'Success!',
		'meaning'	=> 'Withdrawal request successfully submitted, You will be notified via email when admin accept or reject your request!'
	],
	'-4046' => [
		'code' 		=> '-4046',
		'message'	=> 'Success!',
		'meaning'	=> 'Withdrawal request successfully completed!'
	],
	'-4047' => [
		'code' 		=> '-4047',
		'message'	=> 'Success!',
		'meaning'	=> 'Withdrawal request successfully cancelled!'
	],
	'-4048' => [
		'code' 		=> '-4048',
		'message'	=> 'Success!',
		'meaning'	=> 'Loyalty point successfully updated!'
	],
	'-4049' => [
		'code' 		=> '-4049',
		'message'	=> 'Success!',
		'meaning'	=> 'City created successfully!'
	],
	'-4050' => [
		'code' 		=> '-4050',
		'message'	=> 'Success!',
		'meaning'	=> 'City updated successfully!'
	],
	'-4051' => [
		'code' 		=> '-4051',
		'message'	=> 'Success!',
		'meaning'	=> 'City deleted successfully!'
	],
	'-4052' => [
		'code' 		=> '-4052',
		'message'	=> 'Success!',
		'meaning'	=> 'External order details updated successfully!'
	],
	'-4053' => [
		'code' 		=> '-4053',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost updated successfully!'
	],
	'-4054' => [
		'code' 		=> '-4054',
		'message'	=> 'Success!',
		'meaning'	=> 'Commission updated successfully!'
	],
	'-4055' => [
		'code' 		=> '-4055',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver assigned successfully!'
	],
	'-4056' => [
		'code' 		=> '-4056',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant verified successfully!Now merchant can login!'
	],
	'-4057' => [
		'code' 		=> '-4057',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of international order is successfully changed!'
	],
	'-4058' => [
		'code' 		=> '-4058',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Driver is changed successfully!'
	],
	'-4059' => [
		'code' 		=> '-4059',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Your profile has been successfully updated!'
	],
	'-4060' => [
		'code' 		=> '-4060',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Settings of maintenence is changed successfully!'
	],
	'-4061' => [
		'code' 		=> '-4061',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Country added successfully!'
	],
	'-4062' => [
		'code' 		=> '-4061',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Country updated successfully!'
	],
	'-4063' => [
		'code' 		=> '-4061',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Country deleted successfully!'
	],
	'-4064' => [
		'code' 		=> '-4064',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Password changed successfully!'
	],
	'-4065' => [
		'code' 		=> '-4065',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Payment method changed successfully!'
	],
	'-4066' => [
		'code' 		=> '-4066',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Notes for order updated successfully!'
	],
	'-4067' => [
		'code' 		=> '-4067',
		'message'	=> 'Success!',
		'meaning'	=> 'Successfully removed from cart!'
	],
	'-4068' => [
		'code' 		=> '-4068',
		'message'	=> 'Success!',
		'meaning'	=> 'Faq successfully updated!'
	],
	'-4069' => [
		'code' 		=> '-4069',
		'message'	=> 'Success!',
		'meaning'	=> 'Faq deleted successfully!'
	],
	'-4070' => [
		'code' 		=> '-4070',
		'message'	=> 'Success!',
		'meaning'	=> 'Faq added successfully!'
	],
	'-4071' => [
		'code' 		=> '-4071',
		'message'	=> 'Success!',
		'meaning'	=> 'Content updated successfully!'
	],
	'-4072' => [
		'code' 		=> '-4072',
		'message'	=> 'Success!',
		'meaning'	=> 'Success! Driver logged out successfully!'
	],
	'-4073' => [
		'code' 		=> '-4073',
		'message'	=> 'Success!',
		'meaning'	=> 'Password reset email has been sent..!'
	],
	'-4075' => [
		'code' 		=> '-4075',
		'message'	=> 'Success!',
		'meaning'	=> 'OTP is sent to your email for reset password!'
	],
	'-4074' => [
		'code' 		=> '-4074',
		'message'	=> 'Success!',
		'meaning'	=> 'Password is changed! Now you can login!'
	],
	'-4076' => [
		'code' 		=> '-4076',
		'message'	=> 'Success!',
		'meaning'	=> 'Your message has been sent successfully! We will contact you soon!'
	],
	'-4077' => [
		'code' 		=> '-4077',
		'message'	=> 'Success!',
		'meaning'	=> 'Address removed successfully!'
	],
	'-4078' => [
		'code' 		=> '-4078',
		'message'	=> 'Success!',
		'meaning'	=> 'Address added successfully in your address book!'
	],
	'-4079' => [
		'code' 		=> '-4079',
		'message'	=> 'Success!',
		'meaning'	=> 'Add to favourite successfully!'
	],
	'-4080' => [
		'code' 		=> '-4080',
		'message'	=> 'Success!',
		'meaning'	=> 'Remove from favourite successfully!'
	],
	'-4081' => [
		'code' 		=> '-4081',
		'message'	=> 'Success!',
		'meaning'	=> 'Push notification sent successfully!'
	],
	'-4082' => [
		'code' 		=> '-4082',
		'message'	=> 'Success!',
		'meaning'	=> 'Notification will sent!'
	],
	'-4083' => [
		'code' 		=> '-4083',
		'message'	=> 'Success!',
		'meaning'	=> 'Notification sent successfully!'
	],
	'-4084' => [
		'code' 		=> '-4084',
		'message'	=> 'Success!',
		'meaning'	=> 'Review posted successfully!'
	],
	'-4085' => [
		'code' 		=> '-4085',
		'message'	=> 'Success!',
		'meaning'	=> 'Review deleted successfully!'
	],
	'-4086' => [
		'code' 		=> '-4086',
		'message'	=> 'Success!',
		'meaning'	=> 'FAQ category saved successfully!'
	],
	'-4087' => [
		'code' 		=> '-4087',
		'message'	=> 'Success!',
		'meaning'	=> 'FAQ category deleted successfully!'
	],
	'-4088' => [
		'code' 		=> '-4088',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer verification successful.'
	],
	'-4089' => [
		'code' 		=> '-4089',
		'message'	=> 'Success!',
		'meaning'	=> 'App settings successfully saved.'
	],
	'-4090' => [
		'code' 		=> '-4090',
		'message'	=> 'Success!',
		'meaning'	=> 'You have successfully verified you account!'
	],
	'-4091' => [
		'code' 		=> '-4091',
		'message'	=> 'Success!',
		'meaning'	=> 'Phone no/email-id already exist!'
	],
	'-4092' => [
		'code' 		=> '-4092',
		'message'	=> 'Success!',
		'meaning'	=> 'Verification successful!You have successfully updated your email address.Now you can access your account using updated email address!Please check!'
	],
	'-4093' => [
		'code' 		=> '-4093',
		'message'	=> 'Success!',
		'meaning'	=> 'A verification link is sent to your email! Please verify!'
	],
	'-4094' => [
		'code' 		=> '-4094',
		'message'	=> 'Success!',
		'meaning'	=> 'You have successfully verified your email address.'
	],
	'-4095' => [
		'code' 		=> '-4095',
		'message'	=> 'Success!',
		'meaning'	=> 'Link Expired! Please Login!'
	],
	'-4096' => [
		'code' 		=> '-4096',
		'message'	=> 'Success!',
		'meaning'	=> 'You have received a OTP in your mobile! Please verify!'
	],
	'-4097' => [
		'code' 		=> '-4097',
		'message'	=> 'Success!',
		'meaning'	=> 'A Verification Link sent successfully to your email! Please verify!'
	],
	'-4098' => [
		'code' 		=> '-4098',
		'message'	=> 'Success!',
		'meaning'	=> 'Verification successfully! You have successfully update your mobile number!'
	],
	'-4099' => [
		'code' 		=> '-4099',
		'message'	=> 'Error!',
		'meaning'	=> 'Wrong OTP!'
	],
	'-4100' => [
		'code' 		=> '-4100',
		'message'	=> 'Success!',
		'meaning'	=> 'Address updated successfully in your address book!'
	],

	'-4101' => [
		'code' 		=> '-4101',
		'message'	=> 'Success!',
		'meaning'	=> 'A verification link is sent to your email!'
	],

	'-4102' => [
		'code' 		=> '-4102',
		'message'	=> 'Success!',
		'meaning'	=> 'Testimonial added successful!'
	],
	'-4103' => [
		'code' 		=> '-4103',
		'message'	=> 'Success!',
		'meaning'	=> 'Testimonial update successful!'
	],
	'-4104' => [
		'code' 		=> '-4104',
		'message'	=> 'Success!',
		'meaning'	=> 'Testimonial status change successful!'
	],
	'-4105' => [
		'code' 		=> '-4105',
		'message'	=> 'Success!',
		'meaning'	=> 'Testimonial delete successful!'
	],
	'-4106' => [
		'code' 		=> '-4106',
		'message'	=> 'Success!',
		'meaning'	=> 'Bank details added successfully!'
	],
	'-4107' => [
		'code' 		=> '-4107',
		'message'	=> 'Success!',
		'meaning'	=> 'Bank details removed successfully!'
	],
	'-4108' => [
		'code' 		=> '-4108',
		'message'	=> 'Success!',
		'meaning'	=> 'Bank details updated successfully!'
	],
	'-4109' => [
		'code' 		=> '-4109',
		'message'	=> 'Success!',
		'meaning'	=> 'Reset mail has been send.Please check your email.'
	],
	'-4110' => [
		'code' 		=> '-4110',
		'message'	=> 'Success!',
		'meaning'	=> 'Password changed successfully! Please login.'
	],
	'-4111' => [
		'code' 		=> '-4111',
		'message'	=> 'Success!',
		'meaning'	=> 'Category status change successful!'
	],
	'-4112' => [
		'code' 		=> '-4112',
		'message'	=> 'Success!',
		'meaning'	=> 'Phone no already verified. Please login!'
	],
	'-4113' => [
		'code' 		=> '-4113',
		'message'	=> 'Success!',
		'meaning'	=> 'Phone no Verification Successful. Please login!'
	],
	'-4114' => [
		'code' 		=> '-4114',
		'message'	=> 'Success!',
		'meaning'	=> 'User updated successfully!'
	],
	'-4115' => [
		'code' 		=> '-4115',
		'message'	=> 'Success!',
		'meaning'	=> 'Course added successfully!'
	],
	'-4116' => [
		'code' 		=> '-4116',
		'message'	=> 'Success!',
		'meaning'	=> 'Verification Successful. Email Updated.'
	],
	'-4117' => [
		'code' 		=> '-4117',
		'message'	=> 'Success!',
		'meaning'	=> 'Verification Successful. Mobile Updated.'
	],
	'-4118' => [
		'code' 		=> '-4118',
		'message'	=> 'Success!',
		'meaning'	=> 'Bulk pricing added successfully.'
	],
	'-4119' => [
		'code' 		=> '-4119',
		'message'	=> 'Success!',
		'meaning'	=> 'Bulk pricing delete successfully.'
	],
	'-4120' => [
		'code' 		=> '-4120',
		'message'	=> 'Success!',
		'meaning'	=> 'Date exclusions added successfully.'
	],
	'-4121' => [
		'code' 		=> '-4121',
		'message'	=> 'Success!',
		'meaning'	=> 'Date exclusions updated successfully.'
	],
	'-4122' => [
		'code' 		=> '-4122',
		'message'	=> 'Success!',
		'meaning'	=> 'Date exclusions delete successfully.'
	],
	'-4123' => [
		'code' 		=> '-4123',
		'message'	=> 'Success!',
		'meaning'	=> 'Connected  with facebook successfully'
	],
	'-4124' => [
		'code' 		=> '-4124',
		'message'	=> 'Success!',
		'meaning'	=> 'Connected  with linkedin successfully'
	],
	'-4125' => [
		'code' 		=> '-4125',
		'message'	=> 'Success!',
		'meaning'	=> 'Course update successfully!'
	],
	'-4126' => [
		'code' 		=> '-4126',
		'message'	=> 'Success!',
		'meaning'	=> 'Course Schedule added successfully!'
	],
	'-4127' => [
		'code' 		=> '-4127',
		'message'	=> 'Success!',
		'meaning'	=> 'Course Schedule updated successfully!'
	],
	'-4128' => [
		'code' 		=> '-4128',
		'message'	=> 'Success!',
		'meaning'	=> 'Course Schedule deleted successfully!'
	],
	'-4129' => [
		'code' 		=> '-4129',
		'message'	=> 'Success!',
		'meaning'	=> 'Course approval is successfully done.'
	],
	'-4130' => [
		'code' 		=> '-4130',
		'message'	=> 'Success!',
		'meaning'	=> 'Course status change successfully.'
	],
	'-4131' => [
		'code' 		=> '-4131',
		'message'	=> 'Success!',
		'meaning'	=> 'Course deleted successfully.'
	],
	'-4132' => [
		'code' 		=> '-4132',
		'message'	=> 'Success!',
		'meaning'	=> 'Category show successfully done in the header.'
	],
	'-4133' => [
		'code' 		=> '-4133',
		'message'	=> 'Success!',
		'meaning'	=> 'Category hide successfully done from the header.'
	],
	'-4134' => [
		'code' 		=> '-4134',
		'message'	=> 'Success!',
		'meaning'	=> 'Package price update successfully.'
	],
	'-4135' => [
		'code' 		=> '-4135',
		'message'	=> 'Success!',
		'meaning'	=> 'Subscription package purchase successfully.'
	],
	'-4136' => [
		'code' 		=> '-4136',
		'message'	=> 'Success!',
		'meaning'	=> 'Tutor remove successfull your favorite list.'
	],
	'-4137' => [
		'code' 		=> '-4137',
		'message'	=> 'Success!',
		'meaning'	=> 'Topic added successfully.'
	],
	'-4138' => [
		'code' 		=> '-4138',
		'message'	=> 'Success!',
		'meaning'	=> 'Topic status change successfully.'
	],
	'-4139' => [
		'code' 		=> '-4139',
		'message'	=> 'Success!',
		'meaning'	=> 'Topic updated successfully.'
	],
	'-4140' => [
		'code' 		=> '-4140',
		'message'	=> 'Success!',
		'meaning'	=> 'Topic deleted successfully.'
	],
	'-4141' => [
		'code' 		=> '-4141',
		'message'	=> 'Success!',
		'meaning'	=> 'Conversation created successfully.'
	],
	'-4142' => [
		'code' 		=> '-4142',
		'message'	=> 'Success!',
		'meaning'	=> 'Conversation status change successful!'
	],
	'-4143' => [
		'code' 		=> '-4143',
		'message'	=> 'Success!',
		'meaning'	=> 'Conversation updated successfully.'
	],
	'-4145' => [
		'code' 		=> '-4145',
		'message'	=> 'Success!',
		'meaning'	=> 'Request accepted successfully.'
	],
	'-4146' => [
		'code' 		=> '-4146',
		'message'	=> 'Success!',
		'meaning'	=> 'Request delete successfully.'
	],
	'-4147' => [
		'code' 		=> '-4147',
		'message'	=> 'Success!',
		'meaning'	=> 'Request approved successfully.'
	],
	'-4148' => [
		'code' 		=> '-4148',
		'message'	=> 'Success!',
		'meaning'	=> 'This Conversation already fulled.'
	],
	'-4149' => [
		'code' 		=> '-4149',
		'message'	=> 'Success!',
		'meaning'	=> 'Request delete successfully.'
	],
	'-4150' => [
		'code' 		=> '-4150',
		'message'	=> 'Success!',
		'meaning'	=> 'Group created successfully.'
	],
	'-4151' => [
		'code' 		=> '-4151',
		'message'	=> 'Success!',
		'meaning'	=> 'The conversation was canceled successfully.'
	],
	'-4152' => [
		'code' 		=> '-4152',
		'message'	=> 'Success!',
		'meaning'	=> 'The schedule created successfully.'
	],
	'-4153' => [
		'code' 		=> '-4153',
		'message'	=> 'Success!',
		'meaning'	=> 'The schedule updated successfully.'
	],
	'-4154' => [
		'code' 		=> '-4154',
		'message'	=> 'Success!',
		'meaning'	=> 'The schedule deleted successfully.'
	],
	'-4155' => [
		'code' 		=> '-4155',
		'message'	=> 'Success!',
		'meaning'	=> 'You already added a review for this user.'
	],
	'-4156' => [
		'code' 		=> '-4156',
		'message'	=> 'Success!',
		'meaning'	=> 'Your review was added successfully.'
	],
	'-4157' => [
		'code' 		=> '-4157',
		'message'	=> 'Success!',
		'meaning'	=> 'You already added a rating for this conversation.'
	],
	'-4158' => [
		'code' 		=> '-4158',
		'message'	=> 'Success!',
		'meaning'	=> 'Your rating was added successfully.'
	],
	'-4159' => [
		'code' 		=> '-4159',
		'message'	=> 'Success!',
		'meaning'	=> 'OTP is sent to your mobile no! Please verify OTP!'
	],
	'-4160' => [
		'code' 		=> '-4160',
		'message'	=> 'Success!',
		'meaning'	=> 'Package purchase successfully.'
	],
	'-4161' => [
		'code' 		=> '-4161',
		'message'	=> 'Success!',
		'meaning'	=> 'Package added successfully.'
	],
	'-4162' => [
		'code' 		=> '-4162',
		'message'	=> 'Success!',
		'meaning'	=> 'Package updated successfully.'
	],
	'-4163' => [
		'code' 		=> '-4163',
		'message'	=> 'Success!',
		'meaning'	=> 'Package deleted successfully.'
	],
	'-4164' => [
		'code' 		=> '-4164',
		'message'	=> 'Success!',
		'meaning'	=> 'Ycoin successfully sent to the user.'
	],
	'-4165' => [
		'code' 		=> '-4165',
		'message'	=> 'Success!',
		'meaning'	=> 'Thank you for connecting us - we will get back to you soon.'
	],
];
