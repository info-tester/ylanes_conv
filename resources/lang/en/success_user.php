<?php
//surajit
//english language
return [
	'-700' => [
			'code' 		=> '-700',
			'message'	=> 'Success!',
			'meaning'	=> 'Profile successfully updated!'
		],
	'-701' => [
			'code' 		=> '-701',
			'message'	=> 'Error!',
			'meaning'	=> 'Password not changed, You have entered an incorrect old password!'
		],
	'-702' => [
			'code' 		=> '-702',
			'message'	=> 'Success!',
			'meaning'	=> 'Address Book successfully added!'
		],
	'-703' => [
			'code' 		=> '-703',
			'message'	=> 'Error!',
			'meaning'	=> 'Unauthorized access!'
		],
	'-704' => [
			'code' 		=> '-704',
			'message'	=> 'Success!',
			'meaning'	=> 'Address Book successfully updated!'
		],
	'-705' => [
			'code' 		=> '-705',
			'message'	=> 'Success!',
			'meaning'	=> 'Set default address successfully!'
		],
	'-707' => [
			'code' 		=> '-707',
			'message'	=> 'Success!',
			'meaning'	=> 'Mail successfully sent!'
		],
	'-708' => [
			'code' 		=> '-708',
			'message'	=> 'Error!',
			'meaning'	=> 'Please remove the not available product before place order!'
		],
	'-709' => [
			'code' 		=> '-709',
			'message'	=> 'Error!',
			'meaning'	=> 'You and merchant both are outside of Kuwait. Please remove the products of following merchants : '
		],
	'-710' => [
			'code' 		=> '-710',
			'message'	=> 'Error!',
			'meaning'	=> 'International order is not available for following merchants : '
		],
	'-711' => [
			'code' 		=> '-711',
			'message'	=> 'Error!',
			'meaning'	=> 'International order is not available. '
		],
	'-722' => [
			'code' 		=> '-722',
			'message'	=> 'Error!',
			'meaning'	=> 'City name does not exist.'
		],
	'-723' => [
		'code' 		=> '-723',
		'message'	=> 'Error!',
		'meaning'	=> 'International order is not available for following merchants : :mrs'
	],
	'-724' => [
		'code' 		=> '-724',
		'message'	=> 'Success!',
		'meaning'	=> 'Please update your profile. Fill all required field'
	],
	'-725' => [
		'code' 		=> '-725',
		'message'	=> 'Success!',
		'meaning'	=> 'Category added successfully.'
	],
	'-726' => [
		'code' 		=> '-726',
		'message'	=> 'Success!',
		'meaning'	=> 'Category updated successfully.'
	],
	'-727' => [
		'code' 		=> '-727',
		'message'	=> 'Success!',
		'meaning'	=> 'Category delete successfully.'
	],
	'-728' => [
		'code' 		=> '-728',
		'message'	=> 'Success!',
		'meaning'	=> 'Education added successfully.'
	],
	'-729' => [
		'code' 		=> '-729',
		'message'	=> 'Success!',
		'meaning'	=> 'Education updated successfully.'
	],
	'-730' => [
		'code' 		=> '-730',
		'message'	=> 'Success!',
		'meaning'	=> 'Education delete successfully.'
	],
	'-731' => [
		'code' 		=> '-731',
		'message'	=> 'Success!',
		'meaning'	=> 'Award added successfully.'
	],
	'-732' => [
		'code' 		=> '-732',
		'message'	=> 'Success!',
		'meaning'	=> 'Award updated successfully.'
	],
	'-733' => [
		'code' 		=> '-733',
		'message'	=> 'Success!',
		'meaning'	=> 'Award delete successfully.'
	],
	'-734' => [
		'code' 		=> '-734',
		'message'	=> 'Success!',
		'meaning'	=> 'Training added successfully.'
	],
	'-735' => [
		'code' 		=> '-735',
		'message'	=> 'Success!',
		'meaning'	=> 'Training updated successfully.'
	],
	'-736' => [
		'code' 		=> '-736',
		'message'	=> 'Success!',
		'meaning'	=> 'Training delete successfully.'
	],
	'-737' => [
		'code' 		=> '-737',
		'message'	=> 'Success!',
		'meaning'	=> 'User delete successfully.'
	],
	'-738' => [
		'code' 		=> '-738',
		'message'	=> 'Success!',
		'meaning'	=> 'Cart delete successfully.'
	],
	'-739' => [
		'code' 		=> '-739',
		'message'	=> 'Success!',
		'meaning'	=> "You can't book to your own course."
	],
];
