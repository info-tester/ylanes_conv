<?php
//surajit
//english language
return [
	'-700' => [
			'code' 		=> '-700',
			'message'	=> 'Success!',
			'meaning'	=> 'Profile successfully updated!'
		],
	'-701' => [
			'code' 		=> '-700',
			'message'	=> 'Error!',
			'meaning'	=> 'Password not changed, You have entered an incorrect old password!'
		]
];