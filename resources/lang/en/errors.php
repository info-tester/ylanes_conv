<?php
return [
	'-32700' => [
		'code' 		=> '-32700',
		'message'	=> 'Parse error',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.'
	],
	'-33034' => [
		'code' 		=> '-33034',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Your account is inactive by admin!'
	],
	//surojit error added in this file
	'-32700' => [
		'code' 		=> '-32700',
		'message'	=> 'Parse error',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.'
	],
	'-33000' => [
		'code' 		=> '-33000',
		'message'	=> 'Email is already exists.',
		'meaning'	=> 'This email address is already in use'
	],
	'-33001' => [
		'code' 		=> '-33001',
		'message'	=> 'Profile picture has been successfully updated.',
		'meaning'	=> 'Your profile picture has been successfully changed.'
	],
	'-33002' => [
		'code' 		=> '-33002',
		'message'	=> 'This email is already exists. Please verify your mail id.',
		'meaning'	=> 'You are using the mail is already exist in the database and it is unverified.'
	],
	'-33003' => [
		'code' 		=> '-33003',
		'message'	=> 'This email is already exists. Your account is temporarily inactive. Please write to '. env('SUPPORT_EMAIL'),
		'meaning'	=> 'Account is temporarily inactivate by admin. contact our support team.'
	],
	'-33004' => [
		'code' 		=> '-33004',
		'message'	=> 'This email is already exists. Register with another email .',
		'meaning'	=> 'This email is already using an another account.'
	],
	'-33005' => [
		'code' 		=> '-33005',
		'message'	=> 'This account has been deleted from our site. Please contact our support team.',
		'meaning'	=> 'Account deleted by user or admin.'
	],
	'-33006' => [
		'code' 		=> '-33006',
		'message'	=> 'This email is available.',
		'meaning'	=> 'Email is available.'
	],
	'-33007' => [
		'code' 		=> '-33007',
		'message'	=> 'Invalid email format.',
		'meaning'	=> 'The email entered is not valid.'
	],
	'-33008' => [
		'code' 		=> '-33008',
		'message'	=> 'Email is mandatory',
		'meaning'	=> 'Email is mandatory'
	],
	'-33009' => [
		'code' 		=> '-33009',
		'message'	=> 'Wrong login credentials',
		'meaning'	=> 'Email or Password do not match'
	],
	'-33010' => [
		'code' 		=> '-33010',
		'message'	=> 'Could not create token',
		'meaning'	=> 'Token Could not be created'
	],
	'-33017' => [
		'code' 		=> '-33017',
		'message'	=> 'Profile updated successfully',
		'meaning'	=> 'You have updated your profile'
	],
	'-33018' => [
		'code' 		=> '-33018',
		'message'	=> 'No information',
		'meaning'	=> 'The information is not present in our database'
	],
	'-33029' => [
		'code' 		=> '-33029',
		'message'	=> 'Invalid request',
		'meaning'	=> 'Invalid request'
	],
	'-33034' => [
		'code' 		=> '-33034',
		'message'	=> 'Thank you for registering',
		'meaning'	=> 'Thank you for registering.'
	],
	'-33039' => [
		'code' 		=> '-33039',
		'message'	=> 'Wrong OTP verification code',
		'meaning'	=> 'The OTP verification code does not match'
	],
	'-33040' => [
		'code' 		=> '-33040',
		'message'	=> 'Account verified, registration was successful.',
		'meaning'	=> 'Account verified, registration was successful.'
	],
	'-33041' => [
		'code' 		=> '-33041',
		'message'	=> 'No result',
		'meaning'	=> 'Not found.'
	],
	'-33042' => [
		'code' 		=> '-33042',
		'message'	=> 'Password changed correctly',
		'meaning'	=> 'Password changed correctly'
	],
	'-33049' => [
		'code' 		=> '-33049',
		'message'	=> 'Order inserted correctly',
		'meaning'	=> 'Order successfully registered'
	],
	'-33058' => [
		'code' 		=> '-33058',
		'message'	=> 'Profile picture updated.',
		'meaning'	=> 'Profile picture updated.'
	],
	'-33059' => [
		'code' 		=> '-33059',
		'message'	=> 'Profile updated',
		'meaning'	=> 'Profile updated'
	],
	'-33072' => [
		'code' 		=> '-33072',
		'message'	=> 'Password reset successful.',
		'meaning'	=> 'Password reset successful.'
	],
	'-33073' => [
		'code' 		=> '-33073',
		'message'	=> 'Verification successful.',
		'meaning'	=> 'Verification code OK'
	],
	'-33077' => [
		'code' 		=> '-33077',
		'message'	=> 'Your account is not verified',
		'meaning'	=> 'Your account is not verified'
	],
	'-33078' => [
		'code' 		=> '-33078',
		'message'	=> 'Your account has been removed by the administrator',
		'meaning'	=> 'Your account has been removed by the administrator'
	],
	'-33079' => [
		'code' 		=> '-33079',
		'message'	=> 'OTP verification code sent successfully',
		'meaning'	=> 'Check your mailbox'
	],
	'-33080' => [
		'code' 		=> '-33080',
		'message'	=> 'Product added',
		'meaning'	=> 'Product successfully added'
	],
	'-33081' => [
		'code' 		=> '-33081',
		'message'	=> 'No category',
		'meaning'	=> 'No categories found'
    ],
    '-33082' => [
		'code' 		=> '-33082',
		'message'	=> 'Updated product',
		'meaning'	=> 'Updated product'
	],
    '-33083' => [
		'code' 		=> '-33083',
		'message'	=> 'Opening updated',
		'meaning'	=> 'Opening updated successfully'
	],
    '-33084' => [
		'code' 		=> '-33084',
		'message'	=> 'Product status',
		'meaning'	=> 'Product status updated'
	],
    '-33085' => [
		'code' 		=> '-33085',
		'message'	=> 'Token Expired',
		'meaning'	=> 'User session has expired'
	],
	'-33086' => [
		'code' 		=> '-33086',
		'message'	=> 'Login error!',
		'meaning'	=> 'Account temporarily inactive'
	],




	'-5003' => [
		'code' 		=> '-5003',
		'message'	=> 'Error!',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.'
	],
	'-6000' => [
		'code' 		=> '-6000',
		'message'	=> 'Error!',
		'meaning'	=> 'Product has subcategories! Delete of this product category couldnot possible! It is associated with subcategories!'
	],
	'-5001' => [
		'code' 		=> '-5001',
		'message'	=> 'Error!',
		'meaning'	=> 'Something went wrong!'
	],
	'-5002' => [
		'code' 		=> '-5002',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!'
	],
	'-5010' => [
		'code' 		=> '-5010',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Status not changed!'
	],
	'-5004' => [
		'code' 		=> '-5004',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Category details not deleted! It is associated with subcategory So you cannot delete this!'
	],
	'-5005' => [
		'code' 		=> '-5005',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Category details not deleted!It is associated with subcategory !So you cannot delete this'
	],
	'-5006' => [
		'code' 		=> '-5006',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Category details not deleted!Category is associated with Product or  product option or option values or customer order!So you cannot delete this!'
	],
	'-5007' => [
		'code' 		=> '-5007',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Manufacturer details not added! -en'
	],
	'-5008' => [
		'code' 		=> '-5008',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Manufacturer details not deleted!Manufacturer is associated with product & customer internal order ! So you cannot delete this!'
	],
	'-5009' => [
		'code' 		=> '-5009',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Product option values not added!'
	],
	'-5010' => [
		'code' 		=> '-5010',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Product option Details not added!'
	],
	'-5011' => [
		'code' 		=> '-5011',
		'message'	=> 'Error!',
		'meaning'	=> 'This option is associated with values!First delete the associated values!'
	],
	'-5012' => [
		'code' 		=> '-5012',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Product option values not updated!'
	],
	'-5015' => [
		'code' 		=> '-5015',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized Access! Product option values not deleted!Product option values are associated with product or customer order !'
	],
	'-5016' => [
		'code' 		=> '-5016',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Sub-Admin not created!'
	],
	'-5017' => [
		'code' 		=> '-5017',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Product category not updated!'
	],
	'-5018' => [
		'code' 		=> '-5018',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Status of product category not updated!'
	],
	'-5019' => [
		'code' 		=> '-5019',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Status of subadmin not updated!'
	],
	'-5020' => [
		'code' 		=> '-5020',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Details of sub-admin not updated!'
	],
	'-5021' => [
		'code' 		=> '-5021',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Status of product option  not updated!'
	],
	'-5022' => [
		'code' 		=> '-5022',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Merchant not created!'
	],
	'-5023' => [
		'code' 		=> '-5023',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Merchant details not updated!'
	],
	'-5024' => [
		'code' 		=> '-5024',
		'message'	=> 'Error!',
		'meaning'	=> 'Merchant details not deleted!Merchant is associated with product or customer order !'
	],
	'-5025' => [
		'code' 		=> '-5025',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Message has not been sent!'
	],
	'-5026' => [
		'code' 		=> '-5026',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Customer status doesnot updated!'
	],
	'-5027' => [
		'code' 		=> '-5027',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Customer doesnot created!'
	],
	'-5028' => [
		'code' 		=> '-5028',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Customer doesnot updated!'
	],
	'-5029' => [
		'code' 		=> '-5029',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Driver doesnot created!'
	],
	'-5030' => [
		'code' 		=> '-5030',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Driver details not updated!'
	],
	'-5031' => [
		'code' 		=> '-5031',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Driver details not deleted!Driver is associated with order!'
	],
	'-5032' => [
		'code' 		=> '-5032',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Status of driver not updated!'
	],
	'-5033' => [
		'code' 		=> '-5033',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Message not sent!'
	],
	'-5034' => [
		'code' 		=> '-5034',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! This merchant is already added some products!This merchant cannot be deleted!'
	],
	'-5035' => [
		'code' 		=> '-5035',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Shipping cost is not added!'
	],
	'-5036' => [
		'code' 		=> '-5036',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Shipping cost is not updated!'
	],
	'-5037' => [
		'code' 		=> '-5037',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Shipping cost is not deleted!Shipping cost is associated with product or customer order !'
	],
	'-5038' => [
		'code' 		=> '-5038',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! External order is not created!'
	],
	'-5039' => [
		'code' 		=> '-5039',
		'message'	=> 'Unavailable!',
		'meaning'	=> 'Cannot add selected item to cart'
	],
	'-5040' => [
		'code' 		=> '-5040',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Order has not been cancelled!'
	],
	'-5041' => [
		'code' 		=> '-5041',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Status of order has not been changed!'
	],
	'-5042' => [
		'code' 		=> '-5042',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Withdrawl request is not successfull!'
	],
	'-5043' => [
		'code' 		=> '-5043',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Withdrawl request is not successfull!Request status is not changed!'
	],
	'-5044' => [
		'code' 		=> '-5044',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Withdrawl request is not cancelled!Request status is not changed!'
	],
	'-5045' => [
		'code' 		=> '-5045',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! City doesnot created!'
	],
	'-5046' => [
		'code' 		=> '-5046',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! City doesnot updated!'
	],
	'-5047' => [
		'code' 		=> '-5047',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! City doesnot deleted!It is associated with merchant or customer or external order or customer order !So you cannot delete this!'
	],
	'-5050' => [
		'code' 		=> '-5050',
		'message'	=> 'Error!',
		'meaning'	=> 'You and seller both are out side!Your country and seller country both are from outside kuwait!'
	],
	'-5051' => [
		'code' 		=> '-5051',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Shipping cost not updated!'
	],
	'-5052' => [
		'code' 		=> '-5052',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Commission not updated!'
	],
	'-5055' => [
		'code' 		=> '-5055',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Driver is not assigned!'
	],
	'-5053' => [
		'code' 		=> '-5053',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Merchant is not verified!'
	],
	'-5054' => [
		'code' 		=> '-5054',
		'message'	=> 'Error!',
		'meaning'	=> 'Status of international order is not changed!'
	],
	'-5055' => [
		'code' 		=> '-5055',
		'message'	=> 'Error!',
		'meaning'	=> 'Product status is not changed!'
	],
	'-5056' => [
		'code' 		=> '-5056',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Driver is not changed!'
	],
	'-5057' => [
		'code' 		=> '-5057',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Your profile is not updated!'
	],
	'-5058' => [
		'code' 		=> '-5058',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Settings of maintenence is not changed!'
	],
	'-5059' => [
		'code' 		=> '-5059',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Country not added!'
	],
	'-5060' => [
		'code' 		=> '-5060',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Country not updated!'
	],
	'-5061' => [
		'code' 		=> '-5061',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access! Country not deleted!It is associated with merchant or customer or customer order or external order!'
	],
	'-5062' => [
		'code' 		=> '-5062',
		'message'	=> 'Error!',
		'meaning'	=> 'Please add stock for this items! Stock is not enough & will get nagative!'
	],
	'-5063' => [
		'code' 		=> '-5063',
		'message'	=> 'Error!',
		'meaning'	=> 'Password is not changed!'
	],
	'-5064' => [
		'code' 		=> '-5064',
		'message'	=> 'Error!',
		'meaning'	=> 'Already paid by online! Payment method is not changed!So you cannot change online to cash on delivery for internal order if it is already paid by online!'
	],
	'-5065' => [
		'code' 		=> '-5065',
		'message'	=> 'Error!',
		'meaning'	=> 'Many products are asociated with this category and subcategory! So if you delete all the products associated with this category and subcategory then you can add new options for this category and subcategory.'
	],
	'-5066' => [
		'code' 		=> '-5066',
		'message'	=> 'Error!',
		'meaning'	=> 'Note for order is not updated!'
	],
	'-5067' => [
		'code' 		=> '-5067',
		'message'	=> 'Error!',
		'meaning'	=> 'Your account is deleted by admin!'
	],
	'-5068' => [
		'code' 		=> '-5068',
		'message'	=> 'Error!',
		'meaning'	=> 'Your account is inactive by admin!'
	],

	'-5070' => [
		'code' 		=> '-5070',
		'message'	=> 'Logout error!',
		'meaning'	=> 'Sorry, the driver cannot be logged out'
	],
	'-5071' => [
		'code' 		=> '-5071',
		'message'	=> 'Error!',
		'meaning'	=> 'Password reset link is not sent!Something went wrong!'
	],
	'-5072' => [
		'code' 		=> '-5072',
		'message'	=> 'Error!',
		'meaning'	=> 'Pasword is not changed!'
	],
	'-5073' => [
		'code' 		=> '-5073',
		'message'	=> 'Error!',
		'meaning'	=> 'Removed item from wishlist is failed!'
	],
	'-5074' => [
		'code' 		=> '-5074',
		'message'	=> 'Error!',
		'meaning'	=> 'Address removed successfully!'
	],
	'-5075' => [
		'code' 		=> '-5075',
		'message'	=> 'Error!',
		'meaning'	=> 'Remove from favourite successfully!'
	],
	'-5076' => [
		'code' 		=> '-5076',
		'message'	=> 'Error!',
		'meaning'	=> 'Product already added as favourite and added in wishlist!'
	],
	'-5077' => [
		'code' 		=> '-5077',
		'message'	=> 'Error!',
		'meaning'	=> 'Please login to add product in your wishlist!'
	],
	'-5078' => [
		'code' 		=> '-5078',
		'message'	=> 'Error!',
		'meaning'	=> 'Another driver is already grab this order!'
	],
	'-5079' => [
		'code' 		=> '-5079',
		'message'	=> 'Error!',
		'meaning'	=> 'FAQ category cannot be deleted. This category has some FAQ, delete them and try again.'
	],
	'-5080' => [
		'code' 		=> '-5080',
		'message'	=> 'Error!',
		'meaning'	=> 'Email-id or phone number already exist!'
	],
	'-5081' => [
		'code' 		=> '-5081',
		'message'	=> 'Error!',
		'meaning'	=> 'Phone no/email-id already exist!'
	],
	'-5082' => [
		'code' 		=> '-5082',
		'message'	=> 'Error!',
		'meaning'	=> 'Link Expired!This email link has been expired!'
	],
	'-5083' => [
		'code' 		=> '-5083',
		'message'	=> 'Error!',
		'meaning'	=> 'We cannot find an account with that email address!'
	],
	'-5084' => [
		'code' 		=> '-5084',
		'message'	=> 'Error!',
		'meaning'	=> 'if associated with sub category can not be deleted.'
	],
	'-5085' => [
		'code' 		=> '-5085',
		'message'	=> 'Error!',
		'meaning'	=> 'Option is already associated with child or parent category!'
	],
	'-5086' => [
		'code' 		=> '-5086',
		'message'	=> 'Error!',
		'meaning'	=> 'This is associated with product!You cannot delete this!'
	],
	'-33087' => [
		'code' 		=> '-33087',
		'message'	=> '',
		'meaning'	=> 'Please upload a minimum of 2 images!'
	],
	'-33088' => [
		'code' 		=> '-33088',
		'message'	=> 'Error',
		'meaning'	=> 'Maximum 4 image can be uploaded! Please delete any one of the image in order to upload image!'
	],
	'-33089' => [
		'code' 		=> '-33089',
		'message'	=> 'Error',
		'meaning'	=> 'Not covert to seller,  you have already added a product in cart!'
	],
	'-33090' => [
		'code' 		=> '-33090',
		'message'	=> 'Error',
		'meaning'	=> 'Maximum of 4 images can be uploaded!'
	],
	'-33100' => [
		'code' 		=> '-33100',
		'message'	=> '',
		'meaning'	=> 'You cannot cart any item. please login!'
	],
	'-33101' => [
		'code' 		=> '-33101',
		'message'	=> '',
		'meaning'	=> 'Please add one renting item to book anything.'
	],
	'-33102' => [
		'code' 		=> '-33102',
		'message'	=> '',
		'meaning'	=> "You can’t add your own product in the cart"
	],
	'-33103' => [
		'code' 		=> '-33103',
		'message'	=> '',
		'meaning'	=> "Item already exists in the cart."
	],
	'-33104' => [
		'code' 		=> '-33103',
		'message'	=> '',
		'meaning'	=> "Item already exists in the waitlist."
	],
	'-33105' => [
		'code' 		=> '-33105',
		'message'	=> '',
		'meaning'	=> 'Please add one renting item to waitlist anything.'
	],
	'-33106' => [
		'code' 		=> '-33106',
		'message'	=> '',
		'meaning'	=> 'You cannot wistlist any item. please login!'
	],
	'-33107' => [
		'code' 		=> '-33107',
		'message'	=> '',
		'meaning'	=> "You can’t add your own product in the waitlist"
	],
	'-33108' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Some Thing went wrong."
	],
	'-33109' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Category already exist."
	],
	'-33110' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Category data not found."
	],
	'-33111' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Education already exist."
	],
	'-33112' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Education data not found."
	],
	'-33113' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Award already exist."
	],
	'-33114' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Award data not found."
	],
	'-33115' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Training already exist."
	],
	'-33116' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Training data not found."
	],
	'-33117' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Bulk pricing no of session already exist."
	],
	'-33118' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Exclusions date already exist."
	],
	'-33119' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "User data not found."
	],
	'-33120' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Course Schedule already booked. you can not delete this schedule."
	],
	'-33121' => [
		'code' 		=> '',
		'message'	=> '',
		'meaning'	=> "Please upload a csv file!"
	],
	'-33122' => [
		'code' 		=> '-33122',
		'message'	=> 'Error!',
		'meaning'	=> 'if associated with Topic can not be deleted.'
	],
	'-33123' => [
		'code' 		=> '-33123',
		'message'	=> 'Error!',
		'meaning'	=> 'if associated with Conversation can not be deleted.'
	],
	'-33124' => [
		'code' 		=> '-33124',
		'message'	=> 'Error!',
		'meaning'	=> 'If sub category is associated with user profile can not be deleted.'
	],
	'-33125' => [
		'code' 		=> '-33125',
		'message'	=> 'Error!',
		'meaning'	=> 'Please enter a valid timeslot.'
	],
	'-33126' => [
		'code' 		=> '-33126',
		'message'	=> 'Error!',
		'meaning'	=> 'Schedule start date already exists!'
	],
	'-33127' => [
		'code' 		=> '-33127',
		'message'	=> 'Error!',
		'meaning'	=> 'Not sufficient balance to create a conversation..!'
	],
	'-33128' => [
		'code' 		=> '-33128',
		'message'	=> 'Error!',
		'meaning'	=> 'Not sufficient balance for this user. You cannot approve this user for a conversation..!'
	],
	'-33129' => [
		'code' 		=> '-33129',
		'message'	=> 'Error!',
		'meaning'	=> 'You are evicted from the conversation you cant join..'
	],
    '-33130' => [
		'code' 		=> '-33130',
		'message'	=> 'Error!',
		'meaning'	=> "Payment alrady completed"
	],
];
