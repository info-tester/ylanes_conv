<?php
return [
	'invalid_request' => 'Invalid request!',
	'product_out_of_stock' => 'This product is currently not available',
];