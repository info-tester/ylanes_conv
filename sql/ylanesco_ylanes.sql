-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 09, 2022 at 05:29 AM
-- Server version: 10.5.15-MariaDB-cll-lve
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ylanesco_ylanes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('I','A','U') COLLATE utf8mb4_unicode_ci DEFAULT 'A',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `profile_pic`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ylanes Con', 'info@ylanes.in', 8889996547, '1646922468.png', NULL, '$2y$10$HYXUMeLiHPO8Mi/zoKuLN.kC0eEcOcvpUBN9DdNHuuqQ8fnhh0Uiy', 'A', 'NJ442aXpZQGKCvSnBcteiO6k8ZxI5zYeHZ6V1hiEqrFmTUspHgHlROciEIIv', '2021-12-06 18:30:00', '2022-06-13 10:13:58');

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_password_resets`
--

INSERT INTO `admin_password_resets` (`email`, `token`, `created_at`) VALUES
('dasswarup438@gmail.com', '$2y$10$vCe.5c1umnYvtiu29Sor0OY4pEC.OVszYABXauXbbSya5iskC7CZu', '2022-03-09 23:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0 COMMENT '0 - Patent Category',
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `status` enum('A','I','D') DEFAULT NULL COMMENT 'A => Active, I => Inactive, D => Deleted',
  `show_header` enum('Y','N') DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `parent_id`, `name`, `description`, `picture`, `status`, `show_header`, `created_at`, `updated_at`) VALUES
(1, 'relationships-1', 0, 'Relationships', '', '1.png', 'A', 'N', '2022-04-23 13:57:00', '2022-04-23 14:39:17'),
(2, 'reflections-2', 0, 'Reflections', '', '2.png', 'A', 'N', '2022-04-23 13:57:14', '2022-04-23 14:41:34'),
(3, 'exploring-self-3', 0, 'Exploring Self', '', '3.png', 'A', 'N', '2022-04-23 13:57:27', '2022-04-23 14:42:03'),
(4, 'starting-a-romantic-relationship-4', 1, 'Starting a romantic relationship', '', NULL, 'A', 'N', '2022-04-23 13:57:47', '2022-04-23 13:57:47'),
(5, 'marry-why-why-not-5', 1, 'Marry - Why / Why not?', '', NULL, 'D', 'N', '2022-04-23 13:57:56', '2022-04-23 13:57:56'),
(6, '2nd-nth-marriage-6', 1, '2nd (nth) Marriage', '', NULL, 'A', 'N', '2022-04-23 13:58:09', '2022-04-23 13:58:09'),
(7, 'lgbtq-relationships-7', 1, 'LGBTQ+ relationships', '', NULL, 'A', 'N', '2022-04-23 13:58:17', '2022-04-23 13:58:17'),
(8, 'making-a-relationship-marriage-work-8', 1, 'Making a relationship / marriage  work', '', NULL, 'D', 'N', '2022-04-23 13:58:26', '2022-04-23 13:58:26'),
(9, 'in-a-relationship-or-marriage-9', 1, 'In a relationship or marriage', '', NULL, 'D', 'N', '2022-04-23 13:58:36', '2022-06-09 00:01:12'),
(10, 'complicated-relationships-10', 1, 'Complicated Relationships', '', NULL, 'A', 'N', '2022-04-23 13:58:45', '2022-04-23 13:58:45'),
(11, 'divorce-11', 1, 'Divorce', '', NULL, 'A', 'N', '2022-04-23 13:58:53', '2022-04-23 13:58:53'),
(12, 'having-children-12', 1, 'Having children', '', NULL, 'A', 'N', '2022-04-23 13:59:01', '2022-04-23 13:59:01'),
(13, 'parenting-13', 1, 'Parenting', '', NULL, 'A', 'N', '2022-04-23 13:59:12', '2022-04-23 13:59:12'),
(14, 'dealing-with-parents-14', 1, 'Dealing with Parents', '', NULL, 'A', 'N', '2022-04-23 13:59:20', '2022-04-23 13:59:20'),
(15, 'friendship-15', 1, 'Friendship', '', NULL, 'D', 'N', '2022-04-23 13:59:27', '2022-06-09 00:00:58'),
(16, 'sibling-relationship-16', 1, 'Sibling relationship', '', NULL, 'D', 'N', '2022-04-23 13:59:36', '2022-06-09 00:01:34'),
(17, 'dealing-with-death-17', 1, 'Dealing with death', '', NULL, 'A', 'N', '2022-04-23 13:59:45', '2022-04-23 13:59:45'),
(18, 'workplace-boss-colleagues-18', 1, 'Workplace, boss & colleagues', '', NULL, 'D', 'N', '2022-04-23 13:59:54', '2022-04-23 13:59:54'),
(19, 'purpose-and-meaning-19', 2, 'Purpose and Meaning', '', NULL, 'A', 'N', '2022-04-23 14:00:05', '2022-04-23 14:00:05'),
(20, 'philosophy-20', 2, 'Philosophy', '', NULL, 'A', 'N', '2022-04-23 14:00:12', '2022-04-23 14:00:12'),
(21, 'spirituality-21', 2, 'Spirituality', '', NULL, 'A', 'N', '2022-04-23 14:00:20', '2022-04-23 14:00:20'),
(22, 'movies-22', 2, 'Movies', '', NULL, 'A', 'N', '2022-04-23 14:00:29', '2022-04-23 14:00:29'),
(23, 'books-23', 2, 'Books', '', NULL, 'A', 'N', '2022-04-23 14:00:39', '2022-04-23 14:00:39'),
(24, 'poetry-24', 2, 'Poetry', '', NULL, 'A', 'N', '2022-04-23 14:00:47', '2022-04-23 14:00:47'),
(25, 'travel-25', 2, 'Travel', '', NULL, 'A', 'N', '2022-04-23 14:00:54', '2022-04-23 14:00:54'),
(26, 'your-story-26', 2, 'Your story', '', NULL, 'A', 'N', '2022-04-23 14:01:03', '2022-04-23 14:01:03'),
(27, 'risks-27', 2, 'Risks', '', NULL, 'D', 'N', '2022-04-23 14:01:10', '2022-04-23 14:01:10'),
(28, 'mistakes-28', 2, 'Mistakes', '', NULL, 'D', 'N', '2022-04-23 14:01:19', '2022-04-23 14:01:19'),
(29, 'regrets-29', 2, 'Regrets', '', NULL, 'D', 'N', '2022-04-23 14:01:26', '2022-04-23 14:01:26'),
(30, 'gender-identity-30', 3, 'Gender Identity', '', NULL, 'A', 'N', '2022-04-23 14:01:41', '2022-04-23 14:01:41'),
(31, 'self-image-31', 3, 'Self Image', '', NULL, 'A', 'N', '2022-04-23 14:01:49', '2022-04-23 14:01:49'),
(32, 'imposter-syndrome-32', 3, 'Imposter Syndrome', '', NULL, 'A', 'N', '2022-04-23 14:01:57', '2022-04-23 14:01:57'),
(33, 'vulnerability-33', 3, 'Vulnerability', '', NULL, 'A', 'N', '2022-04-23 14:02:05', '2022-04-23 14:02:05'),
(34, 'shame-34', 3, 'Shame', '', NULL, 'A', 'N', '2022-04-23 14:02:13', '2022-04-23 14:02:13'),
(35, 'childhood-patterns-35', 3, 'Childhood patterns', '', NULL, 'A', 'N', '2022-04-23 14:02:20', '2022-04-23 14:02:20'),
(36, 'fear-36', 3, 'Fear', '', NULL, 'A', 'N', '2022-04-23 14:02:27', '2022-04-23 14:02:27'),
(37, 'guilt-37', 3, 'Guilt', '', NULL, 'A', 'N', '2022-04-23 14:02:42', '2022-04-23 14:02:42'),
(38, 'anger-38', 3, 'Anger', '', NULL, 'A', 'N', '2022-04-23 14:02:48', '2022-04-23 14:02:48'),
(39, 'calming-the-mind-39', 3, 'Calming the mind', '', NULL, 'D', 'N', '2022-04-23 14:02:56', '2022-04-23 14:02:56'),
(40, 'forgiveness-40', 3, 'Forgiveness', '', NULL, 'A', 'N', '2022-04-23 14:03:03', '2022-04-23 14:03:03'),
(41, 'addiction-41', 3, 'Addiction', '', NULL, 'D', 'N', '2022-04-23 14:03:10', '2022-06-17 14:24:19'),
(42, 'critical-illness-42', 3, 'Critical illness', '', NULL, 'D', 'N', '2022-04-23 14:03:18', '2022-04-23 14:03:18'),
(43, 'caring-for-critically-ill-43', 3, 'Caring for critically ill', '', NULL, 'D', 'N', '2022-04-23 14:03:26', '2022-06-17 14:21:54'),
(44, 'elders-60-plus-44', 3, 'Elders (60 plus)', '', NULL, 'D', 'N', '2022-04-23 14:03:34', '2022-04-23 14:03:34'),
(45, 'bad-habits-45', 3, 'Bad Habits', '', NULL, 'D', 'N', '2022-04-23 15:13:44', '2022-06-17 14:29:37'),
(54, 'marry-54', 1, 'Marry?', '', NULL, 'A', 'N', '2022-06-17 13:14:34', '2022-06-17 13:14:34'),
(55, 'making-a-relationship-work-55', 1, 'Making a Relationship Work', '', NULL, 'A', 'N', '2022-06-17 13:14:57', '2022-06-17 13:14:57'),
(56, 'complicated-relationships-56', 1, 'Complicated Relationships', '', NULL, 'D', 'N', '2022-06-17 13:15:11', '2022-06-17 13:15:11'),
(57, 'workplace-boss-colleagues-57', 1, 'Workplace, Boss, Colleagues', '', NULL, 'A', 'N', '2022-06-17 13:16:19', '2022-06-17 13:16:19'),
(58, 'time-58', 2, 'Time', '', NULL, 'A', 'N', '2022-06-17 13:17:46', '2022-06-17 13:17:46'),
(59, 'self-love-59', 3, 'Self Love', '', NULL, 'A', 'N', '2022-06-17 13:19:49', '2022-06-17 13:19:49'),
(60, 'chatter-in-the-mind-60', 3, 'Chatter in the Mind', '', NULL, 'A', 'N', '2022-06-17 13:21:02', '2022-06-17 13:21:02'),
(61, 'habits-61', 3, 'Habits', '', NULL, 'A', 'N', '2022-06-17 13:21:26', '2022-06-17 13:21:26'),
(62, 'dealing-with-critical-illness-62', 3, 'Dealing with Critical illness', '', NULL, 'A', 'N', '2022-06-17 13:21:55', '2022-06-17 13:21:55'),
(63, 'caring-for-critically-ill-63', 3, 'Caring for critically ill', '', NULL, 'A', 'N', '2022-06-17 13:22:12', '2022-06-17 14:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `chat_details`
--

CREATE TABLE `chat_details` (
  `id` int(11) NOT NULL,
  `chat_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat_master`
--

CREATE TABLE `chat_master` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `connects_groups`
--

CREATE TABLE `connects_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_name` varchar(299) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` bigint(20) NOT NULL,
  `creator_user_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `sub_category_id` bigint(20) DEFAULT NULL,
  `topic_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `type` enum('ON','OP','CO') DEFAULT 'ON' COMMENT 'ON => 1 to 1 , OP => Open, CO => Condotional',
  `status` enum('A','I','D','C','CO') DEFAULT 'A',
  `no_of_participants` varchar(299) DEFAULT NULL,
  `created_by` enum('U','A') DEFAULT 'U',
  `is_full` enum('Y','N') DEFAULT 'N',
  `call_end_time` datetime DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `no_of_extend` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `creator_user_id`, `category_id`, `sub_category_id`, `topic_id`, `date`, `time`, `type`, `status`, `no_of_participants`, `created_by`, `is_full`, `call_end_time`, `schedule_id`, `no_of_extend`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 6, 10, '2022-06-21', '00:00:00', 'OP', 'A', '1', 'A', 'N', NULL, NULL, 0, '2022-06-20 18:12:36', '2022-06-20 18:14:18'),
(2, 7, 3, 63, 96, '2022-06-22', '00:30:00', 'OP', 'A', '0', 'U', 'N', NULL, NULL, 0, '2022-06-21 19:16:40', '2022-06-22 00:00:38'),
(3, 5, 2, 19, 48, '2022-06-23', '00:00:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-06-22 10:24:19', '2022-06-22 10:24:19'),
(4, 7, 3, 63, 96, '2022-06-23', '00:40:00', 'OP', 'A', '0', 'U', 'N', NULL, NULL, 0, '2022-06-22 16:09:56', '2022-06-22 16:10:04'),
(5, 13, 3, 61, 89, '2022-06-28', '13:00:00', 'OP', 'CO', '2', 'U', 'N', '2022-06-28 12:57:05', NULL, 1, '2022-06-28 12:46:27', '2022-06-28 13:00:47'),
(6, 10, 3, 38, 71, '2022-06-29', '00:00:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-06-28 18:55:33', '2022-06-28 18:55:33'),
(7, 10, 3, 63, 96, '2022-06-28', '20:10:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-06-28 19:53:34', '2022-06-28 19:53:34'),
(8, 5, 3, 38, 71, '2022-06-29', '12:00:00', 'CO', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-06-29 11:54:25', '2022-06-29 11:54:25'),
(9, 10, 2, 23, 55, '2022-06-29', '16:00:00', 'OP', 'A', '2', 'U', 'N', '2022-06-29 16:58:32', NULL, 0, '2022-06-29 15:58:04', '2022-06-29 15:58:32'),
(10, 5, 3, 38, 71, '2022-06-29', '19:40:00', 'OP', 'A', '2', 'U', 'N', '2022-06-29 20:24:16', NULL, 0, '2022-06-29 19:20:08', '2022-06-29 19:24:16'),
(11, 12, 3, 38, 71, '2022-06-29', '22:00:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-06-29 20:55:10', '2022-06-29 20:55:10'),
(12, 5, 3, 38, 71, '2022-07-01', '00:00:00', 'CO', 'A', '2', 'U', 'N', NULL, NULL, 0, '2022-06-30 11:47:58', '2022-06-30 11:49:45'),
(13, 5, 3, 38, 71, '2022-07-01', '03:00:00', 'OP', 'A', '2', 'U', 'N', NULL, NULL, 0, '2022-06-30 11:59:44', '2022-06-30 17:34:07'),
(14, 12, 3, 38, 71, '2022-06-30', '21:00:00', 'OP', 'A', '2', 'U', 'N', NULL, NULL, 0, '2022-06-30 16:11:59', '2022-06-30 16:13:36'),
(15, 5, 2, 23, 55, '2022-06-30', '17:40:00', 'OP', 'A', '2', 'U', 'N', NULL, NULL, 0, '2022-06-30 17:32:10', '2022-06-30 17:35:25'),
(16, 5, 1, 6, 10, '2022-06-30', '17:00:00', 'OP', 'A', '2', 'U', 'N', NULL, NULL, 0, '2022-06-30 17:46:01', '2022-06-30 17:46:35'),
(17, 5, 3, 62, 87, '2022-07-02', '00:00:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-07-01 16:12:24', '2022-07-01 16:12:24'),
(18, 12, 1, 13, 33, '2022-07-02', '21:30:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-07-01 16:12:31', '2022-07-01 16:12:31'),
(19, 5, 1, 7, 11, '2022-07-02', '01:10:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-07-01 16:28:23', '2022-07-01 16:28:23'),
(20, 5, 1, 57, 108, '2022-07-09', '00:00:00', 'OP', 'A', '1', 'U', 'N', NULL, NULL, 0, '2022-07-07 17:12:25', '2022-07-07 17:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `conversations_to_users`
--

CREATE TABLE `conversations_to_users` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `conversation_id` bigint(20) DEFAULT NULL,
  `display_name` varchar(299) DEFAULT NULL,
  `conversation_text` longtext DEFAULT NULL,
  `is_creator` enum('Y','N') DEFAULT 'N',
  `is_anonymously` enum('Y','N') DEFAULT 'N',
  `hearts_received` int(11) DEFAULT 0,
  `is_evicted` enum('Y','N') DEFAULT 'N',
  `reports_received` int(11) DEFAULT 0,
  `status` enum('C','U','R','D') DEFAULT 'U',
  `conv_balance_type` enum('P','F') DEFAULT 'P',
  `join_type` enum('N','R') NOT NULL DEFAULT 'N' COMMENT 'N->Normal, R->request',
  `active_video_call` enum('Y','N') NOT NULL DEFAULT 'N',
  `join_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `duration` bigint(20) NOT NULL DEFAULT 0 COMMENT 'In second',
  `tot_report` int(11) NOT NULL DEFAULT 0,
  `tot_eviction` int(11) NOT NULL DEFAULT 0,
  `is_leave_parament` enum('Y','N') NOT NULL DEFAULT 'N',
  `no_of_extend` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversations_to_users`
--

INSERT INTO `conversations_to_users` (`id`, `user_id`, `conversation_id`, `display_name`, `conversation_text`, `is_creator`, `is_anonymously`, `hearts_received`, `is_evicted`, `reports_received`, `status`, `conv_balance_type`, `join_type`, `active_video_call`, `join_time`, `end_time`, `duration`, `tot_report`, `tot_eviction`, `is_leave_parament`, `no_of_extend`, `created_at`, `updated_at`) VALUES
(1, 5, 1, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 15', 'N', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-20 18:14:18', '2022-06-20 18:14:18'),
(2, 7, 2, NULL, 'your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry', 'Y', 'N', 0, 'N', 0, 'D', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-21 19:16:40', '2022-06-22 00:00:38'),
(3, 5, 3, NULL, 'Wide range of topics, convenient timings, choice of room type, gifting and receiving hearts, amazing people from across the world, authentic connections', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-22 10:24:19', '2022-06-22 10:24:19'),
(4, 7, 4, NULL, 'JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>JUNE 11- Website & APP>J', 'Y', 'N', 0, 'N', 0, 'D', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-22 16:09:56', '2022-06-22 16:10:04'),
(5, 13, 5, NULL, 'adsadfd  dsdfad dfdfad dfdsfd  dfdfd dfdafdfdasfdafd', 'Y', 'N', 5, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 471, 0, 0, 'Y', 1, '2022-06-28 12:46:27', '2022-06-29 20:53:37'),
(6, 12, 5, NULL, 'this is my takethis is my takethis is my takethis is my takethis is my take', 'N', 'N', 5, 'N', 0, 'C', 'F', 'N', 'Y', '2022-06-28 12:52:09', NULL, 0, 1, 0, 'Y', 1, '2022-06-28 12:49:25', '2022-06-28 13:02:47'),
(7, 10, 6, NULL, 'Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc.', 'Y', 'N', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-28 18:55:33', '2022-06-28 18:55:33'),
(8, 10, 7, NULL, 'Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc.', 'Y', 'N', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-28 19:53:34', '2022-06-28 19:53:34'),
(9, 5, 8, NULL, 'Do you want to join anonymously?Do you want to join anonymously?Do you want to join anonymously?Do you want to join anonymously?', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-29 11:54:25', '2022-06-29 11:54:25'),
(10, 10, 9, NULL, 'Introduce yourself as an animal / place / thing or celebrity (real or imaginary) and state the reasons why - e.g. I am a tortoise because I’m wise and steady.', 'Y', 'N', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 304, 0, 0, 'N', 0, '2022-06-29 15:58:04', '2022-06-29 16:03:40'),
(11, 5, 9, NULL, 'Introduce yourself as an animal / place / thing or celebrity (real or imaginary) and state the reasons why - e.g. I am a tortoise because I’m wise and steady.', 'N', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 317, 0, 0, 'N', 0, '2022-06-29 15:58:18', '2022-06-29 16:03:47'),
(12, 5, 10, NULL, 'Wide range of topics, convenient timings, choice of room type, gifting and receiving hearts, amazing people from across the world, authentic connections and much more', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 390, 0, 0, 'N', 0, '2022-06-29 19:20:08', '2022-06-29 19:30:56'),
(13, 10, 10, NULL, 'Wide range of topics, convenient timings, choice of room type, gifting and receiving hearts, amazing people from across the world, authentic connections and much more', 'N', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 387, 0, 0, 'N', 0, '2022-06-29 19:23:48', '2022-06-29 19:30:59'),
(14, 12, 11, NULL, 'i want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to starti want a call to start', 'Y', 'N', 0, 'N', 0, 'C', 'F', 'N', 'Y', '2022-06-29 20:57:30', NULL, 0, 0, 0, 'N', 0, '2022-06-29 20:55:10', '2022-06-29 20:57:30'),
(15, 5, 12, NULL, 'Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc.Please share what you will be bringing to this conversation – a su', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 11:47:58', '2022-06-30 11:47:58'),
(16, 10, 12, NULL, 'Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc.', 'N', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 11:49:26', '2022-06-30 11:49:45'),
(17, 5, 13, NULL, 'Please share what you will be bringing to this conversation – a subtopic, experience, question, perspective, some ideas, etc. You can also share any relevant resources such as YouTube links, websites, Instagram / Pinterest pages, etc.Please share what you will be bringing to this conversation – a su', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 11:59:44', '2022-06-30 11:59:44'),
(18, 12, 14, NULL, 'this is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my takethis is my take', 'Y', 'N', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 16:11:59', '2022-06-30 16:11:59'),
(19, 5, 14, NULL, 'TESTghghhgghggghhghgghhhgghhgghhgghhghghgghghhgghghghhghg', 'N', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 16:13:36', '2022-06-30 16:13:36'),
(20, 5, 15, 'ANO', 'total conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conversationtotal conver', 'Y', 'Y', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 17:32:10', '2022-06-30 17:32:10'),
(21, 3, 13, 'ABC', 'Do you want to reject this rDo you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to rejecequest?', 'N', 'Y', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 17:34:07', '2022-06-30 17:34:07'),
(22, 3, 15, 'AB', 'Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to reject this request?Do you want to rejec', 'N', 'Y', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 17:35:25', '2022-06-30 17:35:25'),
(23, 5, 16, NULL, 'Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 17:46:01', '2022-06-30 17:46:01'),
(24, 3, 16, NULL, 'Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch Going to lunch', 'N', 'N', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-06-30 17:46:35', '2022-06-30 17:46:35'),
(25, 5, 17, NULL, 'eereeereeereeereeereeereeereeereeereeereeereeereeereeereeereeereeere', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-07-01 16:12:24', '2022-07-01 16:12:24'),
(26, 12, 18, NULL, 'hare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideashare your stories & ideas', 'Y', 'N', 0, 'N', 0, 'C', 'F', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-07-01 16:12:31', '2022-07-01 16:12:31'),
(27, 5, 19, NULL, 'Do you want to join anonymously?Do you want to join anonymously?Do you want to join anonymously?Do you want to join anonymously?Do you want to join anonymously?Do you want to join anonymously?', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-07-01 16:28:23', '2022-07-01 16:28:23'),
(28, 5, 20, NULL, 'CREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATIONCREATE CONVERSATION', 'Y', 'N', 0, 'N', 0, 'C', 'P', 'N', 'N', NULL, NULL, 0, 0, 0, 'N', 0, '2022-07-07 17:12:25', '2022-07-07 17:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `conversation_eviction`
--

CREATE TABLE `conversation_eviction` (
  `id` int(11) NOT NULL,
  `conversation_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reported_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_reports`
--

CREATE TABLE `conversation_reports` (
  `id` int(11) NOT NULL,
  `conversation_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reported_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversation_reports`
--

INSERT INTO `conversation_reports` (`id`, `conversation_master_id`, `user_id`, `reported_by_id`, `created_at`, `updated_at`) VALUES
(1, 5, 12, 13, '2022-06-28 13:02:47', '2022-06-28 13:02:47');

-- --------------------------------------------------------

--
-- Table structure for table `conversation_request_join`
--

CREATE TABLE `conversation_request_join` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `from_user` int(11) DEFAULT NULL,
  `to_user` int(11) DEFAULT NULL,
  `status` enum('A','R','S') NOT NULL DEFAULT 'S' COMMENT 'A->accept , R-> Reject , S-> Send Request',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_reviews`
--

CREATE TABLE `conversation_reviews` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rating` enum('happy','ok','sad') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversation_reviews`
--

INSERT INTO `conversation_reviews` (`id`, `conversation_id`, `user_id`, `rating`, `created_at`, `updated_at`) VALUES
(1, 5, 13, 'happy', '2022-06-28 13:00:01', '2022-06-28 13:00:01'),
(2, 5, 12, 'ok', '2022-06-29 20:53:40', '2022-06-29 20:53:40');

-- --------------------------------------------------------

--
-- Table structure for table `conversation_user_reviews`
--

CREATE TABLE `conversation_user_reviews` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `hearts` int(11) DEFAULT NULL,
  `reviews_text` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversation_user_reviews`
--

INSERT INTO `conversation_user_reviews` (`id`, `conversation_id`, `from_user_id`, `to_user_id`, `hearts`, `reviews_text`, `created_at`, `updated_at`) VALUES
(1, 5, 13, 12, 5, 'amazing', '2022-06-28 13:00:51', '2022-06-28 13:00:51'),
(2, 5, 12, 13, 5, NULL, '2022-06-29 20:53:37', '2022-06-29 20:53:37');

-- --------------------------------------------------------

--
-- Table structure for table `conv_schedule_details`
--

CREATE TABLE `conv_schedule_details` (
  `id` int(11) NOT NULL,
  `conv_schedule_master_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `conv_schedule_master`
--

CREATE TABLE `conv_schedule_master` (
  `id` int(11) NOT NULL,
  `freq_hour` int(11) DEFAULT NULL,
  `repeat_after_days` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `next_cron_date` date DEFAULT NULL,
  `topics_json` longtext DEFAULT NULL,
  `is_updated` enum('Y','N') DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `name` varchar(299) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Mountains', '2022-03-09 18:30:00', '2022-03-09 18:30:00'),
(2, 'Beaches', '2022-03-09 18:30:00', '2022-03-09 18:30:00'),
(3, 'Relax at home', '2022-03-09 18:30:00', '2022-03-09 18:30:00'),
(4, 'Party with friends', '2022-03-09 18:30:00', '2022-03-09 18:30:00'),
(5, 'Binge TV', '2022-03-09 18:30:00', '2022-03-09 18:30:00'),
(6, 'Binge books', '2022-03-09 18:30:00', '2022-03-09 18:30:00'),
(7, 'Jungles', '2022-03-09 18:30:00', '2022-03-09 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(299) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'English', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(2, 'Hindi', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(3, 'Bengali', '2022-03-08 18:30:00', '2022-03-08 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_03_09_061042_create_admin_password_resets_table', 1),
(6, '2022_03_09_061042_create_admins_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `my_connect`
--

CREATE TABLE `my_connect` (
  `id` int(11) NOT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `from_user_group_id` int(11) DEFAULT NULL,
  `to_user_group_id` int(11) DEFAULT NULL,
  `status` enum('A','N','B') DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `my_connect`
--

INSERT INTO `my_connect` (`id`, `from_user_id`, `to_user_id`, `topic_id`, `from_user_group_id`, `to_user_group_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 54, NULL, NULL, 'A', '2022-06-10 19:03:03', '2022-06-10 19:04:22'),
(4, 3, 5, 10, NULL, NULL, 'N', '2022-06-30 17:54:53', '2022-06-30 17:54:53');

-- --------------------------------------------------------

--
-- Table structure for table `package_master`
--

CREATE TABLE `package_master` (
  `id` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `coin_free` int(11) DEFAULT NULL,
  `coin_paid` int(11) DEFAULT NULL,
  `coin_total` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `package_master`
--

INSERT INTO `package_master` (`id`, `price`, `coin_free`, `coin_paid`, `coin_total`, `created_at`, `updated_at`) VALUES
(12, '200.00', 0, 200, 200, '2022-06-29 11:40:38', '2022-06-29 11:40:38'),
(13, '500.00', 50, 500, 550, '2022-06-29 11:40:52', '2022-06-29 11:40:52'),
(14, '1000.00', 150, 1000, 1150, '2022-06-29 11:41:50', '2022-06-29 11:42:11'),
(15, '1500.00', 300, 1500, 1800, '2022-06-29 11:42:35', '2022-06-29 11:42:35'),
(16, '2000.00', 500, 2000, 2500, '2022-06-29 11:42:47', '2022-06-29 11:42:47'),
(17, '3000.00', 1000, 3000, 4000, '2022-06-29 11:43:03', '2022-06-29 11:43:03'),
(18, '4000.00', 1500, 4000, 5500, '2022-06-29 11:43:14', '2022-06-29 11:43:14'),
(19, '5000.00', 2000, 5000, 7000, '2022-06-29 11:43:25', '2022-06-29 11:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `packages_id` int(11) DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT 0.00,
  `status` enum('IP','P','F','I') NOT NULL DEFAULT 'IP',
  `transaction_id` varchar(255) DEFAULT NULL,
  `transaction_key` varchar(255) DEFAULT NULL,
  `payment_signature` varchar(255) DEFAULT NULL,
  `response` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `user_id`, `packages_id`, `amount`, `status`, `transaction_id`, `transaction_key`, `payment_signature`, `response`, `created_at`, `updated_at`) VALUES
(1, 'order_Jfo8NFLuhyWBjY', 2, 11, '250.00', 'P', 'zzNl5WeTZf', 'pay_Jfo8WWydTij0aL', 'd717ec9be2162f8260c2709376da1f11a50a2200469d4f420791085f0f69111f', '{\"id\":\"pay_Jfo8WWydTij0aL\",\"entity\":\"payment\",\"amount\":25000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_Jfo8NFLuhyWBjY\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID zzNl5WeTZf\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"soumojit.sad@gmail.com\",\"contact\":\"+917980768406\",\"notes\":{\"pay_id\":\"zzNl5WeTZf\"},\"fee\":590,\"tax\":90,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"bank_transaction_id\":\"5320127\"},\"created_at\":1654867553}', '2022-06-10 18:55:45', '2022-06-10 18:55:59'),
(2, 'order_JfoAnuKhZ59hVu', 2, 3, '1000.00', 'P', 'LBbc7nz5zh', 'pay_JfoAzKImCcU44q', '146d623242dcc36ace953c3b01cb25401423cfc5b933d3a4132a8775e8f0bac3', '{\"id\":\"pay_JfoAzKImCcU44q\",\"entity\":\"payment\",\"amount\":100000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JfoAnuKhZ59hVu\",\"invoice_id\":null,\"international\":false,\"method\":\"wallet\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID LBbc7nz5zh\",\"card_id\":null,\"bank\":null,\"wallet\":\"jiomoney\",\"vpa\":null,\"email\":\"soumojit.sad@gmail.com\",\"contact\":\"+917980768406\",\"notes\":{\"pay_id\":\"LBbc7nz5zh\"},\"fee\":2360,\"tax\":360,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"transaction_id\":null},\"created_at\":1654867693}', '2022-06-10 18:58:03', '2022-06-10 18:58:18'),
(3, 'order_Jh1uFBck5QWMsJ', 8, 11, '250.00', 'P', '5bOAlV1RrI', 'pay_Jh1uYD7grbfYpr', '2794e5a2fa8cfa4a56f2f99743316953e3681c4eb7852e36766ba2e7becc1ab1', '{\"id\":\"pay_Jh1uYD7grbfYpr\",\"entity\":\"payment\",\"amount\":25000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_Jh1uFBck5QWMsJ\",\"invoice_id\":null,\"international\":false,\"method\":\"upi\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID 5bOAlV1RrI\",\"card_id\":null,\"bank\":null,\"wallet\":null,\"vpa\":\"success@razorpay\",\"email\":\"user2@infoware-in.com\",\"contact\":\"+1234567893\",\"notes\":{\"pay_id\":\"5bOAlV1RrI\"},\"fee\":590,\"tax\":90,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"rrn\":\"408010843105\",\"upi_transaction_id\":\"F1E864A4810A3A3401CDE2F3C73B70DA\"},\"created_at\":1655134402}', '2022-06-13 21:03:05', '2022-06-13 21:03:28'),
(4, 'order_JhO27MjoVjPn2w', 5, 3, '1000.00', 'P', 'gl05wBAVm2', 'pay_JhO2EtiMRu9DCd', '3ee68ef3dda8c3274dc9424bf780d0430e40ceb3660f7035d00b9a0c2b806aee', '{\"id\":\"pay_JhO2EtiMRu9DCd\",\"entity\":\"payment\",\"amount\":100000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JhO27MjoVjPn2w\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID gl05wBAVm2\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"soumojit.sad@gmail.com\",\"contact\":\"+917980768406\",\"notes\":{\"pay_id\":\"gl05wBAVm2\"},\"fee\":2360,\"tax\":360,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"bank_transaction_id\":\"6876454\"},\"created_at\":1655212315}', '2022-06-14 18:41:48', '2022-06-14 18:41:59'),
(5, 'order_JkFBDyZzSEYL0O', 7, 4, '2000.00', 'P', '68w0F5nabL', 'pay_JkFBT0b7Z9b4p2', 'bf6846a0f7bc22e5c94748d0f38b9f935bc0627c2b91fe851888bc2defc28649', '{\"id\":\"pay_JkFBT0b7Z9b4p2\",\"entity\":\"payment\",\"amount\":200000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JkFBDyZzSEYL0O\",\"invoice_id\":null,\"international\":false,\"method\":\"upi\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID 68w0F5nabL\",\"card_id\":null,\"bank\":null,\"wallet\":null,\"vpa\":\"success@razorpay\",\"email\":\"user1@infoware-in.com\",\"contact\":\"+1234567892\",\"notes\":{\"pay_id\":\"68w0F5nabL\"},\"fee\":4720,\"tax\":720,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"rrn\":\"614812130136\",\"upi_transaction_id\":\"5F30C16AE592625F14773707AB100421\"},\"created_at\":1655836165}', '2022-06-21 23:59:11', '2022-06-21 23:59:30'),
(6, 'order_JmpnEsGIzMYZXI', 13, 4, '2000.00', 'P', 'x7ARcbxf3I', 'pay_Jmpnaox58MBvdZ', '1f9bdba257d110850a3e13ccf779ab3972dbd2287c1fa6646b98f7433d3747fa', '{\"id\":\"pay_Jmpnaox58MBvdZ\",\"entity\":\"payment\",\"amount\":200000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JmpnEsGIzMYZXI\",\"invoice_id\":null,\"international\":false,\"method\":\"upi\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID x7ARcbxf3I\",\"card_id\":null,\"bank\":null,\"wallet\":null,\"vpa\":\"success@razorpay\",\"email\":\"rajesh.irk@gmail.com\",\"contact\":\"+919820043174\",\"notes\":{\"pay_id\":\"x7ARcbxf3I\"},\"fee\":4720,\"tax\":720,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"rrn\":\"456366384352\",\"upi_transaction_id\":\"82830BE1DDD5148C541D78D228BAA37A\"},\"created_at\":1656401789}', '2022-06-28 13:06:08', '2022-06-28 13:06:34'),
(7, 'order_JnFmkUR4EKhbhX', 5, 12, '200.00', 'P', 'YgOJKNnlgP', 'pay_JnFmqS1QZQMr7D', '974b70148c0d76a073b8805ecb498705a6e91a372d5fc8e6af914e2cd58d91cc', '{\"id\":\"pay_JnFmqS1QZQMr7D\",\"entity\":\"payment\",\"amount\":20000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JnFmkUR4EKhbhX\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID YgOJKNnlgP\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"soumojit.sad@gmail.com\",\"contact\":\"+917980768406\",\"notes\":{\"pay_id\":\"YgOJKNnlgP\"},\"fee\":472,\"tax\":72,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"bank_transaction_id\":\"8317707\"},\"created_at\":1656493308}', '2022-06-29 14:31:42', '2022-06-29 14:31:53'),
(8, 'order_JnKg1NADJ8Q1Sl', 5, 16, '2000.00', 'P', 'xGFM1k1LHb', 'pay_JnKg6fMTngGSGj', 'dd3d60333915cbb66df621e7659f357dfb2c943a435de9e911044bdee8d7f22c', '{\"id\":\"pay_JnKg6fMTngGSGj\",\"entity\":\"payment\",\"amount\":200000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JnKg1NADJ8Q1Sl\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID xGFM1k1LHb\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"soumojit.sad@gmail.com\",\"contact\":\"+917980768406\",\"notes\":{\"pay_id\":\"xGFM1k1LHb\"},\"fee\":4720,\"tax\":720,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"bank_transaction_id\":\"4555522\"},\"created_at\":1656510533}', '2022-06-29 19:18:48', '2022-06-29 19:18:58'),
(9, 'order_JnKkiPIS1tV4WM', 10, 14, '1000.00', 'P', 'LGn5obrilY', 'pay_JnKl4lsZ2VE60z', '7dddc4bc2b3da68d26c917043292d4ffe58175c35c4519d56fc707081e3732a0', '{\"id\":\"pay_JnKl4lsZ2VE60z\",\"entity\":\"payment\",\"amount\":100000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_JnKkiPIS1tV4WM\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"Deal purchase for deal ID LGn5obrilY\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"hemixa8694@syswift.com\",\"contact\":\"+919804559517\",\"notes\":{\"pay_id\":\"LGn5obrilY\"},\"fee\":2360,\"tax\":360,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{\"bank_transaction_id\":\"7760279\"},\"created_at\":1656510816}', '2022-06-29 19:23:15', '2022-06-29 19:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professions`
--

CREATE TABLE `professions` (
  `id` int(11) NOT NULL,
  `name` varchar(299) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `professions`
--

INSERT INTO `professions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'baker', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(2, 'butcher', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(3, 'carpenter', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(4, 'cook', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(5, 'farmer', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(6, 'fireman', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(7, 'fisherman', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(8, 'gardener', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(9, 'hairdresser', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(10, 'journalist', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(11, 'judge', '2022-03-08 18:30:00', '2022-03-01 18:30:00'),
(12, 'lawyer', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(13, 'mason', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(14, 'mechanic', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(15, 'painter', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(16, 'plumber', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(17, 'policeman', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(18, 'postman', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(19, 'secretary', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(20, 'singer', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(21, 'soldier', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(22, 'driver', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(23, 'teacher', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(24, 'waiter', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(25, 'pilot', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(26, 'engineer', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(27, 'doctor', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(28, 'nurse', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(29, 'goldsmith', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(30, 'actor', '2022-03-08 18:30:00', '2022-03-08 18:30:00'),
(31, 'tailor', '2022-03-08 18:30:00', '2022-03-08 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `topic_line_1` longtext DEFAULT NULL,
  `topic_line_2` longtext DEFAULT NULL,
  `status` enum('A','I') DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `category_id`, `sub_category_id`, `topic_line_1`, `topic_line_2`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'What is love?', 'What is your definition of love? What are your views on \'true love\' - is it to be found or nurtured? Do you believe in searching for love or waiting for it to find you? What are your views on the role of love in building a strong relationship?', 'A', '2022-04-23 14:07:47', '2022-06-17 14:51:14'),
(2, 1, 4, 'Dating stories (for the 20s)', 'Talk about your experiences on dating apps, matrimonial sites, dates, hits, misses and all that\'s in between. What would you look for while choosing a partner? How would you know if the partner is right for you? Would you go with your head or your heart while choosing a date and / or making a commitment? Articulate your decision-making process', 'A', '2022-04-23 14:08:06', '2022-06-17 14:51:35'),
(3, 1, 4, 'Dating stories (for the 30s)', 'Talk about your experiences on dating apps, matrimonial sites, dates, hits, misses and all that\'s in between. What would you look for while choosing a partner? How would you know if the partner is right for you? Would you go with your head or your heart while choosing a date and / or making a commitment? Articulate your decision-making process', 'A', '2022-04-23 14:08:27', '2022-06-17 14:52:13'),
(4, 1, 4, 'Dating stories (for the 40s)', 'Talk about your experiences on dating apps, matrimonial sites, dates, hits, misses and all that\'s in between. What would you look for while choosing a partner? How would you know if the partner is right for you? Would you go with your head or your heart while choosing a date and / or making a commitment? Articulate your decision-making process', 'A', '2022-04-23 14:08:48', '2022-06-17 14:52:31'),
(5, 1, 4, 'Dating stories (for the 50s)', 'Talk about your experiences on dating apps, matrimonial sites, dates, hits, misses and all that\'s in between. What would you look for while choosing a partner? How would you know if the partner is right for you? Would you go with your head or your heart while choosing a date and / or making a commitment? Articulate your decision-making process', 'A', '2022-04-23 14:09:14', '2022-06-17 14:52:44'),
(8, 1, 6, 'Marry Again?', 'What were your reasons for getting married the previous time(s)? Why are you considering this marriage - talk about how you have changed and / or how you expect this relationship to be different. What are your thoughts on the timing of this marriage?', 'A', '2022-04-23 14:10:31', '2022-04-23 14:10:31'),
(9, 1, 6, 'One of us was previously married', 'What\'s your story? What is your understanding of the equation among all the stakeholders (you, your partner, the Ex if any, the kids if any)? How are you and your partner planning to handle the past? If there are kids involved, what roles are you expecting all the stakeholders to play going forward? How are you managing the communication of these plans with the stakeholders', 'A', '2022-04-23 14:10:54', '2022-06-17 15:08:10'),
(10, 1, 6, 'Both of us were previously married', 'What\'s your story? What is your understanding of the equation among all the stakeholders (you, your partner, the Exs if any, the kids if any)? How are you and your partner planning to handle your respective pasts? If there are kids involved, what roles are you expecting all the stakeholders to play going forward? How are you managing the communication of these plans with the stakeholders', 'A', '2022-04-23 14:11:17', '2022-06-17 15:08:43'),
(11, 1, 7, 'Dating stories for the LGBTQ+', 'your stories of finding dates, dating experience, stigmas, hits, misses and all that\'s in between. What would you look for while choosing a partner? How would you know if the partner is right for you? Would you go with your head or your heart while choosing a date and / or making a commitment? Articulate your decision-making process', 'A', '2022-04-23 14:12:08', '2022-04-23 14:12:08'),
(12, 1, 7, 'LGBTQ+ relationship', 'What is your story - of finding love and building a relationship? What challenges did you face and how did you deal with them? How are you building your plans for the future?', 'A', '2022-04-23 14:12:28', '2022-04-23 14:12:28'),
(20, 1, 10, 'Reflags in a relationship', 'What according to you are the red flags in a relationship? How to balance them with unwarranted scrutiny of your partner? What approaches have you tried to manage yourself and the relationship when you feel that you are seeing potential red flags?', 'A', '2022-04-23 14:22:37', '2022-04-23 14:22:37'),
(21, 1, 10, 'Abusive relationships', 'How would you view abuse in a relationship? What are your thresholds of tolerance, if any? What has your experience been with abuse? How are you dealing with yourself, your partner (kids if any) and the relationship? What choices are you evaluating in the context of your relationship? How did it all pan out, if you have been through it?', 'A', '2022-04-23 14:22:52', '2022-06-17 15:18:41'),
(22, 1, 10, 'Infidelity', 'How do you see infidelity in a relationship? Where do you draw the line? What has your experience been with infidelity? How did you deal with yourself, your partner, kids (if any) and the relationship post infidelity? What choices have you evaluated? How did it all pan out, if you have been through it?', 'A', '2022-04-23 14:24:10', '2022-06-17 15:19:09'),
(23, 1, 10, 'Second chances in a relationship', 'When would second chances be meaningful according to you? How are you ensuring that the key concerns of either party are well understood before taking a second chance? What according to you are the important elements of re-building the trust, love and respect? Share your story of a second chance in your relationship', 'A', '2022-04-23 14:24:34', '2022-06-17 15:19:31'),
(24, 1, 10, 'Stuck in a complex relationship', 'What is your story? What is keeping you here, what resources are you leaning on - What would tilt the balance one way or the other? What all is going through your mind? What are the aspects in your control and how are you evaluating your choices?', 'A', '2022-04-23 14:24:48', '2022-06-17 15:19:56'),
(25, 1, 10, 'Letting go and moving on', 'What is your story? How to know if it is time to let go - What factors would you consider while evaluating if a relationship has run its course? What according to you are the Dos and Don’ts right after separation / break-up - towards self preservation, dealing with the ex, dealing with the common friends etc.', 'A', '2022-04-23 14:25:13', '2022-06-17 15:20:18'),
(26, 1, 11, 'Ready for Divorce?', 'What is your story? What other options have you considered as an alternative to divorce? How would you evaluate if you are ready for divorce (talk about all the factors you would consider about yourself and the other stakeholders in the relationship)? How do you assess the impact of divorce and prepare yourself, your partner, your kids & other stakeholders (if any) for it?', 'A', '2022-04-23 14:25:41', '2022-06-17 15:21:18'),
(27, 1, 11, 'Going through Divorce', 'What is your story? What all is going through your mind? What resources are you leaning on in managing yourself and your loved ones? What is your approach towards engaging with your ex, partner (if any), kids (if any), other stakeholders while going through divorce? What strategies are you adapting to ensure that you do the right thing for you and your loved ones in this phase.', 'A', '2022-04-23 14:25:59', '2022-06-17 15:22:11'),
(28, 1, 11, 'Moving on after divorce', 'What have been the emotional phases you have experienced after divorce? How are you going about building your life after divorce? What are you excited, curious, confused or scared about? How would you approach new relationship(s) after divorce?', 'A', '2022-04-23 14:26:22', '2022-04-23 14:26:22'),
(29, 1, 12, 'Kids / Pets / Neither?', 'What are your thoughts on having kids? What is your understanding of your readiness for kids - emotional, physical, financial, time etc.? What are the views of other stakeholders, if any, and how are they impacting your decision? What other options are you considering, if not kids? How do you see the implications of your decision in the short, medium and long term?', 'A', '2022-04-23 14:26:45', '2022-06-17 15:23:31'),
(30, 1, 12, 'Biological kids Vs adoption Vs IVF', 'How are you evaluating these options? What are the underlying principles guiding these choices? How do you and your partner (if any) see the implications of these choices across various phases of your life?', 'A', '2022-04-23 14:32:36', '2022-06-17 15:24:00'),
(31, 1, 12, 'Single vs multiple kids / gap between kids', 'What are your thoughts on this? How are you going about making your choices - What evaluation criteria are you using? How do you and your partner (if any) see the implications of these choices across various phases of your life?', 'A', '2022-04-23 14:32:52', '2022-06-17 15:24:32'),
(32, 1, 13, 'Being a parent', 'Define being a parent. What does this mean to you, how does it impact you? What are the most rewarding and toughest aspects of being a parent? What according to you would be the steps needed in nurturing the bond with the child, across stages of life, for mutual wellbeing?', 'A', '2022-04-23 14:34:40', '2022-04-23 14:34:40'),
(33, 1, 13, 'Parenting a special child', 'What\'s your story? What are the beautiful and challenging aspects of your story? What steps are you taking to nurture a mutually enriching life? What resources are you leveraging? What steps are you taking in building a strong future for your child? How are you managing the relationship between the special child and the other child, if any? What is working?', 'A', '2022-04-23 14:34:59', '2022-06-17 15:26:47'),
(34, 1, 13, 'Mixed / Stepfamilies', 'Share your experience about being in a step / mixed family. What have you discovered about handling step relationships? How have you and your partner defined your respective roles in nurturing a mutually fulfilling relationship with the step kid(s)? What approaches have you taken in building a healthy stepfamily? What has worked / what hasn’t?', 'A', '2022-04-23 14:35:21', '2022-04-23 14:35:21'),
(35, 1, 13, 'Making your children \"listen\" to you', 'What is your parenting style? How do you balance between seeing things from the child\'s perspective (their needs / desires etc.) and your expectations? What changes did you make in your parenting style across various stages of your child\'s life? What is working, what isn’t?', 'A', '2022-04-23 14:35:44', '2022-06-17 15:27:29'),
(36, 1, 13, 'Teenage kids with boyfriends / girlfriends', 'What are your views on teenage romance? What according to you is healthy and what isn’t? When do you feel a need to intervene, if at all, and in what way? How do you relate to the lessons from your own teenage romance stories? What strategies have you followed in engaging with your teenager about their relationships? What has worked, what hasn\'t?', 'A', '2022-04-23 14:36:42', '2022-06-17 15:28:06'),
(37, 1, 13, 'Being a single mom', 'What are the beautiful and challenging aspects of your situation? How have you tried to plug the gap of the missing parent? What resources have your leveraged in your journey? How are you building a mutually fulfilling relationship? What is working, what isn\'t?', 'A', '2022-04-23 14:37:01', '2022-04-23 14:37:01'),
(38, 1, 13, 'Being a single dad', 'What are the beautiful and challenging aspects of your situation? How have you tried to plug the gap of the missing parent? What resources have your leveraged in your journey? How are you building a mutually fulfilling relationship? What is working, what isn\'t?', 'A', '2022-04-23 14:37:18', '2022-04-23 14:37:18'),
(39, 1, 13, 'When your kid lives away from you', 'What is your story? How are you working on retaining and strengthening the bond with your child? How are you dealing with the other stakeholders involved (Ex, caretakers etc.)? What is working? What isn’t?', 'A', '2022-04-23 14:37:38', '2022-04-23 14:37:38'),
(40, 1, 14, 'My relationship with my parents', 'What was your childhood like? What was your parent\'s style of parenting? How would you describe your parents\' individual life journeys with all their strengths, flaws, fears, aspirations, successes, and disappointments? What emotions do they evoke in you?', 'A', '2022-04-23 14:38:01', '2022-04-23 14:38:01'),
(41, 1, 14, 'My parents don’t get me', 'What\'s your story? How do you communicate with each other in your family? What are the usual flashpoints, what according to you is the source of these differences? How do you feel about your situation? What are you doing about it?', 'A', '2022-04-23 14:38:28', '2022-04-23 14:38:28'),
(42, 1, 14, 'Divorced parents', 'What\'s your story? How did your parents divorce impact your relationship with each of your parents? What resources did you leverage in your situation? How did your situation shape the other relationships in your life? How did you approach your situation. What have you learnt from your experience?', 'A', '2022-04-23 14:38:51', '2022-06-17 15:29:54'),
(43, 1, 17, 'Losing a dear one', 'What are your thoughts on death? Talk about your situation. What all is going through your mind? How are you dealing with it - what resources are you leaning on? How are you moving on with life and coping with aspects where the departed soul played a major role in your life? How has this experience altered, if at all, your perspectives on life, death, and the meaning of it all?', 'A', '2022-04-23 14:52:26', '2022-06-17 15:30:51'),
(48, 2, 19, 'What is the purpose / meaning of life?', 'Should life have a purpose? Have you found your purpose? Share your approach to finding your purpose - Perhaps a book, a course, a coach, a strategy, a philosophy? Does the purpose of life change with changing phases of life? Share your story', 'A', '2022-04-23 14:54:42', '2022-06-17 15:35:07'),
(49, 2, 20, 'Success', 'What is success to you at this moment in your life? Who is your role model and why? What is something you are currently doing that contributes to your success or share one instance in the past when you considered yourself successful? What is / was unique about this - the outcome, your mindset, your efforts, the people around you? Have you been able to define success for yourself? If yes, what is it?', 'A', '2022-04-23 14:55:18', '2022-06-17 15:36:43'),
(50, 2, 20, 'The Rat Race', 'Have you ever been in a rat race? How would you know if you are in one? What according to you are the factors (upbringing / society etc.) that force us into the rat race? What approaches, mindset or strategies have you tried to escape the rat race, if at all? What is the awareness you currently have to check on yourself to ensure that you don\'t get caught up in one?', 'A', '2022-04-23 14:56:03', '2022-06-17 15:37:08'),
(51, 2, 20, 'Life without Financial burden', 'How would you live your life if you didn’t have to worry about your financial needs? On what activities would you spend your time on a typical day and over your life time? What people will you be involved with? How is it different from your life today - talk about the key similarities and differences in these two lives', 'A', '2022-04-23 14:56:25', '2022-06-17 15:37:31'),
(53, 2, 21, 'Spirituality - Your definition', 'What is your idea of spirituality - talk about all that comes to your mind. What draws you to spirituality? Is there someone who is a personification of your idea of spirituality? What aspect of their life is most appealing to you? Can you attempt a one sentence definition of your idea of spirituality? How do you see your idea of spirituality, if achieved, impacting your life? How would you explain your idea of spirituality to a six year old?', 'A', '2022-04-23 14:57:08', '2022-06-17 15:38:43'),
(54, 2, 22, 'Movies / shows that moved you', 'What genre of movies / shows do you resonate most with? Has any movie moved you deeply - changed the way you see things / touched your heart / awakened your vision / opened the doors of your mind etc.? Talk about the movie / show - the most striking aspects, things that you resonated most with, its impact on you / your life etc. What does it say about who you are?', 'A', '2022-04-23 14:57:32', '2022-06-17 15:40:21'),
(55, 2, 23, 'Books that impacted you', 'What genre of books are you drawn to? Name the books that impacted you the most. What were your takeaways from this book? What new perspectives about life did this book help you build, if any? What do these books say about who you are?', 'A', '2022-04-23 14:57:49', '2022-06-17 15:41:09'),
(56, 2, 24, 'Poems and Poetry', 'What is your connection with poetry? Do you compose your own or do you like to read poetry? Is it a creative or emotional expression for you? Is writing poetry a conscious activity for you or does it flow through you under particular situations / circumstances? Talk about those situations or circumstances if any. Can you share your favourite poem and your association with it (emotions / memories etc.)', 'A', '2022-04-23 14:58:11', '2022-06-17 15:42:01'),
(57, 2, 25, 'Travel', 'What does travel mean to you? What is your idea of a perfect travel experience - Solo or the company / food / scenic beauty / culture / shopping / the activities/ heritage walks or just lazing? Talk about a travel experience that has stood out for you - What about the experience was special? Talk about a travel experience you are longing for - why?', 'A', '2022-04-23 14:58:31', '2022-06-17 15:42:44'),
(58, 2, 26, 'Risks you have taken', 'What are the 3 biggest risks you have ever taken in your life? What frame of mind were you in when you took those risks, what calculations did you make, if any? How did these impact your life - Which of these played out well, which didn’t? How have these experiences shaped your personality?', 'A', '2022-04-23 14:58:58', '2022-06-17 15:44:56'),
(59, 3, 30, 'Gender identity', 'What are your views on evolving gender identities? What has been your journey of discovering your identity, comparing it against the societal norms and choosing your way of life? What were some of the most reliable aids and stubborn obstructions in your journey?', 'A', '2022-04-23 14:59:33', '2022-04-23 14:59:33'),
(60, 3, 31, 'Self-Image', 'What are the most prominent constituents of your self-image, the way you see it (physical appearance, gender, your job etc.)? How are these prominent constituents of your self-image aiding / diminishing your engagement with the world around you? How would the people who genuinely love you describe you? What has been your approach towards building an image that you aspire to achieve? What is working, what is not?', 'A', '2022-04-23 14:59:54', '2022-06-17 14:39:50'),
(61, 3, 32, 'Imposter Syndrome', 'What is your understanding of Imposter Syndrome? How would you know if you are under the grip of this? What approaches have you tried to address this? What resources have you leaned on? What has worked? What hasn’t?', 'A', '2022-04-23 15:00:27', '2022-06-17 14:40:40'),
(62, 3, 33, 'Vulnerability', 'What situations, aspects of yourself or people make you feel the most vulnerable? When was the last time you felt most vulnerable? What emotions, physical changes do you experience when you feel vulnerable? What all approaches have you tried in dealing with your vulnerabilities? What resources, have you leveraged? What has worked? What hasn’t?', 'A', '2022-04-23 15:00:51', '2022-04-23 15:00:51'),
(63, 3, 34, 'Shame', 'What are the triggers for your shame? How do you feel when your shame is triggered? What is your understanding of the sources of shame? What approaches have you tried in overcoming your shame - Compassion towards self, affirmations, focusing on your positive aspects etc.? What has worked, what hasn\'t?', 'A', '2022-04-23 15:01:09', '2022-06-17 14:41:49'),
(64, 3, 35, 'Childhood patterns', 'According to Hoffman institute, we pick up many of our patterns of behaviour from our childhood including beliefs, perceptions, judgements, needs and desires about - how we get love and approval, what life is about, how to relate to others, what is spirituality and the role of work and family. What patterns have you picked up from your childhood? Which of these are you thankful for you in your adult life? Which of them might need a reset?', 'A', '2022-04-23 15:01:26', '2022-04-23 15:01:26'),
(65, 3, 36, 'Fear (Financial)', 'What is your biggest fear with finances? What could be the worst-case scenario? How real and bad does it look? What strengths / positive attributes / resources are you leaning on in fighting your fears? What approaches have you tried in dealing with this fear? What is working?', 'A', '2022-04-23 15:09:59', '2022-04-23 15:09:59'),
(66, 3, 36, 'Fear (Abandonment)', 'Talk about your fear. What could be the worst-case scenario? How real and bad does it look? What strengths / positive attributes / resources are you leaning on in fighting your fears? What approaches have you tried in dealing with this fear? What is working?', 'A', '2022-04-23 15:10:18', '2022-04-23 15:10:18'),
(67, 3, 36, 'Fear (Health)', 'Talk about your fear. What could be the worst-case scenario? How real and bad does it look? What strengths / positive attributes / resources are you leaning on in fighting your fears? What approaches have you tried in dealing with this fear? What is working?', 'A', '2022-04-23 15:10:37', '2022-04-23 15:10:37'),
(68, 3, 36, 'Fear (of judgement)', 'Talk about your fear. What could be the worst-case scenario? How real and bad does it look? What strengths / positive attributes / resources are you leaning on in fighting your fears? What approaches have you tried in dealing with this fear? What is working?', 'A', '2022-04-23 15:10:56', '2022-04-23 15:10:56'),
(69, 3, 36, 'Fear (Others)', 'Talk about your fear. What could be the worst-case scenario? How real and bad does it look? What strengths / positive attributes / resources are you leaning on in fighting your fears? What approaches have you tried in dealing with this fear? What is working?', 'A', '2022-04-23 15:11:14', '2022-04-23 15:11:14'),
(70, 3, 37, 'Guilt', 'What do you feel most guilty of - share your story? How is it impacting you? What is your understanding of the source of this guilt - need for control, expectations, attachments etc.? What approaches have you tried in dealing with guilt? What has worked, what hasn\'t?', 'A', '2022-04-23 15:11:30', '2022-06-17 14:45:07'),
(71, 3, 38, 'Anger', 'What has been your relationship with Anger as an emotion? How has it impacted your life? What are the typical triggers for your anger and who is it usually directed towards (internal or external)? How are you dealing with your anger? What is working, what isn’t?', 'A', '2022-04-23 15:11:48', '2022-06-17 14:45:29'),
(73, 3, 40, 'Forgiveness', 'How do you deal with forgiveness (Self and others) - does it come easy or do you struggle? If there is a struggle, share the aspects that cause this (event, person impacted, timing etc.). Talk about some of your biggest battles with forgiveness - the impact it has had on your emotions, thoughts, physical wellbeing etc. What approaches have you tried in dealing with forgiveness? What has worked? What hasn\'t?', 'A', '2022-04-23 15:12:34', '2022-06-17 14:46:55'),
(84, 3, 62, 'Dealing with Critical illness (Cancer)', 'What has been your journey from the beginning of symptoms to diagnosis and present day? What treatments have you tried? What has been your experience with the healthcare infrastructure - doctors, hospitals, alternate treatment centres etc.? What resources have you leaned on for help (family, friends, websites, information sources etc.)? What has been your emotional and spiritual journey? What have you learnt about yourself and life in this process?', 'A', '2022-06-17 13:29:16', '2022-06-17 13:46:08'),
(85, 3, 62, 'Dealing with Critical illness (Heart related)', 'What has been your journey from the beginning of symptoms to diagnosis and present day? What treatments have you tried? What has been your experience with the healthcare infrastructure - doctors, hospitals, alternate treatment centres etc.? What resources have you leaned on for help (family, friends, websites, information sources etc.)? What has been your emotional and spiritual journey? What have you learnt about yourself and life in this process?', 'A', '2022-06-17 13:44:04', '2022-06-17 13:44:04'),
(86, 3, 62, 'Dealing with Critical illness (Kidney & Liver related)', 'What has been your journey from the beginning of symptoms to diagnosis and present day? What treatments have you tried? What has been your experience with the healthcare infrastructure - doctors, hospitals, alternate treatment centres etc.? What resources have you leaned on for help (family, friends, websites, information sources etc.)? What has been your emotional and spiritual journey? What have you learnt about yourself and life in this process?', 'A', '2022-06-17 13:44:28', '2022-06-17 13:44:28'),
(87, 3, 62, 'Dealing with Critical illness (Brain related)', 'What has been your journey from the beginning of symptoms to diagnosis and present day? What treatments have you tried? What has been your experience with the healthcare infrastructure - doctors, hospitals, alternate treatment centres etc.? What resources have you leaned on for help (family, friends, websites, information sources etc.)? What has been your emotional and spiritual journey? What have you learnt about yourself and life in this process?', 'A', '2022-06-17 13:44:53', '2022-06-17 13:44:53'),
(88, 3, 62, 'Dealing with Critical illness (Others)', '(highlight the illness in the Your Take section) What has been your journey from the beginning of symptoms to diagnosis and present day? What treatments have you tried? What has been your experience with the healthcare infrastructure - doctors, hospitals, alternate treatment centres etc.? What resources have you leaned on for help (family, friends, websites, information sources etc.)? What has been your emotional and spiritual journey? What have you learnt about yourself and life in this process?', 'A', '2022-06-17 13:45:29', '2022-06-17 13:45:29'),
(89, 3, 61, 'Alcohol', 'What has been your equation with alcohol? How has it impacted your life? What stage are you at, in terms of dealing with the habit? What approaches are you taking? What is working? What isn’t?', 'A', '2022-06-17 13:52:39', '2022-06-17 13:52:39'),
(90, 3, 61, 'Smoking', 'What has been your equation with smokingl? How has it impacted your life? What stage are you at, in terms of dealing with the habit? What approaches are you taking? What is working? What isn’t?', 'A', '2022-06-17 13:53:06', '2022-06-17 13:53:06'),
(91, 3, 61, 'Pornography', 'What is your pornography habit story? How has it impacted your life? What stage are you at, in terms of dealing with the habit? What approaches are you taking? What is working? What isn’t?', 'A', '2022-06-17 13:55:17', '2022-06-17 13:55:17'),
(92, 3, 61, 'Gambling', 'What is your gambling story? How has it impacted your life? What stage are you at, in terms of dealing with this habit? What approaches are you taking? What is working? What isn’t?', 'A', '2022-06-17 13:55:36', '2022-06-17 13:55:36'),
(93, 3, 61, 'Others bad habits', '(highlight the habit in the Your Take section) - What is your story? How has it impacted your life? What stage are you at, in terms of dealing with the habit? What approaches are you taking? What is working? What isn’t?', 'A', '2022-06-17 13:55:56', '2022-06-17 13:55:56'),
(94, 3, 59, 'Self-Love', 'What are your thoughts on self love and compassion towards self? How do you differentiate between self love and selfishness? How would you rate yourself on self love and what is your desired rating (Explain)? What approaches are you planning to take to bridge the gap between where you are and where you want to be on self love?', 'A', '2022-06-17 14:42:43', '2022-06-17 14:42:43'),
(95, 3, 60, 'Chatter in the mind', 'many of us hear this constant chatter in our head, an on-going background noise. What kind of an ongoing chatter do you hear in your head - is there a repetitive pattern or is it just random noise? How does this constant chatter affect you? What approaches have you tried to calm your mind? What is working, what isn\'t?', 'A', '2022-06-17 14:46:11', '2022-06-17 14:46:11'),
(96, 3, 63, 'Caring for critically ill', 'Talk about the person you were caring for, the nature of illness and your role. What has been your approach to the various adjustments needed in the physical, emotional and financial set up, if any? What specific challenges did you face and how did you deal with them? How did you balance all these with your own needs? What have you learnt about yourself and life as a whole?', 'A', '2022-06-17 14:47:36', '2022-06-17 14:47:36'),
(97, 1, 54, 'Marriage Vs live-in Vs series of relationships', 'What does a relationship mean to you? How do you assess if a particular kind of relationship is best suited given your value systems, emotional / mental makeup and / or your aspirations from life? How do the views of family / society, impact your choices, if at all?', 'A', '2022-06-17 14:53:46', '2022-06-17 14:53:46'),
(98, 1, 54, 'Remaining single Vs being in a relationship', 'What is your understanding of your relationship needs? What are the great / not so great aspects of these choices? What are the trade-offs? What would be the deciding factors in choosing one vs the other? How do the views of family / society, impact your choices, if at all?', 'A', '2022-06-17 14:54:11', '2022-06-17 14:54:11'),
(99, 1, 55, 'Space in a relationship', 'What is your definition of space in the context of your wellbeing? How do you create space and establish healthy boundaries in a relationship? How do you communicate your needs with your partner? When do you compromise, if at all?', 'A', '2022-06-17 15:13:41', '2022-06-17 15:13:41'),
(100, 1, 55, 'Relationship across cultures', '(religion, language, community, lifestyle, age etc.) - How do you see such differences impacting you and the other important people in your life? What steps are you taking / planning to take to manage the impact of these differences on the long-term wellbeing of the relationship? What other thoughts are going through your mind?', 'A', '2022-06-17 15:14:04', '2022-06-17 15:14:04'),
(101, 1, 55, 'Disagreements/ fights in a relationship', 'What is your perspective on fights in a relationship? How do you resolve conflicts, especially the severe ones? How would you ensure that a fight doesn’t damage an otherwise healthy relationship? How would you differentiate between a regular fight vs a potential red flag?', 'A', '2022-06-17 15:14:22', '2022-06-17 15:14:22'),
(102, 1, 55, 'Dealing with in-laws', 'What has been your experience with your in-laws? What according to you are the key levers (E.g. Communication on mutual expectation, boundary setting, role of the spouse etc.) in managing this relationship under various circumstances - E.g. Short sporadic interactions Vs living with your in-laws. Share what has worked and what hasn’t?', 'A', '2022-06-17 15:14:41', '2022-06-17 15:14:41'),
(103, 1, 55, 'Intimacy in a relationship', 'What are your thoughts on the role of intimacy in a relationship? Share your story. What approaches have you taken in striking a right balance with your partner on your respective intimacy needs? What has worked, what hasn’t?', 'A', '2022-06-17 15:15:00', '2022-06-17 15:15:00'),
(104, 1, 55, 'Long distance relationships', 'What is your story? How do you see long distance relationships? What approaches have you tried in building your relationship given the constraints of space and time? What according to you are the dos and don\'ts in making long distance relations work?', 'A', '2022-06-17 15:15:28', '2022-06-17 15:15:28'),
(105, 1, 55, 'Roles in running a family', 'What\'s your perspective on roles to be played by you, your partner and the other family members in running a family? Which of these roles are fungible, which aren’t? What is your family\'s approach in supporting each other through tough times? What is working, what isn’t?', 'A', '2022-06-17 15:15:46', '2022-06-17 15:15:46'),
(106, 1, 55, 'Open Marriages', 'What is your idea of a marriage? What according to you are the roles of partners in an open marriage? What are the boundary conditions if any? What is the mental / emotional make-up needed to be able to make an open marriage work according to your experience? What are some of the pitfalls that one should be careful about in such relationships?', 'A', '2022-06-17 15:16:02', '2022-06-17 15:16:02'),
(107, 1, 12, 'Pregnancy and postpartum journey', 'How do you view pregnancy? At what stage of pregnancy and postpartum journey are you in - How has the run been so far? What were some of the best and toughest parts of your journey? What resources have you leveraged to make the journey smoother? If you had to go through the journey again, what would you do differently, if any?', 'A', '2022-06-17 15:25:35', '2022-06-17 15:25:35'),
(108, 1, 57, 'My equation with my boss', 'What is the Business Chemistry of your boss and that of you? Talk about the nature of your relationship - aspects that make you feel good (valued, empowered etc.) and not so good (unappreciated, criticised etc.); areas of alignment and disagreement. Which of these, are justified / fair and which aren’t? What steps have you taken to work on yourself and on the relationship? What has worked, what hasn\'t? (Refer to Deloitte\'s Business Chemistry).', 'A', '2022-06-17 15:31:52', '2022-06-17 15:31:52'),
(109, 1, 57, 'My equation with my workplace', 'How do you feel at your current workplace? How would you assess the fitment of your skills with your current role, your values with the organization culture and your chemistry with the immediate colleagues? What is keeping you going at your current organization (earning, learning, parking, others?) What is making you evaluate a move, if at all (Push, Pull, others?)? What would your criteria be in choosing a workplace?', 'A', '2022-06-17 15:32:14', '2022-06-17 15:32:14'),
(110, 1, 57, 'Unwanted attention at workplace', 'What is your story? What steps have you taken, planning to take in your situation? What resources are you leaning on? What is working, what isn’t?', 'A', '2022-06-17 15:32:37', '2022-06-17 15:32:37'),
(111, 1, 57, 'Romantic relationships at workplace', 'What are your views on workplace relationships? What has been your experience? What has been your approach in managing workplace romances? What is working?', 'A', '2022-06-17 15:32:54', '2022-06-17 15:32:54'),
(112, 2, 21, 'Spirituality - In your daily life', 'What is spirituality for you? How have you inculcated spirituality within your daily life - In dealing with your day to day challenges, setbacks, major life events, emotions, aspirations, fears, etc.? Is there a particular practice or approach that has helped you in managing your day-to-day life? Talk about it', 'A', '2022-06-17 15:39:10', '2022-06-17 15:39:10'),
(113, 2, 21, 'Spirituality - Your journey', 'What is spirituality for you? What draws you to spirituality? What according to you is a spiritual path? Where are you on that journey? Can you share any noteworthy experience from your spiritual journey so far? Is there something that you’ve heard of, are considering, or want to experience in the realm of spirituality - book, a retreat, a course, travel etc.? How do you believe this experience would enhance your spiritual journey?', 'A', '2022-06-17 15:39:30', '2022-06-17 15:39:30'),
(114, 2, 58, 'Relationship with Time', 'What does time mean to you? What is your relationship with time (always in time, struggling to make time, no sense of time, something else)? How do you interact with those who have a different relationship with time than yours (Contempt, fear, admiration, others)? What do you wish you had more time for? When does time move the fastest for you? When does it move the slowest? What time of the day do you like most? Why? What does your relationship with time say about who you are?', 'A', '2022-06-17 15:43:42', '2022-06-17 15:43:42'),
(115, 2, 58, 'Thought zone', 'Past, Present, Future - Do you tend to live in the past, present or future? On an average, what is the percentage of time you spend thinking about your past, present and future? What are some of the triggers that take you to any of these zones? What are the emotions you experience when you are in each of these zones? Have you tried to change these thought zones? If yes, which one and how? What has worked, what hasn\'t?', 'A', '2022-06-17 15:43:59', '2022-06-17 15:43:59'),
(116, 2, 26, 'Different lives you have lived', 'Aspects of life such as people, places, ideas, dreams, aspirations, mindsets etc. often tend to define a particular phase of our life. For some, these do not change very frequently, for yet others, they change so dramatically that they may appear to be a few different lives lived. How many different lives have you lived? Briefly talk about each of them. How have these experiences shaped your personality', 'A', '2022-06-17 15:45:18', '2022-06-17 15:45:18'),
(117, 2, 26, 'Your life as a Movie', 'If your life were to be made into a movie, what would it be called? Would it be a series /documentary/feature film? Talk about the movie - The Genre, High level plot, Villains / Heroes, The climax / the anticipated climax', 'A', '2022-06-17 15:45:53', '2022-06-17 15:45:53'),
(118, 2, 26, 'Your Legacy', 'What non-material, non-financial legacy would you want to leave behind for your friends, family or the world? What impact do you want your life to have had on your loved ones? What is the story your great grandchildren are going to hear about your life? In what ways can you leave behind your memories and shared experiences that made up your life?', 'A', '2022-06-17 15:46:12', '2022-06-17 15:46:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_name` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `gender` enum('M','F','O','N','D') COLLATE utf8mb4_unicode_ci DEFAULT 'M',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_from` enum('S','F','G') COLLATE utf8mb4_unicode_ci DEFAULT 'S',
  `provider_id` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('I','A','U','D') COLLATE utf8mb4_unicode_ci DEFAULT 'U',
  `is_email_verified` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `is_phone_verified` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `phone_vcode` int(11) DEFAULT NULL,
  `email_vcode` int(11) DEFAULT NULL,
  `temp_phone` bigint(20) DEFAULT NULL,
  `temp_email` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_me` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `describe_you` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holidays` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holidays_other` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `problem_solve` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualities_you` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `year_of_birth` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` enum('U','M') COLLATE utf8mb4_unicode_ci DEFAULT 'U',
  `profession` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memories` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_headline` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_link` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_code` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_id` varchar(299) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_login` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `tot_hearts` bigint(20) DEFAULT NULL,
  `tot_conversation_completed` bigint(20) DEFAULT 0,
  `tot_person_talked` bigint(20) DEFAULT 0,
  `tot_connects` bigint(20) DEFAULT NULL,
  `conversation_balance_total` bigint(20) NOT NULL DEFAULT 0,
  `conversation_balance_free` bigint(20) NOT NULL DEFAULT 0,
  `conversation_balance_paid` bigint(20) NOT NULL DEFAULT 0,
  `socket_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `profile_name`, `user_id`, `email`, `phone`, `gender`, `image`, `email_verified_at`, `password`, `remember_token`, `signup_from`, `provider_id`, `status`, `is_email_verified`, `is_phone_verified`, `phone_vcode`, `email_vcode`, `temp_phone`, `temp_email`, `about_me`, `describe_you`, `holidays`, `holidays_other`, `problem_solve`, `qualities_you`, `country`, `year_of_birth`, `marital_status`, `profession`, `memories`, `profile_headline`, `media_link`, `ref_code`, `unique_id`, `is_login`, `tot_hearts`, `tot_conversation_completed`, `tot_person_talked`, `tot_connects`, `conversation_balance_total`, `conversation_balance_free`, `conversation_balance_paid`, `socket_id`, `created_at`, `updated_at`) VALUES
(1, 'soumojit Dev', 'soumojit Dev', 'soumojit Dev', NULL, 'ylanesdeveloper@gmail.com', 9804459517, 'M', '112346990698943502378.jpg', NULL, NULL, NULL, 'G', '112346990698943502378', 'A', 'Y', 'Y', NULL, 576689, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2006', NULL, NULL, NULL, NULL, NULL, 'YL00001', 'Y001', '1', NULL, 2, 3, NULL, 400, 400, 0, '9538.20144784', '2022-06-09 11:11:38', '2022-06-09 20:03:07'),
(2, 'Soumo One', 'Soumo One', 'Soumo One', 'US0000002', 'vokosot865@iconzap.com', 2131234561, 'M', '2.1654867148.jpg', NULL, '$2y$10$ATfvR7NvbLOf0gX/Zamlo.Cj/81sAe.bZ.IAPXdGbidAfyCZattAO', 'Mq3aV1vmGBq0sEYY8GOGs8fEpjyV9hlO2OsHAxUWgRKYq0GIYuXFtAnAv7Ak', 'S', NULL, 'A', 'Y', 'Y', 144002, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2006', NULL, NULL, NULL, NULL, NULL, 'YL00002', 'Y002', '1', NULL, 6, 8, 1, 800, 300, 500, '9533.23243336', '2022-06-09 11:28:24', '2022-06-14 18:39:30'),
(3, 'Soumojit Two', 'Soumojit Two', 'Soumojit Two', 'US0000003', 'kolak86453@falkyz.com', 1236457890, 'M', '3.1656591772.jpg', NULL, '$2y$10$XKXj66mG2AefjRZUKSHvE.41vYozarndPSvHOx6YhvYONwg1SYpTy', 'Y5sTRokj8xebbLQivsD6u5lt3SyeejkX5p7m784o3XktNwaLJKgImcr4ZjzU', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2006', NULL, NULL, NULL, NULL, NULL, 'YL00003', 'Y003', '1', NULL, 1, 2, 0, 200, 200, 0, '9533.34183892', '2022-06-09 12:51:54', '2022-06-30 18:04:36'),
(4, NULL, NULL, 'SOUMO80', 'US0000004', 'piyoha7933@iconzap.com', 9804559570, 'M', NULL, NULL, '$2y$10$9zP7fFYqCdIA04wq.c3iiutMsH/2le.8BX6gxIhBjJgtZqnvPyrpC', 'tlUTPFcOooTtFSqzgriMHQ2OImD7HRg7CeAtz1gdIx0CD0IRhV2tZcV2FIGJ', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00004', 'Y004', '0', NULL, 0, 0, NULL, 1000, 1000, 0, '9508.19938315', '2022-06-09 14:46:16', '2022-06-09 14:47:07'),
(5, 'Soumo', 'Soumo', 'Soumo', 'US0000005', 'soumojit.sad@gmail.com', 1234567890, 'M', '5.1656590541.jpg', NULL, '$2y$10$LDVq3Dk7dPNlCIbt0fv6cOTeF7wK.jSFZ3rHMtVj3TzSeyb3g48o2', 'rshVJ0Nx6r8KK85DKPbV1EzKD8P1HXfgnBP6O0eUOMV27cyK9YkiXERTRUPM', 'S', NULL, 'A', 'Y', 'Y', 254531, 254531, NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the', NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the', NULL, 1, '2001', NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the', NULL, 'YL00005', 'Y005', '1', NULL, 4, 4, 1, 800, 600, 200, '9603.5627979', '2022-06-09 16:48:21', '2022-07-09 10:23:43'),
(6, 'sales.convergen', 'sales.convergen', 'sales.convergen', NULL, 'sales.convergentinfoware@gmail.com', 1234567891, 'F', '116130884492924363285.jpg', NULL, NULL, NULL, 'G', '116130884492924363285', 'A', 'Y', 'Y', NULL, 529708, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, '2003', NULL, NULL, NULL, NULL, NULL, 'YL00006', 'Y006', '1', NULL, 0, 0, NULL, 600, 600, 0, '9543.20865629', '2022-06-10 15:59:47', '2022-06-10 21:22:58'),
(7, 'user1', 'user1', 'user1', 'US0000007', 'user1@infoware-in.com', 1234567892, 'N', '7.1654934468.jpg', NULL, '$2y$10$XpfCoWbagFgHjdQ0L1INguGpZRHXz6BX0pxi6HdSBgNQtpVdjH9L.', 'f2Ji02rf5nc0GMGEzmChZ9ZlbgW0z29Ux4whe1LoXrd0bQ7mz4NHOUwU9ie1', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, 'your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry your l;ife stiry', 'this is test', '[\"Mountains\"]', NULL, 'hunger', 'qualities i admire', 101, '2005', NULL, NULL, 'your l;ife stiry your l;ife stiry your l;ife stiry', 'prpofile headline', NULL, 'YL00007', 'Y007', '1', NULL, 0, 0, 0, 1700, 700, 1000, '9511.32599128', '2022-06-11 13:30:28', '2022-06-28 12:32:24'),
(8, 'user2', 'user2', 'user2', 'US0000008', 'user2@infoware-in.com', 1234567893, 'F', '8.1654934585.jpg', NULL, '$2y$10$UBMqczaxTptg3090jFnyFuKFIWQY1qodpRqOBZbc5nHW3w3OzA6Ry', 'xUrmfDp6UNA8UqSc3QKxlbP3qpIkadAFo3ZJ3GyPtoR4tuj3LZPCMFDDsS4t', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, '2003', NULL, NULL, NULL, NULL, NULL, 'YL00008', 'Y008', '1', NULL, 1, 2, 0, 300, 100, 200, '9546.22761115', '2022-06-11 13:32:44', '2022-06-13 21:04:03'),
(9, 'user3', 'user3', 'user3', 'US0000009', 'user3@infoware-in.com', 1234567894, 'D', '9.1654934673.jpg', NULL, '$2y$10$jh/nwJJKrIRDqwKB2hAbwOLPQk8ZfhLoZryMqO9KofM0g.rlwsJP.', 'SBuPEew62osAIivVy1BsBaJTM7XYORiGoLVUvOleD7cZlqq53lbcAzBal6ka', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, '1999', NULL, NULL, NULL, NULL, NULL, 'YL00009', 'Y009', '1', NULL, 1, 2, NULL, 400, 400, 0, '9514.26003162', '2022-06-11 13:34:07', '2022-06-18 16:00:30'),
(10, 'SSMOB TEST', 'SSMOB TEST', 'SSMOB TEST', 'US0000010', 'hemixa8694@syswift.com', 7980768000, 'M', NULL, NULL, '$2y$10$McUknDrtvsLQvs54ayWGiOQpnXPHCsIBaoL6c8SfTtOsTrg3m/psa', NULL, 'S', NULL, 'A', 'N', 'Y', NULL, 922303, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, '2003', NULL, NULL, NULL, NULL, NULL, 'YL00010', 'Y010', '1', NULL, 0, 0, 0, 750, 150, 600, '9537.34040200', '2022-06-14 18:36:56', '2022-06-30 14:41:05'),
(11, NULL, NULL, 'Soumo', 'US0000011', 'fekar62304@serosin.com', 7980768466, 'M', NULL, NULL, '$2y$10$bYXWW/Q7RTDb075MrjhTweK83sNawArceMbt/Kl9Zc13kRd3jsiz2', NULL, 'S', NULL, 'U', 'N', 'Y', 219672, 270911, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL, 'YL00011', 'Y011', '1', NULL, 0, 0, NULL, 1000, 1000, 0, '9543.25221557', '2022-06-17 12:51:18', '2022-06-17 12:54:51'),
(12, 'aroy', 'aroy', 'aroy', 'US0000012', 'aroy@infoware-in.com', 9830109208, 'N', NULL, NULL, '$2y$10$cCABGitJYPDtyY7axM1SQeACdUK0AK/CSKVXyvamfQPZv29Yy.Ilq', 'y5k2uGwDoE1Xmfdnp8QDJAqZQRoEhEW8swcIN3NEwYLvxJqJkpHw0E06oQVk', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, '2001', NULL, NULL, NULL, NULL, NULL, 'YL00012', 'Y012', '1', 5, 1, 1, 1, 200, 200, 0, '9637.5332169', '2022-06-17 13:57:17', '2022-07-08 19:58:03'),
(13, 'Rajesh', 'Rajesh', 'Rajesh', 'US0000013', 'Rajesh.irk@gmail.com', 9820043174, 'M', '13.1655548594.jpg', NULL, '$2y$10$8oUQd7iZq8lVZZ4FxLadH.j8gIRIjkYO6F6KK9OUQqi4F9zeAKxbS', NULL, 'S', NULL, 'A', 'N', 'Y', 491390, NULL, NULL, NULL, NULL, 'Patient, Creative, Compassionate', '[\"Mountains\"]', NULL, 'Hunger', 'Hardwork', 101, '1976', NULL, NULL, 'adfdddfdfdfddadsadsfdafasdfasdfasdfd asdfsdfasdfadsfdsafdsaf asdfadfadsfadfsafdsfd asdfasdfsdafdsdfasdf  asdfasddasf asdfd', 'Divorcee, remarried and currently in a mixed family', 'https://www.linkedin.com/in/rajeshivaturi/', 'YL00013', 'Y013', '1', 5, 1, 1, NULL, 2100, 1100, 1000, '9602.3280665', '2022-06-18 16:01:57', '2022-07-06 18:34:26'),
(14, NULL, NULL, NULL, NULL, 'amatra.natekar@gmail.com', NULL, 'M', '100319093174581398091.jpg', NULL, NULL, NULL, 'G', '100319093174581398091', 'A', 'Y', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00014', 'Y014', '0', NULL, 0, 0, NULL, 1000, 1000, 0, NULL, '2022-06-19 15:34:15', '2022-06-19 15:34:15'),
(15, NULL, NULL, 'SSSoumojit', 'US0000015', 'cawoba8020@hekarro.com', 9804559517, 'M', NULL, NULL, '$2y$10$CPlNX0IoSBdsUB6fzLOCte1HHo7CxNQhKF9fsWUqU4V1aoFpkgzDO', NULL, 'S', NULL, 'A', 'N', 'Y', 662812, 512795, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00015', 'Y015', '0', NULL, 0, 0, NULL, 1000, 1000, 0, '9489.34049798', '2022-06-30 14:47:57', '2022-07-06 15:36:52'),
(16, NULL, NULL, 'DP', 'US0000016', 'deeptipunjabi79@gmail.com', 8747874000, 'M', NULL, NULL, '$2y$10$0PMUz.hjtJxnY9Qt5qkDGOc6nN9VWRrmhEDbJfoE42zjW6W41uYqK', 'F6XWJLiUbZ55B9S7tV8bcWjzCp1pBqFrrkzfORZcvo06o0D2guMne6Q0EeAV', 'S', NULL, 'D', 'Y', 'N', 254730, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00016', 'Y016', '0', NULL, 0, 0, NULL, 1000, 1000, 0, NULL, '2022-07-05 19:52:06', '2022-07-05 20:56:47'),
(17, NULL, NULL, 'PK', 'US0000017', 'drpoojakukreja@gmail.com', 9833986112, 'M', NULL, NULL, '$2y$10$80WC9zQS4l2cbfv/u4Ykhex6Y76KRwXkCOtYyAjC4dYYsSTg3dZQ2', NULL, 'S', NULL, 'A', 'N', 'Y', NULL, 458962, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00017', 'Y017', '0', NULL, 0, 0, NULL, 1000, 1000, 0, '9588.3266484', '2022-07-05 20:38:40', '2022-07-05 20:42:54'),
(18, NULL, NULL, 'DP', 'US0000018', 'deeptipunjabi79@gmail.com', 8747874000, 'M', NULL, NULL, '$2y$10$e.mxx7nzBUVx2yOcUf6Nu.gDctgQHx2f5UTBwuCwBVJToMbFKN1xu', 'MiD49i55KNOd5rnY4bFcoM702frGSTSlkYSHxADjDQl3H2nyM5vElrkUHK5Q', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL, 'YL00018', 'Y018', '1', NULL, 0, 0, NULL, 1000, 1000, 0, '9584.3778510', '2022-07-05 20:57:46', '2022-07-06 16:10:27'),
(19, 'Rajesh', 'Rajesh', 'Rajesh', 'US0000019', 'rajesh.ivaturi@ylanes.com', 9324323577, 'M', NULL, NULL, '$2y$10$fxQdkiw.Y/vnm8zvDU.IQOkQjKokFpXpvu.1orJHQuJUmDQUC1X2O', '9yKpIReTIMFwFz85HH3QGygJ4kSioHhi96oSRPaNKQRLlNkn6zyBqIdf3y0Z', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, 'ASASDFASDF', 'Patient, Creative, Compassionate', '[\"Mountains\"]', NULL, 'Hunger', 'Hardwork', 101, '1976', NULL, NULL, NULL, NULL, NULL, 'YL00019', 'Y019', '1', NULL, 0, 0, NULL, 1000, 1000, 0, '9651.2717665', '2022-07-05 21:14:11', '2022-07-09 01:20:10'),
(20, NULL, NULL, 'roy two', 'US0000020', 'roy2@infoware-in.com', 8334987443, 'M', NULL, NULL, '$2y$10$NbDoJyTvpXjOPXC9je6JXeleXQ9mRHCCYOtL9lL1UljDyVIFedP7G', 'dXQ5mnYAA4Lgo99QCXoy12NNfD1oBmASLyix093ERKR0KddDcbIjBdpieLLp', 'S', NULL, 'A', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL, 'YL00020', 'Y020', '1', NULL, 0, 0, NULL, 1000, 1000, 0, '9631.5333626', '2022-07-06 14:06:37', '2022-07-08 20:00:17'),
(21, NULL, NULL, 'Soumo', 'US0000021', 'gamas84099@hekarro.com', 7980768406, 'M', NULL, NULL, '$2y$10$Rk3EMYMOcbGeons8sxyKvuehkqhZoK9S9sFrk2Q9mkAt6Za6zw3tu', NULL, 'S', NULL, 'A', 'N', 'Y', 908866, 610077, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00021', 'Y021', '0', NULL, 0, 0, NULL, 1000, 1000, 0, '9587.3840646', '2022-07-06 16:00:55', '2022-07-06 17:38:11'),
(22, NULL, NULL, 'SSo', 'US0000022', 'dejayec131@weepm.com', 2013345661, 'M', NULL, NULL, '$2y$10$XOI82zFAifSG7PqB6hJTyu14M6v8prJU7Fw95.SRiEUbNwrdFZfrm', NULL, 'S', NULL, 'U', 'N', 'N', 658755, 110351, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, 'U', NULL, NULL, NULL, NULL, 'YL00022', 'Y022', '0', NULL, 0, 0, NULL, 1000, 1000, 0, NULL, '2022-07-09 10:28:34', '2022-07-09 10:28:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_package_purchase`
--

CREATE TABLE `user_package_purchase` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `conv_free` int(11) DEFAULT NULL,
  `conv_paid` int(11) DEFAULT NULL,
  `payment_master_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_package_purchase`
--

INSERT INTO `user_package_purchase` (`id`, `user_id`, `package_id`, `amount`, `conv_free`, `conv_paid`, `payment_master_id`, `created_at`, `updated_at`) VALUES
(1, 2, 11, '250.00', 100, 200, 1, '2022-06-10 18:55:59', '2022-06-10 18:55:59'),
(2, 2, 3, '1000.00', 200, 500, 2, '2022-06-10 18:58:18', '2022-06-10 18:58:18'),
(3, 8, 11, '250.00', 100, 200, 3, '2022-06-13 21:03:28', '2022-06-13 21:03:28'),
(4, 5, 3, '1000.00', 200, 500, 4, '2022-06-14 18:41:59', '2022-06-14 18:41:59'),
(5, 7, 4, '2000.00', 500, 1000, 5, '2022-06-21 23:59:30', '2022-06-21 23:59:30'),
(6, 13, 4, '2000.00', 500, 1000, 6, '2022-06-28 13:06:34', '2022-06-28 13:06:34'),
(7, 5, 12, '200.00', 0, 200, 7, '2022-06-29 14:31:53', '2022-06-29 14:31:53'),
(8, 5, 16, '2000.00', 500, 2000, 8, '2022-06-29 19:18:58', '2022-06-29 19:18:58'),
(9, 10, 14, '1000.00', 150, 1000, 9, '2022-06-29 19:23:41', '2022-06-29 19:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_ref`
--

CREATE TABLE `user_ref` (
  `id` int(11) NOT NULL,
  `ref_user_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_to_categories`
--

CREATE TABLE `user_to_categories` (
  `id` bigint(20) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` enum('R','M') DEFAULT 'R' COMMENT '(R-regular,M-Moderator) ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_to_categories`
--

INSERT INTO `user_to_categories` (`id`, `category_id`, `user_id`, `type`, `created_at`, `updated_at`) VALUES
(1, 41, 2, 'R', '2022-06-09 11:31:51', '2022-06-09 11:31:51'),
(2, 45, 2, 'R', '2022-06-09 11:31:51', '2022-06-09 11:31:51'),
(3, 23, 2, 'R', '2022-06-09 11:31:51', '2022-06-09 11:31:51'),
(4, 6, 1, 'R', '2022-06-09 11:32:25', '2022-06-09 11:32:25'),
(5, 45, 1, 'R', '2022-06-09 11:32:25', '2022-06-09 11:32:25'),
(6, 6, 3, 'R', '2022-06-09 13:09:19', '2022-06-09 13:09:19'),
(7, 45, 3, 'R', '2022-06-09 13:09:19', '2022-06-09 13:09:19'),
(8, 15, 3, 'R', '2022-06-09 13:09:19', '2022-06-09 13:09:19'),
(9, 6, 5, 'R', '2022-06-09 16:49:15', '2022-06-09 16:49:15'),
(10, 35, 5, 'R', '2022-06-09 16:49:15', '2022-06-09 16:49:15'),
(11, 10, 5, 'R', '2022-06-09 16:49:15', '2022-06-09 16:49:15'),
(12, 6, 6, 'R', '2022-06-10 16:00:43', '2022-06-10 16:00:43'),
(13, 41, 6, 'R', '2022-06-10 16:00:43', '2022-06-10 16:00:43'),
(14, 45, 7, 'R', '2022-06-11 13:30:48', '2022-06-11 13:30:48'),
(15, 45, 8, 'R', '2022-06-11 13:32:56', '2022-06-11 13:32:56'),
(16, 45, 9, 'R', '2022-06-11 13:34:21', '2022-06-11 13:34:21'),
(17, 6, 10, 'R', '2022-06-14 18:46:36', '2022-06-14 18:46:36'),
(18, 44, 10, 'R', '2022-06-14 18:46:36', '2022-06-14 18:46:36'),
(19, 6, 11, 'R', '2022-06-17 12:52:31', '2022-06-17 12:52:31'),
(20, 43, 11, 'R', '2022-06-17 12:52:31', '2022-06-17 12:52:31'),
(21, 42, 11, 'R', '2022-06-17 12:52:31', '2022-06-17 12:52:31'),
(22, 44, 11, 'R', '2022-06-17 12:52:31', '2022-06-17 12:52:31'),
(23, 42, 12, 'R', '2022-06-17 13:57:48', '2022-06-17 13:57:48'),
(24, 11, 12, 'R', '2022-06-17 13:57:48', '2022-06-17 13:57:48'),
(25, 6, 13, 'R', '2022-06-18 16:05:54', '2022-06-18 16:05:54'),
(26, 11, 13, 'R', '2022-06-18 16:05:54', '2022-06-18 16:05:54'),
(27, 13, 13, 'R', '2022-06-18 16:05:54', '2022-06-18 16:05:54'),
(28, 20, 13, 'R', '2022-06-18 16:05:54', '2022-06-18 16:05:54'),
(29, 21, 13, 'R', '2022-06-18 16:05:54', '2022-06-18 16:05:54'),
(30, 6, 13, 'M', '2022-06-18 16:10:46', '2022-06-18 16:10:46'),
(31, 11, 13, 'M', '2022-06-18 16:10:46', '2022-06-18 16:10:46'),
(32, 20, 13, 'M', '2022-06-18 16:10:46', '2022-06-18 16:10:46'),
(33, 6, 5, 'M', '2022-06-20 13:19:27', '2022-06-20 13:19:27'),
(34, 23, 5, 'M', '2022-06-20 13:19:27', '2022-06-20 13:19:27'),
(35, 63, 5, 'M', '2022-06-20 13:19:27', '2022-06-20 13:19:27'),
(36, 37, 7, 'R', '2022-06-21 19:15:32', '2022-06-21 19:15:32'),
(37, 32, 7, 'R', '2022-06-21 19:15:32', '2022-06-21 19:15:32'),
(38, 38, 7, 'M', '2022-06-21 19:15:32', '2022-06-21 19:15:32'),
(39, 39, 7, 'M', '2022-06-21 19:15:32', '2022-06-21 19:15:32'),
(40, 22, 12, 'R', '2022-06-28 12:49:10', '2022-06-28 12:49:10'),
(41, 24, 12, 'R', '2022-06-28 12:49:10', '2022-06-28 12:49:10'),
(42, 27, 12, 'R', '2022-06-28 12:49:10', '2022-06-28 12:49:10'),
(43, 33, 10, 'R', '2022-06-29 15:57:15', '2022-06-29 15:57:15'),
(44, 35, 18, 'R', '2022-07-05 21:03:40', '2022-07-05 21:03:40'),
(45, 14, 18, 'R', '2022-07-05 21:03:40', '2022-07-05 21:03:40'),
(46, 8, 18, 'R', '2022-07-05 21:03:40', '2022-07-05 21:03:40'),
(47, 23, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(48, 11, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(49, 12, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(50, 8, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(51, 28, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(52, 22, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(53, 13, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(54, 20, 19, 'R', '2022-07-05 21:24:31', '2022-07-05 21:24:31'),
(55, 19, 20, 'R', '2022-07-08 20:00:13', '2022-07-08 20:00:13'),
(56, 21, 20, 'R', '2022-07-08 20:00:13', '2022-07-08 20:00:13'),
(57, 33, 20, 'R', '2022-07-08 20:00:13', '2022-07-08 20:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_certificates`
--

CREATE TABLE `user_to_certificates` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `institute_name` varchar(299) DEFAULT NULL,
  `certification_name` varchar(299) DEFAULT NULL,
  `year` varchar(299) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ycoin_history`
--

CREATE TABLE `ycoin_history` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `coin_free` bigint(20) DEFAULT NULL,
  `coin_paid` bigint(20) DEFAULT NULL,
  `balance_coin_free` bigint(20) DEFAULT NULL,
  `balance_coin_paid` bigint(20) DEFAULT NULL,
  `type` enum('IN','OUT') DEFAULT NULL,
  `category` enum('S','C','P','A') DEFAULT NULL COMMENT '(S-SignUP/C-Conversation/P-Purchase/A-Admin send coin)',
  `conversation_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ycoin_history`
--

INSERT INTO `ycoin_history` (`id`, `user_id`, `coin_free`, `coin_paid`, `balance_coin_free`, `balance_coin_paid`, `type`, `category`, `conversation_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-09 11:11:38', '2022-06-09 11:11:38'),
(2, 2, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-09 11:28:24', '2022-06-09 11:28:24'),
(3, 2, 200, 0, 800, 0, 'OUT', 'C', 1, '2022-06-09 11:32:15', '2022-06-09 11:32:15'),
(4, 1, 200, 0, 800, 0, 'OUT', 'C', 1, '2022-06-09 11:32:39', '2022-06-09 11:32:39'),
(5, 3, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-09 12:51:54', '2022-06-09 12:51:54'),
(6, 3, 200, 0, 800, 0, 'OUT', 'C', 2, '2022-06-09 13:09:35', '2022-06-09 13:09:35'),
(7, 2, 200, 0, 600, 0, 'OUT', 'C', 2, '2022-06-09 13:09:51', '2022-06-09 13:09:51'),
(8, 1, 200, 0, 600, 0, 'OUT', 'C', 2, '2022-06-09 13:18:59', '2022-06-09 13:18:59'),
(9, 3, 200, 0, 1000, 0, 'IN', 'C', 2, '2022-06-09 13:20:37', '2022-06-09 13:20:37'),
(10, 2, 200, 0, 800, 0, 'IN', 'C', 2, '2022-06-09 13:20:37', '2022-06-09 13:20:37'),
(11, 1, 200, 0, 800, 0, 'IN', 'C', 2, '2022-06-09 13:20:37', '2022-06-09 13:20:37'),
(12, 1, 200, 0, 600, 0, 'OUT', 'C', 6, '2022-06-09 13:57:24', '2022-06-09 13:57:24'),
(13, 2, 200, 0, 600, 0, 'OUT', 'C', 6, '2022-06-09 13:57:51', '2022-06-09 13:57:51'),
(14, 3, 200, 0, 800, 0, 'OUT', 'C', 6, '2022-06-09 13:58:16', '2022-06-09 13:58:16'),
(15, 4, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-09 14:46:16', '2022-06-09 14:46:16'),
(16, 5, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-09 16:48:21', '2022-06-09 16:48:21'),
(17, 5, 200, 0, 800, 0, 'OUT', 'C', 7, '2022-06-09 16:50:55', '2022-06-09 16:50:55'),
(18, 1, 200, 0, 400, 0, 'OUT', 'C', 7, '2022-06-09 16:51:16', '2022-06-09 16:51:16'),
(19, 6, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-10 15:59:47', '2022-06-10 15:59:47'),
(20, 6, 200, 0, 800, 0, 'OUT', 'C', 9, '2022-06-10 16:01:19', '2022-06-10 16:01:19'),
(21, 6, 200, 0, 600, 0, 'OUT', 'C', 12, '2022-06-10 16:07:37', '2022-06-10 16:07:37'),
(22, 6, 200, 0, 400, 0, 'OUT', 'C', 13, '2022-06-10 16:17:14', '2022-06-10 16:17:14'),
(23, 2, 200, 0, 400, 0, 'OUT', 'C', 13, '2022-06-10 16:18:01', '2022-06-10 16:18:01'),
(24, 6, 200, 0, 600, 0, 'IN', 'C', 13, '2022-06-10 16:20:03', '2022-06-10 16:20:03'),
(25, 2, 200, 0, 600, 0, 'IN', 'C', 13, '2022-06-10 16:20:03', '2022-06-10 16:20:03'),
(26, 2, 200, 0, 400, 0, 'OUT', 'C', 14, '2022-06-10 17:38:00', '2022-06-10 17:38:00'),
(27, 5, 200, 0, 600, 0, 'OUT', 'C', 14, '2022-06-10 17:41:40', '2022-06-10 17:41:40'),
(28, 2, 200, 0, 200, 0, 'OUT', 'C', 15, '2022-06-10 17:58:36', '2022-06-10 17:58:36'),
(29, 5, 200, 0, 400, 0, 'OUT', 'C', 15, '2022-06-10 18:06:13', '2022-06-10 18:06:13'),
(30, 2, 200, 0, 0, 0, 'OUT', 'C', 28, '2022-06-10 18:55:34', '2022-06-10 18:55:34'),
(31, 2, 100, 200, 100, 200, 'IN', 'P', NULL, '2022-06-10 18:55:59', '2022-06-10 18:55:59'),
(32, 2, 0, 200, 100, 0, 'OUT', 'C', 29, '2022-06-10 18:57:49', '2022-06-10 18:57:49'),
(33, 2, 200, 500, 300, 500, 'IN', 'P', NULL, '2022-06-10 18:58:18', '2022-06-10 18:58:18'),
(34, 5, 200, 0, 200, 0, 'OUT', 'C', 29, '2022-06-10 19:02:39', '2022-06-10 19:02:39'),
(35, 7, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-11 13:30:28', '2022-06-11 13:30:28'),
(36, 8, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-11 13:32:44', '2022-06-11 13:32:44'),
(37, 9, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-11 13:34:07', '2022-06-11 13:34:07'),
(38, 9, 200, 0, 800, 0, 'OUT', 'C', 31, '2022-06-11 13:36:00', '2022-06-11 13:36:00'),
(39, 9, 200, 0, 600, 0, 'OUT', 'C', 32, '2022-06-11 13:36:20', '2022-06-11 13:36:20'),
(40, 7, 200, 0, 800, 0, 'OUT', 'C', 67, '2022-06-13 19:58:57', '2022-06-13 19:58:57'),
(41, 8, 200, 0, 800, 0, 'OUT', 'C', 67, '2022-06-13 19:59:55', '2022-06-13 19:59:55'),
(42, 8, 200, 0, 600, 0, 'OUT', 'C', 68, '2022-06-13 20:32:43', '2022-06-13 20:32:43'),
(43, 7, 200, 0, 600, 0, 'OUT', 'C', 68, '2022-06-13 20:34:33', '2022-06-13 20:34:33'),
(44, 9, 200, 0, 400, 0, 'OUT', 'C', 68, '2022-06-13 20:37:01', '2022-06-13 20:37:01'),
(45, 8, 200, 0, 400, 0, 'OUT', 'C', 69, '2022-06-13 20:53:02', '2022-06-13 20:53:02'),
(46, 8, 200, 0, 200, 0, 'OUT', 'C', 70, '2022-06-13 20:53:25', '2022-06-13 20:53:25'),
(47, 8, 200, 0, 0, 0, 'OUT', 'C', 71, '2022-06-13 20:54:20', '2022-06-13 20:54:20'),
(48, 8, 100, 200, 100, 200, 'IN', 'P', NULL, '2022-06-13 21:03:28', '2022-06-13 21:03:28'),
(49, 10, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-14 18:36:56', '2022-06-14 18:36:56'),
(50, 5, 200, 0, 0, 0, 'OUT', 'C', 83, '2022-06-14 18:41:39', '2022-06-14 18:41:39'),
(51, 5, 200, 500, 200, 500, 'IN', 'P', NULL, '2022-06-14 18:41:59', '2022-06-14 18:41:59'),
(52, 10, 200, 0, 800, 0, 'OUT', 'C', 84, '2022-06-14 18:47:06', '2022-06-14 18:47:06'),
(53, 10, 200, 0, 600, 0, 'OUT', 'C', 85, '2022-06-14 18:47:40', '2022-06-14 18:47:40'),
(54, 11, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-17 12:51:18', '2022-06-17 12:51:18'),
(55, 12, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-17 13:57:17', '2022-06-17 13:57:17'),
(56, 13, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-18 16:01:57', '2022-06-18 16:01:57'),
(57, 7, 200, 0, 400, 0, 'OUT', 'C', 119, '2022-06-18 16:15:15', '2022-06-18 16:15:15'),
(58, 7, 200, 0, 200, 0, 'OUT', 'C', 120, '2022-06-18 16:15:53', '2022-06-18 16:15:53'),
(59, 13, 200, 0, 800, 0, 'OUT', 'C', 119, '2022-06-18 16:19:42', '2022-06-18 16:19:42'),
(60, 14, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-19 15:34:15', '2022-06-19 15:34:15'),
(61, 5, 0, 200, 200, 300, 'OUT', 'C', 1, '2022-06-20 18:14:19', '2022-06-20 18:14:19'),
(62, 7, 200, 0, 0, 0, 'OUT', 'C', 2, '2022-06-21 19:16:40', '2022-06-21 19:16:40'),
(63, 7, 500, 1000, 500, 1000, 'IN', 'P', NULL, '2022-06-21 23:59:30', '2022-06-21 23:59:30'),
(64, 7, 200, 0, 700, 1000, 'IN', 'C', 2, '2022-06-22 00:00:38', '2022-06-22 00:00:38'),
(65, 5, 0, 200, 200, 100, 'OUT', 'C', 3, '2022-06-22 10:24:19', '2022-06-22 10:24:19'),
(66, 7, 0, 200, 700, 800, 'OUT', 'C', 4, '2022-06-22 16:09:56', '2022-06-22 16:09:56'),
(67, 7, 0, 200, 700, 1000, 'IN', 'C', 4, '2022-06-22 16:10:04', '2022-06-22 16:10:04'),
(68, 13, 200, 0, 600, 0, 'OUT', 'C', 5, '2022-06-28 12:46:27', '2022-06-28 12:46:27'),
(69, 12, 200, 0, 800, 0, 'OUT', 'C', 5, '2022-06-28 12:49:25', '2022-06-28 12:49:25'),
(70, 13, 500, 1000, 1100, 1000, 'IN', 'P', NULL, '2022-06-28 13:06:34', '2022-06-28 13:06:34'),
(71, 10, 200, 0, 400, 0, 'OUT', 'C', 6, '2022-06-28 18:55:33', '2022-06-28 18:55:33'),
(72, 10, 200, 0, 200, 0, 'OUT', 'C', 7, '2022-06-28 19:53:34', '2022-06-28 19:53:34'),
(73, 5, 0, 200, 100, 0, 'OUT', 'C', 8, '2022-06-29 11:54:25', '2022-06-29 11:54:25'),
(74, 5, 0, 200, 100, 200, 'IN', 'P', NULL, '2022-06-29 14:31:53', '2022-06-29 14:31:53'),
(75, 10, 200, 0, 0, 0, 'OUT', 'C', 9, '2022-06-29 15:58:04', '2022-06-29 15:58:04'),
(76, 5, 0, 200, 100, 0, 'OUT', 'C', 9, '2022-06-29 15:58:18', '2022-06-29 15:58:18'),
(77, 5, 500, 2000, 600, 2000, 'IN', 'P', NULL, '2022-06-29 19:18:58', '2022-06-29 19:18:58'),
(78, 5, 0, 200, 600, 1800, 'OUT', 'C', 10, '2022-06-29 19:20:08', '2022-06-29 19:20:08'),
(79, 10, 150, 1000, 150, 1000, 'IN', 'P', NULL, '2022-06-29 19:23:41', '2022-06-29 19:23:41'),
(80, 10, 0, 200, 150, 800, 'OUT', 'C', 10, '2022-06-29 19:23:48', '2022-06-29 19:23:48'),
(81, 12, 200, 0, 600, 0, 'OUT', 'C', 11, '2022-06-29 20:55:10', '2022-06-29 20:55:10'),
(82, 5, 0, 200, 600, 1600, 'OUT', 'C', 12, '2022-06-30 11:47:58', '2022-06-30 11:47:58'),
(83, 10, 0, 200, 150, 600, 'OUT', 'C', 12, '2022-06-30 11:49:26', '2022-06-30 11:49:26'),
(84, 5, 0, 200, 600, 1400, 'OUT', 'C', 13, '2022-06-30 11:59:44', '2022-06-30 11:59:44'),
(85, 15, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-06-30 14:47:57', '2022-06-30 14:47:57'),
(86, 12, 200, 0, 400, 0, 'OUT', 'C', 14, '2022-06-30 16:11:59', '2022-06-30 16:11:59'),
(87, 5, 0, 200, 600, 1200, 'OUT', 'C', 14, '2022-06-30 16:13:36', '2022-06-30 16:13:36'),
(88, 5, 0, 200, 600, 1000, 'OUT', 'C', 15, '2022-06-30 17:32:10', '2022-06-30 17:32:10'),
(89, 3, 200, 0, 600, 0, 'OUT', 'C', 13, '2022-06-30 17:34:07', '2022-06-30 17:34:07'),
(90, 3, 200, 0, 400, 0, 'OUT', 'C', 15, '2022-06-30 17:35:25', '2022-06-30 17:35:25'),
(91, 5, 0, 200, 600, 800, 'OUT', 'C', 16, '2022-06-30 17:46:01', '2022-06-30 17:46:01'),
(92, 3, 200, 0, 200, 0, 'OUT', 'C', 16, '2022-06-30 17:46:35', '2022-06-30 17:46:35'),
(93, 5, 0, 200, 600, 600, 'OUT', 'C', 17, '2022-07-01 16:12:24', '2022-07-01 16:12:24'),
(94, 12, 200, 0, 200, 0, 'OUT', 'C', 18, '2022-07-01 16:12:31', '2022-07-01 16:12:31'),
(95, 5, 0, 200, 600, 400, 'OUT', 'C', 19, '2022-07-01 16:28:23', '2022-07-01 16:28:23'),
(96, 16, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-05 19:52:06', '2022-07-05 19:52:06'),
(97, 17, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-05 20:38:40', '2022-07-05 20:38:40'),
(98, 18, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-05 20:57:46', '2022-07-05 20:57:46'),
(99, 19, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-05 21:14:11', '2022-07-05 21:14:11'),
(100, 20, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-06 14:06:37', '2022-07-06 14:06:37'),
(101, 21, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-06 16:00:55', '2022-07-06 16:00:55'),
(102, 5, 0, 200, 600, 200, 'OUT', 'C', 20, '2022-07-07 17:12:25', '2022-07-07 17:12:25'),
(103, 22, 1000, 0, 1000, 0, 'IN', 'S', NULL, '2022-07-09 10:28:34', '2022-07-09 10:28:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_details`
--
ALTER TABLE `chat_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_master`
--
ALTER TABLE `chat_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `connects_groups`
--
ALTER TABLE `connects_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations_to_users`
--
ALTER TABLE `conversations_to_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_eviction`
--
ALTER TABLE `conversation_eviction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_reports`
--
ALTER TABLE `conversation_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_request_join`
--
ALTER TABLE `conversation_request_join`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_reviews`
--
ALTER TABLE `conversation_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_user_reviews`
--
ALTER TABLE `conversation_user_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_schedule_details`
--
ALTER TABLE `conv_schedule_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_schedule_master`
--
ALTER TABLE `conv_schedule_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_connect`
--
ALTER TABLE `my_connect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_master`
--
ALTER TABLE `package_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_package_purchase`
--
ALTER TABLE `user_package_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_ref`
--
ALTER TABLE `user_ref`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_categories`
--
ALTER TABLE `user_to_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_certificates`
--
ALTER TABLE `user_to_certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ycoin_history`
--
ALTER TABLE `ycoin_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `chat_details`
--
ALTER TABLE `chat_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_master`
--
ALTER TABLE `chat_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `connects_groups`
--
ALTER TABLE `connects_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `conversations_to_users`
--
ALTER TABLE `conversations_to_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `conversation_eviction`
--
ALTER TABLE `conversation_eviction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversation_reports`
--
ALTER TABLE `conversation_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `conversation_request_join`
--
ALTER TABLE `conversation_request_join`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversation_reviews`
--
ALTER TABLE `conversation_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `conversation_user_reviews`
--
ALTER TABLE `conversation_user_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `conv_schedule_details`
--
ALTER TABLE `conv_schedule_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `conv_schedule_master`
--
ALTER TABLE `conv_schedule_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `my_connect`
--
ALTER TABLE `my_connect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `package_master`
--
ALTER TABLE `package_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `professions`
--
ALTER TABLE `professions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_package_purchase`
--
ALTER TABLE `user_package_purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_ref`
--
ALTER TABLE `user_ref`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_to_categories`
--
ALTER TABLE `user_to_categories`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `user_to_certificates`
--
ALTER TABLE `user_to_certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ycoin_history`
--
ALTER TABLE `ycoin_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
